	
capture log close
clear all
clear matrix
set more off
******************************* SET DRIVES *************************************
gl DRIVE	"$path/data"
gl DO	"$path/do"
gl TAB		"$path/tables"
gl FIG		"$path/figures"
cd "${DRIVE}" 


	tempfile main new
	tempvar todrop
	use $DRIVE/NGA_panel.dta ,clear
	ren *_*_*_* ****
	ren *_*_* ***
	gen ltot=ln(pcexp_n)
	keep if !rural
	egen nid=group(hhid_unique)

	tostring year,replace
	encode year, gen(newt)
		tab year,gen(y)
	sort nid newt
	xtset nid newt
	la var female_head "Female Head"
la var land_d "Owns Land"
svyset hhid_unique,  weight(hhweight) vce(linearized)
local outregopts starlevels(10 5 1) starloc(1) tex replace varlabels se a4 landscape fragment nolegend
local con1 hhsize dep_ratio shschoolsec
local keep hhsize dep_ratio shschoolsec
local if estsample

	foreach var of var shfteformal shfteformalsp{
		gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
	}	
	gen informalhead=cond(head_f==1 & !mi(head_f),0,1,.)
	label var informalhead "Informal HH Head"
	gen informalheadsp=cond(head_sp==1 & !mi(head_sp),0,1,.)
	label var informalheadsp "Informal HH Head"
		la def nv 0 "Formal" 1 "0-99% informal" 2 "100% informal"
	local shempYinformal "Sh Inf Inc"
	local shfteinformal "Sh FTE Inf"
	local poor_n "P(Poor=1)"
	local ltot "ln(TotExp)"
	local sp "(SocProt Based)"
	local con "(Contract Based)"
	foreach bit in " " sp{
	foreach var in shfteinformal{
		gen n`var'`bit'=cond(`var'`bit'==0,0, ///
											cond(`var'`bit'>0 & `var'`bit'<1,1, ///
											cond(`var'`bit'==1,2,.)))
		la val n`var'`bit' nv
		la var n`var'`bit' `"``var''``bit''"'
		}
	}
tempfile master
save `master'
**Now to merge in the combinations that define transitions
keep nshfteinformalsp hhid_unique newt
reshape wide nshfteinformalsp, i(hhid_unique) j(newt)
merge m:1 nshfteinformalsp1 nshfteinformalsp2 nshfteinformalsp3 using combinationsnga, gen(spm1)
merge 1:m hhid_unique using `master', gen(spm2)
save `master',replace
**now to stack the dataset on transition time
keep if newt==1 | newt==2
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
gen post=newt==2
tempfile stacked
egen stacknid=group(nid)
su nid, mean
local max=`r(max)'
/*
save `stacked'
use `master',clear
keep if newt==2 | newt==3
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
egen stacknid=group(nid)
replace stacknid=stacknid+`max'
gen post=newt==3
append using `stacked'
save `stacked',replace
*/
xtset stacknid post
xtbalance , range(0 1) miss(`con1' poor ltot)
duplicates tag nid, gen(timesinpanel)
*keep if timesinpanel>=2
ren *_*_* ***

foreach lhs in poor_n ltot{
	foreach rhs in bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf {
			cap xtreg `lhs' ib0.`rhs'##ib0.post `con1', fe vce(ro)
			mat temp=r(table)
			mat `rhs'sp=nullmat(`rhs'sp),temp[1,8],temp[5,8],temp[6,8],e(N)
		}
	}
mat ressp=bfullffullinfsp \ dfullinffullfsp \ efullfmixsp \ ffullinfmixsp \ gmixfullfsp \ hmixfullinfsp
mat colnames ressp= poorest poorlb poorub poorn ltotest ltotlb ltotub ltotn
mat rownames ressp=bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf
clear
svmat ressp,n(col)
tokenize bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf
local count 0
gen typ=""
forv i=1/6{
replace typ="``i''" in `i'
}
reshape long @est @lb @ub,i(typ) j(lhs,string)
frame copy default table
frame create ris
local c 0
foreach typ in bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf{
	foreach lhs in poor_n ltot{
		use `typ'`lhs',clear
		ren _pm_1 est
		gen typ="`typ'"
		gen lhs="`lhs'"
		local ltotscale "-1.5(.5)1.5"
		local poorscale "-.7(.2).7"
		local ltotrange "-1.5,1.5"
		local poorrange "-.7,.7"
		*note axis gets all warped due to there being one or fewer replications that falls near or outside the axis. It makes no difference to plot, but actually allows one to view things better, hence inrange.
		if `++c'>0 frameappend ris,drop
		frame copy default ris
	}
}
replace lhs="poor" if lhs=="poor_n"
frameappend table,drop
encode typ, gen(type)
gen ri=cond(lb==.,1,0)
local poor "Prob(Poor=1)"
local ltot "ln(Total Expenditure)"
foreach lhs in poor ltot{
two (scatter est type if ri & lhs=="`lhs'" & inrange(est,``lhs'range'), m(Oh) mc(navy%1) ///
leg(lab(1 "RI Estimate"))) ///
(rcap lb ub type if lhs=="`lhs'",leg(lab(2 "HH Clustered CI")) mc(maroon)) ///
(scatter est type if !ri & lhs=="`lhs'", m(O) mc(maroon) leg(lab(3 "Point Estimate"))) ///
, yti("``lhs''") xti("") xlabel(1 "F➔I" 2 "I➔F" 3 "F➔M" 4 "I➔M" 5 "M➔F" 6 "M➔I") leg(order (3 2)) ///
scheme(s1mono) yline(0) ylab(``lhs'scale')
graph export $FIG/nga-ri-`lhs'.eps, cmyk(on) replace
}
