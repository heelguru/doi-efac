
*** This do-file generates incomes for each activity ***

* Farming Income
gen own_farm_inc = i_gi_f3_c1
gen own_farm_inc_net = i_ni_f3_c1
gen own_farm_inc_nnnet = i_nnni_f3_c1

* On-Farm Wage Income
gen farm_wage_inc = wage_i_f0_c2 + wage_i_f1_c2

* Off-Farm AFS Wage Income (nb here they put informal and formal together !)
gen off_farm_AFS_wage_inc = 0
forvalues ofa=3(1)6 {
	replace off_farm_AFS_wage_inc = off_farm_AFS_wage_inc + wage_i_f0_c`ofa'
	replace off_farm_AFS_wage_inc = off_farm_AFS_wage_inc + wage_i_f1_c`ofa'
}
*

* Non AFS Wage Income (nb here they put informal and formal together !)
gen non_AFS_wage_inc = 0
forvalues naw=7(1)12 {
	replace non_AFS_wage_inc = non_AFS_wage_inc + wage_i_f0_c`naw'
	replace non_AFS_wage_inc = non_AFS_wage_inc + wage_i_f1_c`naw'
}
*

*!EVA: only in terms of formal and differentiate between sp and contract!
** (!) NEW VARS on informality (please note: anything coded as 0 is informal vars, those 1 formal!) -- adding:
gen tot_AFS_inc_formal = 0
g tot_AFS_inc_formalcon = 0
g tot_AFS_inc_formalsp = 0
forvalues c=1(1)6 {
	replace tot_AFS_inc_formal= tot_AFS_inc_formal+wage_i_f1_c`c'
	replace tot_AFS_inc_formalcon= tot_AFS_inc_formalcon+wage_i_fcon1_c`c'
	replace tot_AFS_inc_formalsp= tot_AFS_inc_formalsp+wage_i_fsp1_c`c'
}
gen tot_nonAFS_inc_formal = 0
g tot_nonAFS_inc_formalcon = 0
g tot_nonAFS_inc_formalsp = 0
forvalues c=7(1)12 {
	replace tot_nonAFS_inc_formal= tot_nonAFS_inc_formal+wage_i_f1_c`c'
	replace tot_nonAFS_inc_formalcon= tot_nonAFS_inc_formalcon+wage_i_fcon1_c`c'
	replace tot_nonAFS_inc_formalsp= tot_nonAFS_inc_formalsp+wage_i_fsp1_c`c'
}

egen wage_formal = rowtotal(tot_AFS_inc_formal tot_nonAFS_inc_formal)
egen wage_formalcon = rowtotal(tot_AFS_inc_formalcon tot_nonAFS_inc_formalcon)
egen wage_formalsp = rowtotal(tot_AFS_inc_formalsp tot_nonAFS_inc_formalsp)

* Enterprise Net Income by AFS
gen nfe_AFS_net_inc = 0
gen nfe_nonAFS_net_inc = 0
forvalues ea=1(1)6 {
	replace nfe_AFS_net_inc = nfe_AFS_net_inc + nfe_i_c`ea'
}
*
forvalues ea=7(1)12 {
	replace nfe_nonAFS_net_inc = nfe_nonAFS_net_inc + nfe_i_c`ea'
}
*
gen nfe_AFS_nnnet_inc = 0
gen nfe_nonAFS_nnnet_inc = 0
forvalues ea=1(1)6 {
	replace nfe_AFS_nnnet_inc = nfe_AFS_nnnet_inc + nfe_i_c`ea' if nfe_i_c`ea'>=0
}
*
forvalues ea=7(1)12 {
	replace nfe_nonAFS_nnnet_inc = nfe_nonAFS_nnnet_inc + nfe_i_c`ea' if nfe_i_c`ea'>=0
}
*

* Enterprise Gross Income by AFS
gen nfe_AFS_gross_inc = 0
gen nfe_nonAFS_gross_inc = 0
forvalues ea=1(1)6 {
	replace nfe_AFS_gross_inc = nfe_AFS_gross_inc + nfe_gi_c`ea'
}
*
forvalues ea=7(1)12 {
	replace nfe_nonAFS_gross_inc = nfe_nonAFS_gross_inc + nfe_gi_c`ea'
}
*

* Enterprise Costs by AFS
gen nfe_AFS_costs = 0
gen nfe_nonAFS_costs = 0
forvalues ea=1(1)6 {
	replace nfe_AFS_costs = nfe_AFS_costs + nfe_c_c`ea'
}
*
forvalues ea=7(1)12 {
	replace nfe_nonAFS_costs = nfe_nonAFS_costs + nfe_c_c`ea'
}
*

* Other Income
gen other_income = i_gi_f3_c13

*** Farm income is either on farm income or farm labor income
gen farm_income = 0
forvalues c=1(1)2 {
	replace farm_income= farm_income + i_gi_f3_c`c'
}
*
gen farm_income_net = 0
forvalues c=1(1)2 {
	replace farm_income_net= farm_income_net + i_ni_f3_c`c'
}
*
gen farm_income_nnnet = 0
forvalues c=1(1)2 {
	replace farm_income_nnnet= farm_income_nnnet + i_nnni_f3_c`c'
}
*

* Non Farm Income
gen nonfarm_income = 0
forvalues c=3(1)13 {
	replace nonfarm_income = nonfarm_income + i_gi_f3_c`c'
}
*
gen nonfarm_income_net = 0
forvalues c=3(1)13 {
	replace nonfarm_income_net = nonfarm_income_net + i_ni_f3_c`c'
}
*
gen nonfarm_income_nnnet = 0
forvalues c=3(1)13 {
	replace nonfarm_income_nnnet = nonfarm_income_nnnet + i_nnni_f3_c`c'
}
*

* Non Farm Income from Employment
gen nonfarm_income_emp = 0
forvalues c=3(1)12 {
	replace nonfarm_income_emp = nonfarm_income_emp + i_gi_f3_c`c'
}
*
gen nonfarm_income_net_emp = 0
forvalues c=3(1)12 {
	replace nonfarm_income_net_emp = nonfarm_income_net_emp + i_ni_f3_c`c'
}
*
gen nonfarm_income_nnnet_emp = 0
forvalues c=3(1)12 {
	replace nonfarm_income_nnnet_emp = nonfarm_income_nnnet_emp + i_nnni_f3_c`c'
}
*

* Total Income
gen total_income = 0
forvalues c=1(1)13 {
	replace total_income = total_income + i_gi_f3_c`c'
}
*
gen tot_inc_net = 0
forvalues c=1(1)13 {
	replace tot_inc_net = tot_inc_net + i_ni_f3_c`c'
}
*
gen tot_inc_nnnet = 0
forvalues c=1(1)13 {
	replace tot_inc_nnnet = tot_inc_nnnet + i_nnni_f3_c`c'
}
*

* Total Income from Employment
gen total_income_emp = 0
forvalues c=1(1)12 {
	replace total_income_emp = total_income_emp + i_gi_f3_c`c'
}
*
gen tot_inc_net_emp = 0
forvalues c=1(1)12 {
	replace tot_inc_net_emp = tot_inc_net_emp + i_ni_f3_c`c'
}
*
gen tot_inc_nnnet_emp = 0
forvalues c=1(1)12 {
	replace tot_inc_nnnet_emp = tot_inc_nnnet_emp + i_nnni_f3_c`c'
}
*

* Total AFS Income
gen total_AFS_income = 0
forvalues c=1(1)6 {
	replace total_AFS_income = total_AFS_income + i_gi_f3_c`c'
}
*
gen total_AFS_income_net = 0
forvalues c=1(1)6 {
	replace total_AFS_income_net = total_AFS_income_net + i_ni_f3_c`c'
}
*
gen total_AFS_income_nnnet = 0
forvalues c=1(1)6 {
	replace total_AFS_income_nnnet = total_AFS_income_nnnet + i_nnni_f3_c`c'
}
*

* Total Non AFS Income
gen total_nonAFS_income = 0
forvalues c=7(1)13 {
	replace total_nonAFS_income = total_nonAFS_income + i_gi_f3_c`c'
}
*
gen total_nonAFS_income_net = 0
forvalues c=7(1)13 {
	replace total_nonAFS_income_net = total_nonAFS_income_net + i_ni_f3_c`c'
}
*
gen total_nonAFS_income_nnnet = 0
forvalues c=7(1)13 {
	replace total_nonAFS_income_nnnet = total_nonAFS_income_nnnet + i_nnni_f3_c`c'
}
*

* Total Non AFS Income from Employment
gen total_nonAFS_emp_income = 0
forvalues c=7(1)12 {
	replace total_nonAFS_emp_income = total_nonAFS_emp_income + i_gi_f3_c`c'
}
*
gen total_nonAFS_emp_income_net = 0
forvalues c=7(1)12 {
	replace total_nonAFS_emp_income_net = total_nonAFS_emp_income_net + i_ni_f3_c`c'
}
*
gen total_nonAFS_emp_income_nnnet = 0
forvalues c=7(1)12 {
	replace total_nonAFS_emp_income_nnnet = total_nonAFS_emp_income_nnnet + i_nnni_f3_c`c'
}
*

*Poverty variable

g poor = (pcexp<1.9)

* variables to keep:
keep ccode country hhid_unique ea_id hhweight hhsize poor pcexp remit rural ///
	youth_share_24 youth_share_34 female_head school_secondary_hh educ_max credit land_owned ///
	N_pension N_health_insurance informalsp land_d ///
	farm_sales own_farm_inc farm_income farm_wage_inc nonfarm_income nonfarm_income_emp ///
	off_farm_AFS_wage_inc non_AFS_wage_inc nfe_gross_inc nfe_costs nfe_net_inc nfe_AFS_gross_inc ///
	nfe_AFS_costs nfe_AFS_net_inc nfe_nonAFS_gross_inc nfe_nonAFS_costs nfe_nonAFS_net_inc other_income ///
	total_income total_income_emp total_AFS_income total_nonAFS_income total_nonAFS_emp_income  ///
	own_farm_inc_net tot_inc_net wage_formal wage_formalcon wage_formalsp tot_AFS_inc_formal ///
	tot_nonAFS_inc_formal tot_AFS_inc_formalcon tot_nonAFS_inc_formalcon tot_AFS_inc_formalsp tot_nonAFS_inc_formalsp ///
	sh_* t_* ///
	admin_1 admin_2 admin_3



ex
