********************************************************************************
*					       	WEALTH INDEX DATASET	 						   *
*																			   *
*Description:																   *
*The dofile creates the dataset for the computation of the wealth index from   *
*	the raw data of each country. 											   *
********************************************************************************

gl tempdata "$path\data\temp"
gl rawdata "$path\data"


********************************************************************************
*					       	DATASET CREATION		   						   *
********************************************************************************

*********************************ETHIOPIA***************************************
use "$rawdata\Ethiopia_ESS_1516\raw data\Household\sect9_hh_w3", clear

g hhid = household_id2
lab var hhid "Household identifier"

gen drinking_water1=hh_s9q13
replace drinking_water1=hh_s9q14 if hh_s9q14!=.
lab val drinking_water1 HH_S9Q14
decode drinking_water1, gen(drinking_water)
drop drinking_water1

decode hh_s9q21, gen(cooking_fuel)

gen sleeping_rooms=hh_s9q04

decode hh_s9q07, gen(housing_floor)

decode hh_s9q10, gen(toilet_facility)

decode hh_s9q19_a, gen(lighting_fuel)

keep hhid drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

save "$tempdata\ethiopia_housing", replace

use "$rawdata\Ethiopia_ESS_1516\raw data\Household\sect10_hh_w3", clear

g hhid = household_id2
lab var hhid "Household identifier"

bys hhid: egen radio=sum(hh_s10q01) if inlist(hh_s10q00, 9) 
bys hhid: egen television=sum(hh_s10q01) if inlist(hh_s10q00, 10)
bys hhid: egen phone=sum(hh_s10q01) if inlist(hh_s10q00, 7, 8)
bys hhid: egen bicycle=sum(hh_s10q01) if inlist(hh_s10q00, 14)
bys hhid: egen motorbike=sum(hh_s10q01) if inlist(hh_s10q00, 15)
bys hhid: egen car=sum(hh_s10q01) if inlist(hh_s10q00, 23)
*bys hhid: egen van=sum(hh_s10q01) if inlist(hh_s10q00, 519
bys hhid: egen animal_cart=sum(hh_s10q01) if inlist(hh_s10q00, 17)
bys hhid: egen refrigerator=sum(hh_s10q01) if inlist(hh_s10q00, 22)
*bys hhid: egen washing_machine=sum(hh_s10q01) if inlist(hh_s10q00,)
*bys hhid: egen iron=sum(hh_s10q01) if inlist(hh_s10q00,

keep hhid radio television phone bicycle motorbike car animal_cart refrigerator
collapse radio television phone bicycle motorbike car animal_cart refrigerator, by(hhid)

save "$tempdata\ethiopia_assets", replace

use "$tempdata\ethiopia_housing", clear
merge 1:1 hhid using "$tempdata\ethiopia_assets"
drop _m

gl var_house drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel 

foreach v in $var_house {
	rename `v' `v'1
}

gen drinking_water=1 if inlist(drinking_water1, "OTHER,SPECIFY.", "RAINWATER COLLECTION", "SURFACE WATER", "UNPROTECTED DUGWELL", "UNPROTECTED SPRING")
replace drinking_water=2 if inlist(drinking_water1, "CART WITH SMALL TANK/DRUM", "PIPED WATER KIOSK/RETAILER", "TUBEWELL/BOREHOLE") 
replace drinking_water=2 if inlist(drinking_water1, "PIPED WATER PUBLIC TAP/STANDPIPE", "PROTECTED SPRING", "PROTECTED DUG WELL", "TANKER-TRUCK")
replace drinking_water=3 if inlist(drinking_water1, "BOTTLED WATER", "PIPED WATER INTO DWELLING", "PIPED WATER INTO YARD/PLOT")
tab drinking_water
drop drinking_water1

gen cooking_fuel=1 if inlist(cooking_fuel1, "Collecting fire wood", "Crop residue / leaves", "Dung / manure", "None", "Other specify", "Sawdust")
replace cooking_fuel=2 if inlist(cooking_fuel1, "Charcoal", "Kerosene", "Purchase fire wood")
replace cooking_fuel=3 if inlist(cooking_fuel1, "Butane / gas", "Electricity", "Solar energy")
tab cooking_fuel
drop cooking_fuel1

gen sleeping_rooms=1 if sleeping_rooms1<2
replace sleeping_rooms=2 if sleeping_rooms1==2
replace sleeping_rooms=3 if sleeping_rooms1>2 & sleeping_rooms1!=.
tab sleeping_rooms
drop sleeping_rooms1

gen housing_floor=1 if inlist(housing_floor1, "Mud / dung", "Others")
replace housing_floor=2 if inlist(housing_floor1, "Cement screed", "Reed / bamboo", "Wood planks")
replace housing_floor=3 if inlist(housing_floor1, "Ceramic / marble tiles", "Parquet of polished wood", "Plastic tiles", "Cement tiles", "Brick Tiles")
tab housing_floor
drop housing_floor1

gen toilet_facility=1 if inlist(toilet_facility1, "Field /forest", "Others(SPECIFY)", "Bucket", "COMPOSTING TOILET", "PIT LATRINE, WITHOUT SLAB")
replace toilet_facility=2 if inlist(toilet_facility1, "Pit latrine, ventilated VIP", "Pit latrine, with slab")
replace toilet_facility=3 if inlist(toilet_facility1, "Flush toilet")
tab toilet_facility
drop toilet_facility1

gen electricity= inlist(lighting_fuel1, "Electrical battery", "Electricity from generator", "Electricity meter - private", "Electricity meter - shared", "Solar energy")
replace electricity=. if lighting_fuel1==""
tab electricity
drop lighting_fuel1

gl var_assets radio television phone bicycle motorbike car  refrigerator

foreach v in $var_assets {
	replace `v'=1 if `v'!=0 & `v'!=.
}

drop animal_cart
gen ccode="ETH"
egen hhid_unique=concat(ccode hhid)
order hhid_unique, after(hhid)
merge 1:1 hhid using "$rawdata\Ethiopia_ESS_1516\clean data\Ethiopia1516_hhid"
keep if _m==3
drop _m

save "$tempdata\ethiopia_wealth", replace

*********************************MALAWI*****************************************
use "$rawdata\Malawi_IHS_1617\raw data\HH_MOD_F", clear

gen hhid = case_id
lab var hhid "Household identifier"

decode hh_f36, gen(drinking_water)
replace drinking_water=hh_f36_oth if drinking_water=="OTHER (SPECIFY)"

decode hh_f12, gen(cooking_fuel) 
replace cooking_fuel=hh_f12_oth if cooking_fuel=="OTHER (SPECIFY)"

gen sleeping_rooms=hh_f10

decode hh_f09, gen(housing_floor)
replace housing_floor=hh_f09_oth if housing_floor=="OTHER(SPECIFY)"

decode hh_f41, gen(toilet_facility)
replace toilet_facility=hh_f41_oth if toilet_facility=="OTHER(SPECIFY)"

decode hh_f11, gen(lighting_fuel)
replace lighting_fuel=hh_f11_oth if lighting_fuel=="OTHER(SPECIFY)"

gen phone=hh_f34

keep hhid drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel phone

save "$tempdata\malawi_housing", replace

use "$rawdata\Malawi_IHS_1617\raw data\HH_MOD_L", clear

gen hhid = case_id
lab var hhid "Household identifier"

bys hhid: egen radio=sum(hh_l03) if inlist(hh_l02, 507, 5081) 
bys hhid: egen television=sum(hh_l03) if inlist(hh_l02, 509)
*bys hhid: egen phone=sum(hh_l03) if inlist(hh_l02
bys hhid: egen bicycle=sum(hh_l03) if inlist(hh_l02, 516)
bys hhid: egen motorbike=sum(hh_l03) if inlist(hh_l02, 517)
bys hhid: egen car=sum(hh_l03) if inlist(hh_l02, 518)
bys hhid: egen van=sum(hh_l03) if inlist(hh_l02, 519, 520)
*bys hhid: egen animal_cart=sum(hh_l03) if inlist(hh_l02,
bys hhid: egen refrigerator=sum(hh_l03) if inlist(hh_l02, 514)
bys hhid: egen washing_machine=sum(hh_l03) if inlist(hh_l02, 515)
bys hhid: egen iron=sum(hh_l03) if inlist(hh_l02, 528)

keep hhid radio television bicycle motorbike car van refrigerator washing_machine iron
collapse radio television bicycle motorbike car van refrigerator washing_machine iron, by(hhid)

save "$tempdata\malawi_assets", replace

use "$tempdata\malawi_housing", clear
merge 1:1 hhid using "$tempdata\malawi_assets"
drop _m

gl var_house drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

foreach v in $var_house {
	rename `v' `v'1
}

gen drinking_water=1 if inlist(drinking_water1, "DAM", "OPEN PUBLIC WELL", "OPEN WELL IN YARD/PLOT", "POND/LAKE", "RIVER", "RIVER/STREAM", "SPRING", "UNPROTECTED WELL", "WATERBOARD")
replace drinking_water=2 if inlist(drinking_water1, "BOREHOLE", "BUY FROM KIOSK", "COMMUNAL STANDPIPE", "PROTECTED PUBLIC WELL", "PROTECTED WELL") 
replace drinking_water=2 if inlist(drinking_water1, "PROTECTED WELL IN YARD/PLOT", "PUMPED USING ENGINE", "TANKER TRUCK/BOWSER")
replace drinking_water=3 if inlist(drinking_water1, "BOTTLED WATER", "PIPED INTO DWELLING", "PIPED INTO YARD/PLOT", "PIPED WATER TO THE YARD")
tab drinking_water
drop drinking_water1

gen cooking_fuel=1 if inlist(cooking_fuel1, "COLLECTED FIREWOOD", "CROP RESIDUE", "MAPESI", "WOOD HUSKS", "SAW DUST")
replace cooking_fuel=2 if inlist(cooking_fuel1, "CHARCOAL", "PAPER", "PARAFFIN", "PURCHASED FIREWOOD")
replace cooking_fuel=3 if inlist(cooking_fuel1, "ELECTRICITY", "GAS")
tab cooking_fuel
drop cooking_fuel1

gen sleeping_rooms=1 if sleeping_rooms1<2
replace sleeping_rooms=2 if sleeping_rooms1==2
replace sleeping_rooms=3 if sleeping_rooms1>2 & sleeping_rooms1!=.
tab sleeping_rooms
drop sleeping_rooms1

gen housing_floor=1 if inlist(housing_floor1, "MUD", "SAND", "SMOOTHED MUD")
replace housing_floor=2 if inlist(housing_floor1, "CEMENT", "CONCRETE", "SMOOTH CEMENT", "WOOD", "WOODPOLES")
replace housing_floor=3 if inlist(housing_floor1, "TILE", "TILES")
tab housing_floor
drop housing_floor1

gen toilet_facility=1 if inlist(toilet_facility1, "BUSH", "NONE", "TRADITIONAL LATRINE W/ ROOF", "TRADITIONAL LATRINE W/O ROOF", "TRADITIONAL LATRINE WITH NO WALL")
replace toilet_facility=2 if inlist(toilet_facility1, "COMMUNITY TOILET (UNSPECIFIED TYPE)", "NEIGHBOURS TOILET (UNSPECIFIED TYPE)", "VIP LATRINE")
replace toilet_facility=3 if inlist(toilet_facility1, "FLUSH TOILET")
tab toilet_facility
drop toilet_facility1

gen electricity= inlist(lighting_fuel1, "ELECTRICITY", "SOLAR")
replace electricity=. if lighting_fuel1==""
tab electricity
drop lighting_fuel1

gl var_assets phone radio television bicycle motorbike car van refrigerator washing_machine iron

foreach v in $var_assets {
	replace `v'=1 if `v'!=0 & `v'!=.
}

replace car=1 if car==0 & van==1
drop van
gen ccode="MWI"
egen hhid_unique=concat(ccode hhid)
order hhid_unique, after(hhid)
merge 1:1 hhid using "$rawdata\Malawi_IHS_1617\clean data\Malawi1617_hhid"
keep if _m==3
drop _m

save "$tempdata\malawi_wealth", replace

*********************************NIGER***************************************
use "$rawdata\Niger_ECVMA_14\raw data\ECVMA2_MS06P1", clear

 foreach v in GRAPPE MENAGE EXTENSION {
	tostring `v', g(s_`v')
}
replace s_MENAGE = "0" + s_MENAGE if MENAGE<10
g hhid = s_GRAPPE + s_MENAGE + s_EXTENSION
drop s_*
lab var hhid "Household identifier"

#delimit;
lab def drink_code
          11 "Eau du robinet dans le logement"
          12 "Eau du robinet dans la cour/concession"
          13 "Robinet du voisin"
          14 "Borne fontaine/Robinet public"
          15 "Puits ouvert dans le logement"
          16 "Puits ouvert dans la cour/concession"
          17 "Puits ouvert ailleurs"
          18 "Puits couvert dans le logement"
          19 "Puits couvert dans la cour/concession"
          20 "Puits protege ailleurs"
          21 "Forage"
          22 "Source amenagee"
          23 "Source non amenagee"
          24 "Fleuve/Riviere/Lac/Barrage/Eau de pluie"
          25 "Camion citerne"
          26 "Vendeurs ambulants (Garoua)"
          27 "Eau en bouteille"
          28 "Mini AEP"
          29 "Eau de pluie"
          30 "Autres"
          99 "Manquant"
	;
#delimit cr
lab val MS06Q18A drink_code
decode MS06Q18A, gen(drinking_water)

#delimit;
lab def cook_code
           1 "Bois ramasse"
           2 "Bois achete"
           3 "Charbon de bois"
           4 "Gaz"
           5 "Electricite"
           6 "Petrole"
           7 "Biomasse"
           8 "Autre"
           9 "Manquant"
	;
#delimit cr
lab val MS06Q43A cook_code
decode MS06Q43A, gen(cooking_fuel)

gen sleeping_rooms=MS06Q02

#delimit;
lab def floor_code
           1 "Terre/Sable"
           2 "Ciment/Beton"
           3 "Carreaux/Marbre"
           4 "Moquette/ Parquet cire"
           5 "Autre"
           9 "Manquant"
	;
#delimit cr
lab val MS06Q12 floor_code
decode MS06Q12, gen(housing_floor)

#delimit;
lab def toilet_code
           1 "W C avec chasse eau"
           2 "Latrines ameliorees couvertes"
           3 "Latrines ameliorees non couvertes"
           4 "Fosse rudimentaire/trou court"
           5 "Aucune toilette (dans la nature)"
           6 "Autre"
           9 "Manquant"
	;
#delimit cr
lab val MS06Q45 toilet_code
decode MS06Q45, gen(toilet_facility)


#delimit;
lab def light_code
           1 "Electricite"
           2 "Generateur"
           3 "Lampe a petrole"
           4 "Lampe a pile"
           5 "Energie solaire"
           6 "Autre"
           9 "Manquant"
	;
#delimit cr
lab val MS06Q26 light_code
decode MS06Q26, gen(lighting_fuel)

keep hhid drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

save "$tempdata\niger_housing", replace

use "$rawdata\Niger_ECVMA_14\raw data\ECVMA2_MS07P1", clear

 foreach v in GRAPPE MENAGE EXTENSION {
	tostring `v', g(s_`v')
}
replace s_MENAGE = "0" + s_MENAGE if MENAGE<10
g hhid = s_GRAPPE + s_MENAGE + s_EXTENSION
drop s_*
lab var hhid "Household identifier"

bys hhid: egen radio=sum(MS07Q03) if inlist(MS07Q01, 160) 
bys hhid: egen television=sum(MS07Q03) if inlist(MS07Q01, 161)
bys hhid: egen phone=sum(MS07Q03) if inlist(MS07Q01, 169, 170)
bys hhid: egen bicycle=sum(MS07Q03) if inlist(MS07Q01, 166)
bys hhid: egen motorbike=sum(MS07Q03) if inlist(MS07Q01, 165)
bys hhid: egen car=sum(MS07Q03) if inlist(MS07Q01, 164)
*bys hhid: egen van=sum(MS07Q03) if inlist(MS07Q01, 
*bys hhid: egen animal_cart=sum(MS07Q03) if inlist(MS07Q01,
bys hhid: egen refrigerator=sum(MS07Q03) if inlist(MS07Q01, 157)
*bys hhid: egen washing_machine=sum(MS07Q03) if inlist(MS07Q01
bys hhid: egen iron=sum(MS07Q03) if inlist(MS07Q01, 150)

keep hhid radio television phone bicycle motorbike car refrigerator iron
collapse radio television phone bicycle motorbike car refrigerator iron, by(hhid)

save "$tempdata\niger_assets", replace

use "$tempdata\niger_housing", clear
merge 1:1 hhid using "$tempdata\niger_assets"
drop _m

gl var_house drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

foreach v in $var_house {
	rename `v' `v'1
}

gen drinking_water=1 if inlist(drinking_water1, "Autres", "Eau de pluie", "Fleuve/Riviere/Lac/Barrage/Eau de pluie", "Puits ouvert ailleurs", "Puits ouvert dans la cour/concession")
replace drinking_water=1 if inlist(drinking_water1, "Puits ouvert dans le logement", "Source non amenagee")
replace drinking_water=2 if inlist(drinking_water1, "Borne fontaine/Robinet public", "Camion citerne", "Forage", "Mini AEP", "Puits couvert dans la cour/concession", "Puits couvert dans le logement") 
replace drinking_water=2 if inlist(drinking_water1, "Puits protege ailleurs", "Robinet du voisin", "Source amenagee", "Vendeurs ambulants (Garoua)")
replace drinking_water=3 if inlist(drinking_water1, "Eau du robinet dans la cour/concession", "Eau du robinet dans le logement")
tab drinking_water
drop drinking_water1

gen cooking_fuel=1 if inlist(cooking_fuel1, "Autre", "Bois ramasse", "Manquant")
replace cooking_fuel=2 if inlist(cooking_fuel1, "Biomasse", "Bois achete", "Charbon de bois", "Petrole")
replace cooking_fuel=3 if inlist(cooking_fuel1, "Electricite", "Gaz")
tab cooking_fuel
drop cooking_fuel1

gen sleeping_rooms=1 if sleeping_rooms1<2
replace sleeping_rooms=2 if sleeping_rooms1==2
replace sleeping_rooms=3 if sleeping_rooms1>2 & sleeping_rooms1!=.
tab sleeping_rooms
drop sleeping_rooms1

gen housing_floor=1 if inlist(housing_floor1, "Autre", "Manquant", "Terre/Sable")
replace housing_floor=2 if inlist(housing_floor1, "Ciment/Beton")
replace housing_floor=3 if inlist(housing_floor1, "Carreaux/Marbre", "Moquette/ Parquet cire")
tab housing_floor
drop housing_floor1

gen toilet_facility=1 if inlist(toilet_facility1, "Aucune toilette (dans la nature)", "Autre", "Manquant", "Fosse rudimentaire/trou court")
replace toilet_facility=2 if inlist(toilet_facility1, "Latrines ameliorees couvertes", "Latrines ameliorees non couvertes")
replace toilet_facility=3 if inlist(toilet_facility1, "W C avec chasse eau")
tab toilet_facility
drop toilet_facility1

gen electricity= inlist(lighting_fuel1, "Electricite", "Energie solaire")
replace electricity=. if lighting_fuel1==""
tab electricity
drop lighting_fuel1

gl var_assets radio television phone bicycle motorbike car refrigerator iron

foreach v in $var_assets {
	replace `v'=1 if `v'!=0 & `v'!=.
}

gen ccode="NER"
egen hhid_unique=concat(ccode hhid)
order hhid_unique, after(hhid)
merge 1:1 hhid using "$rawdata\Niger_ECVMA_14\clean data\Niger14_hhid"
keep if _m==3
drop _m

save "$tempdata\niger_wealth", replace

*********************************NIGERIA***************************************
use "$rawdata\Nigeria_GHS_1516\raw data\sect11_plantingw3", clear

rename hhid HHID
tostring HHID, g(hhid)
lab var hhid "Household identifier"

decode s11q33a, gen(drinking_water)
replace drinking_water=s11q33a_os if drinking_water=="12. OTHER (SPECIFY)"

decode s11q11, gen(cooking_fuel)
replace cooking_fuel=s11q11b if cooking_fuel=="9. OTHER (SPECIFY)"

gen sleeping_rooms=s11q9

decode s11q8, gen(housing_floor)
replace housing_floor=s11q8b if housing_floor=="6. OTHER (SPECIFY)"

decode s11q36, gen(toilet_facility)

decode s11q10, gen(lighting_fuel)
replace lighting_fuel=s11q10b if lighting_fuel=="10. OTHER (SPECIFY)"

keep hhid drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

save "$tempdata\nigeria_housing", replace

use "$rawdata\Nigeria_GHS_1516\raw data\sect5_plantingw3", clear

rename hhid HHID
tostring HHID, g(hhid)
lab var hhid "Household identifier"

bys hhid: egen radio=sum(s5q1) if inlist(item_cd, 322) 
bys hhid: egen television=sum(s5q1) if inlist(item_cd,327)
bys hhid: egen phone=sum(s5q1) if inlist(item_cd, 402, 332)
bys hhid: egen bicycle=sum(s5q1) if inlist(item_cd, 317)
bys hhid: egen motorbike=sum(s5q1) if inlist(item_cd, 318)
bys hhid: egen car=sum(s5q1) if inlist(item_cd, 319)
*bys hhid: egen van=sum(s5q1) if inlist(item_cd, 
*bys hhid: egen animal_cart=sum(s5q1) if inlist(item_cd,
bys hhid: egen refrigerator=sum(s5q1) if inlist(item_cd, 312, 313)
bys hhid: egen washing_machine=sum(s5q1) if inlist(item_cd, 315)
bys hhid: egen iron1=sum(s5q1) if inlist(item_cd, 326)
bys hhid: egen iron2=sum(s5q1) if inlist(item_other, "CHACOAL IRON", "CHACOOL IRON", "CHARCOAL IRON", "CHARCOAL iron2", "IRON CHARCOAL", "charcoal iron")

keep hhid radio television phone bicycle motorbike car refrigerator washing_machine iron1 iron2
collapse radio television phone bicycle motorbike car refrigerator washing_machine iron1 iron2, by(hhid)

gen iron=iron1+iron2
replace iron=iron1 if iron==.
drop iron1 iron2

save "$tempdata\nigeria_assets", replace

use "$tempdata\nigeria_housing", clear
merge 1:1 hhid using "$tempdata\nigeria_assets"
drop _m

gl var_house drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

foreach v in $var_house {
	rename `v' `v'1
}

gen drinking_water=1 if inlist(drinking_water1, "CHARITY", "8. RAIN WATER", "6. RIVER/SPRING", "7. LAKE/RESERVOIR", "5. WELL/SPRING UNPROTECTED", "10. SATCHET WATER")
replace drinking_water=2 if inlist(drinking_water1, "4. WELL/SPRING PROTECTED", "9. TANKER/TRUCK/VENDOR", "3. BORE HOLE/HAND PUMP","2. PIPE BORNE WATER UNTREATED") 
replace drinking_water=3 if inlist(drinking_water1, "11. BOTTLED WATER", "1. PIPE BORNE WATER TREATED")
tab drinking_water
drop drinking_water1

gen cooking_fuel=1 if inlist(cooking_fuel1, "1. COLLECTED FIREWOOD", "4. GRASS", "DOES NOT COOK", "EAT OUTSIDE", "FIRE WOOD STOVE2")
replace cooking_fuel=1 if inlist(cooking_fuel1, "LAMP", "LOCAL FIREWOOD", "NONE", "NOON", "TOUCHLIGHT", "2", "SAWDUST")
replace cooking_fuel=2 if inlist(cooking_fuel1, "2. PURCHASED FIREWOOD", "3. COAL", "5. KEROSENE", "BUYING", "CHARCOAL", "WOOD FROM HUSBAND SHOP")
replace cooking_fuel=3 if inlist(cooking_fuel1, "6. ELECTRICITY/PHCN", "7. GENERATOR", "8. GAS")
tab cooking_fuel
drop cooking_fuel1

gen sleeping_rooms=1 if sleeping_rooms1<2
replace sleeping_rooms=2 if sleeping_rooms1==2
replace sleeping_rooms=3 if sleeping_rooms1>2 & sleeping_rooms1!=.
tab sleeping_rooms
drop sleeping_rooms1

gen housing_floor=1 if inlist(housing_floor1, "1. SAND/DIRT/STRAW", "2. SMOOTHED MUD", "MUD", "RAFIA POLE")
replace housing_floor=2 if inlist(housing_floor1, "3. SMOOTH CEMENT", "4. WOOD")
replace housing_floor=3 if inlist(housing_floor1, "5. TILE", "TARAZO")
tab housing_floor
drop housing_floor1

gen toilet_facility=1 if inlist(toilet_facility1, "1. NONE", "5. PAIL/BUCKET","7. UNCOVERED PIT LATRINE")
replace toilet_facility=2 if inlist(toilet_facility1, "8. V.I.P LATRINE", "6. COVERED PIT LATRINE")
replace toilet_facility=3 if inlist(toilet_facility1, "2. TOILET ON WATER", "4. FLUSH TO SEPTIC TANK", "3. FLUSH TO SEWAGE")
tab toilet_facility
drop toilet_facility1

gen electricity= inlist(lighting_fuel1, "5. ELECTRICITY/PHCN", "COMMUNITY ELECTRICITY", "COMMUNITY ELECTRICITY112", "RURAL ELECTRICITY", "RURAL ELECTRIFICATION", "SOLA", "SOLAR")
replace electricity=. if lighting_fuel1==""
tab electricity
drop lighting_fuel1

gl var_assets radio television phone bicycle motorbike car refrigerator washing_machine iron

foreach v in $var_assets {
	replace `v'=1 if `v'!=0 & `v'!=.
}

gen ccode="NGA"
egen hhid_unique=concat(ccode hhid)
order hhid_unique, after(hhid)
merge 1:1 hhid using "$rawdata\Nigeria_GHS_1516\clean data\Nigeria1516_hhid"
keep if _m==3
drop _m

save "$tempdata\nigeria_wealth", replace

*********************************TANZANIA***************************************
use "$rawdata\Tanzania_NPS_1415\raw data\hh_sec_i", clear

gen hhid = y4_hhid
lab var hhid "Household identifier"

gen drinking_water1=hh_i19
replace drinking_water1=hh_i29 if hh_i29!=.
lab val drinking_water1 HH_I29
decode drinking_water1, gen(drinking_water)
drop drinking_water1

decode hh_i16, gen(cooking_fuel)

gen sleeping_rooms=hh_i07_1

decode hh_i10, gen(housing_floor)

decode hh_i12, gen(toilet_facility)

decode hh_i17, gen(lighting_fuel)

keep hhid drinking_water cooking_fuel sleeping_rooms housing_floor hh_i08 hh_i09 toilet_facility lighting_fuel

save "$tempdata\tanzania_housing", replace

use "$rawdata\Tanzania_NPS_1415\raw data\hh_sec_m", clear

gen hhid = y4_hhid
lab var hhid "Household identifier"

bys hhid: egen radio=sum(hh_m01) if inlist(itemcode, 401) 
bys hhid: egen television=sum(hh_m01) if inlist(itemcode, 406)
bys hhid: egen phone=sum(hh_m01) if inlist(itemcode, 402, 403)
bys hhid: egen bicycle=sum(hh_m01) if inlist(itemcode, 427)
bys hhid: egen motorbike=sum(hh_m01) if inlist(itemcode, 426)
bys hhid: egen car=sum(hh_m01) if inlist(itemcode, 425)
*bys hhid: egen van=sum(hh_m01) if inlist(itemcode, 
bys hhid: egen animal_cart=sum(hh_m01) if inlist(itemcode, 429)
bys hhid: egen refrigerator=sum(hh_m01) if inlist(itemcode, 404)
*bys hhid: egen washing_machine=sum(hh_m01) if inlist(itemcode
bys hhid: egen iron=sum(hh_m01) if inlist(itemcode, 418)

keep hhid radio television phone bicycle motorbike car animal_cart refrigerator iron
collapse radio television phone bicycle motorbike car animal_cart refrigerator iron, by(hhid)

save "$tempdata\tanzania_assets", replace

use "$tempdata\tanzania_housing", clear
merge 1:1 hhid using "$tempdata\tanzania_assets"
drop _m

gl var_house drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility lighting_fuel

foreach v in $var_house {
	rename `v' `v'1
}

gen drinking_water=1 if inlist(drinking_water1, "OTHER, SPECIFY", "RAINWATER COLLECTION", "UNPROTECTED DUGWELL", "UNPROTECTED SPRING")
replace drinking_water=1 if inlist(drinking_water1, "SURFACE WATER (RIVER, DAM, LAKE, POND,  STREAM, CANAL, IRRIGATION CHANNELS)")
replace drinking_water=2 if inlist(drinking_water1, "CART WITH SMALL TANK/DRUM", "PROTECTED DUG WELL", "TANKER-TRUCK", "TUBEWELL / BOREHOLE", "PROTECTED SPRING") 
replace drinking_water=3 if inlist(drinking_water1, "BOTTLED WATER", "PIPED WATER")
tab drinking_water
drop drinking_water1

gen cooking_fuel=1 if inlist(cooking_fuel1, "OTHER (SPECIFY)", "firewood")
replace cooking_fuel=2 if inlist(cooking_fuel1, "charcoal", "paraffin")
replace cooking_fuel=3 if inlist(cooking_fuel1, "electricity", "gas")
tab cooking_fuel
drop cooking_fuel1

gen sleeping_rooms=1 if sleeping_rooms1<2
replace sleeping_rooms=2 if sleeping_rooms1==2
replace sleeping_rooms=3 if sleeping_rooms1>2 & sleeping_rooms1!=.
tab sleeping_rooms
drop sleeping_rooms1

gen housing_floor=1 if inlist(housing_floor1, "earth", "OTHER (SPECIFY)")
replace housing_floor=2 if inlist(housing_floor1, "CONCRETE,CEMENT,TILES,TIMBER")
replace housing_floor=3 if housing_floor1=="CONCRETE,CEMENT,TILES,TIMBER" & hh_i08==6 & hh_i09==3
tab housing_floor
drop housing_floor1 hh_i08 hh_i09

gen toilet_facility=1 if inlist(toilet_facility1, "NO TOILET", "OPEN PIT WITHOUT SLAB", "OTHER (SPECIFY)", "PIT LATRINE WITH SLAB (NOT WASHABLE)")
replace toilet_facility=2 if inlist(toilet_facility1, "vip", "PIT LATRINE WITH SLAB (WASHABLE)", "ecosan", "POUR FLUSH")
replace toilet_facility=3 if inlist(toilet_facility1, "FLUSH TOILET")
tab toilet_facility
drop toilet_facility1

gen electricity= inlist(lighting_fuel1, "electricity", "solar")
replace electricity=. if lighting_fuel1==""
tab electricity
drop lighting_fuel1

gl var_assets radio television phone bicycle motorbike car refrigerator iron

foreach v in $var_assets {
	replace `v'=1 if `v'!=0 & `v'!=.
}

drop animal_cart
gen ccode="TZA"
egen hhid_unique=concat(ccode hhid)
order hhid_unique, after(hhid)
merge 1:1 hhid using "$rawdata\Tanzania_NPS_1415\clean data\Tanz1415_hhid"
keep if _m==3
drop _m

save "$tempdata\tanzania_wealth", replace


***********************************APPEND***************************************

use "$tempdata\ethiopia_wealth", clear

append using "$tempdata\malawi_wealth"
order washing_machine iron, after(refrigerator)

append using "$tempdata\niger_wealth"

append using "$tempdata\nigeria_wealth"

append using "$tempdata\tanzania_wealth"

g hh_appliances=0
replace hh_appliances=1 if (refrigerator==1 | washing_machine==1 | iron==1)
replace hh_appliances=. if (refrigerator==. & washing_machine==. & iron==.)

lab var hh_appliances "If the household own appliances [refrigerator; washing machine; iron] (1=yes)"
lab val hh_appliances dummy_code

lab def dummy_code 0 "No" 1 "Yes"
foreach v in radio television phone bicycle motorbike car refrigerator washing_machine iron electricity {
	lab var `v' "If the household own `v' (1=yes)"
	lab val `v' dummy_code
}

lab def cat_code 1 "Low" 2 "Medium" 3 "High"
foreach v in drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility {
	lab val `v' cat_code
}

lab var drinking_water "Quality of drinking water"
lab var cooking_fuel "Quality of cooking fuel"
lab var sleeping_rooms "Number of sleeping rooms"
lab var housing_floor "Quality of floor material"
lab var toilet_facility "Quality of toilet facility"

save "$tempdata\wealthindex_dataset", replace

*******************************************************************************
********************************************************************************
*					       		INDEX CREATION		   						   *
********************************************************************************

use "$tempdata\wealthindex_dataset", clear

#delimit;		//Asset variables (without dummy)
	global asset_var
	radio television phone bicycle motorbike car hh_appliances 
	drinking_water cooking_fuel sleeping_rooms housing_floor toilet_facility electricity
	;
#delimit cr

*Cleaning for missing data
egen missing=rowmiss($asset_var)
foreach v in $asset_var{
	drop if `v'==.
}
drop missing

*		5 Ethiopia (0.003% of dataset)
*		62 Malawi (0.038% of dataset)

****************************************************

gen radio_tv= (radio==1 | television==1)
lab var radio_tv "If the household own at least a radio or tv (1=yes)"
lab val radio_tv dummy_code
order radio_tv, after(television)

*****************Household wealth index

#delimit;	//Wealth index PPCA method
	global asset_wealthppca
	radio_tv phone bicycle motorbike car 
	sleeping_rooms housing_floor toilet_facility electricity
	;
#delimit cr

polychoricpca $asset_wealthppca, score(ppca_wealthindex) nscore(1)
drop __ttradio_tv - __ttelectricity
ren ppca_wealthindex1 ppca_wealthindex
		
egen min_ppca=min(ppca_wealthindex)
egen max_ppca=max(ppca_wealthindex)

gen IWI_ppcawealthindex=(100*(ppca_wealthindex + (-min_ppca)))/((-min_ppca)+max_ppca)
drop min_ppca max_ppca

lab var IWI_ppcawealthindex "PPCA wealth index (IWI standard)"

keep ccode hhid_unique IWI_ppcawealthindex

save "$tempdata\wealth_wa", replace


exit, clear

********************************************************************************
********************************************************************************
********************************************************************************
