
******************************* SET DRIVES *************************************

gl DRIVE	"$path/data"
gl DO	"$path/do"
gl TAB		"$path/tables"
gl FIG		"$path/figures"

cd "${DRIVE}" 

use "CHEVAL_allcountries_clean.dta", clear

* by country: how many individuals have a formal job? how many individuals indirectly benefit from a formal job of their household member
ren *_*_*_* ****
ren *_*_* ***
keep if analysis ==1
ren IWI_ppcawealthindex wealth
ren log_pcexp logpcexp
gen countryw= ///
cond(country=="Ethiopia",.227, ///
cond(country=="Malawi",.040, ///
cond(country=="Niger",0.051, ///
cond(country=="Nigeria",.462, ///
cond(country=="Tanzania",.128,. ///
)))))
la var female_head "Female Head"
la var land_d "Owns Land"
svyset hhid_unique,  weight(hhweight) vce(linearized)
local outregopts starlevels(10 5 1) starloc(1) tex replace varlabels se a4 landscape fragment nolegend
local con1 i.admin1 hhsize female_head age_head land_d shschoolsec dep_ratio total_jobs 
local keep hhsize female_head age_head land_d shschoolsec dep_ratio total_jobs
encode ccode,gen(cccode)
levelsof ccode, l(ccodelist)
local if estsample
set tracedepth 1
*set trace on
foreach var of var shempYformal shempYformalcon shempYformalsp shfteformal shfteformalsp shfteformalcon{
	gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
}
gen informalhead=cond(head_f==1 & !mi(head_f),0,1,.)
label var informalhead "Informal HH Head"
gen informalheadsp=cond(head_sp==1 & !mi(head_sp),0,1,.)
label var informalheadsp "Informal HH Head"
gen informalheadcon=cond(head_con==1 & !mi(head_con),0,1,.)
label var informalheadcon "Informal HH Head"
	la def nv 0 "Formal" 1 "0-50% informal" 2 "50-99% informal" 3 "100% informal"
local shempYinformal "Sh Inf Inc"
local shfteinformal "Sh FTE Inf"
local poor "P(Poor=1)"
local logpcexp "ln(ExpPC)"
local sp "(SocProt Based)"
local con "(Contract Based)"
foreach bit in " " sp con{
foreach var in shempYinformal shfteinformal{
	gen n`var'`bit'=cond(`var'`bit'==0,0, ///
										cond(`var'`bit'>0 & `var'`bit'<=.5,1, ///
										cond(`var'`bit'>.5 & `var'`bit'<1,2, ///
										cond(`var'`bit'==1,3,.))))
	la val n`var'`bit' nv
	la var n`var'`bit' `"``var''``bit''"'
	}
}

	local p 0
foreach ccode of local ccodelist{
local counter 1
	local ++p
	foreach lhs of var wealth { 
		foreach bit in " " sp{
			foreach rhs of var shempYinformal shfteinformal{
			if `counter'==1 {
					local merge store
					local addtable
				}
				else {
					local merge merge
					local addtable addtable
				}
			ren n`rhs'`bit' informaldepth
			la var informaldepth "Informal Depth"
			noi cap noi svy: reg `lhs' informalhead`bit' `con1' if ccode=="`ccode'" & !rural
			cap gen estsample=e(sample)
			mat temp=r(table)
			outreg using $TAB/c`ccode'-hhead-urban-wealth.tex, addrows("Geog FEs","yes"\"Distance FEs","yes") nod `merge'(c`ccode'hh)  keep(informalhead`bit' `keep') ctitles("","Informal HH Head"\"","``bit''") `outregopts'
			mat inf=temp[1,1],temp[5,1],temp[6,1]
			noi cap noi svy: reg `lhs' ib0.informaldepth `con1' if `if'
			mat temp=r(table)
			outreg using $TAB/c`ccode'-urban-wealth.tex, addrows("Geog FEs","yes"\"Distance FEs","yes") nod `merge'(c`ccode')  keep(1.informaldepth 2.informaldepth 3.informaldepth `keep') ctitles("","``lhs''"\"","``rhs''"\"","``bit''") `outregopts'
			mat `lhs'`rhs'`bit'=nullmat(`lhs'`rhs'`bit') \ `p' ,inf,temp[1,2],temp[5,2],temp[6,2],temp[1,3],temp[5,3],temp[6,3],temp[1,4],temp[5,4],temp[6,4]
			drop estsample
			local ++counter
			ren informaldepth n`rhs'`bit'
			}
		}
	}
!perl -pi -e 's/\\noalign\{\\smallskip\}//g' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/\\smallskip.*//' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/[\[\]]+//g' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -0777 -pi -e 's/hline/toprule/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -0777 -pi -e 's/hline/midrule/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -0777 -pi -e 's/hline/bottomrule/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/(\d{3})\.000/$1/g' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/\\(begin|end)\{center\}//g' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/(\\bottomrule\\end\{tabular\})\\\\/$1/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/1bn.informaldepth/0\-50\\\% informal/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/2.informaldepth/51\-99\\\% informal/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/3.informaldepth/100\\\% informal/' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/\\noalign\{\\smallskip\}//g' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -pi -e 's/\\smallskip.*//' ~/Dropbox/CHEVAL/tables/c`ccode'-urban-wealth.tex
!perl -pi -e 's/[\[\]]+//g' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -0777 -pi -e 's/hline/toprule/' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -0777 -pi -e 's/hline/midrule/' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -0777 -pi -e 's/hline/bottomrule/' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -pi -e 's/(\d{3})\.000/$1/g' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -pi -e 's/\\(begin|end)\{center\}//g' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
!perl -pi -e 's/(\\bottomrule\\end\{tabular\})\\\\/$1/' ~/Dropbox/CHEVAL/tables/c`ccode'-hhead-urban-wealth.tex
}

foreach rhs in shempYinformal shfteinformal{
mat wealth`rhs'=wealth`rhs'\wealth`rhs'sp
}
mat colname wealthshempYinformal=cccode Yinf25be Yinf25ll Yinf25ul Yinf50be Yinf50ll Yinf50ul Yinf75be Yinf75ll Yinf75ul Yinf100be Yinf100ll Yinf100ul
mat colname wealthshfteinformal=cccode fteinf25be fteinf25ll fteinf25ul fteinf50be fteinf50ll fteinf50ul fteinf75be fteinf75ll fteinf75ul fteinf100be fteinf100ll fteinf100ul
local n 0
foreach mat in wealthshempYinformal  wealthshfteinformal {
	if `++n'<=1 {
		local name Y
	}
	else {
		local name fte
	}
local n1 wealth
local n2 wealth
local c1 ETH
local c2 MWI
local c3 NER 
local c4 NGA
local c5 TZA
local c6 UGA
clear
svmat `mat', n(col)
gen ccode=""
gen type=""
local c 0
foreach ccode of local ccodelist{
replace ccode="`ccode'" in `++c'
replace type="plain" in `c'
}
foreach ccode of local ccodelist{
replace ccode="`ccode'" in `++c'
replace type="sp" in `c'
}
reshape long `name'@, i(ccode cccode type) j(new,string)
gen est=substr(new,-2,.)
gen inf=subinstr(subinstr(new,"inf","",.),est,"",.)
drop new
reshape wide `name',i(ccode cccode type inf) j(est,string)
destring inf,replace
save `mat'-urban,replace
	forv ccode=1/5{
		foreach type in plain sp{
		two (scatter `name'be inf,leg(lab(1 "Estimate"))) ///
		(rcap `name'll `name'ul inf, leg(lab(2 "95%CI"))) ///
		if type=="`type'" & cccode==`ccode', yline(0) scheme(s1mono) ///
		xlabel(25 "Dummy" 50 "1-50" 75 "51-99" 100 "100%") xti("") ti(`c`ccode'')
		graph save  $FIG/c`ccode'`type'`name'`n`n''-urban.gph,replace
		}
	}
}
cd $FIG
foreach type in plain sp{
	foreach rhs in Y fte{
		foreach lhs in wealth{ 
		graph combine c1`type'`rhs'`lhs'-urban.gph c2`type'`rhs'`lhs'-urban.gph c3`type'`rhs'`lhs'-urban.gph c4`type'`rhs'`lhs'-urban.gph c5`type'`rhs'`lhs'-urban.gph, scheme(s1mono)
		graph export `type'`rhs'`lhs'-urban.eps, cmyk(on) replace
		}
	}
}
