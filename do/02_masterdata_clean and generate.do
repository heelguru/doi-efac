********************************************************************************
********** CHEVAL : FORMALITY AT HOUSEHOLD LEVEL *******************************
********************************************************************************
** Create one data file from all countries with indicators of interest *********
********************************************************************************

/*
This file cleans and generates variables of interest:
1) cleaning income variables before generating income shares
2) generate occupation shares

The datasets used for this are:
"CHEVAL_allcountries_raw.dta"

Resulting dataset with all countries "CHEVAL_allcountries_clean.dta"

Stata MP version 15 
*/

capture log close
clear
clear matrix
set more off

********************************************************************************

******************************* SET DRIVES *************************************
gl DRIVE	"$path\data"
gl TEMP	"${DRIVE}/temp"
gl DO	"$path\do"

cd "${DRIVE}" 
********************************************************************************

use "CHEVAL_allcountries_raw.dta", clear

*1) Clean income variables *

*clean outliers of income components and then generate income aggregates from these 'clean' income components
	local varlist pcexp own_farm_inc own_farm_inc_net farm_sales farm_wage_inc off_farm_AFS_wage_inc non_AFS_wage_inc nfe_AFS_gross_inc ///
			nfe_nonAFS_gross_inc nfe_AFS_net_inc nfe_nonAFS_net_inc nfe_net_inc other_income wage_formal tot_AFS_inc_formal tot_nonAFS_inc_formal ///
			wage_formalcon tot_AFS_inc_formalcon tot_nonAFS_inc_formalcon wage_formalsp tot_AFS_inc_formalsp tot_nonAFS_inc_formalsp
	foreach v of local varlist{
		recode `v' .=0 
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
	}
replace farm_sales=own_farm_inc if farm_sales>own_farm_inc & farm_sales!=.

*drop shares and total incomes that were created earlier to now generate 'clean' versions, i.e. each component of income aggregates was cleaned from outliers
drop total_income farm_income nfe_gross_inc total_income_emp ///
 total_AFS_income total_nonAFS_income total_nonAFS_emp_income nonfarm_income nonfarm_income_emp tot_inc_net  nfe_net_inc 

//generate total income with outlier-cleaned income components
egen total_income = rowtotal(own_farm_inc farm_wage_inc off_farm_AFS_wage_inc non_AFS_wage_inc nfe_AFS_gross_inc nfe_nonAFS_gross_inc other_income)
egen tot_inc_net = rowtotal(own_farm_inc_net farm_wage_inc off_farm_AFS_wage_inc non_AFS_wage_inc nfe_AFS_net_inc nfe_nonAFS_net_inc other_income)

egen nfe_gross_inc = rowtotal(nfe_AFS_gross_inc nfe_nonAFS_gross_inc)
egen nfe_net_inc = rowtotal(nfe_AFS_net_inc nfe_nonAFS_net_inc)	
	
egen total_AFS_income = rowtotal(own_farm_inc farm_wage_inc off_farm_AFS_wage_inc nfe_AFS_gross_inc )
egen total_AFS_income_net = rowtotal(own_farm_inc_net farm_wage_inc off_farm_AFS_wage_inc nfe_AFS_net_inc )
egen total_nonAFS_income = rowtotal(non_AFS_wage_inc nfe_nonAFS_gross_inc other_income)
egen total_nonAFS_income_net = rowtotal(non_AFS_wage_inc nfe_nonAFS_net_inc other_income)
egen total_nonAFS_emp_income = rowtotal(non_AFS_wage_inc nfe_nonAFS_gross_inc )
egen total_nonAFS_emp_income_net = rowtotal(non_AFS_wage_inc nfe_nonAFS_net_inc )
egen farm_income = rowtotal(farm_wage_inc own_farm_inc)
egen farm_income_net = rowtotal(farm_wage_inc own_farm_inc_net)
egen nonfarm_income = rowtotal(off_farm_AFS_wage_inc non_AFS_wage_inc nfe_gross_inc other_income )
egen nonfarm_income_net = rowtotal(off_farm_AFS_wage_inc non_AFS_wage_inc nfe_net_inc other_income )
egen nonfarm_income_emp = rowtotal(off_farm_AFS_wage_inc non_AFS_wage_inc nfe_gross_inc)
egen nonfarm_income_emp_net = rowtotal(off_farm_AFS_wage_inc non_AFS_wage_inc nfe_net_inc)
egen total_income_emp = rowtotal(own_farm_inc farm_wage_inc off_farm_AFS_wage_inc non_AFS_wage_inc nfe_gross_inc)
egen total_income_emp_net = rowtotal(own_farm_inc_net farm_wage_inc off_farm_AFS_wage_inc non_AFS_wage_inc nfe_net_inc)

	//generate income shares
	g share_ownfarm_totinc = (own_farm_inc / total_income) 
	g share_nonfarmemp_totinc = (nonfarm_income_emp / total_income) 
	g share_sold_ownprod = (farm_sales / own_farm_inc) 
	g share_ownfarm_AFS = (own_farm_inc / farm_income) 
	g share_farminc_totinc =(farm_income / total_income) 
	g share_farminc_totempinc =(farm_income / total_income_emp) 
	g share_farminc_AFSinc =(farm_income / total_AFS_income) 
	g share_nonfarm_totinc =(nonfarm_income / total_income) 
	g share_nonfarm_totempinc =(nonfarm_income_emp /total_income_emp) 
	g share_AFS_totinc = (total_AFS_income / total_income) 
	g share_AFS_totempinc = (total_AFS_income / total_income_emp) 
	g share_nonAFS_totinc = (total_nonAFS_income / total_income) 
	g share_nonAFSemp_totinc = (total_nonAFS_emp_income / total_income_emp) 
	g share_ownfarm_totempinc = (own_farm_inc / total_income_emp) 
	g share_ownfarm_farminc = (own_farm_inc / farm_income) 

	//generate formal income shares
g sh_empY_formal = (wage_formal / total_income)
g sh_empY_AFS_formal = (tot_AFS_inc_formal / total_income)
g sh_empY_nonAFS_formal = (tot_nonAFS_inc_formal / total_income)

foreach x in con sp{
	g sh_empY_formal`x' = wage_formal`x'/ total_income
	g sh_empY_AFS_formal`x' = tot_AFS_inc_formal`x' / total_income
	g sh_empY_nonAFS_formal`x' = tot_nonAFS_inc_formal`x' / total_income
}

	*for all income shares limit shares btw [0,1]:
	foreach v in share_ownfarm_totinc share_nonfarmemp_totinc share_sold_ownprod share_ownfarm_AFS share_farminc_totinc share_farminc_totempinc share_farminc_AFSinc share_nonfarm_totinc share_nonfarm_totempinc ///
				share_AFS_totinc share_AFS_totempinc share_nonAFS_totinc share_nonAFSemp_totinc share_ownfarm_totempinc share_ownfarm_farminc  ///
				sh_empY_formal sh_empY_AFS_formal sh_empY_nonAFS_formal sh_empY_formalcon sh_empY_formalsp sh_empY_AFS_formalcon sh_empY_AFS_formalsp sh_empY_nonAFS_formalcon sh_empY_nonAFS_formalsp{
		replace `v'=1 if `v'>1 & `v'!=.
		replace `v'=0 if `v'<0 & `v'!=.
	}

* 2) generate FTE shares:
egen fte_wage_formal = rowtotal(fte_on_farm_wage_f fte_AFS_wage_f fte_non_AFS_wage_f)
egen fte_wage_formal_sp = rowtotal(fte_on_farm_wage_sp fte_AFS_wage_sp fte_non_AFS_wage_sp)
egen fte_wage_formal_con = rowtotal(fte_on_farm_wage_con fte_AFS_wage_con fte_non_AFS_wage_con)

g sh_fte_formal = (fte_wage_formal/fte_total)
g sh_fte_formal_sp = (fte_wage_formal_sp/fte_total)
g sh_fte_formal_con = (fte_wage_formal_con/fte_total)
	*limit shares btw [0,1]:
foreach v in sh_fte_formal sh_fte_formal_sp sh_fte_formal_con{
		replace `v'=1 if `v'>1 & `v'!=.
		replace `v'=0 if `v'<0 & `v'!=.
		recode `v' .=0
	}

* 3) generate ccupation count share:
*occupation count for two types of formality: social protection and contract status
egen occ_wage_f = rowtotal(occ_AFS_wage_f occ_on_farm_wage_f occ_non_AFS_wage_f )
egen occ_wage_con = rowtotal(occ_AFS_wage_con occ_on_farm_wage_con occ_non_AFS_wage_con )
egen occ_wage_sp = rowtotal(occ_AFS_wage_sp occ_on_farm_wage_sp occ_non_AFS_wage_sp)

g sh_occ_formal = (occ_wage_f/total_jobs)
g sh_occ_formal_sp = (occ_wage_sp/total_jobs)
g sh_occ_formal_con = (occ_wage_con/total_jobs)
	
	*limit shares btw [0,1]:
foreach v in sh_occ_formal sh_occ_formal_sp sh_occ_formal_con{
		replace `v'=1 if `v'>1 & `v'!=.
		replace `v'=0 if `v'<0 & `v'!=.
		recode `v' .=0
	}

* 4) generate further variables:

*DEMOGRAPHICS:
g dep_ratio = dep_n/indep_n

*LOG transformations:
	replace pcexp=0.000000001 if pcexp==0
g log_pcexp = log(pcexp)


*Income diversification: out of the 6 main sources and 'other income'
foreach v in d_ownfarm d_on_farm_AFS_wage d_off_farm_AFS_wage d_non_AFS_wage d_enterprise_AFS d_enterprise_nonAFS other_income{
	g c`v' = (`v'>=1 & `v'!=.)
}
egen inc_count = rowtotal(cd_ownfarm cd_on_farm_AFS_wage cd_off_farm_AFS_wage cd_non_AFS_wage cd_enterprise_AFS cd_enterprise_nonAFS cother_income)
drop cd_ownfarm cd_on_farm_AFS_wage cd_off_farm_AFS_wage cd_non_AFS_wage cd_enterprise_AFS cd_enterprise_nonAFS cother_income

*How important are different formal wage income activities conditional on having wage income
**Add conditional on households with wage incomes
g c_wage = (d_on_farm_AFS_wage>=1 | d_off_farm_AFS_wage>=1 | d_non_AFS_wage>=1)

*************
* ensure administrative area identifiers are unique:
egen admin1 = group(country admin_1)
egen admin2 = group(country admin_1 admin_2)
egen admin3 = group(country admin_1 admin_2 admin_3)
drop admin_1 admin_2 admin_3

*label all remaining variables
do "$DO/labels.do"
	
order country ccode rural admin1 admin2 admin3 ea_id hhid_unique hhweight pcexp poor female_head age_head age_mean dep_ratio youth_share_24 total_active_youth school_secondary_hh educ_max N_inschool sh_school_secondary NEET NEET_34 sh_NEET sh_NEET_34 out_labor_mkt credit land_d land_owned remit fte_* occ_* share_* sh_* total_*

drop educ_max school_secondary_hh

**get wealth index and share of working age members:
merge 1:1 hhid_unique using "temp/wealth_wa", nogen keep(1 3)

*** Define analysis sample: urban and with all variables:

	*check missing values:
	misstable sum hhsize female_head age_head land_d sh_school_secondary dep_ratio total_jobs sh_fte_formal_sp sh_empY_formalsp log_pcexp poor
	
g analysis=1
	replace analysis=. if age_head==. | dep_ratio==. | sh_empY_formalsp==. | sh_fte_formal_sp==. | log_pcexp==. | poor==. | head_sp==.

keep if rural==0 //only urban households 

save "CHEVAL_allcountries_clean.dta", replace

ex
