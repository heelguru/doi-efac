
capture log close
clear
clear matrix
set more off
******************************* SET DRIVES *************************************

cd "$path/data" 

use "CHEVAL_allcountries_clean.dta", clear
	
foreach var of var sh_fte_formal_sp sh_empY_formalsp {
	gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
}

gen informalhead_sp=cond(head_sp==1 & !mi(head_sp),0,1,.)
label var informalhead_sp "Informal HH Head"
	la def nv 0 "Formal" 1 "1-50% informal" 2 "51-99% informal" 3 "100% informal"

foreach var in sh_empY_informalsp sh_fte_informal_sp{
	gen n`var'`bit'=cond(`var'`bit'==0,0, ///
										cond(`var'`bit'>0 & `var'`bit'<=.5,1, ///
										cond(`var'`bit'>.5 & `var'`bit'<1,2, ///
										cond(`var'`bit'==1,3,.))))
	la val n`var'`bit' nv
	la var n`var'`bit' `"``var''``bit''"'
	}



** Summary statistics of sample
svyset hhid_unique,  weight(hhweight) vce(linearized)

	** Table of main variables by country:
	bys country: sutex sh_fte_informal_sp sh_empY_informalsp informalhead_sp if analysis==1 [aw=hhweight], label digits(2) file("$path/tables\Sumstats_mainvars_country.tex") replace	
	
	** Table of proportion of households in each bin by country
tab nsh_fte_informal_sp, g(new)
tabout country if analysis==1 [aw=hhweight] using "$path/tables\Sumstats_binsFTE_country.txt" , c(mean new1 mean new2 mean new3 mean new4) f(2) sum replace
	drop new*
tab nsh_empY_informalsp, g(new)
tabout country if analysis==1 [aw=hhweight] using "$path/tables\Sumstats_binsY_country.txt" , c(mean new1 mean new2 mean new3 mean new4) f(2) sum replace
	drop new*

	
	*** Table with household characteristics across bins:
	bys country: sutex pcexp poor female_head age_head hhsize dep_ratio sh_school_secondary land_d total_jobs if analysis==1 [aw=hhweight], label 
	digits(2) file("$path/tables\Sumstats_mainvars_country.tex") replace	

		local xvars "pcexp poor female_head age_head hhsize dep_ratio sh_school_secondary land_d total_jobs"
	matrix xsum= J(20, 9, -99)
	matrix rownames xsum= Ethiopia Malawi Niger Nigeria Tanzania Ethiopia Malawi Niger Nigeria Tanzania Ethiopia Malawi Niger Nigeria Tanzania Ethiopia Malawi Niger Nigeria Tanzania
	matrix colnames xsum= PerCapitaExpenditure Poor Femalehead Ageofhead Householdsize Dependencyratio secondaryschooling Householdownsland TotalJobs
	qui	{
		local r 1
		forv b=0/3{
			foreach x in Ethiopia Malawi Niger Nigeria Tanzania {
				local c 1
				foreach var of varlist `xvars'{
					cap svy: mean `var' if analysis==1 & country=="`x'" & nsh_empY_informalsp==`b'
					mat temp=r(table)
					matrix xsum[`r',`c'] = temp[1,1]
					local ++c
				}
				local ++r
			}	
		}
	}
	matrix list xsum, format(%15.2f)
	xml_tab xsum , save("$path/tables\Sumstats_country_infbin.xml") replace 
	
	
	