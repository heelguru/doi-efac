	
capture log close
clear
clear matrix
set more off
******************************* SET DRIVES *************************************
gl DRIVE	"$path/data"
gl DO	"$path/do"
gl TAB		"$path/tables"
gl FIG		"$path/figures"
cd "${DRIVE}" 

	tempfile main new
	tempvar todrop
	use $DRIVE/NGA_panel.dta ,clear
	ren *_*_*_* ****
	ren *_*_* ***
	gen ltot=ln(pcexp_n)
	keep if !rural
	egen nid=group(hhid_unique)

	tostring year,replace
	encode year, gen(newt)
		tab year,gen(y)
	sort nid newt
	xtset nid newt
	la var female_head "Female Head"
la var land_d "Owns Land"
svyset hhid_unique,  weight(hhweight) vce(linearized)
local outregopts starlevels(10 5 1) starloc(1) tex replace varlabels se a4 landscape fragment nolegend
local con1 hhsize dep_ratio shschoolsec
local keep hhsize dep_ratio shschoolsec
local if estsample

	foreach var of var shfteformal shfteformalsp{
		gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
	}	
	gen informalhead=cond(head_f==1 & !mi(head_f),0,1,.)
	label var informalhead "Informal HH Head"
	gen informalheadsp=cond(head_sp==1 & !mi(head_sp),0,1,.)
	label var informalheadsp "Informal HH Head"
		la def nv 0 "Formal" 1 "0-99% informal" 2 "100% informal"
	local shempYinformal "Sh Inf Inc"
	local shfteinformal "Sh FTE Inf"
	local poor_n "Prob(Poor=1)"
	local ltot "ln(Total Expenditure)"
	local sp "(SocProt Based)"
	local con "(Contract Based)"
	foreach bit in " " sp{
	foreach var in shfteinformal{
		gen n`var'`bit'=cond(`var'`bit'==0,0, ///
											cond(`var'`bit'>0 & `var'`bit'<1,1, ///
											cond(`var'`bit'==1,2,.)))
		la val n`var'`bit' nv
		la var n`var'`bit' `"``var''``bit''"'
		}
	}
tempfile master
save `master'
**Now to merge in the combinations that define transitions
keep nshfteinformalsp hhid_unique newt
reshape wide nshfteinformalsp, i(hhid_unique) j(newt)
merge m:1 nshfteinformalsp1 nshfteinformalsp2 nshfteinformalsp3 using combinationsnga, gen(spm1)
merge 1:m hhid_unique using `master', gen(spm2)
save `master',replace
**now to stack the dataset on transition time
keep if newt==1 | newt==2
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
gen post=newt==2
tempfile stacked
egen stacknid=group(nid)
su nid, mean
local max=`r(max)'
/*
save `stacked'
use `master',clear
keep if newt==2 | newt==3
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
egen stacknid=group(nid)
replace stacknid=stacknid+`max'
gen post=newt==3
append using `stacked'
save `stacked',replace
*/
xtset stacknid post
xtbalance , range(0 1) miss(`con1' poor ltot)
duplicates tag nid, gen(timesinpanel)
*keep if timesinpanel>=2
ren *_*_* ***

foreach lhs in poor_n ltot{
	foreach rhs in bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf {
			cap xtreg `lhs' ib0.`rhs'##ib0.post `con1', fe vce(ro)
			mat temp=r(table)
			mat `rhs'sp=nullmat(`rhs'sp),temp[1,8],temp[2,8],e(N)
		}
		mat ressp=bfullffullinfsp \ dfullinffullfsp \ efullfmixsp \ ffullinfmixsp \ gmixfullfsp \ hmixfullinfsp
local bc = rowsof(ressp)
		local cols = colsof(ressp)
		mat stars = J(`bc',`cols',0)
		forv j=1(3)`cols' {
			forv k = 1/`bc' {
				mat stars[`k' ,`j'] = ///
				(abs(ressp[`k' ,`j']/ressp[`k' ,`j'+1]) > invttail(ressp[`k' ,`j'+2] ,0.10/2)) + ///
				(abs(ressp[`k' ,`j']/ressp[`k' ,`j'+1]) > invttail(ressp[`k' ,`j'+2] ,0.05/2)) + ///
				(abs(ressp[`k' ,`j']/ressp[`k' ,`j'+1]) > invttail(ressp[`k' ,`j'+2] ,0.01/2))
			}
}
frmttable using $TAB/diff-nga-resultssp-`lhs'.tex, statmat(ressp) substat(2) sdec(3) annotate(stars) asymbol(*,**,***) tex replace ///
	rtitles("Fully formal switched to fully informal (H1)"\""\"N"\ "Fully informal switched to fully formal (H2)"\""\"N"\"Fully formal switched to mix (H3)"\""\"N"\ ///
	"Fully informal switched to mix (H4)"\"" \"N" \ "Mix to fully formal (H5)"\"" \"N" \ "Mix to fully informal (H6)"\"" \"N")  ///
	ctitles("LHS","``lhs''") fragment plain

!perl -pi -e 's/\\noalign\{\\smallskip\}//g' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -pi -e 's/[\[\]]+//g' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -0777 -pi -e 's/hline/toprule/' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -0777 -pi -e 's/hline/midrule/' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -0777 -pi -e 's/hline/bottomrule/' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -pi -e 's/(\d{3})\.000/$1/g' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -pi -e 's/\\(begin|end)\{center\}//g' $TAB/diff-nga-resultssp-`lhs'.tex
!perl -pi -e 's/(\\bottomrule\\end\{tabular\})\\\\/$1/' $TAB/diff-nga-resultssp-`lhs'.tex
mat drop _all
	}
