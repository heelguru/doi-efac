
*** This do-file generates occupation dummies and FTEs for each occupation ***

* Name / Create Pooled Variables for FTE and Occupation Count
* Own Farming
gen fte_ownfarm = farm_f_c1
gen occ_ownfarm = farm_o_c1

* On-Farm AFS Wage Work
gen fte_on_farm_AFS_wage = f_f0_c2 + f_f1_c2
gen occ_on_farm_AFS_wage = o_f0_c2 + o_f1_c2
	*!EVA: formal on-farm wage work
gen fte_on_farm_wage_f = f_f1_c2
gen occ_on_farm_wage_f = o_f1_c2
gen fte_on_farm_wage_sp = f_spf1_c2
gen occ_on_farm_wage_sp = o_spf1_c2
gen fte_on_farm_wage_con = f_conf1_c2
gen occ_on_farm_wage_con = o_conf1_c2

* Off-Farm AFS Wage Work	
gen fte_off_farm_AFS_wage = 0
gen occ_off_farm_AFS_wage = 0
*!EVA: formal on-farm wage work
gen fte_AFS_wage_f = 0
gen occ_AFS_wage_f = 0
gen fte_AFS_wage_sp = 0
gen occ_AFS_wage_sp = 0
gen fte_AFS_wage_con = 0
gen occ_AFS_wage_con = 0
forvalues afsw=3(1)6 {
	replace fte_off_farm_AFS_wage = fte_off_farm_AFS_wage + f_f0_c`afsw' + f_f1_c`afsw'
	replace occ_off_farm_AFS_wage = occ_off_farm_AFS_wage + o_f0_c`afsw' + o_f1_c`afsw'
	*!EVA: formal AFS wage FTEs
	replace fte_AFS_wage_f = fte_AFS_wage_f + f_f1_c`afsw'
	replace occ_AFS_wage_f = occ_AFS_wage_f + o_f1_c`afsw'
	replace fte_AFS_wage_sp = fte_AFS_wage_sp + f_spf1_c`afsw'
	replace occ_AFS_wage_sp = occ_AFS_wage_sp + o_spf1_c`afsw'
	replace fte_AFS_wage_con = fte_AFS_wage_con + f_conf1_c`afsw'
	replace occ_AFS_wage_con = occ_AFS_wage_con + o_conf1_c`afsw'
}
*

* Total AFS Wage Work [nb it includes farm wage-workers & off-farm AFS workers]
gen fte_AFS_wage = 0
gen occ_AFS_wage = 0
forvalues afsw=1(1)6 {
	replace fte_AFS_wage = fte_AFS_wage + f_f0_c`afsw' + f_f1_c`afsw'
	replace occ_AFS_wage = occ_AFS_wage + o_f0_c`afsw' + o_f1_c`afsw'
}
*

* Non AFS Wage Work
gen fte_non_AFS_wage = 0
gen occ_non_AFS_wage = 0
*!EVA: formal on-farm wage work
gen fte_non_AFS_wage_f = 0
gen occ_non_AFS_wage_f = 0
gen fte_non_AFS_wage_sp = 0
gen occ_non_AFS_wage_sp = 0
gen fte_non_AFS_wage_con = 0
gen occ_non_AFS_wage_con = 0
forvalues nfa=7(1)12 {
	replace fte_non_AFS_wage = fte_non_AFS_wage + f_f0_c`nfa' + f_f1_c`nfa'
	replace occ_non_AFS_wage = occ_non_AFS_wage + o_f0_c`nfa' + o_f1_c`nfa'
	*!EVA: formal AFS wage FTEs
	replace fte_non_AFS_wage_f = fte_non_AFS_wage_f + f_f1_c`nfa'
	replace occ_non_AFS_wage_f = occ_non_AFS_wage_f + o_f1_c`nfa'
	replace fte_non_AFS_wage_sp = fte_non_AFS_wage_sp + f_spf1_c`nfa'
	replace occ_non_AFS_wage_sp = occ_non_AFS_wage_sp + o_spf1_c`nfa'
	replace fte_non_AFS_wage_con = fte_non_AFS_wage_con + f_conf1_c`nfa'
	replace occ_non_AFS_wage_con = occ_non_AFS_wage_con + o_conf1_c`nfa'
}
*

* Enterprise
gen fte_enterprise = 0
gen occ_enterprise = 0
forvalues e=1(1)12 {
	replace fte_enterprise = fte_enterprise + nfe_f_c`e'
	replace occ_enterprise = occ_enterprise + nfe_o_c`e'
}
*

* Enterprise by AFS
gen fte_enterprise_AFS = 0
gen occ_enterprise_AFS = 0
gen fte_enterprise_nonAFS = 0
gen occ_enterprise_nonAFS = 0
forvalues ea=1(1)6 {
	replace fte_enterprise_AFS = fte_enterprise_AFS + nfe_f_c`ea'
	replace occ_enterprise_AFS = occ_enterprise_AFS + nfe_o_c`ea'
}
*
forvalues ea=7(1)12 {
	replace fte_enterprise_nonAFS = fte_enterprise_nonAFS + nfe_f_c`ea'
	replace occ_enterprise_nonAFS = occ_enterprise_nonAFS + nfe_o_c`ea'
}
*


*** Farm labor
gen fte_farm = 0
gen occ_farm = 0
forvalues c=1(1)2 {
	replace fte_farm = fte_farm + f_f3_c`c'
	replace occ_farm = occ_farm + o_f3_c`c'
}
*

*** Non Farm labor
gen fte_nonfarm = 0
gen occ_nonfarm = 0
forvalues c=3(1)12 {
	replace fte_nonfarm = fte_nonfarm + f_f3_c`c'
	replace occ_nonfarm = occ_nonfarm + o_f3_c`c'
}
*

* Total AFS
gen fte_total_AFS = 0
gen occ_total_AFS = 0
forvalues c=1(1)6 {
	replace fte_total_AFS = fte_total_AFS + f_f3_c`c'
	replace occ_total_AFS = occ_total_AFS + o_f3_c`c'
}
*

* Total Non AFS
gen fte_total_nonAFS = 0
gen occ_total_nonAFS = 0
forvalues c=7(1)12 {
	replace fte_total_nonAFS = fte_total_nonAFS + f_f3_c`c'
	replace occ_total_nonAFS = occ_total_nonAFS + o_f3_c`c'
}
*

* Cap individual FTE at 2
gen fte_scale = 1
replace fte_scale = 2/fte_total if fte_total>2
	*!EVA: added formal_sp and formal_con FTEs
foreach fv in fte_total fte_ownfarm fte_on_farm_AFS_wage fte_off_farm_AFS_wage fte_AFS_wage ///
			fte_non_AFS_wage fte_enterprise fte_enterprise_AFS fte_enterprise_nonAFS fte_farm ///
			fte_nonfarm fte_total_AFS fte_total_nonAFS fte_on_farm_wage_f fte_on_farm_wage_sp  fte_on_farm_wage_con ///
			fte_non_AFS_wage_f fte_non_AFS_wage_con fte_non_AFS_wage_sp fte_AFS_wage_f fte_AFS_wage_sp fte_AFS_wage_con {
	replace `fv' = `fv'*fte_scale
}

*
forvalues f=1(1)12 {
	replace f_f0_c`f' = f_f0_c`f'*fte_scale
	replace f_f1_c`f' = f_f1_c`f'*fte_scale
	replace f_f2_c`f' = f_f2_c`f'*fte_scale
	replace f_f3_c`f' = f_f3_c`f'*fte_scale
}
*
forvalues f=1(1)8 {
	replace f_f0_nc`f' = f_f0_nc`f'*fte_scale
	replace f_f1_nc`f' = f_f1_nc`f'*fte_scale
	replace f_f2_nc`f' = f_f2_nc`f'*fte_scale
	replace f_f3_nc`f' = f_f3_nc`f'*fte_scale
}
*

** Calculate Out of Labor Market by those without a job who are not unemployed
gen out_labor_mkt = 1
*replace out_labor_mkt = 0 if unemployed==1
replace out_labor_mkt = 0 if total_jobs>0
replace out_labor_mkt = 0 if age_tier==1
mvdecode out_labor_mkt, mv(0)
mvencode out_labor_mkt, mv(0)
gen occ_out = out_labor_mkt

* Calculate FTE of Out of Labor Market
gen fte_out = 1 - fte_total
replace fte_out = 0 if age_tier==1

* Calculate share of econ active youth in econ active hh members
gen active_mem = 1 if fte_total>0
gen active_youth_24 = 1 if fte_total>0 & age_tier==2
replace active_youth_24 = 1 if fte_total>0 & age_tier==3
gen active_youth_34 = active_youth_24
replace active_youth_34 = 1 if fte_total>0 & age_tier==4
mvencode active*, mv(0)

* Calculate a NEET variable
gen NEET = out_labor_mkt
replace NEET = 0 if school_currently==1
replace NEET = 0 if age_tier==1
replace NEET = 0 if age_tier>=4

gen NEET_34 = out_labor_mkt
replace NEET_34 = 0 if school_currently==1
replace NEET_34 = 0 if age_tier==1
replace NEET_34 = 0 if age_tier>=5

foreach d in ownfarm on_farm_AFS_wage farm off_farm_AFS_wage AFS_wage non_AFS_wage enterprise ///
			enterprise_AFS enterprise_nonAFS nonfarm total_AFS total_nonAFS ///
			on_farm_wage_f on_farm_wage_sp on_farm_wage_con non_AFS_wage_f non_AFS_wage_con non_AFS_wage_sp AFS_wage_f AFS_wage_sp AFS_wage_con {
	gen d_`d' = 1 if occ_`d'>0
}
*
mvencode d_*, mv(0)

*DEMOGRAPHICS:
** household level variables that require individual level information:
*  Dependency ratio 
g dep = (age>=0 & age<=14 | age>=65) if age!=.
g indep = (age>=15 & age<=64) if age!=.

*  Adult with at least secondary school 
g adult_school = (school_secondary==1  & age_tier>2 & age_tier!=.)
g adult = (age_tier>2 & age_tier!=.)


*characteristics of household head (sex, age, education)

egen d_in = rmax (d_ownfarm d_on_farm_AFS_wage d_farm d_off_farm_AFS_wage d_AFS_wage d_non_AFS_wage d_enterprise d_enterprise_AFS d_enterprise_nonAFS d_nonfarm d_total_AFS d_total_nonAFS)
g d_out = (d_in==0) //out of labor market

lab var d_ownfarm "Occupied, own farm"
lab var d_farm "Occupied, own farm and on-farm wage work"
lab var d_nonfarm "Occupied, off-farm work"
lab var d_on_farm_AFS_wage "Occupied, on-farm wage work"
lab var d_off_farm_AFS_wage  "Occupied, off-farm AFS wage work"
lab var d_AFS_wage  "Occupied, AFS wage work"
lab var d_non_AFS_wage  "Occupied, non-AFS wage work"
lab var d_enterprise  "Occupied, own enterprise"
lab var d_enterprise_AFS  "Occupied, AFS enterprise"
lab var d_enterprise_nonAFS  "Occupied, non-AFS enterprise"
lab var d_total_AFS "Occupied, AFS any activity"
lab var d_total_nonAFS "Occupied, non-AFS any activity"
lab var adult_school "Adult individual with at least the secondary school"
label var adult "Adult individual ( age>=18)"

foreach v in f sp con{
	g d_wage_`v' = (d_on_farm_wage_`v'==1 | d_non_AFS_wage_`v'==1 | d_AFS_wage_`v'==1)
	g head_`v' = ( head==1 & d_wage_`v'==1)
}
lab var head_f "Head has formal wage job"
lab var head_sp "Head has formal wage job, social protection"
lab var head_con "Head has formal wage job, contract"

g age_head = age if head==1
	lab var age_head "Age of household head"
	
*keep only relevant variables:
keep ccode country hhid_unique occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep indep active_mem ///
		active_youth_24 school_currently out_labor_mkt school_secondary female_head age_head age head_* pension health_insurance

ex
