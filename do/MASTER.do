*** DEPTH OF INFORMALITY ***
***************************************
** MASTER DO-FILE **
***************************************

** Organisation of files:

/*
Do-files:
	- in folder "do"
	- one sub-folder called "countries" -> with another sub-folder called "NGA panel"

Data are in folder "data" with a sub-folder "temp" for temporary files. 
	- the authors provide three datasets that should be saved in the main data folder: 
		- "combinations-freeman" and "combinationsnga" are auxiliary files
		- "CHEVAL_allcountries_clean" is the main file for the cross-sectional analysis
		- "NGA_panel" is the main file for the panel analysis
		
If the researcher wants to replicate the data generation process, s/he should create the following data structure:	
1. Create a sub-folder for each country in the "data" folder.
2. Each country folder has additional sub-folders:
	- "raw data": The researcher should save the original LSMS data here.
	- "clean data": 
		- The researcher should create a sub-folder "intermediate" here.
		- The researcher should add the data files "~countryname_ISIC_AFS" in the respective country sub-folder. Those files are provided by the authors.
	- The researcher should add the data files "~countryname_spatial" in the respective country folder. Those files are provided by the authors.

The original LSMS data is available at : https://microdata.worldbank.org/index.php/catalog/lsms/?page=1&ps=15&repo=lsms

!! Malawi: Three data files that contain conversion factors for crops are provided by the authors of this paper and should be added to the folder "data\Malawi_IHS_1617\clean data":
	- "Malawi1617_kg_conversion"
	- "Malawi1617_kg_conversion_no_other"
	- "Malawi1617_kg_conversion_permanent"

*/

***********************************
*** !!! RESEARCHER SHOULD CHANGE THE FOLLOWING ***
*** SET MAIN DRIVE *** //should be the folder above "data" and "do" folder
gl path "C:/Replicationfiles"


**********************************************
**!! Programme Dependencies***
**********************************************
/*
The Do files assume that one has the following commands installed (uncomment the line to install)
*/
*ssc install outreg // version 4.32
*ssc install tabout // version 3.0.9
*ssc install sutex // 04Sep2001
*ssc install xtbalance // version 2.01
*ssc install ietoolkit // version 6.4
*ssc install ritest // version 1.1.7

*For most of our latex tables we do some very minor tidying using perl (stuff to do with formatting) 
*we ran this on a Linux box, but even if perl is not installed the tables will compile 
*(they just wont look 100% like in the paper). To install perl please see your package manager, or here:
*https://www.perl.org/

********************************************************************************
******** DATA PREPARATION *********
/*
** 1) Generate variables in each country data (Note: for Nigeria, we create one cross-sectional and one panel data set):

	** a) for each country: 
foreach c in Ethiopia1516 Malawi1617 Niger14 Tanz1415 {
	do "$path/do/countries/`c'_Demographics.do"
	do "$path/do/countries/`c'_Inc.do"
	do "$path/do/countries/`c'_Occ.do"
}

foreach c in Nigeria1516 Nigeria12 Nigeria10{
	do "$path/do/countries/`c'_Demographics.do"
	do "$path/do/countries/`c'_Occ.do"
	do "$path/do/countries/`c'_Poverty.do"
}
do "$path/do/countries/Nigeria1516_Inc.do"
do "$path/do/countries/Nigeria1516_Occ_panel.do" //a consistent definition of social protection across the three panel waves.

	** b) for panel analysis:
		*combine all three waves of Nigeria:
	do "$path/do/countries/NGA panel/Nigeria_panel_2010-15.do"
		//uses "countries/NGA panel/CHEVAL_Occ_clean_NGA.do" "countries/NGA panel/CHEVAL_labels_nga.do" 

	**c) wealth index:
	do "$path/do/00_wealth_index_dataset"
		
*** 2) Compile into one main data set for cross-sectional analysis ***

do "$path/do/01_masterdata_append all" // uses "Inc_clean.do" and "Occ_clean.do"

do "$path/do/02_masterdata_clean and generate" // uses "labels.do"
*/
********************************************************************************
****** ANALYSIS *******

*1) Descriptive statistics:
do "$path/do/03_sample description.do"  // Replicates tables A2, A3 A4
// uses "data/CHEVAL_allcountries_clean"
// uses "informaldepth-sumstatsgr.do"

*2) cross-sectional analysis:
// uses "data/CHEVAL_allcountries_clean"
do "$path/do/04_analysis-urban"  // Produces Underlying Cross Sectional regressions (Appendix E) and plots (Fig 1, Fig 2, Fig B1, Fig B2)
do "$path/do/05_analysis-urban-wealth" // Replicates analysis looking for wealth channel (All output in Appendix C)

*3) panel analysis: 
// uses "data/NGA_panel"
do "$path/do/06_nga-panel-freeman-rep" //produces table 1 and Table B1
do "$path/do/07_nga-sumstats" // balance statistics Table 2
do "$path/do/08_nga-panel-stacked" //hypotheses tests in stacked DiD Tables 3, B2
do "$path/do/09_nga-panel-stacked-jobs" // hypotheses tested in stacked DiD by job cells (Appendix D)
do "$path/do/10_nga-panel-ri-stacked" //randomization inference (Underlying tables for Figure 3 and B3 )
do "$path/do/11-ri-plots" //randomization inference results plot Figure 3 and B3



