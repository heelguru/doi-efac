********************************************************************************
*********************** MALAWI 2016/2017: DEMOGRAPHICS ************************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Malawi 2016/2017 Fourth Integrated Household Survey

The resulting data set is "Malawi1617_demographics"

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl MALAWI	"$path\data\Malawi_IHS_1617"

gl DO   	    ${MALAWI}/do
gl raw data     ${MALAWI}/raw data 
gl clean data 	${MALAWI}/clean data
gl FIG 		    ${MALAWI}/graphs
gl TAB 		    ${MALAWI}/tables
 
********************************************************************************
cd "$MALAWI"


* Location
use "raw data/HH_MOD_A_FILT.dta", clear
rename hh_a02a TA
rename hh_a03 ea
gen rural = 0
replace rural = 1 if reside==2
rename hh_wgt hhweight
order case_id rural region district TA ea hhweight
keep case_id rural region district TA ea hhweight

save "clean data/Malawi1617_location.dta", replace

** Household Member Demographic information
use "raw data/HH_MOD_B.dta", clear
gen female = hh_b03-1
gen head = 0
replace head = 1 if hh_b04==1
gen female_head = 0
replace female_head = 1 if head==1 & female==1

/* Age Tiers
1 = 0-14
2 = 15-17
3 = 18-24
4 = 25-34
5 = 35-64
6 = 65+
*/
rename hh_b05a age
* One missing age, but parent of mother/father. Younger spouce is 40, therefore estimate age at 60 for missing.
replace age = 60 if age==.
gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth*, mv(0)

keep case_id PID age age_tier female head female_head youth youth_34
order case_id PID age age_tier female head female_head youth youth_34

* Bring in Education
joinby case_id PID using "raw data/HH_MOD_C.dta", unmatched(both) _merge(_merge)

gen school_read = 1 if hh_c05b==1
gen school_any = 1 if hh_c06==1
gen school_currently = 1 if hh_c13==1
gen school_complete = 1 if hh_c08>=8 & hh_c08~=.
gen school_secondary = 1 if hh_c08>=12 & hh_c08~=. 
gen school_level = hh_c08
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep case_id PID age age_tier female head female_head youth youth_34 school_*

joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)

* HH Size
by case_id, sort : egen float hhsize = count(case_id)

save "clean data/Malawi1617_member_demos.dta", replace

** Credit
use "Raw Data/HH_MOD_S1.dta", clear
gen credit = 1 if hh_s01==1
keep if credit==1
keep case_id credit
collapse (max) credit, by (case_id)

save "clean data/intermediate/Malawi1617_credit.dta", replace

** Land Holdings
use "Raw Data/AG_MOD_B1.dta", clear
joinby using "raw data/AG_MOD_B2.dta", unmatched(none)
gen land = 1 if ag_b203<5 | ag_b203==12 | ag_b203==13
gen hectares = ag_b104a2
replace hectares = . if hectares>10 & ag_b104a<10 & ag_b104b==1
replace hectares = ag_b104a if ag_b104b==1 & hectares==.
replace hectares = hectares * 0.404686
replace hectares = ag_b104a if ag_b104b==2 & hectares==.
replace hectares = ag_b104a/10000 if hectares==. & ag_b104b==3
keep if land==1
collapse (sum) hectares (max) land, by (case_id)
mvdecode hectares, mv(0)

save "clean data/intermediate/Malawi1617_temp_land.dta", replace
 
use "Raw Data/AG_MOD_I1.dta", clear
joinby using "raw data/AG_MOD_I2.dta", unmatched(none)
gen land = 1 if ag_i203<5 | ag_i203==12 | ag_i203==13
gen hectares = ag_i105a_1
replace hectares = ag_i105a if ag_i105b==1 & hectares==.
replace hectares = hectares * 0.404686
replace hectares = ag_i105a/10000 if hectares==. & ag_i105b==3
keep if land==1
collapse (sum) hectares (max) land, by (case_id)
mvdecode hectares, mv(0)

append using "clean data/intermediate/Malawi1617_temp_land.dta"
collapse (sum) hectares (max) land, by (case_id)
mvdecode hectares, mv(0)

drop land
rename hectares land_owned

save "clean data/intermediate/Malawi1617_land.dta", replace

* HH demographics
use "clean data/Malawi1617_member_demos.dta", clear
* Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
mvencode youth_share_*, mv(0)

collapse (sum) youth_share_* (max) female_head school_secondary school_level (first) rural region district TA ea hhweight hhsize , by (case_id )

replace youth_share_24 = youth_share_24/hhsize
replace youth_share_34 = youth_share_34/hhsize
rename school_secondary school_secondary_hh
rename school_level educ_max

joinby case_id using "clean data/intermediate/Malawi1617_credit.dta", unmatched(both) _merge(_merge)
mvencode credit, mv(0)
drop _merge

joinby case_id using "clean data/intermediate/Malawi1617_land.dta", unmatched(both) _merge(_merge)
mvencode land_owned, mv(0)
drop _merge

gen popweight = hhweight*hhsize

save "clean data/Malawi1617_demographics.dta", replace


** Build hhid & iid
use "clean data/Malawi1617_member_demos.dta", clear
keep case_id PID

gen hhid = case_id
tostring PID, g(iid)
replace iid = "0" +iid if PID<10
replace iid = hhid + iid 

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Malawi1617_iid.dta", replace
collapse (min) PID, by (case_id hhid)
drop PID
save "clean data/Malawi1617_hhid.dta", replace
