********************************************************************************
***************** NIGER 2014: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)


The dataset used for this is:
Niger 2014 General Household Survey-Panel Wave 3

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")

The resulting data set is "NIGER_incomeshares"

Stata Version 15.1

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGER	"$path\data\Niger_ECVMA_14"

gl DO   	    ${NIGER}/do
gl raw data   	${NIGER}/raw data 
gl clean data 	${NIGER}/clean data
gl FIG 		    ${NIGER}/graphs
gl TAB 		    ${NIGER}/tables
 
********************************************************************************
cd "$NIGER"

** Occupation by labor module (Separate labor Occupations to own observations)
** Main labor (worked during past 7 days)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q23 CIC
rename MS04Q25 months
rename MS04Q26 weekspm
rename MS04Q27 dayspw
rename MS04Q28 hourspd
rename MS04Q29 type
/*
types 1-6 are wage employment
type 7 is a "patron", which I interpret to be a local cheif
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q31 contract
rename MS04Q38 pension
rename MS04Q40 insurance
rename MS04Q32A ann_income
rename MS04Q32B inc_period
rename MS04Q35A ann_inkind_benefit
rename MS04Q35B inkind_period
rename MS04Q37A ann_food_benefit
rename MS04Q37B food_period

**adding ANY emp type performed in last 12 months:

keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 1
save "clean data/intermediate/Niger14_temp_labor_1.dta", replace

** Secondary labor(worked during past 7 days)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q49 CIC
rename MS04Q51 months
rename MS04Q51B weekspm
rename MS04Q52 dayspw
rename MS04Q53 hourspd
rename MS04Q54 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q56 contract
rename MS04Q63 pension
rename MS04Q65 insurance
rename MS04Q57A ann_income
rename MS04Q57B inc_period
rename MS04Q60A ann_inkind_benefit
rename MS04Q60B inkind_period
rename MS04Q62A ann_food_benefit
rename MS04Q62B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 2
save "clean data/intermediate/Niger14_temp_labor_2.dta", replace

** Main labor (worked during past 12 months)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q69 CIC
rename MS04Q71 months
rename MS04Q71B weekspm
rename MS04Q72 dayspw
rename MS04Q73 hourspd
rename MS04Q74 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q76 contract
rename MS04Q83 pension
rename MS04Q84 paidleave
rename MS04Q85 insurance
rename MS04Q77A ann_income
rename MS04Q77B inc_period
rename MS04Q80A ann_inkind_benefit
rename MS04Q80B inkind_period
rename MS04Q82A ann_food_benefit
rename MS04Q82B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period paidleave //(!)
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period paidleave //(!)
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 3
save "clean data/intermediate/Niger14_temp_labor_3.dta", replace

** Secondary labor (worked during past 12 months)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q89 CIC
rename MS04Q91 months
rename MS04Q91B weekspm
rename MS04Q92 dayspw
rename MS04Q93 hourspd
rename MS04Q94 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q96 contract
rename MS04Q103 pension
rename MS04Q105 insurance
rename MS04Q97A ann_income
rename MS04Q97B inc_period
rename MS04Q100A ann_inkind_benefit
rename MS04Q100B inkind_period
rename MS04Q102A ann_food_benefit
rename MS04Q102B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 4
save "clean data/intermediate/Niger14_temp_labor_4.dta", replace

use "clean data/intermediate/Niger14_temp_labor_1.dta", clear
append using "clean data/intermediate/Niger14_temp_labor_2.dta"
append using "clean data/intermediate/Niger14_temp_labor_3.dta"
append using "clean data/intermediate/Niger14_temp_labor_4.dta"

* Clear Errors in data
* Only 7 days in week (886/13,905)
replace dayspw=. if dayspw>7
**** Assume that reported hours per day above 16 hours per day are in error, so adjust to 16 hours per day. (affects calculations of 6 of 13,905 observations)
replace hourspd = . if hourspd>16
**** Use median of days per week by occupation type of those in the smallest geographic region with 10 observations of monthly labor
joinby using "clean data/Niger14_member_demos.dta", unmatched(none)
* Count observations with dayspw reported by geographic location.
by type CIC female rural region department town ea, sort : egen float count_1 = count(1) if dayspw~=.
by type CIC female rural region department town, sort : egen float count_2 = count(1) if dayspw~=.
by type CIC female rural region department, sort : egen float count_3 = count(1) if dayspw~=.
by type CIC female rural region, sort : egen float count_4 = count(1) if dayspw~=.
by type CIC female rural, sort : egen float count_5 = count(1) if dayspw~=.
* Calculate median of dayspw by geographic location - later used for predictive purposes.
by type CIC female rural region department town ea, sort : egen float temp_est_dayspw_1 = median(dayspw) if dayspw~=.
by type CIC female rural region department town, sort : egen float temp_est_dayspw_2 = median(dayspw) if dayspw~=.
by type CIC female rural region department, sort : egen float temp_est_dayspw_3 = median(dayspw) if dayspw~=.
by type CIC female rural region, sort : egen float temp_est_dayspw_4 = median(dayspw) if dayspw~=.
by type CIC female rural, sort : egen float temp_est_dayspw_5 = median(dayspw) if dayspw~=.
mvencode temp_*, mv(0)
by type CIC female rural region department town ea, sort : egen float est_dayspw_1 = max(temp_est_dayspw_1)
by type CIC female rural region department town, sort : egen float est_dayspw_2 = max(temp_est_dayspw_2)
by type CIC female rural region department, sort : egen float est_dayspw_3 = max(temp_est_dayspw_3)
by type CIC female rural region, sort : egen float est_dayspw_4 = max(temp_est_dayspw_4)
by type CIC female rural, sort : egen float est_dayspw_5 = max(temp_est_dayspw_5)
drop temp*
* Build dayspw prediction variable if at least 10 observations of reported dayspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_dayspw = est_dayspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_4 if track==4
replace track = 5 if est_dayspw==.
replace est_dayspw = est_dayspw_5 if track==5
drop est_dayspw_* count_* track
**** Replace missing value of dayspw (813 changes)
replace dayspw = est_dayspw if dayspw==.
drop est_*
* If cannot use other observations to estimate, assume 7 days, as unrealistic days per week were all above 7 (Changes: 53)
replace dayspw = 7 if dayspw==.
save "clean data/intermediate/Niger14_temp_labor.dta", replace


** Wage employment
use "clean data/intermediate/Niger14_temp_labor.dta", clear
keep if type<7

* Casual vs Formal Wage 
gen formal = 0
replace formal = 1 if inc_period>3 & inc_period<.
replace formal = 1 if inkind_period>3 & inkind_period<.
replace formal = 1 if contract==1
replace formal = 1 if pension==1
replace formal = 1 if insurance==1
replace formal = 1 if paidleave==1 	

gen formalcon = 0 //added definition of "formality" due to contract structure
replace formalcon = 1 if contract==1
replace formalcon = 1 if paidleave==1 	//choice to include receipt of  paid leave as "contract"

gen formalsp = 0 //added definition of "formality" due to SP
replace formalsp = 1 if pension==1
replace formalsp = 1 if insurance==1

* Clean up income measures with missing units
replace ann_income = . if inc_period==9
replace inc_period = . if inc_period==9
replace ann_inkind_benefit = . if inkind_period==9
replace inkind_period = . if inkind_period==9
replace ann_food_benefit = . if food_period==9
replace food_period = . if food_period==9
* Remove missing values
replace ann_income = 0 if ann_income>6000000
replace ann_inkind_benefit = 0 if ann_inkind_benefit>6000000
replace ann_food_benefit = 0 if ann_food_benefit>6000000

* Convert payment into average payment per day
* Payment in cash
* Duration in days
**** Need estimated days per week
gen cash_inc = ((ann_income*dayspw)/7)*(weekspm/4.3)*(months/12) if inc_period==1
* Duration in weeks
replace cash_inc = ann_income*(1/7)*(weekspm/4.3)*(months/12) if inc_period==2
* Duration in months
replace cash_inc = ann_income*(1/30.4)*(months/12) if inc_period==3
* Duration in years
replace cash_inc = ann_income/365 if inc_period==4
* Payment in kind
* Duration in days
gen inkind_inc = ((ann_inkind_benefit*dayspw)/7)*(weekspm/4.3)*(months/12) if inkind_period==1
* Duration in weeks
replace inkind_inc = ann_inkind_benefit*(1/7)*(weekspm/4.3)*(months/12) if inkind_period==2
* Duration in months
replace inkind_inc = ann_inkind_benefit*(1/30.4)*(months/12) if inkind_period==3
* Duration in years
replace inkind_inc = ann_inkind_benefit/365 if inkind_period==4
* Payment in food
* Duration in days
gen food_inc = ((ann_food_benefit*dayspw)/7)*(weekspm/4.3)*(months/12) if food_period==1
* Duration in weeks
replace food_inc = ann_food_benefit*(1/7)*(weekspm/4.3)*(months/12) if food_period==2
* Duration in months
replace food_inc = ann_food_benefit*(1/30.4)*(months/12) if food_period==3
* Duration in years
replace food_inc = ann_food_benefit/365 if food_period==4

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + cash_inc if cash_inc~=.
replace wage_income = wage_income + inkind_inc if inkind_inc~=.
replace wage_income = wage_income + food_inc if food_inc~=.
drop if wage_income==0

* Bring in information for ISIC Groupings
joinby using "clean data/Niger14_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
/// for Niger 2014 the calculations are as follows
///   Rural -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
///   Urban -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
*******************************************************************************************
replace AFS_share = 0.7252143 if AFS_share==. & rural==1
replace AFS_share = 0.567008 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen wage_i_f`f'_c`c' = 0
	gen wage_i_fcon`f'_c`c' = 0 //(!)
	gen wage_i_fsp`f'_c`c' = 0 //(!)
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
	replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f' //(!)
	replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f' //(!)
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'
	*(!)
	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	*(!)
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}
*

g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0)

** primary or secondary occ: 
g job1=(labor==1 | labor==3)
g job2=(labor==2 | labor==4)

ren insurance health_insurance	

*correct codes for dummies:
recode health_insurance 2=0
recode pension 2=0

* Keep relevant variables
order GRAPPE MENAGE EXTENSION CIC ISIC_group wage_income wage_i_* job1 job2 informal* formal* pension health_insurance 
keep GRAPPE MENAGE EXTENSION CIC ISIC_group wage_income wage_i_* job1 job2 informal* formal* pension health_insurance

* Collapse to household level
collapse (sum) wage_income wage_i_* job1 job2 Ninformal=informal N_pension=pension N_health_insurance=health_insurance (mean) informal* formal* sh_pension=pension sh_health_insurance=health_insurance , by (GRAPPE MENAGE EXTENSION)


**labels
foreach v in pension health_insurance {
la var sh_`v' "Share of HHmemb with `v'"
la var N_`v' "N HHmemb with `v'"
}
la var job1 		"N HHmemb with wagejob(primary)"
la var job2 		"N HHmemb with wagejob(secondary)"
la var informal 	"Informal sh of wagework(no contract,SP)"
la var Ninformal	"N HHmemb with informal wagework"
la var formal		"Formal sh of wagework(any contract,SP)"
la var informalcon 	"Informal(no contract)"
la var formalcon  	"Formal sh of wagework(contract)"
la var informalsp 	"Informal sh of wagework(SP:insurance/pension)"
la var formalsp 	"Formal sh of wagework(SP:insurance/pension)"

global lab1 "OwnFarming"	
global lab2 "Farm Labor"	
global lab3 "Food&Ag Manufacturing w/in AFS"
global lab4 "FoodPrep"	
global lab5 "FoodSystem Marketing&Transport"
global lab6 "AFS Unclassified"	
global lab7 "Non-AFS Manufacturing"
global lab8 "Non-AFS Industry"	
global lab9 "Non-AFS Marketing&Transport"
global lab10 "Non-AFS Other"
global lab11 "Non-AFS PublicService"
global lab12 "Non-AFS Unclassifed"

forv f=1/12 {
la var wage_i_f0_c`f'    "Informal w from ${lab`f'}"
la var wage_i_fcon0_c`f' "Informal w from ${lab`f'}(contract)"
la var wage_i_fsp0_c`f'  "Informal w from ${lab`f'}(SP)"
la var wage_i_f1_c`f'    "Formal w from ${lab`f'}"
la var wage_i_fcon1_c`f' "Formal w from ${lab`f'}(contract)"
la var wage_i_fsp1_c`f'  "Formal w from ${lab`f'}(SP)"
}


save "clean data/intermediate/Niger14_wage_incomeCHEVAL.dta", replace


** Occupations in Non-Farm Own Enterprises
use "raw data/ECVMA2_MS05BP1.dta", clear
rename MS05Q25 CIC
rename MS05Q65 months
rename MS05Q67DA PID
rename MS05Q54 cost_1
rename MS05Q55 cost_2
rename MS05Q56 cost_3
rename MS05Q57 cost_4
rename MS05Q62 cost_5
rename MS05Q63 cost_6
rename MS05Q58 revenue_1
rename MS05Q59 revenue_2
rename MS05Q60 revenue_3

replace months = . if months==99

**** Use median of months by occupation of those in the smallest geographic region with 10 observations of monthly labor
joinby using "clean data/Niger14_location.dta", unmatched(none)
* Count observations with months reported by geographic location.
by CIC rural region department town ea, sort : egen float count_1 = count(1) if months~=.
by CIC rural region department town, sort : egen float count_2 = count(1) if months~=.
by CIC rural region department, sort : egen float count_3 = count(1) if months~=.
by CIC rural region, sort : egen float count_4 = count(1) if months~=.
by CIC rural, sort : egen float count_5 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by CIC rural region department town ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by CIC rural region department town, sort : egen float temp_est_months_2 = median(months) if months~=.
by CIC rural region department, sort : egen float temp_est_months_3 = median(months) if months~=.
by CIC rural region, sort : egen float temp_est_months_4 = median(months) if months~=.
by CIC rural, sort : egen float temp_est_months_5 = median(months) if months~=.
mvencode temp_*, mv(0)
by CIC rural region department town ea, sort : egen float est_months_1 = max(temp_est_months_1)
by CIC rural region department town, sort : egen float est_months_2 = max(temp_est_months_2)
by CIC rural region department, sort : egen float est_months_3 = max(temp_est_months_3)
by CIC rural region, sort : egen float est_months_4 = max(temp_est_months_4)
by CIC rural, sort : egen float est_months_5 = max(temp_est_months_5)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if est_months==.
replace est_months = est_months_5 if track==5
drop est_months_* count_* track
**** Replace missing value of months (13 changes)
replace months = est_months if months==.
replace months = 1 if months==.
drop est_*

* Remove values that are in place of missing values
forvalues r=1(1)3 {
	replace revenue_`r' = . if revenue_`r'==9999999999
}
*
forvalues c=1(1)6 {
	replace cost_`c' = . if cost_`c'==9999999999
}
*

* Calculate profit
gen nfe_mo_gross_inc = 0
gen nfe_mo_costs = 0
forvalues r=1(1)3 {
	replace nfe_mo_gross_inc = nfe_mo_gross_inc + revenue_`r' if revenue_`r'~=.
}
*
forvalues c=1(1)6 {
	replace nfe_mo_costs = nfe_mo_costs + cost_`c' if cost_`c'~=.
}
*
gen nfe_mo_net_inc = nfe_mo_gross_inc - nfe_mo_costs

gen nfe_ann_gross_inc = nfe_mo_gross_inc*months
gen nfe_gross_inc = nfe_ann_gross_inc/365
gen nfe_ann_costs = nfe_mo_costs*months
gen nfe_costs = nfe_ann_costs/365
gen nfe_ann_net_inc = nfe_mo_net_inc*months
gen nfe_net_inc = nfe_ann_net_inc/365
drop if nfe_net_inc==.

keep GRAPPE MENAGE EXTENSION CIC nfe_net_inc nfe_gross_inc nfe_costs
order GRAPPE MENAGE EXTENSION CIC nfe_net_inc nfe_gross_inc nfe_costs

* Bring in information for ISIC Groupings
joinby using "clean data/Niger14_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby GRAPPE MENAGE EXTENSION using "clean data/Niger14_demographics.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
/// for Niger 2014 the calculations are as follows
///   Rural -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
///   Urban -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
*******************************************************************************************
replace AFS_share = 0.7252143 if AFS_share==. & rural==1
replace AFS_share = 0.567008 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612

* Keep relevant variables
order GRAPPE MENAGE EXTENSION CIC ISIC_group nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*
keep GRAPPE MENAGE EXTENSION CIC ISIC_group nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*, by (GRAPPE MENAGE EXTENSION)

* Drop 58 observations with zero earning
drop if nfe_net_inc==0

save "clean data/intermediate/Niger14_nfe_inc.dta", replace

** Own Farm Employment
* Crop Production
* raw data/ECVMA2_AS2E1P2.dta provides data by plot, I used total crop production
use "raw data/ECVMA2_AS2E2P2.dta", clear
rename AS02EQ110A crop_name
rename AS02EQ110B crop_code
rename AS02EQ110E harvest_qty
rename AS02EQ12C sales_qty
rename AS02EQ13 value_sold
rename AS02EQ19C value_processed_sold
keep GRAPPE MENAGE EXTENSION crop_name crop_code harvest_qty sales_qty value_sold value_processed_sold
drop if harvest_qty==.
save "clean data/intermediate/Niger14_temp_harvest.dta", replace

* Price estimates to be built by sales data
use "clean data/intermediate/Niger14_temp_harvest.dta", clear
drop if sales_qty==0 | sales_qty==. | sales_qty==9999 | value_sold==0 | value_sold==. | value_sold==9999999

* revealed prices
gen revealed_price = value_sold / sales_qty

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Niger14_location.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by crop_code rural region department town ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code rural region department town, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code rural region department, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code rural region, sort : egen float count_4 = count(1) if revealed_price~=.
by crop_code rural, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code rural region department town ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by crop_code rural region department town, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by crop_code rural region department, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by crop_code rural region, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by crop_code rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by crop_code, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.

mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code rural region department town ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code rural region department town, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code rural region department, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

keep rural region department town ea crop_name crop_code predict_price

collapse (median) predict_price, by (rural region department town ea crop_code)

save "clean data/intermediate/Niger14_crop_price.dta", replace

* Crop Sales
use "clean data/intermediate/Niger14_temp_harvest.dta", clear
keep GRAPPE MENAGE EXTENSION crop_name crop_code sales_qty
drop if sales_qty==0 | sales_qty==. | sales_qty==9999
joinby using "clean data/Niger14_location.dta", unmatched(none)

* Bring in Prices
joinby using "clean data/intermediate/Niger14_crop_price.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Calculate Crop Sales Value
gen crop_sales_value = sales_qty * predict_price

* Collapse to Household Level
collapse (sum) crop_sales_value, by (GRAPPE MENAGE EXTENSION)

replace crop_sales_value = crop_sales_value/365

save "clean data/intermediate/Niger14_crop_sales_value.dta", replace

* Crop Harvest Value
use "clean data/intermediate/Niger14_temp_harvest.dta", clear
keep GRAPPE MENAGE EXTENSION crop_name crop_code harvest_qty
joinby using "clean data/Niger14_location.dta", unmatched(none)
* Consider unknown harvest quantity to be equal to zero
replace harvest_qty = 0 if harvest_qty==9999

joinby using "clean data/intermediate/Niger14_crop_price.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by crop_code rural region department town ea, sort : egen float count_1 = count(1) if predict_price~=.
by crop_code rural region department town, sort : egen float count_2 = count(1) if predict_price~=.
by crop_code rural region department, sort : egen float count_3 = count(1) if predict_price~=.
by crop_code rural region, sort : egen float count_4 = count(1) if predict_price~=.
by crop_code rural, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code rural region department town ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by crop_code rural region department town, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by crop_code rural region department, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by crop_code rural region, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by crop_code rural, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by crop_code, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code rural region department town ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code rural region department town, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code rural region department, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*
* 20/4799 do not have predicted prices, so consider those prices to be zero
replace predict_price = 0 if predict_price==.

* Calculate Crop Sales Value
gen crop_harvest_value = harvest_qty * predict_price

collapse (sum) crop_harvest_value , by (GRAPPE MENAGE EXTENSION)

replace crop_harvest_value = crop_harvest_value/365

save "clean data/intermediate/Niger14_crop_harvest.dta", replace

* Perenial perm Production
use "raw data/ECVMA2_AS05P2.dta", clear
rename AS05Q02 perm_code
rename AS05Q03 perm_name
rename AS05Q05 months
rename AS05Q06 mo_harvest_qty
rename AS05Q07 mo_harvest_unit
rename AS05Q10A sales_qty
rename AS05Q10B sales_unit
rename AS05Q11 value_sales
keep if months~=.
keep GRAPPE MENAGE EXTENSION perm_name perm_code months mo_harvest_qty mo_harvest_unit sales_qty sales_unit value_sales
save "clean data/intermediate/Niger14_temp_harvest_perm.dta", replace

* Price estimates to be built by sales data
use "clean data/intermediate/Niger14_temp_harvest_perm.dta", clear
drop if sales_qty==0 | sales_qty==. | value_sales==0 | value_sales==.

* revealed prices
gen revealed_price = value_sales / sales_qty

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Niger14_location.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by perm_code sales_unit rural region department town ea, sort : egen float count_1 = count(1) if revealed_price~=.
by perm_code sales_unit rural region department town, sort : egen float count_2 = count(1) if revealed_price~=.
by perm_code sales_unit rural region department, sort : egen float count_3 = count(1) if revealed_price~=.
by perm_code sales_unit rural region, sort : egen float count_4 = count(1) if revealed_price~=.
by perm_code sales_unit rural, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by perm_code sales_unit rural region department town ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by perm_code sales_unit rural region department town, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by perm_code sales_unit rural region department, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by perm_code sales_unit rural region, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by perm_code sales_unit rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by perm_code sales_unit, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.

mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by perm_code sales_unit rural region department town ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by perm_code sales_unit rural region department town, sort : egen float predict_price_2 = max(temp_predict_price_2)
by perm_code sales_unit rural region department, sort : egen float predict_price_3 = max(temp_predict_price_3)
by perm_code sales_unit rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by perm_code sales_unit rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by perm_code sales_unit, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

keep rural region department town ea perm_name perm_code sales_unit predict_price

collapse (median) predict_price, by (rural region department town ea perm_code sales_unit)

rename sales_unit unit

save "clean data/intermediate/Niger14_perm_price.dta", replace

* perm Sales
use "clean data/intermediate/Niger14_temp_harvest_perm.dta", clear
keep GRAPPE MENAGE EXTENSION perm_name perm_code sales_qty sales_unit
rename sales_unit unit
drop if sales_qty==0 | sales_qty==.
joinby using "clean data/Niger14_location.dta", unmatched(none)

* Bring in Prices
joinby using "clean data/intermediate/Niger14_perm_price.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by perm_code unit rural region department town ea, sort : egen float count_1 = count(1) if predict_price~=.
by perm_code unit rural region department town, sort : egen float count_2 = count(1) if predict_price~=.
by perm_code unit rural region department, sort : egen float count_3 = count(1) if predict_price~=.
by perm_code unit rural region, sort : egen float count_4 = count(1) if predict_price~=.
by perm_code unit rural, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by perm_code unit rural region department town ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by perm_code unit rural region department town, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by perm_code unit rural region department, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by perm_code unit rural region, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by perm_code unit rural, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by perm_code unit, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by perm_code unit rural region department town ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by perm_code unit rural region department town, sort : egen float predict_price_2 = max(temp_predict_price_2)
by perm_code unit rural region department, sort : egen float predict_price_3 = max(temp_predict_price_3)
by perm_code unit rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by perm_code unit rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by perm_code unit, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*

* Calculate perm Sales Value
gen perm_value_sales = sales_qty * predict_price

* Collapse to Household Level
collapse (sum) perm_value_sales, by (GRAPPE MENAGE EXTENSION)

replace perm_value_sales = perm_value_sales/365

save "clean data/intermediate/Niger14_perm_value_sales.dta", replace

* perm Harvest Value
use "clean data/intermediate/Niger14_temp_harvest_perm.dta", clear
drop if months==0
rename mo_harvest_unit unit
gen harvest_qty = months*mo_harvest_qty
keep GRAPPE MENAGE EXTENSION perm_name perm_code harvest_qty unit

joinby using "clean data/Niger14_location.dta", unmatched(none)

joinby using "clean data/intermediate/Niger14_perm_price.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace predict_price = 0 if unit==99

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by perm_code unit rural region department town ea, sort : egen float count_1 = count(1) if predict_price~=.
by perm_code unit rural region department town, sort : egen float count_2 = count(1) if predict_price~=.
by perm_code unit rural region department, sort : egen float count_3 = count(1) if predict_price~=.
by perm_code unit rural region, sort : egen float count_4 = count(1) if predict_price~=.
by perm_code unit rural, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by perm_code unit rural region department town ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by perm_code unit rural region department town, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by perm_code unit rural region department, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by perm_code unit rural region, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by perm_code unit rural, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by perm_code unit, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvdecode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by perm_code unit rural region department town ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by perm_code unit rural region department town, sort : egen float predict_price_2 = max(temp_predict_price_2)
by perm_code unit rural region department, sort : egen float predict_price_3 = max(temp_predict_price_3)
by perm_code unit rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by perm_code unit rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by perm_code unit, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*
* 18/113 do not have predicted prices, so consider those prices to be zero
replace predict_price = 0 if predict_price==.

* Calculate perm Sales Value
gen perm_harvest_value = harvest_qty * predict_price

collapse (sum) perm_harvest_value , by (GRAPPE MENAGE EXTENSION)

replace perm_harvest_value = perm_harvest_value/365

save "clean data/intermediate/Niger14_perm_harvest.dta", replace

******NON EMPLOYMENT INCOME
** Non-Employment Revenue
use "raw data/ECVMA2_MS08P1.dta", clear
gen non_emp_inc = 0
replace non_emp_inc = non_emp_inc + MS08Q03AB if MS08Q03AB~=.
replace non_emp_inc = non_emp_inc + MS08Q03BB if MS08Q03BB~=.
replace non_emp_inc = non_emp_inc + MS08Q03CB if MS08Q03CB~=.

* Collapse to household level
collapse (sum) non_emp_inc, by (GRAPPE MENAGE EXTENSION)
drop if non_emp_inc==0

replace non_emp_inc = non_emp_inc/365

save "clean data/intermediate/Niger14_non_emp_inc.dta", replace

** Transfers Received
use "raw data/ECVMA2_MS09AP1.dta", clear
rename MS09Q12 transfers

* Collapse to household level
collapse (sum) transfers, by (GRAPPE MENAGE EXTENSION)
drop if transfers==0

replace transfers = transfers/365

save "clean data/intermediate/Niger14_transfers.dta", replace

** Assistance
use "raw data/ECVMA2_MS09CP1.dta", clear
rename MS09Q22 assistance
replace assistance = 0 if assistance==99999999

* Collapse to household level
collapse (sum) assistance, by (GRAPPE MENAGE EXTENSION)
drop if assistance==0

replace assistance = assistance/365

save "clean data/intermediate/Niger14_assistance.dta", replace


*** Livestock
* General Livestock
use "Raw Data/ECVMA2_AS4AP2.dta", clear
rename AS4AQ0A livestock_code
rename AS4AQ47 livestock_givenaway
rename AS4AQ51 livestock_sold
rename AS4AQ55 livestock_sold_price
rename AS4AQ46A livestock_slaughtered
keep GRAPPE MENAGE EXTENSION livestock*
mvdecode livestock_*, mv(0)
drop if livestock_givenaway==. & livestock_sold==. & livestock_sold_price==. & livestock_slaughtered==.

* estimate value of livestock income with estimated prices from sales
* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Niger14_location.dta", unmatched(none)
* Count observations with livestock_sold_price reported by geographic location.
by livestock_code rural region department town ea, sort : egen float count_1 = count(1) if livestock_sold_price~=.
by livestock_code rural region department town, sort : egen float count_2 = count(1) if livestock_sold_price~=.
by livestock_code rural region department, sort : egen float count_3 = count(1) if livestock_sold_price~=.
by livestock_code rural region, sort : egen float count_4 = count(1) if livestock_sold_price~=.
by livestock_code rural, sort : egen float count_5 = count(1) if livestock_sold_price~=.
* Calculate median of livestock_sold_price by geographic location - later used for predictive purposes.
by livestock_code rural region department town ea, sort : egen float temp_est_price_1 = median(livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region department town, sort : egen float temp_est_price_2 = median(livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region department, sort : egen float temp_est_price_3 = median(livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region, sort : egen float temp_est_price_4 = median(livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural, sort : egen float temp_est_price_5 = median(livestock_sold_price) if livestock_sold_price~=.
mvencode temp_*, mv(0)
by livestock_code rural region department town ea, sort : egen float est_price_1 = max(temp_est_price_1)
by livestock_code rural region department town, sort : egen float est_price_2 = max(temp_est_price_2)
by livestock_code rural region department, sort : egen float est_price_3 = max(temp_est_price_3)
by livestock_code rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by livestock_code rural, sort : egen float est_price_5 = max(temp_est_price_5)
drop temp*
* Build livestock_sold_price prediction variable if at least 10 observations of reported livestock_sold_price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if est_price==.
replace est_price = est_price_5 if track==5
drop est_price_* count_* track
mvdecode livestock_* est_price, mv(0)
mvencode livestock_* est_price, mv(0)
gen livestock_to_income = livestock_givenaway+livestock_sold+livestock_slaughtered
gen livestock_income_yr = livestock_to_income*est_price
****
**** Do not know the number of slaughtered that were sold, but will capture meat from slaughtered later
gen livestock_sold_yr = livestock_sold*est_price

keep GRAPPE MENAGE EXTENSION livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (GRAPPE MENAGE EXTENSION)
drop if livestock_income==0 & livestock_sold==0

save "clean data/intermediate/Niger14_livestock_cattle.dta", replace

* General meat
use "Raw Data/ECVMA2_AS4GP2.dta", clear
rename AS4GQ0 meat_code
* Too few observations to estimate reliable prices that improve upon stated values of meat and skins
rename AS4GQ8 meat_sold_value
replace meat_sold_value = . if meat_sold_value==9999999
rename AS4GQ23 skins_sold_value
keep GRAPPE MENAGE EXTENSION meat* skins*
mvdecode meat* skins*, mv(0)
mvencode meat* skins*, mv(0)

gen meat_hide_sold_yr = meat_sold_value*12 + skins_sold_value

keep GRAPPE MENAGE EXTENSION meat_hide_sold_yr

gen meat_hide_sold = meat_hide_sold_yr/365

collapse (sum) meat_*, by (GRAPPE MENAGE EXTENSION)
drop if meat_hide_sold==0

save "clean data/intermediate/Niger14_meat.dta", replace

* Milk
use "Raw Data/ECVMA2_AS4HP2.dta", clear
rename AS4H1QA milk_code
rename AS4H1Q6 milk_num_animals
rename AS4H1Q5 milk_months_prod
replace milk_months_prod = . if milk_months_prod==99
rename AS4H1Q7D milk_qty_prod
replace milk_qty_prod = . if milk_qty_prod==9999
rename AS4H1Q13 milk_qty_sold
rename AS4H1Q14 milk_value_sold
rename AS4H1Q23 milk_proc_qty_sold
rename AS4H1Q25 milk_value_curd_sold
rename AS4H1Q28 milk_value_butter_sold
rename AS4H1Q32 milk_value_cheese_sold
keep GRAPPE MENAGE EXTENSION milk*
mvdecode milk*, mv(0)
drop if milk_qty_prod==. & milk_qty_sold==. & milk_proc_qty_sold==. & milk_value_curd_sold==. & milk_value_butter_sold==. & milk_value_cheese_sold==.

* estimate value of milk income with estimated prices from sales 
gen milk_sold_price = milk_value_sold / milk_qty_sold
* No quantities sold reported nor input quantities collected

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Niger14_location.dta", unmatched(none)
* Count observations with milk_sold_price reported by geographic location.
by milk_code rural region department town ea, sort : egen float count_1 = count(1) if milk_sold_price~=.
by milk_code rural region department town, sort : egen float count_2 = count(1) if milk_sold_price~=.
by milk_code rural region department, sort : egen float count_3 = count(1) if milk_sold_price~=.
by milk_code rural region, sort : egen float count_4 = count(1) if milk_sold_price~=.
by milk_code rural, sort : egen float count_5 = count(1) if milk_sold_price~=.
* Calculate median of milk_sold_price by geographic location - later used for predictive purposes.
by milk_code rural region department town ea, sort : egen float temp_est_price_1 = median(milk_sold_price) if milk_sold_price~=.
by milk_code rural region department town, sort : egen float temp_est_price_2 = median(milk_sold_price) if milk_sold_price~=.
by milk_code rural region department, sort : egen float temp_est_price_3 = median(milk_sold_price) if milk_sold_price~=.
by milk_code rural region, sort : egen float temp_est_price_4 = median(milk_sold_price) if milk_sold_price~=.
by milk_code rural, sort : egen float temp_est_price_5 = median(milk_sold_price) if milk_sold_price~=.
mvencode temp_*, mv(0)
by milk_code rural region department town ea, sort : egen float est_price_1 = max(temp_est_price_1)
by milk_code rural region department town, sort : egen float est_price_2 = max(temp_est_price_2)
by milk_code rural region department, sort : egen float est_price_3 = max(temp_est_price_3)
by milk_code rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by milk_code rural, sort : egen float est_price_5 = max(temp_est_price_5)
drop temp*
* Build milk_sold_price prediction variable if at least 10 observations of reported milk_sold_price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if est_price==.
replace est_price = est_price_5 if track==5
drop est_price_* count_* track
mvdecode milk_* est_price, mv(0)
mvencode milk_* est_price, mv(0)

gen milk_income_yr = milk_num_animals*milk_qty_prod*milk_months_prod*30.4*est_price + milk_value_curd_sold + milk_value_butter_sold + milk_value_cheese_sold
gen milk_sold_yr = milk_qty_sold *milk_months_prod*30.4*est_price + milk_value_curd_sold + milk_value_butter_sold + milk_value_cheese_sold

keep GRAPPE MENAGE EXTENSION milk_income_yr milk_sold_yr

gen milk_income = milk_income_yr/365
gen milk_sold = milk_sold_yr/365

collapse (sum) milk_*, by (GRAPPE MENAGE EXTENSION)
drop if milk_income==0 & milk_sold==0

save "clean data/intermediate/Niger14_milk.dta", replace

* eggs
use "Raw Data/ECVMA2_AS4IP2.dta", clear
rename AS4IQ1 eggs_code
rename AS4IQ3B eggs_months_prod
rename AS4IQ4 eggs_qty_prod
rename AS4IQ5 eggs_qty_sold
rename AS4IQ6 eggs_value_sold
keep GRAPPE MENAGE EXTENSION eggs*
mvdecode eggs*, mv(0)
drop if eggs_qty_prod==. & eggs_qty_sold==. & eggs_value_sold==.

* estimate value of eggs income with estimated prices from sales 
gen eggs_sold_price = eggs_value_sold / eggs_qty_sold
* Drop unrealistic prices
replace eggs_sold_price = . if eggs_sold_price>1000

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Niger14_location.dta", unmatched(none)
* Count observations with eggs_sold_price reported by geographic location.
by eggs_code rural region department town ea, sort : egen float count_1 = count(1) if eggs_sold_price~=.
by eggs_code rural region department town, sort : egen float count_2 = count(1) if eggs_sold_price~=.
by eggs_code rural region department, sort : egen float count_3 = count(1) if eggs_sold_price~=.
by eggs_code rural region, sort : egen float count_4 = count(1) if eggs_sold_price~=.
by eggs_code rural, sort : egen float count_5 = count(1) if eggs_sold_price~=.
* Calculate median of eggs_sold_price by geographic location - later used for predictive purposes.
by eggs_code rural region department town ea, sort : egen float temp_est_price_1 = median(eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region department town, sort : egen float temp_est_price_2 = median(eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region department, sort : egen float temp_est_price_3 = median(eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region, sort : egen float temp_est_price_4 = median(eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural, sort : egen float temp_est_price_5 = median(eggs_sold_price) if eggs_sold_price~=.
mvencode temp_*, mv(0)
by eggs_code rural region department town ea, sort : egen float est_price_1 = max(temp_est_price_1)
by eggs_code rural region department town, sort : egen float est_price_2 = max(temp_est_price_2)
by eggs_code rural region department, sort : egen float est_price_3 = max(temp_est_price_3)
by eggs_code rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by eggs_code rural, sort : egen float est_price_5 = max(temp_est_price_5)
drop temp*
* Build eggs_sold_price prediction variable if at least 10 observations of reported eggs_sold_price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if est_price==.
replace est_price = est_price_5 if track==5
drop est_price_* count_* track
mvdecode eggs_* est_price, mv(0)
mvencode eggs_* est_price, mv(0)

gen eggs_income_yr = eggs_qty_prod*eggs_months_prod*est_price
gen eggs_sold_yr = eggs_qty_sold*eggs_months_prod*est_price

keep GRAPPE MENAGE EXTENSION eggs_income_yr eggs_sold_yr

gen eggs_income = eggs_income_yr/365
gen eggs_sold = eggs_sold_yr/365

collapse (sum) eggs_*, by (GRAPPE MENAGE EXTENSION)
drop if eggs_income==0 & eggs_sold==0

save "clean data/intermediate/Niger14_eggs.dta", replace

* Livestock use
use "Raw Data/ECVMA2_AS4JP2.dta", clear
rename AS4JQ5 livestock_use_transportation
rename AS4JQ8 livestock_use_fieldwork
keep GRAPPE MENAGE EXTENSION livestock_use*
mvdecode livestock_use*, mv(0)
mvencode livestock_use*, mv(0)

gen livestock_use_nfe_yr = livestock_use_transportation+livestock_use_fieldwork

collapse (sum) livestock_use_nfe_yr, by (GRAPPE MENAGE EXTENSION)

gen livestock_use_nfe = livestock_use_nfe_yr/365

drop if livestock_use_nfe==0

save "clean data/intermediate/Niger14_livestock_use.dta", replace

* Livestock use
use "Raw Data/ECVMA2_AS4KLP2.dta", clear
rename AS4KQ6 livestock_other_manure
rename AS4LQ4 livestock_other_shepherd
keep GRAPPE MENAGE EXTENSION livestock_other*
mvdecode livestock_other*, mv(0)
mvencode livestock_other*, mv(0)

gen livestock_other_nfe_yr = livestock_other_manure+livestock_other_shepherd

collapse (sum) livestock_other_nfe_yr, by (GRAPPE MENAGE EXTENSION)

gen livestock_other_nfe = livestock_other_nfe_yr/365

drop if livestock_other_nfe==0

save "clean data/intermediate/Niger14_livestock_other.dta", replace

* Costs
* Seeds & Fertilizer
use "raw data/ECVMA2_AS02CP1.dta", clear
rename AS02CQ08C cost_seeds_fert
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_seeds_fert, by (GRAPPE MENAGE EXTENSION)
drop if cost_seeds_fert==0
save "clean data/intermediate/Niger14_costs_seeds_fert_1.dta", replace
use "raw data/ECVMA2_AS05P2.dta", clear
rename AS05Q15 cost_seeds_fert
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_seeds_fert, by (GRAPPE MENAGE EXTENSION)
drop if cost_seeds_fert==0
append using "clean data/intermediate/Niger14_costs_seeds_fert_1.dta"
collapse (sum) cost_seeds_fert, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_costs_seeds_fert.dta", replace

* Hired Crop Labor
use "raw data/ECVMA2_AS05P2.dta", clear
rename AS05Q16 cost_crop_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_crop_labor, by (GRAPPE MENAGE EXTENSION)
drop if cost_crop_labor==0
save "clean data/intermediate/Niger14_costs_crop_labor_1.dta", replace
use "raw data/ECVMA2_AS2AP1.dta", clear
mvdecode AS02AQ23E AS02AQ24E, mv(0)
mvencode AS02AQ23E AS02AQ24E, mv(0)
gen cost_crop_labor = AS02AQ23E + AS02AQ24E
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_crop_labor, by (GRAPPE MENAGE EXTENSION)
drop if cost_crop_labor==0
save "clean data/intermediate/Niger14_costs_crop_labor_2.dta", replace
use "raw data/ECVMA2_AS2AP2.dta", clear
mvdecode AS02AQ34E AS02AQ35E AS02AQ42E AS02AQ43E, mv(0)
mvencode AS02AQ34E AS02AQ35E AS02AQ42E AS02AQ43E, mv(0)
gen cost_crop_labor = AS02AQ34E + AS02AQ35E + AS02AQ42E + AS02AQ43E
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_crop_labor, by (GRAPPE MENAGE EXTENSION)
drop if cost_crop_labor==0
append using "clean data/intermediate/Niger14_costs_crop_labor_1.dta"
append using "clean data/intermediate/Niger14_costs_crop_labor_2.dta"
collapse (sum) cost_crop_labor, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_costs_crop_labor.dta", replace

* Hired Animal Labor & Animal Feed
use "raw data/ECVMA2_AS4FP2.dta", clear
rename AS4FQ7 cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_animal_labor==0
save "clean data/intermediate/Niger14_costs_animal_labor.dta", replace

* Animal Feed
use "raw data/ECVMA2_AS4CP2.dta", clear
mvdecode AS4CQ15 AS4CQ21, mv(0)
mvencode AS4CQ15 AS4CQ21, mv(0)
gen cost_animal_feed = AS4CQ15 + AS4CQ21
collapse (sum) cost_animal_feed, by (GRAPPE MENAGE EXTENSION)
drop if cost_animal_feed==0
save "clean data/intermediate/Niger14_costs_animal_feed_1.dta", replace
use "raw data/ECVMA2_AS4DP2.dta", clear
mvdecode AS4DQ11, mv(0)
mvencode AS4DQ11, mv(0)
gen cost_animal_feed = AS4DQ11
collapse (sum) cost_animal_feed, by (GRAPPE MENAGE EXTENSION)
drop if cost_animal_feed==0
append using "clean data/intermediate/Niger14_costs_animal_feed_1.dta"
collapse (sum) cost_animal_feed, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_costs_animal_feed.dta", replace

* Land Rent
use "raw data/ECVMA2_AS1P1.dta", clear
rename AS01Q22 cost_land_rent
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_land_rent==0
save "clean data/intermediate/Niger14_costs_land_rent.dta", replace

* Crop Fuel, Water, Transport
use "raw data/SectionAS2DP2.dta", clear
keep if AS02DQ02==2 | AS02DQ02==3 | AS02DQ02==9
rename AS02DQ05 cost_fuel_water_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_fuel_water_transport==0
save "clean data/intermediate/Niger14_costs_fuel_water_transport.dta", replace

* Machine & Animal Rent
use "raw data/ECVMA2_AS2AP1.dta", clear
mvdecode AS02AQ26 AS02AQ28, mv(0)
mvencode AS02AQ26 AS02AQ28, mv(0)
gen cost_machine_rent = AS02AQ26 + AS02AQ28
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_machine_rent==0
save "clean data/intermediate/Niger14_costs_machine_rent_1.dta", replace
use "raw data/ECVMA2_AS03P1.dta", clear
mvdecode AS03Q10, mv(0)
mvencode AS03Q10, mv(0)
gen cost_machine_rent = AS03Q10
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_machine_rent==0
append using "clean data/intermediate/Niger14_costs_machine_rent_1.dta"
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_costs_machine_rent.dta", replace

* Transport Costs
use "raw data/ECVMA2_AS2E2P2.dta", clear
rename AS02EQ16 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_transport==0
save "clean data/intermediate/Niger14_costs_transport_1.dta", replace
use "raw data/ECVMA2_AS05P2.dta", clear
rename AS05Q14 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
drop if cost_transport==0
append using "clean data/intermediate/Niger14_costs_transport_1.dta"
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_costs_transport.dta", replace


* Aggregate Costs
use "clean data/intermediate/Niger14_costs_seeds_fert.dta", clear
joinby using "clean data/intermediate/Niger14_costs_crop_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_land_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_fuel_water_transport.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_machine_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_animal_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_animal_feed.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs_transport.dta", unmatched(both) _merge(_merge)
drop _merge

* Convert Missing to zero
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (GRAPPE MENAGE EXTENSION)
local costs "cost_seeds_fert cost_crop_labor cost_land_rent cost_fuel_water_transport cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach c in `costs' {
	replace `c' = `c'/365
}
*

save "clean data/intermediate/Niger14_costs.dta", replace

**** COMBINE ALL ****
use "clean data/Niger14_demographics.dta", clear
joinby using "clean data/intermediate/Niger14_wage_incomeCHEVAL.dta", unmatched(both) _merge(_merge)	//(!)
drop _merge
joinby using "clean data/intermediate/Niger14_nfe_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_crop_harvest.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_crop_sales_value.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_perm_harvest.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_perm_value_sales.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_non_emp_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_livestock_cattle.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_meat.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_milk.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_eggs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_livestock_use.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_livestock_other.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_assistance.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_transfers.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Niger14_costs.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode wage_* nfe_* crop_* perm_* livestock_* meat_* milk_* eggs_* non_emp_inc assistance transfers cost_*, mv(0)
mvencode wage_* nfe_* crop_* perm_* livestock_* meat_* milk_* eggs_* non_emp_inc assistance transfers cost_*, mv(0)

gen pop_people = hhsize*hhweight
gen pop_youth = hhsize*youth_share_24*hhweight
gen data_obs = 1
gen hh_represented = hhweight

* Convert to Per Capita USD constant PPP
local USD "wage_income nfe_net_inc nfe_gross_inc nfe_costs crop_harvest_value crop_sales_value perm_harvest_value perm_value_sales livestock_income livestock_sold meat_hide_sold milk_income milk_sold eggs_income eggs_sold livestock_use_nfe livestock_other_nfe non_emp_inc assistance transfers cost_seeds_fert cost_crop_labor cost_land_rent cost_fuel_water_transport cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 12/1/2014
	gen USD_`u' = `u'*0.0019037912	
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (2/3 * 2014 GNI constant 2011 PPP / 2014 GNI Atlas Method) + (1/3 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method)
	replace USD_`u' = USD_`u'*2.20832
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 12/1/2014
	gen USD_`usd'`u' = `usd'`u'*0.0019037912
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (2/3 * 2014 GNI constant 2011 PPP / 2014 GNI Atlas Method) + (1/3 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method)
	replace USD_`usd'`u' = USD_`usd'`u'*2.20832
}
}
*	

* Drop former variables
local USD "wage_income nfe_net_inc nfe_gross_inc nfe_costs crop_harvest_value crop_sales_value perm_harvest_value perm_value_sales livestock_income livestock_sold meat_hide_sold milk_income milk_sold eggs_income eggs_sold livestock_use_nfe livestock_other_nfe non_emp_inc assistance transfers cost_seeds_fert cost_crop_labor cost_land_rent cost_fuel_water_transport cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	

* Rename
local USD "wage_income nfe_net_inc nfe_gross_inc nfe_costs crop_harvest_value crop_sales_value perm_harvest_value perm_value_sales livestock_income livestock_sold meat_hide_sold milk_income milk_sold eggs_income eggs_sold livestock_use_nfe livestock_other_nfe non_emp_inc assistance transfers cost_seeds_fert cost_crop_labor cost_land_rent cost_fuel_water_transport cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	


* Impute farm income components before creating the aggregate variable
local varlist crop_harvest_value perm_harvest_value livestock_income milk_income eggs_income meat_hide_sold
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}

replace crop_sales_value=crop_harvest_value  if crop_sales_value>crop_harvest_value   & crop_sales_value!=.
replace perm_value_sales=perm_harvest_value if perm_value_sales>perm_harvest_value  & perm_value_sales!=. 
replace livestock_sold=livestock_income  if livestock_sold>livestock_income & livestock_sold!=.
replace milk_sold=milk_income if milk_sold>milk_income & milk_sold!=.
replace eggs_sold=eggs_income if eggs_sold>eggs_income & eggs_sold!=.


generate farm_sales = crop_sales_value + perm_value_sales + livestock_sold + meat_hide_sold + milk_sold + eggs_sold
gen own_farm_crop_inc_costs = cost_seeds_fert + cost_crop_labor + cost_land_rent + cost_fuel_water_transport + cost_machine_rent + cost_transport
gen own_farm_livestock_inc_costs = cost_animal_labor + cost_animal_feed
gen own_farm_crop_inc_gross = crop_harvest_value + perm_harvest_value
gen own_farm_livestock_inc_gross = livestock_income + milk_income + eggs_income+ meat_hide_sold
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

* Bring in livestock enterprise income
local nfe "nfe_gross_inc nfe_gi_c2 nfe_net_inc nfe_i_c2"
foreach n in `nfe' {
	replace `n' = `n' + livestock_use_nfe + livestock_other_nfe
}
*

** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + non_emp_inc + assistance + transfers
}
*

joinby using "clean data/Niger14_hhid.dta", unmatched(none)

gen remit = 1 if transfers>0
replace remit = 0 if remit==.

merge 1:1 GRAPPE MENAGE EXTENSION using "raw data/ECVMA2014welfare_T1c.dta", nogen


gen USD_pcexp = pcexp*0.00112636 
* Constant 2011 PPP conversion using World Bank GNI Data
* (2/3 * 2014 GNI constant 2011 PPP / 2014 GNI Atlas Method) + (1/3 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method)
* = (2/3 * 887.8181379 / 420) + (1/3 * 889.0363402 / 390)
replace USD_pcexp = USD_pcexp*2.1690953 //2.20832 //

replace USD_pcexp=USD_pcexp/365
drop pcexp
ren USD_pcexp pcexp
g hhexp = pcexp*hhsize


keep hhid GRAPPE rural hhweight hhsize youth_share_24 youth_share_34 female_head school_secondary_hh educ_max credit land_owned job1 job2 Ninformal N_pension N_health_insurance informal informalcon informalsp formal formalcon formalsp sh_pension sh_health_insurance remit pcexp nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales own_farm* i_gi* i_ni* i_nnni* nfe_gross_inc nfe_costs nfe_net_inc farm_sales admin_1 admin_2 admin_3

save "clean data/NIGER_incomesharesCHEVAL.dta", replace

