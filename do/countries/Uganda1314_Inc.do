********************************************************************************
***************** UGANDA 2013/2014: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)


The dataset used for this is:
Uganda 2013/2014 National Panel Survey

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")

The resulting data set is "UGANDA_incomesharesCHEVAL"

Stata Version 15.1

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl UGANDA	"$path/data\Uganda_NPS_1314"

gl DO   	    ${UGANDA}/do
gl raw data   	${UGANDA}/raw data 
gl clean data 	${UGANDA}/clean data
gl FIG 		    ${UGANDA}/graphs
gl TAB 		    ${UGANDA}/tables
 
********************************************************************************
cd "$UGANDA"

*** INCOME SHARES OF FARMING AND FARM PRODUCE SALES ***

*get from each module the value of production, sales, own consumption and aggregate at household level:
*** AG-INCOME = crops, crop products, forestry, livestock, livestock by-products
		*reference period for ag. income is past 12 months
	

** Occupations in Non-Farm Own Enterprises
use "raw data/gsec12.dta", clear
rename hhid HHID
rename h12q4 CIC
rename h12q3b CIC_name
rename h12q12 months
rename h12q13 gross_revenue
rename h12q15 wages
rename h12q16 materials
rename h12q17 other_costs
drop if h12q1==. & months==.
drop if h12q1==2 & months==.
drop if h12q1==2 & months==0
drop if h12q4a==2 & months==.
drop if h12q4a==2 & months==0
drop if months==. & gross_revenue==.

* Correct missing codes
replace CIC = 1011 if CIC==. & CIC_name=="brewing beer"
replace CIC = 500 if CIC==. & CIC_name=="Brick Making Business"
replace CIC = 4300 if CIC==. & CIC_name=="Buying and selling cows"
replace CIC = 4300 if CIC==. & CIC_name=="Buying and selling Matooke"
replace CIC = 4300 if CIC==. & CIC_name=="buying and selling of crop produce"
replace CIC = 4300 if CIC==. & CIC_name=="Buysvand Sells Cattle"
replace CIC = 6130 if CIC==. & CIC_name=="carpentry workshop"
replace CIC = 9311 if CIC==. & CIC_name=="CINEMA"
replace CIC = 8610 if CIC==. & CIC_name=="drug shop and clinic"
replace CIC = 3211 if CIC==. & CIC_name=="Gnuts Grinding Mill"
replace CIC = 3100 if CIC==. & CIC_name=="making baskets and crafts"
replace CIC = 741 if CIC==. & CIC_name=="making of local beer"
replace CIC = 3299 if CIC==. & CIC_name=="MENDING BICYCLES"
replace CIC = 1010 if CIC==. & CIC_name=="operation of abar(beer selling)"
replace CIC = 8510 if CIC==. & CIC_name=="primary sch"
replace CIC = 8510 if CIC==. & CIC_name=="primary school"
replace CIC = 5620 if CIC==. & CIC_name=="Produce trading"
replace CIC = 1071 if CIC==. & CIC_name=="produces charcoal for salr"
replace CIC = 4300 if CIC==. & CIC_name=="RETAIL SALE OF FISH PRODUCTS"
replace CIC = 4771 if CIC==. & CIC_name=="retail sale of goods"
replace CIC = 4771 if CIC==. & CIC_name=="Retail Shop"
replace CIC = 4771 if CIC==. & CIC_name=="Retail shop"
replace CIC = 4771 if CIC==. & CIC_name=="retail shop in general merchandise"
replace CIC = 4771 if CIC==. & CIC_name=="retail trade of general merchandise"
replace CIC = 4921 if CIC==. & CIC_name=="riding a bodaboda"
replace CIC = 4771 if CIC==. & CIC_name=="Runs a drug shop"
replace CIC = 1010 if CIC==. & CIC_name=="Saloon"
replace CIC = 4300 if CIC==. & CIC_name=="SELL PORK"
replace CIC = 4300 if CIC==. & CIC_name=="Selling alcohol"
replace CIC = 4300 if CIC==. & CIC_name=="Selling Cattle"
replace CIC = 4771 if CIC==. & CIC_name=="selling firewood"
replace CIC = 4771 if CIC==. & CIC_name=="Selling general merchandise"
replace CIC = 4300 if CIC==. & CIC_name=="Selling pancakes"
replace CIC = 4300 if CIC==. & CIC_name=="Selling produce"
replace CIC = 4771 if CIC==. & CIC_name=="selling second hand clothes"
replace CIC = 4300 if CIC==. & CIC_name=="selling soft drinks"
replace CIC = 4300 if CIC==. & CIC_name=="SELLS CATTLE"
replace CIC = 4771 if CIC==. & CIC_name=="Stationery shop"
replace CIC = 4921 if CIC==. & CIC_name=="transport people and goods"
replace CIC = 7730 if CIC==. & CIC_name=="weaving baskets"
replace CIC = 4300 if CIC==. & CIC_name=="WHOLESALE OF FARM PRODUCE"

* Keep relevant data
keep HHID CIC months gross_revenue wages materials other_costs

**** Correct clear errors
replace CIC = 99999 if CIC==.
* If claimed to have worked during past year, and have income, it is not possible to have worked zero months
replace months = . if months==0
* Estimate missing months, revenue and costs, using medians of like enterprises by spatial regions
joinby HHID using "clean data/Uganda1314_location.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Count observations with months reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if months~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if months~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if months~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if months~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if months~=.
by CIC rural region, sort : egen float count_6 = count(1) if months~=.
by CIC rural, sort : egen float count_7 = count(1) if months~=.
by CIC, sort : egen float count_8 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_months_2 = median(months) if months~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_months_3 = median(months) if months~=.
by CIC rural region subregion district, sort : egen float temp_est_months_4 = median(months) if months~=.
by CIC rural region subregion, sort : egen float temp_est_months_5 = median(months) if months~=.
by CIC rural region, sort : egen float temp_est_months_6 = median(months) if months~=.
by CIC rural, sort : egen float temp_est_months_7 = median(months) if months~=.
by CIC, sort : egen float temp_est_months_8 = median(months) if months~=.
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_months_1 = max(temp_est_months_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_months_2 = max(temp_est_months_2)
by CIC rural region subregion district subcounty, sort : egen float est_months_3 = max(temp_est_months_3)
by CIC rural region subregion district, sort : egen float est_months_4 = max(temp_est_months_4)
by CIC rural region subregion, sort : egen float est_months_5 = max(temp_est_months_5)
by CIC rural region, sort : egen float est_months_6 = max(temp_est_months_6)
by CIC rural, sort : egen float est_months_7 = max(temp_est_months_7)
by CIC, sort : egen float est_months_8 = max(temp_est_months_8)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_months==.
replace est_months = est_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_months==.
replace est_months = est_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_months==.
replace est_months = est_months_7 if track==7
replace track = 8 if est_months==.
replace est_months = est_months_8 if track==8
drop est_months_* count_* track
**** Replace missing value of months (7 changes)
replace months = est_months if months==.
* Count observations with gross_revenue reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if gross_revenue~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if gross_revenue~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if gross_revenue~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if gross_revenue~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if gross_revenue~=.
by CIC rural region, sort : egen float count_6 = count(1) if gross_revenue~=.
by CIC rural, sort : egen float count_7 = count(1) if gross_revenue~=.
by CIC, sort : egen float count_8 = count(1) if gross_revenue~=.
* Calculate median of gross_revenue by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_gross_revenue_1 = median(gross_revenue) if gross_revenue~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_gross_revenue_2 = median(gross_revenue) if gross_revenue~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_gross_revenue_3 = median(gross_revenue) if gross_revenue~=.
by CIC rural region subregion district, sort : egen float temp_est_gross_revenue_4 = median(gross_revenue) if gross_revenue~=.
by CIC rural region subregion, sort : egen float temp_est_gross_revenue_5 = median(gross_revenue) if gross_revenue~=.
by CIC rural region, sort : egen float temp_est_gross_revenue_6 = median(gross_revenue) if gross_revenue~=.
by CIC rural, sort : egen float temp_est_gross_revenue_7 = median(gross_revenue) if gross_revenue~=.
by CIC, sort : egen float temp_est_gross_revenue_8 = median(gross_revenue) if gross_revenue~=.
mvdecode temp_*, mv(0)
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_gross_revenue_1 = max(temp_est_gross_revenue_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_gross_revenue_2 = max(temp_est_gross_revenue_2)
by CIC rural region subregion district subcounty, sort : egen float est_gross_revenue_3 = max(temp_est_gross_revenue_3)
by CIC rural region subregion district, sort : egen float est_gross_revenue_4 = max(temp_est_gross_revenue_4)
by CIC rural region subregion, sort : egen float est_gross_revenue_5 = max(temp_est_gross_revenue_5)
by CIC rural region, sort : egen float est_gross_revenue_6 = max(temp_est_gross_revenue_6)
by CIC rural, sort : egen float est_gross_revenue_7 = max(temp_est_gross_revenue_7)
by CIC, sort : egen float est_gross_revenue_8 = max(temp_est_gross_revenue_8)
drop temp*
* Build gross_revenue prediction variable if at least 10 observations of reported gross_revenue in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_gross_revenue = est_gross_revenue_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_7 if track==7
replace track = 8 if est_gross_revenue==.
replace est_gross_revenue = est_gross_revenue_8 if track==8
drop est_gross_revenue_* count_* track
**** Replace missing value of gross_revenue (5 changes)
replace gross_revenue = est_gross_revenue if gross_revenue==.
* Count observations with wages reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if wages~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if wages~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if wages~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if wages~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if wages~=.
by CIC rural region, sort : egen float count_6 = count(1) if wages~=.
by CIC rural, sort : egen float count_7 = count(1) if wages~=.
by CIC, sort : egen float count_8 = count(1) if wages~=.
* Calculate median of wages by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_wages_1 = median(wages) if wages~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_wages_2 = median(wages) if wages~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_wages_3 = median(wages) if wages~=.
by CIC rural region subregion district, sort : egen float temp_est_wages_4 = median(wages) if wages~=.
by CIC rural region subregion, sort : egen float temp_est_wages_5 = median(wages) if wages~=.
by CIC rural region, sort : egen float temp_est_wages_6 = median(wages) if wages~=.
by CIC rural, sort : egen float temp_est_wages_7 = median(wages) if wages~=.
by CIC, sort : egen float temp_est_wages_8 = median(wages) if wages~=.
mvdecode temp_*, mv(0)
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_wages_1 = max(temp_est_wages_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_wages_2 = max(temp_est_wages_2)
by CIC rural region subregion district subcounty, sort : egen float est_wages_3 = max(temp_est_wages_3)
by CIC rural region subregion district, sort : egen float est_wages_4 = max(temp_est_wages_4)
by CIC rural region subregion, sort : egen float est_wages_5 = max(temp_est_wages_5)
by CIC rural region, sort : egen float est_wages_6 = max(temp_est_wages_6)
by CIC rural, sort : egen float est_wages_7 = max(temp_est_wages_7)
by CIC, sort : egen float est_wages_8 = max(temp_est_wages_8)
drop temp*
* Build wages prediction variable if at least 10 observations of reported wages in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wages = est_wages_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wages==.
replace est_wages = est_wages_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wages==.
replace est_wages = est_wages_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wages==.
replace est_wages = est_wages_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wages==.
replace est_wages = est_wages_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wages==.
replace est_wages = est_wages_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wages==.
replace est_wages = est_wages_7 if track==7
replace track = 8 if est_wages==.
replace est_wages = est_wages_8 if track==8
drop est_wages_* count_* track
**** Replace missing value of wages (5 changes)
replace wages = est_wages if wages==.
* Count observations with materials reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if materials~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if materials~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if materials~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if materials~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if materials~=.
by CIC rural region, sort : egen float count_6 = count(1) if materials~=.
by CIC rural, sort : egen float count_7 = count(1) if materials~=.
by CIC, sort : egen float count_8 = count(1) if materials~=.
* Calculate median of materials by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_materials_1 = median(materials) if materials~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_materials_2 = median(materials) if materials~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_materials_3 = median(materials) if materials~=.
by CIC rural region subregion district, sort : egen float temp_est_materials_4 = median(materials) if materials~=.
by CIC rural region subregion, sort : egen float temp_est_materials_5 = median(materials) if materials~=.
by CIC rural region, sort : egen float temp_est_materials_6 = median(materials) if materials~=.
by CIC rural, sort : egen float temp_est_materials_7 = median(materials) if materials~=.
by CIC, sort : egen float temp_est_materials_8 = median(materials) if materials~=.
mvdecode temp_*, mv(0)
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_materials_1 = max(temp_est_materials_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_materials_2 = max(temp_est_materials_2)
by CIC rural region subregion district subcounty, sort : egen float est_materials_3 = max(temp_est_materials_3)
by CIC rural region subregion district, sort : egen float est_materials_4 = max(temp_est_materials_4)
by CIC rural region subregion, sort : egen float est_materials_5 = max(temp_est_materials_5)
by CIC rural region, sort : egen float est_materials_6 = max(temp_est_materials_6)
by CIC rural, sort : egen float est_materials_7 = max(temp_est_materials_7)
by CIC, sort : egen float est_materials_8 = max(temp_est_materials_8)
drop temp*
* Build materials prediction variable if at least 10 observations of reported materials in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_materials = est_materials_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_materials==.
replace est_materials = est_materials_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_materials==.
replace est_materials = est_materials_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_materials==.
replace est_materials = est_materials_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_materials==.
replace est_materials = est_materials_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_materials==.
replace est_materials = est_materials_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_materials==.
replace est_materials = est_materials_7 if track==7
replace track = 8 if est_materials==.
replace est_materials = est_materials_8 if track==8
drop est_materials_* count_* track
**** Replace missing value of materials (4 changes)
replace materials = est_materials if materials==.
* Count observations with other_costs reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if other_costs~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if other_costs~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if other_costs~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if other_costs~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if other_costs~=.
by CIC rural region, sort : egen float count_6 = count(1) if other_costs~=.
by CIC rural, sort : egen float count_7 = count(1) if other_costs~=.
by CIC, sort : egen float count_8 = count(1) if other_costs~=.
* Calculate median of other_costs by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_other_costs_1 = median(other_costs) if other_costs~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_other_costs_2 = median(other_costs) if other_costs~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_other_costs_3 = median(other_costs) if other_costs~=.
by CIC rural region subregion district, sort : egen float temp_est_other_costs_4 = median(other_costs) if other_costs~=.
by CIC rural region subregion, sort : egen float temp_est_other_costs_5 = median(other_costs) if other_costs~=.
by CIC rural region, sort : egen float temp_est_other_costs_6 = median(other_costs) if other_costs~=.
by CIC rural, sort : egen float temp_est_other_costs_7 = median(other_costs) if other_costs~=.
by CIC, sort : egen float temp_est_other_costs_8 = median(other_costs) if other_costs~=.
mvdecode temp_*, mv(0)
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_other_costs_1 = max(temp_est_other_costs_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_other_costs_2 = max(temp_est_other_costs_2)
by CIC rural region subregion district subcounty, sort : egen float est_other_costs_3 = max(temp_est_other_costs_3)
by CIC rural region subregion district, sort : egen float est_other_costs_4 = max(temp_est_other_costs_4)
by CIC rural region subregion, sort : egen float est_other_costs_5 = max(temp_est_other_costs_5)
by CIC rural region, sort : egen float est_other_costs_6 = max(temp_est_other_costs_6)
by CIC rural, sort : egen float est_other_costs_7 = max(temp_est_other_costs_7)
by CIC, sort : egen float est_other_costs_8 = max(temp_est_other_costs_8)
drop temp*
* Build other_costs prediction variable if at least 10 observations of reported other_costs in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_other_costs = est_other_costs_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_other_costs==.
replace est_other_costs = est_other_costs_7 if track==7
replace track = 8 if est_other_costs==.
replace est_other_costs = est_other_costs_8 if track==8
drop est_other_costs_* count_* track
**** Replace missing value of other_costs (4 changes)
replace other_costs = est_other_costs if other_costs==.

rename gross_revenue nfe_avg_gross_inc
gen nfe_avg_costs = wages + materials + other_costs
gen nfe_avg_net_inc = nfe_avg_gross_inc - nfe_avg_costs

* Generate Annual Income Variable
gen nfe_ann_gross_inc = months*nfe_avg_gross_inc
gen nfe_ann_costs = months*nfe_avg_costs
gen nfe_ann_net_inc = months*nfe_avg_net_inc

* Calculate Income Per Day
gen nfe_gross_inc = nfe_ann_gross_inc/365
gen nfe_costs = nfe_ann_costs/365
gen nfe_net_inc = nfe_ann_net_inc/365

joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby HHID using "clean data/Uganda1314_location.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.453 if AFS_share==. & rural==1
replace AFS_share = 0.386 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612

* Keep relevant variables
order HHID CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*
keep HHID CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*, by (HHID)

save "clean data/intermediate/Uganda1314_nfe_inc.dta", replace

** Gross Income from Wage Earnings
* Wage Labor information
use "raw data/GSEC8_1.dta", clear

**adding ANY emp type performed in last 12 months:
rename h8q20A main_CIC_name
rename h8q20B main_CIC
rename h8q22 main_type
gen main_hourspw = 0
local alpha "a b c d e f g"
foreach a in `alpha' {
	replace  main_hourspw = main_hourspw + h8q36`a' if h8q36`a'~=.
}
*
rename h8q23 main_pension
rename h8q25 main_health_insurance
rename h8q27 main_contract
rename h8q30a main_months
rename h8q30b main_weekspm
rename h8q31a main_cash_pmt
rename h8q31b main_inkind_pmt
rename h8q31c main_pmt_duration
rename h8q39A secondary_CIC_name
rename h8q39B secondary_CIC
rename h8q41 secondary_type
rename h8q43 secondary_hourspw
rename h8q44 secondary_months
rename h8q44b secondary_weekspm
rename h8q45a secondary_cash_pmt
rename h8q45b secondary_inkind_pmt
rename h8q45c secondary_pmt_duration
rename h8q50A umain_CIC_name
rename h8q50B umain_CIC
rename h8q51 umain_type
rename h8q52 umain_months
rename h8q52_1 umain_weekspm
rename h8q52_2 umain_hourspw
rename h8q53a umain_cash_pmt
rename h8q53b umain_inkind_pmt
rename h8q53c umain_pmt_duration
rename h8q56A usecondary_CIC_name
rename h8q56B usecondary_CIC
rename h8q51a usecondary_type
rename h8q57 usecondary_months
rename h8q57_1 usecondary_weekspm
rename h8q57_2 usecondary_hourspw
rename h8q58a usecondary_cash_pmt
rename h8q58b usecondary_inkind_pmt
rename h8q58c usecondary_pmt_duration
keep HHID PID main* secondary* umain* usecondary*

* Allocate generally defined umain_CICs
replace main_CIC = "47" if main_CIC=="Y9998" & main_CIC_name=="Sales in a shop"
replace main_CIC = "111" if main_CIC=="Y9998" & main_CIC_name=="Agricultural field assistant"
replace main_CIC = "502" if main_CIC=="Y9998" & main_CIC_name=="road maintainance"
replace main_CIC = "502" if main_CIC=="Y9998" & main_CIC_name=="vechicle mechanic activities"
replace main_CIC = "525" if main_CIC=="Y9998" & main_CIC_name=="taking peoples cattle to market places for pay"
replace main_CIC = "525" if main_CIC=="Y9998" & main_CIC_name=="transporting fish"
replace main_CIC = "710" if main_CIC=="Y9998" & main_CIC_name=="FISHING"
replace main_CIC = "710" if main_CIC=="Y9998" & main_CIC_name=="sand minning Sector"
replace main_CIC = "1061" if main_CIC=="Y9998" & main_CIC_name=="Slaughters animals"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="business of griding peoples maize"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="casual worker at the milling factory"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="Making a hebal drink"
replace main_CIC = "3220" if main_CIC=="Y9998" & main_CIC_name=="Fabrication and welding"
replace main_CIC = "3220" if main_CIC=="Y9998" & main_CIC_name=="Making Bricks"
replace main_CIC = "4100" if main_CIC=="Y9998" & main_CIC_name=="Brick laying.."
replace main_CIC = "4100" if main_CIC=="Y9998" & main_CIC_name=="Builder"
replace main_CIC = "4312" if main_CIC=="Y9998" & main_CIC_name=="Selling charcoal"
replace main_CIC = "4600" if main_CIC=="Y9998" & main_CIC_name=="retailsale of agricultural produce"
replace main_CIC = "4600" if main_CIC=="Y9998" & main_CIC_name=="SELLINGAGRICULTURAL PRODUCE"
replace main_CIC = "4921" if main_CIC=="Y9998" & main_CIC_name=="CAB DRIVER IN OWN CAR"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="schedules delivery of polythene paper"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="transporting luggage"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="whole sale and retail sale of cement( construction materials)"
replace main_CIC = "5510" if main_CIC=="Y9998" & main_CIC_name=="logging"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Education"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Inspector of Schools under ministry of Education"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Secondary school"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Teaching"
replace main_CIC = "9491" if main_CIC=="Y9998" & main_CIC_name=="preaching"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="field interviewer..."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Law and Order"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="local council five"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Naads cordinator...."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Population officer.."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Town council"
replace main_CIC = "9511" if main_CIC=="Y9998" & main_CIC_name=="bcycle repair"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="casual labourer"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="doing all HH chores at the Ejons Hh"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="personal services"
replace main_CIC = "47" if main_CIC=="Z9999" & main_CIC_name=="Middleman"
replace main_CIC = "111" if main_CIC=="Z9999" & main_CIC_name=="subsistance crop farming"
replace main_CIC = "111" if main_CIC=="Z9999" & main_CIC_name=="works on peoples farms for pay"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="BUILDING houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="building houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="construction of buildings"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="construction of houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="Contruction industry"
replace main_CIC = "5510" if main_CIC=="Z9999" & main_CIC_name=="logging"
replace main_CIC = "6419" if main_CIC=="Z9999" & main_CIC_name=="bibco uganda investment"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="bethel primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="bombo barracks army primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="glenburnie primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="primary teaching"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="private primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="rakai district headquaters"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="school matron"
replace main_CIC = "9499" if main_CIC=="Z9999" & main_CIC_name=="district health assistant"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="casual labourer"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="housemaid"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="subsistance causal labourer"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="retail trade of general merchandise"
replace main_CIC = "3211" if main_CIC=="" & main_CIC_name=="butcher services"
replace main_CIC = "8510" if main_CIC=="" & main_CIC_name=="primary school"
replace main_CIC = "" if main_CIC=="Y9998" | main_CIC=="Z9999"
replace main_CIC = "4540" if main_CIC=="" & main_CIC_name=="Buying and selling roduce"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="Buys and sells charcoal"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="buys and sells used metals"
replace main_CIC = "4540" if main_CIC=="" & main_CIC_name=="buys matooke from villages and distributes them in town"
replace main_CIC = "210" if main_CIC=="" & main_CIC_name=="Charcoal burning and selling"
replace main_CIC = "210" if main_CIC=="" & main_CIC_name=="Charcoal production"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="clinic owner"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="computer service activites"
replace main_CIC = "410" if main_CIC=="" & main_CIC_name=="consruction of buldings"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="dry cleaners"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="Dry cleaning..."
replace main_CIC = "2511" if main_CIC=="" & main_CIC_name=="Fabrication of metal products"
replace main_CIC = "8100" if main_CIC=="" & main_CIC_name=="fumigation"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="Hardware shop"
replace main_CIC = "3900" if main_CIC=="" & main_CIC_name=="Interior designing"
replace main_CIC = "5610" if main_CIC=="" & main_CIC_name=="Makes local porridge and sells it"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="Manicure shop"
replace main_CIC = "2030" if main_CIC=="" & main_CIC_name=="manufacturing"
replace main_CIC = "4730" if main_CIC=="" & main_CIC_name=="manufacturing an mobile retail trade of metal products s"
replace main_CIC = "454" if main_CIC=="" & main_CIC_name=="mechanic"
replace main_CIC = "4730" if main_CIC=="" & main_CIC_name=="mobile retail trade of metal products"
replace main_CIC = "454" if main_CIC=="" & main_CIC_name=="Motorcycle Repairing"
replace main_CIC = "99999" if main_CIC=="" & main_CIC_name=="not reported...."
replace main_CIC = "7410" if main_CIC=="" & main_CIC_name=="operating a saloon"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="phone charging service"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="provides a service of selling milk"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="providing rentals....."
replace main_CIC = "479" if main_CIC=="" & main_CIC_name=="pushing acart  carriage as aform of transport"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="Rairs of machinery"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="Repairing chain saw machines"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="REPARES BICYCLES"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail sale of Agriculture inputs such as fertilizers & pesticides"
replace main_CIC = "4659" if main_CIC=="" & main_CIC_name=="retail sale of electrical wiring equipments"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="retail sale of household items"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="retail sale of household merchandise"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="retail sale of meat..."
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="Retail Shop"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail trade of loca brew <maalwa>"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="retail trader"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail trading in food staffs"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="retailsale of cooking oil at home"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Sale of food stuffs in the market.."
replace main_CIC = "8610" if main_CIC=="" & main_CIC_name=="sanyu clinic"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="selling charcoal"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="selling firewood"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Sells cows"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="sells goods in spouse's grocery shop"
replace main_CIC = "479" if main_CIC=="" & main_CIC_name=="Service Deiverly Providers"
replace main_CIC = "1393" if main_CIC=="" & main_CIC_name=="Sewing clothes"
replace main_CIC = "4722" if main_CIC=="" & main_CIC_name=="tends to household bar"
replace main_CIC = "8621" if main_CIC=="" & main_CIC_name=="Traddition medicine"
replace main_CIC = "8621" if main_CIC=="" & main_CIC_name=="Tradditional healing"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="transporting of Goods"
replace main_CIC = "9511" if main_CIC=="" & main_CIC_name=="weldinng"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Wholesale trade of fish"
destring main_CIC, replace
replace secondary_CIC = "502" if secondary_CIC=="Y9998" & secondary_CIC_name=="road maintencece service(clearing drainage ,blockages and shoulders)"
replace secondary_CIC = "3220" if secondary_CIC=="Y9998" & secondary_CIC_name=="makes bricks"
replace secondary_CIC = "4100" if secondary_CIC=="Y9998" & secondary_CIC_name=="BULDING ACTIVITY"
replace secondary_CIC = "4600" if secondary_CIC=="Y9998" & secondary_CIC_name=="fish selling"
replace secondary_CIC = "4600" if secondary_CIC=="Y9998" & secondary_CIC_name=="taking animals to the market for sale"
replace secondary_CIC = "4921" if secondary_CIC=="Y9998" & secondary_CIC_name=="land transport"
replace secondary_CIC = "8610" if secondary_CIC=="Y9998" & secondary_CIC_name=="Proffessional nurse.."
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="DUTIES OF AN L.C1 CHAIRPERSON"
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="provides a leadership service to the community"
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="provides security i.e an Askari..."
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="store guard"
replace secondary_CIC = "454" if secondary_CIC=="Z9999" & secondary_CIC_name=="retail trade in repair and sale of bicycle and motor cycle parts"
replace secondary_CIC = "502" if secondary_CIC=="Z9999" & secondary_CIC_name=="Road repairs"
replace secondary_CIC = "1701" if secondary_CIC=="Z9999" & secondary_CIC_name=="nile victoria industry dealing in making plastics"
replace secondary_CIC = "4600" if secondary_CIC=="Z9999" & secondary_CIC_name=="retail sale of pork"
replace secondary_CIC = "9499" if secondary_CIC=="Z9999" & secondary_CIC_name=="local government"
replace secondary_CIC = "" if secondary_CIC=="Y9998" | secondary_CIC=="Z9999"
destring secondary_CIC, replace
replace umain_CIC = "111" if umain_CIC_name=="WORKING ON OTHER PEOPLES FARMS FOR PAYMENT" & umain_CIC=="9609"
replace umain_CIC = "111" if umain_CIC_name=="WORKING ON PEOPLES FARMS" & umain_CIC=="9609"
replace umain_CIC = "4922" if umain_CIC_name=="service in transport sector" & umain_CIC=="9609"
replace umain_CIC = "4773" if umain_CIC_name=="she is aretailer who deals in selling household machandise" & umain_CIC=="9609"
replace umain_CIC = "111" if umain_CIC_name=="EMPLOYED BY VARIOUS HOUSEHOLDS TO DO THEIR FARMING" & umain_CIC=="9700"
replace umain_CIC = "111" if umain_CIC_name=="WORKS FOR DIFFERENT HOUSEHOLDS ON THEIR HOUSEHOLD FARMS" & umain_CIC=="9700"
replace umain_CIC = "111" if umain_CIC_name=="Subsistance farmer" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="Tending to farm crops" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="subsistance farmer" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="subsistance farming" & umain_CIC=="Y9998"
replace umain_CIC = "8621" if umain_CIC_name=="Cultural Surgeon<Circumsicion" & umain_CIC=="Y9998"
replace umain_CIC = "9609" if umain_CIC_name=="Dry cleaning.." & umain_CIC=="Y9998"
replace umain_CIC = "1410" if umain_CIC_name=="TAIRLOR" & umain_CIC=="Y9998"
replace umain_CIC = "1071" if umain_CIC_name=="burning and selling of charcoal" & umain_CIC=="Y9998"
replace umain_CIC = "200" if umain_CIC_name=="collecting firewood" & umain_CIC=="Y9998"
replace umain_CIC = "500" if umain_CIC_name=="mining clay for brick moulding" & umain_CIC=="Y9998"
replace umain_CIC = "4773" if umain_CIC_name=="trade" & umain_CIC=="Y9998"
replace umain_CIC = "5100" if umain_CIC_name=="works at the airport" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="growing of perenial and non perenial crops" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance  crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance animal and crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop  farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop and animal farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop farmer" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed crop and animal farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed farking" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed farming" & umain_CIC=="Z9999"
replace umain_CIC = "4620" if umain_CIC_name=="retail brewing of local beer" & umain_CIC=="Z9999"
replace umain_CIC = "8510" if umain_CIC_name=="koboyo primary school" & umain_CIC=="Z9999"
replace umain_CIC = "8510" if umain_CIC_name=="primary teacher" & umain_CIC=="Z9999"
replace umain_CIC = "1701" if umain_CIC_name=="making metal objects" & umain_CIC=="Z9999"
replace umain_CIC = "1701" if umain_CIC_name=="manufacturing" & umain_CIC=="Z9999"
replace umain_CIC = "500" if umain_CIC_name=="minin clay for moulding bricks" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC=="" & umain_CIC_name=="crop production"
replace umain_CIC = "9609" if umain_CIC=="" & umain_CIC_name=="casual lab"
replace umain_CIC = "4771" if umain_CIC=="" & umain_CIC_name=="retail sale"
destring umain_CIC, replace
replace usecondary_CIC = "111" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Crop farming"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Gold mining"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Gold minning"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Sand mining"
replace usecondary_CIC = "1071" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="manufacture of charcoal"
replace usecondary_CIC = "1622" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="MANUFACTURE OF CONSTRUCTION MATERIALS-BRICKS"
replace usecondary_CIC = "1701" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="MANUFACTURE AND SALE  OF PAPER CRAFTS"
replace usecondary_CIC = "4540" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="makes and se'ls malwa"
replace usecondary_CIC = "4610" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="WHOLESALE OF TIMBER FOR FISHING RODS"
replace usecondary_CIC = "4610" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="collecting and selling firwood.."
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="retail sale of agricultural produce..maize,beans,"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sale of cassava"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells caattlle"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells produce"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells waragi"
replace usecondary_CIC = "4722" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="retailing alcohol"
replace usecondary_CIC = "6120" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="communication"
replace usecondary_CIC = "8100" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="cleaner at the university"
replace usecondary_CIC = "9499" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Defence"
replace usecondary_CIC = "9609" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="odd jobsperson"
replace usecondary_CIC = "9609" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="repairs of bicycles"
replace usecondary_CIC = "4771" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="trade"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop and animal farming"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop and animal mixed farming"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop farming"
replace usecondary_CIC = "1071" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="Manufacturing Charcoal"
replace usecondary_CIC = "4610" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="buying and selling timber"
replace usecondary_CIC = "4721" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="offers a service of selling onions"
replace usecondary_CIC = "4721" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="provides a sales service by selling passion fruits at the market"
replace usecondary_CIC = "9609" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="casual labourer"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance mixed farming"
replace usecondary_CIC = "4610" if usecondary_CIC=="9609" & usecondary_CIC_name=="baskets for sale"
replace usecondary_CIC = "4771" if usecondary_CIC=="9609" & usecondary_CIC_name=="retai shop"
replace usecondary_CIC = "4721" if usecondary_CIC=="9609" & usecondary_CIC_name=="retail sale in agricultural goods"
replace usecondary_CIC = "4771" if usecondary_CIC=="9609" & usecondary_CIC_name=="sells clothes"
replace usecondary_CIC = "1410" if usecondary_CIC=="9609" & usecondary_CIC_name=="tailoring"
replace usecondary_CIC = "4721" if usecondary_CIC=="9810" & usecondary_CIC_name=="buys and sells agricultural produce"
replace usecondary_CIC = "1101" if usecondary_CIC=="9820" & usecondary_CIC_name=="brewing"
destring usecondary_CIC, replace

* Bring in ISIC Groupings
rename main_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC main_CIC
rename ISIC_group main_ISIC_group
rename AFS_share main_AFS_share
rename secondary_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC secondary_CIC
rename ISIC_group secondary_ISIC_group
rename AFS_share secondary_AFS_share
rename umain_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC umain_CIC
rename ISIC_group umain_ISIC_group
rename AFS_share umain_AFS_share
rename usecondary_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC usecondary_CIC
rename ISIC_group usecondary_ISIC_group
rename AFS_share usecondary_AFS_share

save "clean data/intermediate/Uganda1314_wage_activities.dta", replace

* Convert Data to one observation per activity per individual
use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID main*
keep if main_type==1 | main_type==5
gen apprentice = 1 if main_type==5
mvdecode main_months main_weekspm main_hourspw main_cash_pmt main_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share pension health_insurance contract"
foreach v in `vars' {
	rename main_`v' `v'
}
*
gen job = 1
save "clean data/intermediate/Uganda1314_main_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID secondary*
keep if secondary_type==1 | secondary_type==5
gen apprentice = 1 if secondary_type==5
mvdecode secondary_months secondary_weekspm secondary_hourspw secondary_cash_pmt secondary_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename secondary_`v' `v'
}
*
gen job = 2
save "clean data/intermediate/Uganda1314_secondary_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID umain*
keep if umain_type==1 | umain_type==5
gen apprentice = 1 if umain_type==5
mvdecode umain_months umain_weekspm umain_hourspw umain_cash_pmt umain_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename umain_`v' `v'
}
*
gen job = 3
save "clean data/intermediate/Uganda1314_umain_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID usecondary*
keep if usecondary_type==1 | usecondary_type==5
gen apprentice = 1 if usecondary_type==5
mvdecode usecondary_months usecondary_weekspm usecondary_hourspw usecondary_cash_pmt usecondary_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename usecondary_`v' `v'
}
*
gen job = 4
save "clean data/intermediate/Uganda1314_usecondary_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_main_wage_activities.dta", clear
append using "clean data/intermediate/Uganda1314_secondary_wage_activities.dta"
append using "clean data/intermediate/Uganda1314_umain_wage_activities.dta"
append using "clean data/intermediate/Uganda1314_usecondary_wage_activities.dta"
replace ISIC_group = 612 if ISIC_group==.
replace CIC = 99999 if CIC==.

* Estimate missing values of months, weeks per month and hours per month if one of these are missing
joinby HHID PID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Count observations with months reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if months~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if months~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if months~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if months~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if months~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if months~=.
by type CIC female rural, sort : egen float count_7 = count(1) if months~=.
by type CIC female, sort : egen float count_8 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_months_2 = median(months) if months~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_months_3 = median(months) if months~=.
by type CIC female rural region subregion district, sort : egen float temp_est_months_4 = median(months) if months~=.
by type CIC female rural region subregion, sort : egen float temp_est_months_5 = median(months) if months~=.
by type CIC female rural region, sort : egen float temp_est_months_6 = median(months) if months~=.
by type CIC female rural, sort : egen float temp_est_months_7 = median(months) if months~=.
by type CIC female, sort : egen float temp_est_months_8 = median(months) if months~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_months_1 = max(temp_est_months_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_months_2 = max(temp_est_months_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_months_3 = max(temp_est_months_3)
by type CIC female rural region subregion district, sort : egen float est_months_4 = max(temp_est_months_4)
by type CIC female rural region subregion, sort : egen float est_months_5 = max(temp_est_months_5)
by type CIC female rural region, sort : egen float est_months_6 = max(temp_est_months_6)
by type CIC female rural, sort : egen float est_months_7 = max(temp_est_months_7)
by type CIC female, sort : egen float est_months_8 = max(temp_est_months_8)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_months==.
replace est_months = est_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_months==.
replace est_months = est_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_months==.
replace est_months = est_months_7 if track==7
replace track = 8 if est_months==.
replace est_months = est_months_8 if track==8
drop est_months_* count_* track
**** Replace missing value of months (20 changes)
replace months = est_months if months==.
* Count observations with weekspm reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if weekspm~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if weekspm~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if weekspm~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if weekspm~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if weekspm~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if weekspm~=.
by type CIC female rural, sort : egen float count_7 = count(1) if weekspm~=.
by type CIC female, sort : egen float count_8 = count(1) if weekspm~=.
* Calculate median of weekspm by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_weekspm_1 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_weekspm_2 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_weekspm_3 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district, sort : egen float temp_est_weekspm_4 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion, sort : egen float temp_est_weekspm_5 = median(weekspm) if weekspm~=.
by type CIC female rural region, sort : egen float temp_est_weekspm_6 = median(weekspm) if weekspm~=.
by type CIC female rural, sort : egen float temp_est_weekspm_7 = median(weekspm) if weekspm~=.
by type CIC female, sort : egen float temp_est_weekspm_8 = median(weekspm) if weekspm~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_weekspm_1 = max(temp_est_weekspm_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_weekspm_2 = max(temp_est_weekspm_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_weekspm_3 = max(temp_est_weekspm_3)
by type CIC female rural region subregion district, sort : egen float est_weekspm_4 = max(temp_est_weekspm_4)
by type CIC female rural region subregion, sort : egen float est_weekspm_5 = max(temp_est_weekspm_5)
by type CIC female rural region, sort : egen float est_weekspm_6 = max(temp_est_weekspm_6)
by type CIC female rural, sort : egen float est_weekspm_7 = max(temp_est_weekspm_7)
by type CIC female, sort : egen float est_weekspm_8 = max(temp_est_weekspm_8)
drop temp*
* Build weekspm prediction variable if at least 10 observations of reported weekspm in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_weekspm = est_weekspm_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_7 if track==7
replace track = 8 if est_weekspm==.
replace est_weekspm = est_weekspm_8 if track==8
drop est_weekspm_* count_* track
**** Replace missing value of weekspm (55 changes)
replace weekspm = est_weekspm if weekspm==.
* Count observations with hourspw reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if hourspw~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if hourspw~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if hourspw~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if hourspw~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if hourspw~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if hourspw~=.
by type CIC female rural, sort : egen float count_7 = count(1) if hourspw~=.
by type CIC female, sort : egen float count_8 = count(1) if hourspw~=.
* Calculate median of hourspw by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_hourspw_1 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_hourspw_2 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_hourspw_3 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district, sort : egen float temp_est_hourspw_4 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion, sort : egen float temp_est_hourspw_5 = median(hourspw) if hourspw~=.
by type CIC female rural region, sort : egen float temp_est_hourspw_6 = median(hourspw) if hourspw~=.
by type CIC female rural, sort : egen float temp_est_hourspw_7 = median(hourspw) if hourspw~=.
by type CIC female, sort : egen float temp_est_hourspw_8 = median(hourspw) if hourspw~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_hourspw_1 = max(temp_est_hourspw_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_hourspw_2 = max(temp_est_hourspw_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_hourspw_3 = max(temp_est_hourspw_3)
by type CIC female rural region subregion district, sort : egen float est_hourspw_4 = max(temp_est_hourspw_4)
by type CIC female rural region subregion, sort : egen float est_hourspw_5 = max(temp_est_hourspw_5)
by type CIC female rural region, sort : egen float est_hourspw_6 = max(temp_est_hourspw_6)
by type CIC female rural, sort : egen float est_hourspw_7 = max(temp_est_hourspw_7)
by type CIC female, sort : egen float est_hourspw_8 = max(temp_est_hourspw_8)
drop temp*
* Build hourspw prediction variable if at least 10 observations of reported hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_hourspw = est_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_7 if track==7
replace track = 8 if est_hourspw==.
replace est_hourspw = est_hourspw_8 if track==8
drop est_hourspw_* count_* track
**** Replace missing value of hourspw (221 changes)
replace hourspw = est_hourspw if hourspw==.
* If cannot use other observations to estimate, assume at least one month, one week, one hour (Changes: 13, 12, 13)
replace months = 1 if months==.
replace weekspm = 1 if weekspm==.
replace hourspw = 1 if hourspw==.

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.453 if AFS_share==. & rural==1
replace AFS_share = 0.386 if AFS_share==. & rural==0

gen annual_hours = months*weekspm*hourspw

*** Convert payment into average payment per day
* Duration in hours
replace cash_pmt = ((cash_pmt*hourspw)/7)*(weekspm/4.3)*(months/12) if pmt_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace cash_pmt = ((cash_pmt*est_wage_days)/7)*(weekspm/4.3)*(months/12) if pmt_duration==2
* Duration in weeks
replace cash_pmt = cash_pmt*(1/7)*(weekspm/4.3)*(months/12) if pmt_duration==3
* Duration in months
replace cash_pmt = cash_pmt*(1/30.4)*(months/12) if pmt_duration==4
* Duration in years ******* NO VARIABLE FOR OTHER PAYMENT DURATION... ASSUME YEAR
replace cash_pmt = cash_pmt*(1/12)*(1/30.4)*(months/12) if pmt_duration==5
replace cash_pmt = 0 if pmt_duration==.
* Payment in kind
* Duration in hours
replace inkind_pmt = ((inkind_pmt*hourspw)/7)*(weekspm/4.2)*(months/12) if pmt_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace inkind_pmt = ((inkind_pmt*est_wage_days)/7)*(weekspm/4.3)*(months/12) if pmt_duration==2
* Duration in weeks
replace inkind_pmt = inkind_pmt*(1/7)*(weekspm/4.3)*(months/12) if pmt_duration==3
* Duration in months
replace inkind_pmt = inkind_pmt*(1/30.4)*(months/12) if pmt_duration==4
* Duration in years
replace inkind_pmt = inkind_pmt*(1/12)*(1/30.4)*(months/12) if pmt_duration==5
replace inkind_pmt = 0 if pmt_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + cash_pmt if cash_pmt~=.
replace wage_income = wage_income + inkind_pmt if inkind_pmt~=.
drop if wage_income==0

* Casual vs Formal Wage
gen formal = 0
replace formal = 1 if pmt_duration==4 //Formal if payment is monthly
replace formal = 1 if contract==1 //written contract vs. verbal
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

//added definition of "formality" due to contract structure 
g formalcon =  (contract==1) 

//added definition of "formality" due to SP
g formalsp = (pension==1 | health_insurance==1)

** Generate Categorical Variables
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen wage_i_f`f'_c`c' = 0
	gen wage_i_fcon`f'_c`c' = 0 
	gen wage_i_fsp`f'_c`c' = 0 
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
	replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f' 
	replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f' 
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'
	*(!)
	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	*(!)
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}
*

g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0)


* keep only for those 16 and older: (!)
keep if age>=16

tab job, g(job) // added! number of wage jobs

* Keep relevant variables
order HHID CIC ISIC_group wage_income wage_i_* formal* informal* job* contract pension health_insurance
keep HHID CIC ISIC_group wage_income wage_i_* formal* informal* job* contract pension health_insurance 

recode pension health_insurance contract (2=0) 

* Collapse to household level
collapse (sum) wage_income wage_i_* job1 job2 job3 job4 Ninformal=informal ///
 N_pension=pension N_health_insurance=health_insurance N_contract=contract (mean)  informal* formal* ///
sh_pension=pension sh_health_insurance=health_insurance sh_contract=contract , by (HHID) 

**labels
foreach v in pension health_insurance contract {
la var sh_`v' "Share of HHmemb with `v'"
la var N_`v' "N HHmemb with `v'"
}
la var job1 		"N HHmemb with wagejob(primary)"
la var job2 		"N HHmemb with wagejob(secondary)"
la var informal 	"Informal sh of wagework(no contract,SP)"	
la var Ninformal	"N HHmemb with informal wagework"
la var formal		"Formal sh of wagework(any contract,SP)"
la var informalcon 	"Informal(no contract)"
la var formalcon  	"Formal sh of wagework(contract)"
la var informalsp 	"Informal sh of wagework(SP:insurance/pension)"
la var formalsp 	"Formal sh of wagework(SP:insurance/pension)"


global lab1 "OwnFarming"	
global lab2 "Farm Labor"	
global lab3 "Food&Ag Manufacturing w/in AFS"
global lab4 "FoodPrep"	
global lab5 "FoodSystem Marketing&Transport"
global lab6 "AFS Unclassified"	
global lab7 "Non-AFS Manufacturing"
global lab8 "Non-AFS Industry"	
global lab9 "Non-AFS Marketing&Transport"
global lab10 "Non-AFS Other"
global lab11 "Non-AFS PublicService"
global lab12 "Non-AFS Unclassifed"

forv f=1/12 {
la var wage_i_f0_c`f'    "Informal w from ${lab`f'}"
la var wage_i_fcon0_c`f' "Informal w from ${lab`f'}(contract)"
la var wage_i_fsp0_c`f'  "Informal w from ${lab`f'}(SP)"
la var wage_i_f1_c`f'    "Formal w from ${lab`f'}"
la var wage_i_fcon1_c`f' "Formal w from ${lab`f'}(contract)"
la var wage_i_fsp1_c`f'  "Formal w from ${lab`f'}(SP)"
}


save "clean data/intermediate/Uganda1314_wage_incCHEVAL.dta", replace

* Own Farm Income
* First Season Data - Price Estimates
use "raw data/AGSEC5A.dta", clear
rename HHID ag_HHID
gen kg_sold = a5aq7a
mvdecode kg_sold, mv(0)
replace kg_sold = a5aq7a*a5aq7d if a5aq7c~=1 & a5aq7d~=.
**** 44/3302 observations without units. ASSUME KGs
keep if kg_sold~=.
rename a5aq8 sales_value
rename a5aq7b crop_condition
keep ag_HHID cropID kg_sold crop_condition sales_value
drop if cropID==.
drop if crop_condition==. | crop_condition==99
drop if kg_sold==0

gen revealed_price = sales_value / kg_sold

joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
drop HHID

* Count observations with revealed_price reported by geographic location.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float count_3 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district, sort : egen float count_4 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion, sort : egen float count_5 = count(1) if revealed_price~=.
by cropID crop_condition rural region, sort : egen float count_6 = count(1) if revealed_price~=.
by cropID crop_condition rural, sort : egen float count_7 = count(1) if revealed_price~=.
by cropID crop_condition, sort : egen float count_8 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion, sort : egen float temp_est_revealed_price_5 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region, sort : egen float temp_est_revealed_price_6 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural, sort : egen float temp_est_revealed_price_7 = median(revealed_price) if revealed_price~=.
by cropID crop_condition, sort : egen float temp_est_revealed_price_8 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by cropID crop_condition rural region subregion district subcounty, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by cropID crop_condition rural region subregion district, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
by cropID crop_condition rural region subregion, sort : egen float est_revealed_price_5 = max(temp_est_revealed_price_5)
by cropID crop_condition rural region, sort : egen float est_revealed_price_6 = max(temp_est_revealed_price_6)
by cropID crop_condition rural, sort : egen float est_revealed_price_7 = max(temp_est_revealed_price_7)
by cropID crop_condition, sort : egen float est_revealed_price_8 = max(temp_est_revealed_price_8)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & price==.
replace price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & price==.
replace price = est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & price==.
replace price = est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & price==.
replace price = est_revealed_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & price==.
replace price = est_revealed_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & price==.
replace price = est_revealed_price_7 if track==7
replace track = 8 if price==.
replace price = est_revealed_price_8 if track==8
drop est_revealed_price_* count_* track

keep cropID crop_condition price rural region subregion district subcounty parish ea

collapse (median) price, by (cropID crop_condition rural region subregion district subcounty parish ea)

save "clean data/intermediate/Uganda1314_first_prices.dta", replace

* Second Season Data - Price Estimates
use "raw data/AGSEC5B.dta", clear
rename HHID ag_HHID
gen kg_sold = a5bq7a
mvdecode kg_sold, mv(0)
replace kg_sold = a5bq7a*a5bq7d if a5bq7c~=1 & a5bq7d~=.
**** 34/3162 observations without units. ASSUME KGs
keep if kg_sold~=.
rename a5bq8 sales_value
rename a5bq7b crop_condition
keep ag_HHID cropID kg_sold crop_condition sales_value
drop if cropID==.
drop if crop_condition==. | crop_condition==99
drop if kg_sold==0
drop if sales_value==0

gen revealed_price = sales_value / kg_sold

joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
drop HHID

* Count observations with revealed_price reported by geographic location.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float count_3 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion district, sort : egen float count_4 = count(1) if revealed_price~=.
by cropID crop_condition rural region subregion, sort : egen float count_5 = count(1) if revealed_price~=.
by cropID crop_condition rural region, sort : egen float count_6 = count(1) if revealed_price~=.
by cropID crop_condition rural, sort : egen float count_7 = count(1) if revealed_price~=.
by cropID crop_condition, sort : egen float count_8 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion district, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region subregion, sort : egen float temp_est_revealed_price_5 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural region, sort : egen float temp_est_revealed_price_6 = median(revealed_price) if revealed_price~=.
by cropID crop_condition rural, sort : egen float temp_est_revealed_price_7 = median(revealed_price) if revealed_price~=.
by cropID crop_condition, sort : egen float temp_est_revealed_price_8 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by cropID crop_condition rural region subregion district subcounty, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by cropID crop_condition rural region subregion district, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
by cropID crop_condition rural region subregion, sort : egen float est_revealed_price_5 = max(temp_est_revealed_price_5)
by cropID crop_condition rural region, sort : egen float est_revealed_price_6 = max(temp_est_revealed_price_6)
by cropID crop_condition rural, sort : egen float est_revealed_price_7 = max(temp_est_revealed_price_7)
by cropID crop_condition, sort : egen float est_revealed_price_8 = max(temp_est_revealed_price_8)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & price==.
replace price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & price==.
replace price = est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & price==.
replace price = est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & price==.
replace price = est_revealed_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & price==.
replace price = est_revealed_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & price==.
replace price = est_revealed_price_7 if track==7
replace track = 8 if price==.
replace price = est_revealed_price_8 if track==8
drop est_revealed_price_* count_* track

keep cropID crop_condition price rural region subregion district subcounty parish ea

collapse (median) price, by (cropID crop_condition rural region subregion district subcounty parish ea)

save "clean data/intermediate/Uganda1314_second_prices.dta", replace

* First Season Data - Sales Value
use "raw data/AGSEC5A.dta", clear
rename HHID ag_HHID
rename a5aq8 sales_value
mvdecode sales_value, mv(0)
collapse (sum) sales_value, by (ag_HHID)
drop if sales_value==0
replace sales_value = sales_value/365
save "clean data/intermediate/Uganda1314_first_cropsales.dta", replace

* Second Season Data - Sales Value
use "raw data/AGSEC5B.dta", clear
rename HHID ag_HHID
rename a5bq8 sales_value
mvdecode sales_value, mv(0)
collapse (sum) sales_value, by (ag_HHID)
drop if sales_value==0
replace sales_value = sales_value/365
save "clean data/intermediate/Uganda1314_second_cropsales.dta", replace

** Combine harvest seasons
use "clean data/intermediate/Uganda1314_first_cropsales.dta", clear
append using "clean data/intermediate/Uganda1314_second_cropsales.dta"
joinby ag_HHID using "clean data/Uganda1314_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
collapse (sum) sales_value, by (HHID)
save "clean data/intermediate/Uganda1314_farmsales_inc.dta", replace

* First Season Data - Harvest Value
use "raw data/AGSEC5A.dta", clear
rename HHID ag_HHID
drop if cropID==.
gen kg_harvest = a5aq6a
replace kg_harvest = a5aq6a*a5aq6d if a5aq6c~=1 & a5aq6d~=.
**** 8/8759 observations without units. ASSUME KGs
keep if kg_harvest~=.
rename a5aq6b crop_condition

* Account for post harvest loss
rename a5aq16 harvest_loss
replace harvest_loss = 0 if harvest_loss==.
replace kg_harvest = kg_harvest * ((100-harvest_loss)/100)

keep ag_HHID cropID kg_harvest crop_condition

collapse (sum) kg_harvest, by (ag_HHID cropID crop_condition)

joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
drop HHID

joinby cropID crop_condition rural region subregion district subcounty parish ea using "clean data/intermediate/Uganda1314_first_prices.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Estimate prices from the prices merged in (this accounts for localities without sales)
* Count observations with price reported by geographic location.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float count_3 = count(1) if price~=.
by cropID crop_condition rural region subregion district, sort : egen float count_4 = count(1) if price~=.
by cropID crop_condition rural region subregion, sort : egen float count_5 = count(1) if price~=.
by cropID crop_condition rural region, sort : egen float count_6 = count(1) if price~=.
by cropID crop_condition rural, sort : egen float count_7 = count(1) if price~=.
by cropID crop_condition, sort : egen float count_8 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median(price) if price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float temp_est_price_3 = median(price) if price~=.
by cropID crop_condition rural region subregion district, sort : egen float temp_est_price_4 = median(price) if price~=.
by cropID crop_condition rural region subregion, sort : egen float temp_est_price_5 = median(price) if price~=.
by cropID crop_condition rural region, sort : egen float temp_est_price_6 = median(price) if price~=.
by cropID crop_condition rural, sort : egen float temp_est_price_7 = median(price) if price~=.
by cropID crop_condition, sort : egen float temp_est_price_8 = median(price) if price~=.
mvencode temp_*, mv(0)
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by cropID crop_condition rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by cropID crop_condition rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by cropID crop_condition rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by cropID crop_condition rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by cropID crop_condition rural, sort : egen float est_price_7 = max(temp_est_price_7)
by cropID crop_condition, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
replace price = est_price if price==. & est_price~=.
**** 156 of 7392 do not have prices and will be dropped
drop if price==.
gen ann_harvest_value = kg_harvest * price
gen harvest_value = ann_harvest_value/365

collapse (sum) harvest_value, by (ag_HHID)

gen second_season = 0

save "clean data/intermediate/Uganda1314_first_harvest.dta", replace

* Second Season Data - Harvest Value
use "raw data/AGSEC5B.dta", clear
rename HHID ag_HHID
drop if cropID==.
gen kg_harvest = a5bq6a
replace kg_harvest = a5bq6a*a5bq6d if a5bq6c~=1 & a5bq6d~=.
**** 379/8887 observations without units. ASSUME KGs
keep if kg_harvest~=.
rename a5bq6b crop_condition

* Account for post harvest loss
rename a5bq16 harvest_loss
replace harvest_loss = 0 if harvest_loss==.
replace kg_harvest = kg_harvest * ((100-harvest_loss)/100)

keep ag_HHID cropID kg_harvest crop_condition

collapse (sum) kg_harvest, by (ag_HHID cropID crop_condition)

joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
drop HHID

joinby cropID crop_condition rural region subregion district subcounty parish ea using "clean data/intermediate/Uganda1314_first_prices.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Estimate prices from the prices merged in (this accounts for localities without sales)
* Count observations with price reported by geographic location.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float count_3 = count(1) if price~=.
by cropID crop_condition rural region subregion district, sort : egen float count_4 = count(1) if price~=.
by cropID crop_condition rural region subregion, sort : egen float count_5 = count(1) if price~=.
by cropID crop_condition rural region, sort : egen float count_6 = count(1) if price~=.
by cropID crop_condition rural, sort : egen float count_7 = count(1) if price~=.
by cropID crop_condition, sort : egen float count_8 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median(price) if price~=.
by cropID crop_condition rural region subregion district subcounty, sort : egen float temp_est_price_3 = median(price) if price~=.
by cropID crop_condition rural region subregion district, sort : egen float temp_est_price_4 = median(price) if price~=.
by cropID crop_condition rural region subregion, sort : egen float temp_est_price_5 = median(price) if price~=.
by cropID crop_condition rural region, sort : egen float temp_est_price_6 = median(price) if price~=.
by cropID crop_condition rural, sort : egen float temp_est_price_7 = median(price) if price~=.
by cropID crop_condition, sort : egen float temp_est_price_8 = median(price) if price~=.
mvencode temp_*, mv(0)
by cropID crop_condition rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by cropID crop_condition rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by cropID crop_condition rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by cropID crop_condition rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by cropID crop_condition rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by cropID crop_condition rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by cropID crop_condition rural, sort : egen float est_price_7 = max(temp_est_price_7)
by cropID crop_condition, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
replace price = est_price if price==. & est_price~=.
**** 727 of 7594 do not have prices and will be dropped
drop if price==.
gen ann_harvest_value = kg_harvest * price
gen harvest_value = ann_harvest_value/365

collapse (sum) harvest_value, by (ag_HHID)

gen second_season = 1

save "clean data/intermediate/Uganda1314_second_harvest.dta", replace

** Combine harvest seasons
use "clean data/intermediate/Uganda1314_first_harvest.dta", clear
append using "clean data/intermediate/Uganda1314_second_harvest.dta"
joinby ag_HHID using "clean data/Uganda1314_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
collapse (sum) harvest_value, by (HHID)
save "clean data/intermediate/Uganda1314_farm_inc.dta", replace

** Livestock
* General Livestock
use "Raw Data/AGSEC6A.dta", clear
rename HHID ag_HHID
rename LiveStockID livestock_code
rename a6aq9 livestock_givenaway
rename a6aq14a livestock_sold
rename a6aq14b livestock_sold_price
rename a6aq15 livestock_slaughtered
keep ag_HHID livestock*
drop if livestock_givenaway==. & livestock_sold==. & livestock_sold_price==. & livestock_slaughtered==.

* estimate value of livestock income with estimated prices from sales
* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by livestock_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if livestock_sold_price~=.
by livestock_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if livestock_sold_price~=.
by livestock_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if livestock_sold_price~=.
by livestock_code rural region subregion district, sort : egen float count_4 = count(1) if livestock_sold_price~=.
by livestock_code rural region subregion, sort : egen float count_5 = count(1) if livestock_sold_price~=.
by livestock_code rural region, sort : egen float count_6 = count(1) if livestock_sold_price~=.
by livestock_code rural, sort : egen float count_7 = count(1) if livestock_sold_price~=.
by livestock_code, sort : egen float count_8 = count(1) if livestock_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by livestock_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region subregion district, sort : egen float temp_est_price_4 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region subregion, sort : egen float temp_est_price_5 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural region, sort : egen float temp_est_price_6 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code rural, sort : egen float temp_est_price_7 = median (livestock_sold_price) if livestock_sold_price~=.
by livestock_code, sort : egen float temp_est_price_8 = median (livestock_sold_price) if livestock_sold_price~=.
mvencode temp_*, mv(0)
by livestock_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by livestock_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by livestock_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by livestock_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by livestock_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by livestock_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by livestock_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by livestock_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
mvdecode livestock_* est_price, mv(0)
mvencode livestock_* est_price, mv(0)
gen livestock_to_income = livestock_givenaway+livestock_sold+livestock_slaughtered
gen livestock_income_yr = livestock_to_income*est_price
****
**** Do not know the number of slaughtered that were sold
gen livestock_sold_yr = livestock_sold*est_price

keep ag_HHID livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (ag_HHID)
drop if livestock_income==0 & livestock_sold==0

save "clean data/intermediate/Uganda1314_livestock_cattle.dta", replace

* General sm_livestock
use "Raw Data/AGSEC6B.dta", clear
rename HHID ag_HHID
rename ALiveStock_Small_ID sm_livestock_code
rename a6bq9 sm_livestock_givenaway
rename a6bq14a sm_livestock_sold
rename a6bq14b sm_livestock_sold_price
rename a6bq15 sm_livestock_slaughtered
keep ag_HHID sm_livestock*
drop if sm_livestock_givenaway==. & sm_livestock_sold==. & sm_livestock_sold_price==. & sm_livestock_slaughtered==.

* estimate value of sm_livestock income with estimated prices from sales
* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by sm_livestock_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district, sort : egen float count_4 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion, sort : egen float count_5 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural region, sort : egen float count_6 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code rural, sort : egen float count_7 = count(1) if sm_livestock_sold_price~=.
by sm_livestock_code, sort : egen float count_8 = count(1) if sm_livestock_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by sm_livestock_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion district, sort : egen float temp_est_price_4 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural region subregion, sort : egen float temp_est_price_5 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural region, sort : egen float temp_est_price_6 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code rural, sort : egen float temp_est_price_7 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
by sm_livestock_code, sort : egen float temp_est_price_8 = median (sm_livestock_sold_price) if sm_livestock_sold_price~=.
mvencode temp_*, mv(0)
by sm_livestock_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by sm_livestock_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by sm_livestock_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by sm_livestock_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by sm_livestock_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by sm_livestock_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by sm_livestock_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by sm_livestock_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
mvdecode sm_livestock_* est_price, mv(0)
mvencode sm_livestock_* est_price, mv(0)
gen sm_livestock_to_income = sm_livestock_givenaway+sm_livestock_sold+sm_livestock_slaughtered
gen sm_livestock_income_yr = sm_livestock_to_income*est_price
****
**** Do not know the number of slaughtered that were sold
gen sm_livestock_sold_yr = sm_livestock_sold*est_price

keep ag_HHID sm_livestock_income_yr sm_livestock_sold_yr

gen sm_livestock_income = sm_livestock_income_yr/365
gen sm_livestock_sold = sm_livestock_sold_yr/365

collapse (sum) sm_livestock_*, by (ag_HHID)
drop if sm_livestock_income==0 & sm_livestock_sold==0

save "clean data/intermediate/Uganda1314_sm_livestock.dta", replace

* General poultry
use "Raw Data/AGSEC6C.dta", clear
rename HHID ag_HHID
rename APCode poultry_code
rename a6cq9 poultry_givenaway
rename a6cq14a poultry_sold
rename a6cq14b poultry_sold_price
rename a6cq15 poultry_slaughtered
keep ag_HHID poultry*
drop if poultry_givenaway==. & poultry_sold==. & poultry_sold_price==. & poultry_slaughtered==.

* estimate value of poultry income with estimated prices from sales
* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by poultry_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if poultry_sold_price~=.
by poultry_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if poultry_sold_price~=.
by poultry_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if poultry_sold_price~=.
by poultry_code rural region subregion district, sort : egen float count_4 = count(1) if poultry_sold_price~=.
by poultry_code rural region subregion, sort : egen float count_5 = count(1) if poultry_sold_price~=.
by poultry_code rural region, sort : egen float count_6 = count(1) if poultry_sold_price~=.
by poultry_code rural, sort : egen float count_7 = count(1) if poultry_sold_price~=.
by poultry_code, sort : egen float count_8 = count(1) if poultry_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by poultry_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural region subregion district, sort : egen float temp_est_price_4 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural region subregion, sort : egen float temp_est_price_5 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural region, sort : egen float temp_est_price_6 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code rural, sort : egen float temp_est_price_7 = median (poultry_sold_price) if poultry_sold_price~=.
by poultry_code, sort : egen float temp_est_price_8 = median (poultry_sold_price) if poultry_sold_price~=.
mvencode temp_*, mv(0)
by poultry_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by poultry_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by poultry_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by poultry_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by poultry_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by poultry_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by poultry_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by poultry_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
mvdecode poultry_* est_price, mv(0)
mvencode poultry_* est_price, mv(0)
gen poultry_to_income = poultry_givenaway+poultry_sold+poultry_slaughtered
gen poultry_income_yr = poultry_to_income*est_price
****
**** Do not know the number of slaughtered that were sold
gen poultry_sold_yr = poultry_sold*est_price

keep ag_HHID poultry_income_yr poultry_sold_yr

gen poultry_income = poultry_income_yr/365
gen poultry_sold = poultry_sold_yr/365

collapse (sum) poultry_*, by (ag_HHID)
drop if poultry_income==0 & poultry_sold==0

save "clean data/intermediate/Uganda1314_poultry.dta", replace

* Meat
use "Raw Data/AGSEC8A.dta", clear
rename HHID ag_HHID
rename AGroup_ID meat_code
rename a8aq3 meat_sold
rename a8aq5 meat_sold_value
keep ag_HHID meat*
mvdecode meat*, mv(0)
drop if meat_sold==. & meat_sold_value==.

* estimate value of livestock income with estimated prices from sales
gen meat_sold_price = meat_sold_value / meat_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by meat_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if meat_sold_price~=.
by meat_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if meat_sold_price~=.
by meat_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if meat_sold_price~=.
by meat_code rural region subregion district, sort : egen float count_4 = count(1) if meat_sold_price~=.
by meat_code rural region subregion, sort : egen float count_5 = count(1) if meat_sold_price~=.
by meat_code rural region, sort : egen float count_6 = count(1) if meat_sold_price~=.
by meat_code rural, sort : egen float count_7 = count(1) if meat_sold_price~=.
by meat_code, sort : egen float count_8 = count(1) if meat_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by meat_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural region subregion district, sort : egen float temp_est_price_4 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural region subregion, sort : egen float temp_est_price_5 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural region, sort : egen float temp_est_price_6 = median (meat_sold_price) if meat_sold_price~=.
by meat_code rural, sort : egen float temp_est_price_7 = median (meat_sold_price) if meat_sold_price~=.
by meat_code, sort : egen float temp_est_price_8 = median (meat_sold_price) if meat_sold_price~=.
mvencode temp_*, mv(0)
by meat_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by meat_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by meat_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by meat_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by meat_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by meat_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by meat_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by meat_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track
mvdecode meat_* est_price, mv(0)
mvencode meat_* est_price, mv(0)
gen meat_sold_yr = meat_sold*est_price

keep ag_HHID meat_sold_yr

gen meat_sold = meat_sold_yr/365

collapse (sum) meat_*, by (ag_HHID)
drop if meat_sold==0

save "clean data/intermediate/Uganda1314_meat_sold.dta", replace

* Milk
use "Raw Data/AGSEC8B.dta", clear
rename HHID ag_HHID
rename AGroup_ID milk_code
rename a8bq1 milk_num_animals
rename a8bq2 milk_months_prod
rename a8bq3 milk_qty_prod
rename a8bq5_1 milk_qty_sold
rename a8bq6 milk_qty_input
* Data errors where values were placed in qty data
replace milk_qty_input = . if milk_qty_input>5
rename a8bq7 milk_proc_qty_sold
rename a8bq9 milk_value_sold
keep ag_HHID milk*
mvdecode milk*, mv(0)
drop if milk_qty_prod==. & milk_qty_sold==. & milk_proc_qty_sold==. & milk_value_sold==.

* estimate value of milk income with estimated prices from sales (income is pooled for milk & milk products, so only capture from individual sales
gen milk_sold_price = milk_value_sold / (milk_qty_sold*milk_months_prod*30.4) if milk_proc_qty_sold==.
gen milk_proc_sold_price = milk_value_sold / (milk_proc_qty_sold*milk_months_prod*30.4) if milk_qty_sold==.

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by milk_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if milk_sold_price~=.
by milk_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if milk_sold_price~=.
by milk_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if milk_sold_price~=.
by milk_code rural region subregion district, sort : egen float count_4 = count(1) if milk_sold_price~=.
by milk_code rural region subregion, sort : egen float count_5 = count(1) if milk_sold_price~=.
by milk_code rural region, sort : egen float count_6 = count(1) if milk_sold_price~=.
by milk_code rural, sort : egen float count_7 = count(1) if milk_sold_price~=.
by milk_code, sort : egen float count_8 = count(1) if milk_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by milk_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural region subregion district, sort : egen float temp_est_price_4 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural region subregion, sort : egen float temp_est_price_5 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural region, sort : egen float temp_est_price_6 = median (milk_sold_price) if milk_sold_price~=.
by milk_code rural, sort : egen float temp_est_price_7 = median (milk_sold_price) if milk_sold_price~=.
by milk_code, sort : egen float temp_est_price_8 = median (milk_sold_price) if milk_sold_price~=.
mvencode temp_*, mv(0)
by milk_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by milk_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by milk_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by milk_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by milk_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by milk_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by milk_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by milk_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track

* Only 10 prices of processed milk
by milk_code, sort : egen float temp_est_price = median (milk_proc_sold_price) if milk_proc_sold_price~=.
mvencode temp_*, mv(0)
by milk_code, sort : egen float est_proc_price = max(temp_est_price)

mvdecode milk_* est_*, mv(0)
mvencode milk_* est_*, mv(0)

gen milk_income_yr = (milk_num_animals*milk_qty_prod-milk_qty_input)*milk_months_prod*30.4*est_price + milk_proc_qty_sold*milk_months_prod*30.4*est_proc_price
gen milk_sold_yr = milk_qty_sold*milk_months_prod*30.4*est_price + milk_proc_qty_sold*milk_months_prod*30.4*est_proc_price

keep ag_HHID milk_income_yr milk_sold_yr

gen milk_income = milk_income_yr/365
gen milk_sold = milk_sold_yr/365

collapse (sum) milk_*, by (ag_HHID)
drop if milk_income==0 & milk_sold==0

save "clean data/intermediate/Uganda1314_milk.dta", replace

* Eggs
use "Raw Data/AGSEC8C.dta", clear
rename HHID ag_HHID
rename AGroup_ID eggs_code
rename a8cq2 eggs_qty_prod
rename a8cq3 eggs_qty_sold
rename a8cq5 eggs_value_sold
keep ag_HHID eggs*
mvdecode eggs*, mv(0)
drop if eggs_qty_prod==. & eggs_qty_sold==. & eggs_value_sold==.

* estimate value of eggs income with estimated prices from sales (income is pooled for eggs & eggs products, so only capture from individual sales
gen eggs_sold_price = eggs_value_sold / eggs_qty_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby ag_HHID using "clean data/Uganda1314_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by eggs_code rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if eggs_sold_price~=.
by eggs_code rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if eggs_sold_price~=.
by eggs_code rural region subregion district subcounty, sort : egen float count_3 = count(1) if eggs_sold_price~=.
by eggs_code rural region subregion district, sort : egen float count_4 = count(1) if eggs_sold_price~=.
by eggs_code rural region subregion, sort : egen float count_5 = count(1) if eggs_sold_price~=.
by eggs_code rural region, sort : egen float count_6 = count(1) if eggs_sold_price~=.
by eggs_code rural, sort : egen float count_7 = count(1) if eggs_sold_price~=.
by eggs_code, sort : egen float count_8 = count(1) if eggs_sold_price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by eggs_code rural region subregion district subcounty parish ea, sort : egen float temp_est_price_1 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region subregion district subcounty parish, sort : egen float temp_est_price_2 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region subregion district subcounty, sort : egen float temp_est_price_3 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region subregion district, sort : egen float temp_est_price_4 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region subregion, sort : egen float temp_est_price_5 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural region, sort : egen float temp_est_price_6 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code rural, sort : egen float temp_est_price_7 = median (eggs_sold_price) if eggs_sold_price~=.
by eggs_code, sort : egen float temp_est_price_8 = median (eggs_sold_price) if eggs_sold_price~=.
mvencode temp_*, mv(0)
by eggs_code rural region subregion district subcounty parish ea, sort : egen float est_price_1 = max(temp_est_price_1)
by eggs_code rural region subregion district subcounty parish, sort : egen float est_price_2 = max(temp_est_price_2)
by eggs_code rural region subregion district subcounty, sort : egen float est_price_3 = max(temp_est_price_3)
by eggs_code rural region subregion district, sort : egen float est_price_4 = max(temp_est_price_4)
by eggs_code rural region subregion, sort : egen float est_price_5 = max(temp_est_price_5)
by eggs_code rural region, sort : egen float est_price_6 = max(temp_est_price_6)
by eggs_code rural, sort : egen float est_price_7 = max(temp_est_price_7)
by eggs_code, sort : egen float est_price_8 = max(temp_est_price_8)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_price==.
replace est_price = est_price_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_price==.
replace est_price = est_price_7 if track==7
replace track = 8 if est_price==.
replace est_price = est_price_8 if track==8
drop est_price_* count_* track

mvdecode eggs_* est_*, mv(0)
mvencode eggs_* est_*, mv(0)

gen eggs_income_yr = 4*eggs_qty_prod*est_price
gen eggs_sold_yr = 4*eggs_qty_sold*est_price

keep ag_HHID eggs_income_yr eggs_sold_yr

gen eggs_income = eggs_income_yr/365
gen eggs_sold = eggs_sold_yr/365

collapse (sum) eggs_*, by (ag_HHID)
drop if eggs_income==0 & eggs_sold==0

save "clean data/intermediate/Uganda1314_eggs.dta", replace

* Animal Other
use "Raw Data/AGSEC11.dta", clear
rename HHID ag_HHID
rename a11q1c livestock_dung_yr
rename a11q5 livestock_use_income_yr
keep ag_HHID livestock_dung_yr livestock_use_income_yr
mvdecode livestock_dung_yr livestock_use_income_yr, mv(0)
mvencode livestock_dung_yr livestock_use_income_yr, mv(0)
drop if livestock_dung_yr==0 & livestock_use_income_yr==0

gen livestock_other_nfegi_yr = livestock_dung_yr + livestock_use_income_yr

collapse (sum) livestock_other_nfegi_yr, by (ag_HHID)

gen livestock_other_nfegi = livestock_other_nfegi_yr/365

save "clean data/intermediate/Uganda1314_livestock_other.dta", replace


** Non-Employment Income
use "raw data/GSEC11B.dta", clear
gen non_emp_income = 0
replace non_emp_income = non_emp_income+h11q5 if h11q5~=.
replace non_emp_income = non_emp_income+h11q6 if h11q6~=.

gen d_remittances = 1 if h11q2==42 & h11q5>0 & h11q5~=.
replace d_remittances = 1 if h11q2==42 & h11q6>0 & h11q6~=.
replace d_remittances = 1 if h11q2==43 & h11q5>0 & h11q5~=.
replace d_remittances = 1 if h11q2==43 & h11q6>0 & h11q6~=.
replace d_remittances = 0 if d_remittances==.

collapse (sum) non_emp_income (max) d_remittances, by (HHID)
replace non_emp_income = non_emp_income/365
save "clean data/intermediate/Uganda1314_other_inc.dta", replace

* Costs
* Seeds
use "raw data/AGSEC4A.dta", clear
rename HHID ag_HHID
rename a4aq15 cost_seeds
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_seeds, by (ag_HHID)
drop if cost_seeds==0
save "clean data/intermediate/Uganda1314_costs_seeds_1.dta", replace
use "raw data/AGSEC4B.dta", clear
rename HHID ag_HHID
rename a4bq15 cost_seeds
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_seeds, by (ag_HHID)
drop if cost_seeds==0
append using "clean data/intermediate/Uganda1314_costs_seeds_1.dta"
collapse (sum) cost_seeds, by (ag_HHID)
save "clean data/intermediate/Uganda1314_costs_seeds.dta", replace

* Fertilizer
use "raw data/AGSEC3A.dta", clear
rename HHID ag_HHID
mvdecode a3aq8 a3aq18 a3aq27, mv(0)
mvencode a3aq8 a3aq18 a3aq27, mv(0)
gen cost_fertilizer = a3aq8 + a3aq18 + a3aq27
collapse (sum) cost_fertilizer, by (ag_HHID)
drop if cost_fertilizer==0
save "clean data/intermediate/Uganda1314_costs_fertilizer_1.dta", replace
use "raw data/AGSEC3B.dta", clear
rename HHID ag_HHID
mvdecode a3bq8 a3bq18 a3bq27, mv(0)
mvencode a3bq8 a3bq18 a3bq27, mv(0)
gen cost_fertilizer = a3bq8 + a3bq18 + a3bq27
collapse (sum) cost_fertilizer, by (ag_HHID)
drop if cost_fertilizer==0
append using "clean data/intermediate/Uganda1314_costs_fertilizer_1.dta"
collapse (sum) cost_fertilizer, by (ag_HHID)
save "clean data/intermediate/Uganda1314_costs_fertilizer.dta", replace

* Hired Crop Labor
use "raw data/AGSEC3A.dta", clear
rename HHID ag_HHID
mvdecode a3aq35a a3aq35b a3aq35c a3aq36, mv(0)
mvencode a3aq35a a3aq35b a3aq35c a3aq36, mv(0)
gen labor_days = a3aq35a+a3aq35b+a3aq35c
gen cost_crop_labor = a3aq36 * labor_days
collapse (sum) cost_crop_labor, by (ag_HHID)
drop if cost_crop_labor==0
save "clean data/intermediate/Uganda1314_costs_crop_labor_1.dta", replace
use "raw data/AGSEC3B.dta", clear
rename HHID ag_HHID
mvdecode a3bq35a a3bq35b a3bq35c a3bq36, mv(0)
mvencode a3bq35a a3bq35b a3bq35c a3bq36, mv(0)
gen labor_days = a3bq35a+a3bq35b+a3bq35c
gen cost_crop_labor = a3bq36 * labor_days
collapse (sum) cost_crop_labor, by (ag_HHID)
drop if cost_crop_labor==0
append using "clean data/intermediate/Uganda1314_costs_crop_labor_1.dta"
collapse (sum) cost_crop_labor, by (ag_HHID)
save "clean data/intermediate/Uganda1314_costs_crop_labor.dta", replace

* Hired Animal Labor & Animal Feed
use "raw data/AGSEC6A.dta", clear
rename HHID ag_HHID
rename a6aq5c cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_animal_labor==0
save "clean data/intermediate/Uganda1314_costs_animal_labor_1.dta", replace
use "raw data/AGSEC6B.dta", clear
rename HHID ag_HHID
rename a6bq5c cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_animal_labor==0
save "clean data/intermediate/Uganda1314_costs_animal_labor_2.dta", replace
use "raw data/AGSEC6C.dta", clear
rename HHID ag_HHID
rename a6cq5c cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_animal_labor==0
append using "clean data/intermediate/Uganda1314_costs_animal_labor_1.dta"
append using "clean data/intermediate/Uganda1314_costs_animal_labor_2.dta"
collapse (sum) cost_*, by (ag_HHID)
save "clean data/intermediate/Uganda1314_costs_animal_labor.dta", replace

* Animal Feed
use "raw data/AGSEC7.dta", clear
rename HHID ag_HHID
mvdecode a7bq2e a7bq3f, mv(0)
mvencode a7bq2e a7bq3f, mv(0)
gen cost_animal_feed = a7bq2e + a7bq3f
collapse (sum) cost_animal_feed, by (ag_HHID)
drop if cost_animal_feed==0
save "clean data/intermediate/Uganda1314_costs_animal_feed.dta", replace

* Land Rent
use "raw data/AGSEC2B.dta", clear
rename HHID ag_HHID
rename a2bq9 cost_land_rent
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_land_rent==0
save "clean data/intermediate/Uganda1314_costs_land_rent.dta", replace

* Machine & Animal Rent
use "raw data/AGSEC10.dta", clear
rename HHID ag_HHID
rename a10q8 cost_machine_rent
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_machine_rent==0
save "clean data/intermediate/Uganda1314_costs_machine_rent.dta", replace

* Transport Costs
use "raw data/AGSEC5A.dta", clear
rename HHID ag_HHID
rename a5aq10 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_transport==0
save "clean data/intermediate/Uganda1314_costs_transport_1.dta", replace
use "raw data/AGSEC5B.dta", clear
rename HHID ag_HHID
rename a5bq10 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
drop if cost_transport==0
append using "clean data/intermediate/Uganda1314_costs_transport_1.dta"
collapse (sum) cost_*, by (ag_HHID)
save "clean data/intermediate/Uganda1314_costs_transport.dta", replace


* Aggregate Costs
use "clean data/intermediate/Uganda1314_costs_seeds.dta", clear
joinby using "clean data/intermediate/Uganda1314_costs_fertilizer.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_crop_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_land_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_machine_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_animal_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_animal_feed.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Uganda1314_costs_transport.dta", unmatched(both) _merge(_merge)
drop _merge

* Convert Missing to zero
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (ag_HHID)
local costs "cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach c in `costs' {
	replace `c' = `c'/365
}
*

save "clean data/intermediate/Uganda1314_costs.dta", replace

**** COMBINE ALL ****

use "clean data/Uganda1314_demographics.dta", clear
joinby HHID using "clean data/intermediate/Uganda1314_nfe_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID using "clean data/intermediate/Uganda1314_wage_incCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID using "clean data/intermediate/Uganda1314_farm_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID using "clean data/intermediate/Uganda1314_farmsales_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_livestock_cattle.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_sm_livestock.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_poultry.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_meat_sold.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_milk_cattle.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_eggs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_livestock_other.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID using "clean data/intermediate/Uganda1314_other_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby ag_HHID using "clean data/intermediate/Uganda1314_costs.dta", unmatched(both) _merge(_merge)
drop _merge


mvdecode nfe_* wage_* harvest_value sales_value livestock_* sm_* poultry_* meat_sold milk_* eggs_* non_emp_income d_remittances cost_*, mv(0)
mvencode nfe_* wage_* harvest_value sales_value livestock_* sm_* poultry_* meat_sold milk_* eggs_* non_emp_income d_remittances cost_*, mv(0)

* Convert to Per Capita USD constant PPP
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold sm_livestock_income sm_livestock_sold poultry_income poultry_sold meat_sold milk_income milk_sold eggs_income eggs_sold livestock_other_nfegi non_emp_income cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 3/1/2014
	gen USD_`u' = `u'*0.0003952569
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2017 data not available
	* 1/3*(2013 GNI per capita constant 2011 PPP / 2013 GNI per capita Atlas Method)+2/3*(2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method)
	replace USD_`u' = USD_`u'*2.46396102
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 3/1/2014
	gen USD_`usd'`u' = `usd'`u'*0.0003952569
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2017 data not available
	* 1/3*(2013 GNI per capita constant 2011 PPP / 2013 GNI per capita Atlas Method)+2/3*(2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method)
	replace USD_`usd'`u' = USD_`usd'`u'*2.46396102
}
}
*	
* Drop former variables
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold sm_livestock_income sm_livestock_sold poultry_income poultry_sold meat_sold milk_income milk_sold eggs_income eggs_sold livestock_other_nfegi non_emp_income cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	
* Rename
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold sm_livestock_income sm_livestock_sold poultry_income poultry_sold meat_sold milk_income milk_sold eggs_income eggs_sold livestock_other_nfegi non_emp_income cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_transport"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	

* Impute farm income components before creating the aggregate variable
local varlist harvest_value livestock_income sm_livestock_income poultry_income meat_sold milk_income eggs_income
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}

replace sales_value=harvest_value if sales_value>harvest_value & sales_value!=.
replace livestock_sold=livestock_income if livestock_sold>livestock_income & livestock_sold!=.
replace sm_livestock_sold=sm_livestock_income if sm_livestock_sold>sm_livestock_income & sm_livestock_sold!=.
replace poultry_sold=poultry_income if poultry_sold>poultry_income & poultry_sold!=.
replace milk_sold=milk_income if milk_sold>milk_income & milk_sold!=.
replace eggs_sold=eggs_income if eggs_sold>eggs_income & eggs_sold!=.


gen farm_sales = sales_value + livestock_sold + sm_livestock_sold + poultry_sold + meat_sold + milk_sold + eggs_sold
gen own_farm_crop_inc_costs = cost_seeds + cost_fertilizer + cost_crop_labor + cost_land_rent + cost_machine_rent + cost_transport
gen own_farm_livestock_inc_costs = cost_animal_labor + cost_animal_feed 
gen own_farm_crop_inc_gross = harvest_value
gen own_farm_livestock_inc_gross = livestock_income + sm_livestock_income + poultry_income + milk_income + eggs_income+meat_sold
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

* Bring in livestock enterprise income
local livestock_nfe "nfe_gross_inc nfe_gi_c2 nfe_net_inc nfe_i_c2"
foreach ln in `livestock_nfe' {
	replace `ln' = `ln' + livestock_other_nfegi
}
*
** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + non_emp_income
}
*

joinby using "clean data/Uganda1314_hhid.dta", unmatched(none)

rename d_remittances remit

preserve

use "raw data/UNPS 2013-14 Consumption Aggregate.dta" , clear

duplicates drop HHID, force

tempfile consumption_aggregate
save `consumption_aggregate',replace
restore

merge 1:1 HHID using `consumption_aggregate'

g hhexp = nrrexp30
g pcexp = nrrexp30/hhsize

replace pcexp = pcexp/30.4
replace hhexp = hhexp/30.4

foreach u of varlist pcexp hhexp {
	gen USD_`u' = `u'*0.0003952569
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2017 data not available
	* 1/3*(2013 GNI per capita constant 2011 PPP / 2013 GNI per capita Atlas Method)+2/3*(2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method)
	replace USD_`u' = USD_`u'*2.46396102
	drop `u'
	ren USD_`u' `u'
}

g poor = (pcexp<1.9)

drop hhweight
ren panel_hhweight hhweight

//(!) keep only necessary variables !Ce removed t_* and others that won't be needed
keep HHID hhid rural hhweight hhsize youth_share_24 youth_share_34 female_head school_secondary_hh ///
educ_max credit land_owned job1 job2 /*Ninformal_agri Ninformal_nonagri Nformal_agri Nformal_nonagri*/ ///
Ninformal N_pension N_health_insurance /*t_wagepaid t_nonfarmbiz t_farmbiz t_wageunpaid t_any_w_paidunpaid*/ ///
informal informalcon informalsp /*informal_agri informal_nonagri formal_agri formal_nonagri*/ ///
formal formalcon formalsp sh_pension sh_health_insurance remit pcexp ///
nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales own_farm* i_gi* i_ni* i_nnni*  ///
nfe_gross_inc nfe_costs nfe_net_inc farm_sales

save "clean data/UGANDA_incomesharesCHEVAL.dta", replace

ex
