********************************************************************************
***************** NIGERIA 2012: Get expenditure ***************
********************************************************************************

/*
This file gets the total household expenditure and defines households as poor.

The dataset used for this is:
Nigeria 2012 General Household Survey-Panel Wave 2

Stata Version 16.1
-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "$clean/NIGERIAIA12_expenditure.dta", replace

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\NGA_2012_GHSP-W2_v02_M_STATA"

gl DO   	    ${NIGERIA}/do
gl raw   		${NIGERIA}/raw data 
gl clean 	"${NIGERIA}/clean data"
********************************************************************************

use "$raw/cons_agg_wave2_visit1.dta", clear

* Convert expenditures variables to  USD constant PPP
g pcexp= totcons

foreach u of varlist pcexp  {
	* Currency conversion from XE.com 31/12/2012
	gen USD_`u' = `u'*0.0064040840
	* Constant 2011 PPP conversion using World Bank GNI Data 
	* Average of 2012 and 2013 GNI per capita constant 2011 PPP / Average of 2012 and 2013 GNI per capita Atlas Method [((5065.762253+5205.294235)/2) / ((2480+2700)/2)] (Source WDI)
	replace USD_`u' = USD_`u'*1.9828294  
	drop `u'
	ren USD_`u' `u'
	replace `u' = `u'/365 //Daily expenditure to later define poverty
	}
	
keep hhid pcexp totcons

merge 1:1 hhid using "$raw/NGA_HouseholdGeovars_Y2", nogen keep(1 2 3)

joinby using "$clean/Nigeria12_demographics.dta", unmatched(none)

g year=2012

save "$clean/NIGERIA12_expenditure.dta", replace

 