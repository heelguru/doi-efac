********************************************************************************
***************** NIGER 2014: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Niger 2014 General Household Survey-Panel Wave 3

generates 
save "$CLEAN\NIGER_FTEs.dta", replace

Stata Version 15.1

Create the full time equivalents of hours worked by employment types (formal/informal) 
to inform about how households distribute their time across formal and informal activities;

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGER	"$path/data\Niger_ECVMA_14"

gl DO   	    ${NIGER}/do
gl raw data   	${NIGER}/raw data 
gl clean data 	${NIGER}/clean data
gl FIG 		    ${NIGER}/graphs
gl TAB 		    ${NIGER}/tables
 
********************************************************************************
cd "$NIGER"

** Occupation by labor module (Separate labor Occupations to own observations)
** Main labor (worked during past 7 days)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q23 CIC
rename MS04Q25 months
rename MS04Q26 weekspm
rename MS04Q27 dayspw
rename MS04Q28 hourspd
rename MS04Q29 type
/*
types 1-6 are wage employment
type 7 is a "patron", which I interpret to be a local cheif
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q31 contract
rename MS04Q38 pension
rename MS04Q40 insurance
rename MS04Q32A ann_income
rename MS04Q32B inc_period
rename MS04Q35A ann_inkind_benefit
rename MS04Q35B inkind_period
rename MS04Q37A ann_food_benefit
rename MS04Q37B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 1
save "clean data/intermediate/Niger14_temp_labor_1.dta", replace

** Secondary labor(worked during past 7 days)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q49 CIC
rename MS04Q51 months
rename MS04Q51B weekspm
rename MS04Q52 dayspw
rename MS04Q53 hourspd
rename MS04Q54 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q56 contract
rename MS04Q63 pension
rename MS04Q65 insurance
rename MS04Q57A ann_income
rename MS04Q57B inc_period
rename MS04Q60A ann_inkind_benefit
rename MS04Q60B inkind_period
rename MS04Q62A ann_food_benefit
rename MS04Q62B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 2
save "clean data/intermediate/Niger14_temp_labor_2.dta", replace

** Main labor (worked during past 12 months)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q69 CIC
rename MS04Q71 months
rename MS04Q71B weekspm
rename MS04Q72 dayspw
rename MS04Q73 hourspd
rename MS04Q74 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q76 contract
rename MS04Q83 pension
rename MS04Q84 paidleave	
rename MS04Q85 insurance
rename MS04Q77A ann_income
rename MS04Q77B inc_period
rename MS04Q80A ann_inkind_benefit
rename MS04Q80B inkind_period
rename MS04Q82A ann_food_benefit
rename MS04Q82B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period paidleave 
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period paidleave 
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 3
save "clean data/intermediate/Niger14_temp_labor_3.dta", replace

** Secondary labor (worked during past 12 months)
use "raw data/ECVMA2_MS04P1.dta", clear
rename MS04Q00 PID
rename MS04Q89 CIC 
rename MS04Q91 months
rename MS04Q91B weekspm
rename MS04Q92 dayspw
rename MS04Q93 hourspd
rename MS04Q94 type
/*
types 1-6 are wage employment
type 7 is employer
type 8 is enterprise
type 9 is work on the household
*/
rename MS04Q96 contract
rename MS04Q103 pension		
rename MS04Q105 insurance	
rename MS04Q97A ann_income
rename MS04Q97B inc_period
rename MS04Q100A ann_inkind_benefit
rename MS04Q100B inkind_period
rename MS04Q102A ann_food_benefit
rename MS04Q102B food_period
keep GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period 
order GRAPPE MENAGE EXTENSION PID CIC months weekspm dayspw hourspd type contract pension insurance ann_income inc_period ann_inkind_benefit inkind_period ann_food_benefit food_period 
drop if months==. & weekspm==. & dayspw==. & hourspd==. & ann_income==. & ann_inkind_benefit==. & ann_food_benefit==.
gen labor = 4
save "clean data/intermediate/Niger14_temp_labor_4.dta", replace

use "clean data/intermediate/Niger14_temp_labor_1.dta", clear
append using "clean data/intermediate/Niger14_temp_labor_2.dta"
append using "clean data/intermediate/Niger14_temp_labor_3.dta"
append using "clean data/intermediate/Niger14_temp_labor_4.dta"

* Clear Errors in data
* Only 7 days in week (886/13,905)
replace dayspw=. if dayspw>7
**** Assume that reported hours per day above 16 hours per day are in error, so adjust to 16 hours per day. (affects calculations of 6 of 13,905 observations)
replace hourspd = . if hourspd>16
**** Use median of days per week by occupation type of those in the smallest geographic region with 10 observations of monthly labor
joinby using "clean data/Niger14_member_demos.dta", unmatched(none)
* Count observations with dayspw reported by geographic location.
by type CIC female rural region department town ea, sort : egen float count_1 = count(1) if dayspw~=.
by type CIC female rural region department town, sort : egen float count_2 = count(1) if dayspw~=.
by type CIC female rural region department, sort : egen float count_3 = count(1) if dayspw~=.
by type CIC female rural region, sort : egen float count_4 = count(1) if dayspw~=.
by type CIC female rural, sort : egen float count_5 = count(1) if dayspw~=.
* Calculate median of dayspw by geographic location - later used for predictive purposes.
by type CIC female rural region department town ea, sort : egen float temp_est_dayspw_1 = median(dayspw) if dayspw~=.
by type CIC female rural region department town, sort : egen float temp_est_dayspw_2 = median(dayspw) if dayspw~=.
by type CIC female rural region department, sort : egen float temp_est_dayspw_3 = median(dayspw) if dayspw~=.
by type CIC female rural region, sort : egen float temp_est_dayspw_4 = median(dayspw) if dayspw~=.
by type CIC female rural, sort : egen float temp_est_dayspw_5 = median(dayspw) if dayspw~=.
mvencode temp_*, mv(0)
by type CIC female rural region department town ea, sort : egen float est_dayspw_1 = max(temp_est_dayspw_1)
by type CIC female rural region department town, sort : egen float est_dayspw_2 = max(temp_est_dayspw_2)
by type CIC female rural region department, sort : egen float est_dayspw_3 = max(temp_est_dayspw_3)
by type CIC female rural region, sort : egen float est_dayspw_4 = max(temp_est_dayspw_4)
by type CIC female rural, sort : egen float est_dayspw_5 = max(temp_est_dayspw_5)
drop temp*
* Build dayspw prediction variable if at least 10 observations of reported dayspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_dayspw = est_dayspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_dayspw==.
replace est_dayspw = est_dayspw_4 if track==4
replace track = 5 if est_dayspw==.
replace est_dayspw = est_dayspw_5 if track==5
drop est_dayspw_* count_* track
**** Replace missing value of dayspw (813 changes)
replace dayspw = est_dayspw if dayspw==.
drop est_*
* If cannot use other observations to estimate, assume 7 days, as unrealistic days per week were all above 7 (Changes: 53)
replace dayspw = 7 if dayspw==.
save "clean data/intermediate/Niger14_temp_labor.dta", replace


** Wage employment
use "clean data/intermediate/Niger14_temp_labor.dta", clear
keep if type<7 

* Casual vs Formal Wage
gen formal = 0
replace formal = 1 if inc_period>3 & inc_period<.
replace formal = 1 if inkind_period>3 & inkind_period<.
replace formal = 1 if contract==1
replace formal = 1 if pension==1
replace formal = 1 if insurance==1
replace formal = 1 if paidleave==1 	

gen formalcon = 0 //added definition of "formality" due to contract structure
replace formalcon = 1 if contract==1
replace formalcon = 1 if paidleave==1 	//choice to include receipt of  paid leave as "contract"

gen formalsp = 0 //added definition of "formality" due to SP
replace formalsp = 1 if pension==1
replace formalsp = 1 if insurance==1


* Generate Full Time Equivalents
gen fte_wage = months*weekspm*dayspw*hourspd/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2
ren insurance health_insurance
keep GRAPPE MENAGE EXTENSION PID CIC formal fte_wage formal* pension health_insurance

* FTEs by Formal
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

** inf2= the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
*
recode pension health_insurance (2=0)
* Collapse to HH / CIC level 
collapse (sum) fte* emp* formal* (max) pension health_insurance, by (GRAPPE MENAGE EXTENSION PID CIC)

* Bring in information for ISIC Groupings
joinby using "clean data/Niger14_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby GRAPPE MENAGE EXTENSION PID using "clean data/Niger14_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
/// for Niger 2014 the calculations are as follows
///   Rural -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
///   Urban -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
*******************************************************************************************
replace AFS_share = 0.7252143 if AFS_share==. & rural==1
replace AFS_share = 0.567008 if AFS_share==. & rural==0

** Generate Categorical Variables
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}

*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
forvalues w=1(1)2 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
}
*

* Collapse to the household level
collapse (sum) w_o_* w_f_* (max) pension health_insurance, by (GRAPPE MENAGE EXTENSION PID)

save "clean data/intermediate/Niger14_w_occ.dta", replace

** Occupations in Non-Farm Own Enterprises
* Using Data from Enterprise Module

* Convert to one observation per worker
use "raw data/ECVMA2_MS05BP1.dta", clear
rename MS05Q25 CIC
rename MS05Q67AA PID
rename MS05Q67AB months
rename MS05Q67AC dayspm
rename MS05Q67AD hourspd
keep GRAPPE MENAGE EXTENSION CIC PID months dayspm hourspd
drop if PID>89
gen enterprise = 1
save "clean data/intermediate/Niger14_temp_NFE_1_occ.dta", replace

use "raw data/ECVMA2_MS05BP1.dta", clear
rename MS05Q25 CIC
rename MS05Q67BA PID
rename MS05Q67BB months
rename MS05Q67BC dayspm
rename MS05Q67BD hourspd
keep GRAPPE MENAGE EXTENSION CIC PID months dayspm hourspd
drop if PID>89
gen enterprise = 2
save "clean data/intermediate/Niger14_temp_NFE_2_occ.dta", replace

use "raw data/ECVMA2_MS05BP1.dta", clear
rename MS05Q25 CIC
rename MS05Q67CA PID
rename MS05Q67CB months
rename MS05Q67CC dayspm
rename MS05Q67CD hourspd
keep GRAPPE MENAGE EXTENSION CIC PID months dayspm hourspd
drop if PID>89
gen enterprise = 3
save "clean data/intermediate/Niger14_temp_NFE_3_occ.dta", replace

use "raw data/ECVMA2_MS05BP1.dta", clear
rename MS05Q25 CIC
rename MS05Q67DA PID
rename MS05Q67DB months
rename MS05Q67DC dayspm
rename MS05Q67DD hourspd
keep GRAPPE MENAGE EXTENSION CIC PID months dayspm hourspd
drop if PID>89
gen enterprise = 4
save "clean data/intermediate/Niger14_temp_NFE_4_occ.dta", replace

use "clean data/intermediate/Niger14_temp_NFE_1_occ.dta", clear
append using "clean data/intermediate/Niger14_temp_NFE_2_occ.dta"
append using "clean data/intermediate/Niger14_temp_NFE_3_occ.dta"
append using "clean data/intermediate/Niger14_temp_NFE_4_occ.dta"

replace months = . if months==99
replace dayspm = . if dayspm==99
replace hourspd = . if hourspd==999
* assume that individual does not work more than 16 hours per day
replace hourspd = . if hourspd>16
* if hours per day is reported as zero, assume the worker works less than an hour a day (0.5 hours per day)
replace hourspd = 0.5 if hourspd==0


**** Use median of months by occupation of those in the smallest geographic region with 10 observations of monthly labor
joinby using "clean data/Niger14_member_demos.dta", unmatched(none)
* Count observations with months reported by geographic location.
by CIC female rural region department town ea, sort : egen float count_1 = count(1) if months~=.
by CIC female rural region department town, sort : egen float count_2 = count(1) if months~=.
by CIC female rural region department, sort : egen float count_3 = count(1) if months~=.
by CIC female rural region, sort : egen float count_4 = count(1) if months~=.
by CIC female rural, sort : egen float count_5 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by CIC female rural region department town ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by CIC female rural region department town, sort : egen float temp_est_months_2 = median(months) if months~=.
by CIC female rural region department, sort : egen float temp_est_months_3 = median(months) if months~=.
by CIC female rural region, sort : egen float temp_est_months_4 = median(months) if months~=.
by CIC female rural, sort : egen float temp_est_months_5 = median(months) if months~=.
mvencode temp_*, mv(0)
by CIC female rural region department town ea, sort : egen float est_months_1 = max(temp_est_months_1)
by CIC female rural region department town, sort : egen float est_months_2 = max(temp_est_months_2)
by CIC female rural region department, sort : egen float est_months_3 = max(temp_est_months_3)
by CIC female rural region, sort : egen float est_months_4 = max(temp_est_months_4)
by CIC female rural, sort : egen float est_months_5 = max(temp_est_months_5)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if est_months==.
replace est_months = est_months_5 if track==5
drop est_months_* count_* track
**** Replace missing value of months (5 changes)
replace months = est_months if months==.
replace months = 1 if months==.
drop est_*
* Count observations with dayspm reported by geographic location.
by CIC female rural region department town ea, sort : egen float count_1 = count(1) if dayspm~=.
by CIC female rural region department town, sort : egen float count_2 = count(1) if dayspm~=.
by CIC female rural region department, sort : egen float count_3 = count(1) if dayspm~=.
by CIC female rural region, sort : egen float count_4 = count(1) if dayspm~=.
by CIC female rural, sort : egen float count_5 = count(1) if dayspm~=.
* Calculate median of dayspm by geographic location - later used for predictive purposes.
by CIC female rural region department town ea, sort : egen float temp_est_dayspm_1 = median(dayspm) if dayspm~=.
by CIC female rural region department town, sort : egen float temp_est_dayspm_2 = median(dayspm) if dayspm~=.
by CIC female rural region department, sort : egen float temp_est_dayspm_3 = median(dayspm) if dayspm~=.
by CIC female rural region, sort : egen float temp_est_dayspm_4 = median(dayspm) if dayspm~=.
by CIC female rural, sort : egen float temp_est_dayspm_5 = median(dayspm) if dayspm~=.
mvencode temp_*, mv(0)
by CIC female rural region department town ea, sort : egen float est_dayspm_1 = max(temp_est_dayspm_1)
by CIC female rural region department town, sort : egen float est_dayspm_2 = max(temp_est_dayspm_2)
by CIC female rural region department, sort : egen float est_dayspm_3 = max(temp_est_dayspm_3)
by CIC female rural region, sort : egen float est_dayspm_4 = max(temp_est_dayspm_4)
by CIC female rural, sort : egen float est_dayspm_5 = max(temp_est_dayspm_5)
drop temp*
* Build dayspm prediction variable if at least 10 observations of reported dayspm in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_dayspm = est_dayspm_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_dayspm==.
replace est_dayspm = est_dayspm_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_dayspm==.
replace est_dayspm = est_dayspm_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_dayspm==.
replace est_dayspm = est_dayspm_4 if track==4
replace track = 5 if est_dayspm==.
replace est_dayspm = est_dayspm_5 if track==5
drop est_dayspm_* count_* track
**** Replace missing value of dayspm (6 changes)
replace dayspm = est_dayspm if dayspm==.
replace dayspm = 1 if dayspm==.
drop est_*
* Count observations with hourspd reported by geographic location.
by CIC female rural region department town ea, sort : egen float count_1 = count(1) if hourspd~=.
by CIC female rural region department town, sort : egen float count_2 = count(1) if hourspd~=.
by CIC female rural region department, sort : egen float count_3 = count(1) if hourspd~=.
by CIC female rural region, sort : egen float count_4 = count(1) if hourspd~=.
by CIC female rural, sort : egen float count_5 = count(1) if hourspd~=.
* Calculate median of hourspd by geographic location - later used for predictive purposes.
by CIC female rural region department town ea, sort : egen float temp_est_hourspd_1 = median(hourspd) if hourspd~=.
by CIC female rural region department town, sort : egen float temp_est_hourspd_2 = median(hourspd) if hourspd~=.
by CIC female rural region department, sort : egen float temp_est_hourspd_3 = median(hourspd) if hourspd~=.
by CIC female rural region, sort : egen float temp_est_hourspd_4 = median(hourspd) if hourspd~=.
by CIC female rural, sort : egen float temp_est_hourspd_5 = median(hourspd) if hourspd~=.
mvencode temp_*, mv(0)
by CIC female rural region department town ea, sort : egen float est_hourspd_1 = max(temp_est_hourspd_1)
by CIC female rural region department town, sort : egen float est_hourspd_2 = max(temp_est_hourspd_2)
by CIC female rural region department, sort : egen float est_hourspd_3 = max(temp_est_hourspd_3)
by CIC female rural region, sort : egen float est_hourspd_4 = max(temp_est_hourspd_4)
by CIC female rural, sort : egen float est_hourspd_5 = max(temp_est_hourspd_5)
drop temp*
* Build hourspd prediction variable if at least 10 observations of reported hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_hourspd = est_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_hourspd==.
replace est_hourspd = est_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_hourspd==.
replace est_hourspd = est_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_hourspd==.
replace est_hourspd = est_hourspd_4 if track==4
replace track = 5 if est_hourspd==.
replace est_hourspd = est_hourspd_5 if track==5
drop est_hourspd_* count_* track
**** Replace missing value of hourspd (75, 1 changes)
replace hourspd = est_hourspd if hourspd==.
replace hourspd = 1 if hourspd==.
drop est_*

* Gen FTE
gen fte_nfe = months*dayspm*hourspd/2016
mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
replace fte_nfe = 2 if fte_nfe>2
* 58 Changes

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe>0

* Collapse to HH / CIC level
collapse (sum) fte_nfe emp_nfe, by (GRAPPE MENAGE EXTENSION PID CIC)

* Bring in information for ISIC Groupings
joinby using "clean data/Niger14_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby GRAPPE MENAGE EXTENSION PID using "clean data/Niger14_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
/// for Niger 2014 the calculations are as follows
///   Rural -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
///   Urban -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
*******************************************************************************************
replace AFS_share = 0.7252143 if AFS_share==. & rural==1
replace AFS_share = 0.567008 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*
forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*

* Four mixed CIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612
replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612

* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (GRAPPE MENAGE EXTENSION PID)

save "clean data/intermediate/Niger14_nfe_occ.dta", replace

** Own Farm Employment
* Occupation cannot be listed more than once, but FTE is summed
* Prep Labor
forvalues m=17(1)22 {
	use "raw data/ECVMA2_AS2AP1.dta", clear
	rename AS02AQ`m'A PID
	rename AS02AQ`m'B prep_days
	gen own_farm_labor = `m'
	drop if prep_days==0
	drop if prep_days==.
	keep GRAPPE MENAGE EXTENSION PID prep_days own_farm_labor
	save "clean data/intermediate/Niger14_farm_labor_`m'.dta", replace

}
*
* Grow Labor
forvalues m=28(1)33 {
	use "raw data/ECVMA2_AS2AP2.dta", clear
	rename AS02AQ`m'A PID
	rename AS02AQ`m'B grow_days
	gen own_farm_labor = `m'
	drop if grow_days==0
	drop if grow_days==.
	keep GRAPPE MENAGE EXTENSION PID grow_days own_farm_labor
	save "clean data/intermediate/Niger14_farm_labor_`m'.dta", replace

}
*
* Harvest Labor
forvalues m=36(1)41 {
	use "raw data/ECVMA2_AS2AP2.dta", clear
	rename AS02AQ`m'A PID
	rename AS02AQ`m'B harvest_days
	gen own_farm_labor = `m'
	drop if harvest_days==0
	drop if harvest_days==.
	keep GRAPPE MENAGE EXTENSION PID harvest_days own_farm_labor
	save "clean data/intermediate/Niger14_farm_labor_`m'.dta", replace

}
*
* Household Own Farm Labor
use "clean data/intermediate/Niger14_farm_labor_17.dta", clear
forvalues m=18(1)22 {
	append using "clean data/intermediate/Niger14_farm_labor_`m'.dta"
}
*
forvalues m=28(1)33 {
	append using "clean data/intermediate/Niger14_farm_labor_`m'.dta"
}
*
forvalues m=36(1)41 {
	append using "clean data/intermediate/Niger14_farm_labor_`m'.dta"
}
*

* Calculate total hours worked
gen farm_days = 0
replace farm_days = farm_days + prep_days if prep_days~=.
replace farm_days = farm_days + grow_days if grow_days~=.
replace farm_days = farm_days + harvest_days if harvest_days~=.

keep GRAPPE MENAGE EXTENSION PID farm_days

gen farm = 1 if farm_days~=0

collapse (max) farm (sum) farm_days, by (GRAPPE MENAGE EXTENSION PID)

* Generate Full Time Equivalents (252 days in year is full time)
/*** Impose hours per day from observed hours per day in Ethiopia, Malawi, Nigeria and Tanzania, as it is unreasonable to assume 8 hours per day of labor
Rural: Ethiopia 4.70, Malawi  (4.15, 4.20, 5.13, 4.36, 4.05, 4.22), Nigeria  (6.16, 5.75, 5.44), Tanzania  (5.64, 5.58, 5.22) ==> 5.08
Rural Scale = 5.08/8 = 0.6348

Urban: Ethiopia 2.72, Malawi  (4.24, 4.13, 5.26, 4.07, 3.99, 3.94), Nigeria  (5.50, 5.16, 4.86), Tanzania  (5.48, 5.49, 5.19) ==> 4.39
Urban Scale = 4.39/8 = 0.5485
*/
joinby GRAPPE MENAGE EXTENSION using "clean data/Niger14_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace farm_days = farm_days * 0.5485 if rural==0
replace farm_days = farm_days * 0.6348 if rural==1

gen farm_fte = farm_days/252
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
replace farm_fte = 2 if farm_fte>2
* Chages 7 of 6192 observations

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

* Keep Farming Data at the household level
keep GRAPPE MENAGE EXTENSION PID farm_o_c1 farm_f_c1

save "clean data/intermediate/Niger14_farm_occ.dta", replace


**** COMBINE ALL ****

use "clean data/intermediate/Niger14_nfe_occ.dta", clear
joinby GRAPPE MENAGE EXTENSION PID using "clean data/intermediate/Niger14_w_occ.dta", unmatched(both) _merge(_merge)
drop _merge
joinby GRAPPE MENAGE EXTENSION PID using "clean data/intermediate/Niger14_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Demographics
joinby GRAPPE MENAGE EXTENSION PID using "clean data/Niger14_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==1
drop _merge
mvdecode nfe* w* farm*, mv(0)
mvencode nfe* w* farm*, mv(0)

** Generate Grouped Categorical Variables for formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*
*
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}
*

*non-farm enterprise
forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}

*
*farm enterprise
replace o_f2_c1 = o_f2_c1 + farm_o_c1

replace f_f2_c1 = f_f2_c1 + farm_f_c1

* Total (f3) Casual, Formal and Self Employed
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f0_c`c'
	replace total_jobs = total_jobs + o_f1_c`c'
	replace total_jobs = total_jobs + o_f2_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	


joinby using "clean data/Niger14_iid.dta", unmatched(none)

*!EVA: keep only relevant variables:
keep hhid iid fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* ///
total_jobs occ_total age age_tier female head female_head youth youth_34 school_read ///
school_any school_currently school_complete school_secondary school_level pension health_insurance

gen country="Niger"

save "clean data/NIGER_FTEsCHEVAL.dta", replace
