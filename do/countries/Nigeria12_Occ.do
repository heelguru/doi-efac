**************** CHEVAL - DEPTH OF INFORMALITY ***************************
********************************************************************************
***************** NIGERIA 2012: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Nigeria 2012 General Household Survey-Panel Wave 3


-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
generates 
save "$clean/intermediate/Nigeria12_wage_occCHEVAL.dta", replace
save "$clean/NIGERIA12_FTEsCHEVAL.dta", replace

Stata Version 16.1 (Ce's version:14.2)

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\NGA_2012_GHSP-W2_v02_M_STATA"

gl DO   	    ${NIGERIA}/do
gl raw   		${NIGERIA}/raw data 
	gl phhh		"${raw}/Post Harvest Wave 2/Household"
	gl phag		"${raw}/Post Harvest Wave 2/Agriculture"
	gl pphh		"${raw}/Post Planting Wave 2/Household"
	gl ppag		"${raw}/Post Planting Wave 2/Agriculture"
gl clean 	${NIGERIA}/clean data

********************************************************************************

** Occupations in Non-Farm Own Enterprises (ONLY in POST HARVEST)
//use "$phhh/sect9_harvestw2.dta", clear  //this section does not ask about hours worked, so we cannot use it. We can however use the employment section to identify those who work self-employed not in agriculture. 

use "$pphh/sect3a_plantingw2", clear
//joinby hhid indiv using "$phhh/sect3a_harvestw2.dta", unmatched(both) _merge(_merge)

*keep only the self-employed:  //s3aq11!=1 & s3aq12a==9
keep if s3aq6==1 & s3aq14!=1 //own account not in agriculture

rename s3aq14b nfe_CIC
ren s3aq16 nfe_months
* question asks for the last 12 months, so must assume that those who reported greater than 12 months did not understand the question. It is possible that those who understood the question only reported 12 months even if they worked more than 12 of past 15.
replace nfe_months = 12 if nfe_months>12
ren s3aq18 nfe_hourspw
ren s3aq17 nfe_weeks 

* Assume that reported hours per day above 16 hours per day are in error. (19)
replace nfe_hourspw = . if nfe_hourspw>80
* Only 52 weeks in a year
replace nfe_weeks = 52 if nfe_weeks>52 & nfe_weeks<.

mvdecode nfe_months nfe_weeks nfe_hourspw, mv(0)

* Bring in Demographic Info
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_months~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_months_5 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
by nfe_CIC female rural, sort : egen float est_nfe_months_5 = max(temp_est_nfe_months_5)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
replace track = 5 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_5 if track==5
drop est_nfe_months_* count_* track
**** Replace missing value of nfe_months (4 changes)
replace nfe_months = est_nfe_months if nfe_months==.
replace nfe_months = 1 if nfe_months==.
* Count observations with nfe_weeks reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_weeks~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_weeks~=.
* Calculate median of nfe_weeks by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_weeks_1 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_weeks_2 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_weeks_3 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_weeks_4 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_weeks_5 = median(nfe_weeks) if nfe_weeks~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_weeks_1 = max(temp_est_nfe_weeks_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_weeks_2 = max(temp_est_nfe_weeks_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_weeks_3 = max(temp_est_nfe_weeks_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_weeks_4 = max(temp_est_nfe_weeks_4)
by nfe_CIC female rural, sort : egen float est_nfe_weeks_5 = max(temp_est_nfe_weeks_5)
drop temp*
* Build nfe_weeks prediction variable if at least 10 observations of reported nfe_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_weeks = est_nfe_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_3 if track==4
replace track = 5 if est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_5 if track==5
drop est_nfe_weeks_* count_* track
**** Replace missing value of nfe_weeks (2 changes)
replace nfe_weeks = est_nfe_weeks if nfe_weeks==.
replace nfe_weeks = 1 if nfe_weeks==.
* Count observations with nfe_hourspw reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_hourspw~=.
* Calculate median of nfe_hourspw by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_hourspw_1 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_hourspw_2 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_hourspw_3 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_hourspw_4 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_hourspw_5 = median(nfe_hourspw) if nfe_hourspw~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_hourspw_1 = max(temp_est_nfe_hourspw_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_hourspw_2 = max(temp_est_nfe_hourspw_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_hourspw_3 = max(temp_est_nfe_hourspw_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_hourspw_4 = max(temp_est_nfe_hourspw_4)
by nfe_CIC female rural, sort : egen float est_nfe_hourspw_5 = max(temp_est_nfe_hourspw_5)
drop temp*
* Build nfe_hourspw prediction variable if at least 10 observations of reported nfe_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_hourspw = est_nfe_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_3 if track==4
replace track = 5 if est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_5 if track==5
drop est_nfe_hourspw_* count_* track
**** Replace missing value of nfe_hourspw (22 changes)
replace nfe_hourspw = est_nfe_hourspw if nfe_hourspw==.
replace nfe_hourspw = 1 if nfe_hourspw==.

* Generate Full Time Equivalents
gen fte_nfe = nfe_weeks*nfe_hourspw/2016
mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
* 1 change
replace fte_nfe = 2 if fte_nfe>2 & fte_nfe~=.

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe~=0
mvdecode emp_nfe, mv(0)
mvencode emp_nfe, mv(0)

* Collapse to HH / ISIC level
collapse (sum) fte_nfe emp_nfe, by (hhid indiv nfe_CIC)

* Bring in information for ISIC Groupings
rename nfe_CIC CIC
joinby hhid indiv using "$clean/Nigeria12_member_demos.dta", unmatched(none)

ren emp_nfe nfe_o
ren fte_nfe nfe_f 

* Collapse to the household level
collapse (sum) nfe_o nfe_f, by (hhid indiv)

save "$clean/intermediate/Nigeria12_nfe1_occCHEVAL.dta", replace

** second job
use "$pphh/sect3a_plantingw2", clear
keep if s3aq25==1 & s3aq28==10 & s3aq27!=1

ren s3aq27 nfe_CIC
ren s3aq29 nfe_months   
* question asks for the last 12 months, so must assume that those who reported greater than 12 months did not understand the question. It is possible that those who understood the question only reported 12 months even if they worked more than 12 of past 15.
replace nfe_months = 12 if nfe_months>12
ren s3aq31 nfe_hourspw
ren s3aq30 nfe_weeks 

* Assume that reported hours per day above 16 hours per day are in error. (19)
replace nfe_hourspw = . if nfe_hourspw>80
* Only 52 weeks in a year
replace nfe_weeks = 52 if nfe_weeks>52 & nfe_weeks<.

mvdecode nfe_months nfe_weeks nfe_hourspw, mv(0)

* Bring in Demographic Info
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_months~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_months_5 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
by nfe_CIC female rural, sort : egen float est_nfe_months_5 = max(temp_est_nfe_months_5)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
replace track = 5 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_5 if track==5
drop est_nfe_months_* count_* track
**** Replace missing value of nfe_months (4 changes)
replace nfe_months = est_nfe_months if nfe_months==.
replace nfe_months = 1 if nfe_months==.
* Count observations with nfe_weeks reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_weeks~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_weeks~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_weeks~=.
* Calculate median of nfe_weeks by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_weeks_1 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_weeks_2 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_weeks_3 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_weeks_4 = median(nfe_weeks) if nfe_weeks~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_weeks_5 = median(nfe_weeks) if nfe_weeks~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_weeks_1 = max(temp_est_nfe_weeks_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_weeks_2 = max(temp_est_nfe_weeks_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_weeks_3 = max(temp_est_nfe_weeks_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_weeks_4 = max(temp_est_nfe_weeks_4)
by nfe_CIC female rural, sort : egen float est_nfe_weeks_5 = max(temp_est_nfe_weeks_5)
drop temp*
* Build nfe_weeks prediction variable if at least 10 observations of reported nfe_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_weeks = est_nfe_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_3 if track==4
replace track = 5 if est_nfe_weeks==.
replace est_nfe_weeks = est_nfe_weeks_5 if track==5
drop est_nfe_weeks_* count_* track
**** Replace missing value of nfe_weeks (2 changes)
replace nfe_weeks = est_nfe_weeks if nfe_weeks==.
replace nfe_weeks = 1 if nfe_weeks==.
* Count observations with nfe_hourspw reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_hourspw~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_hourspw~=.
* Calculate median of nfe_hourspw by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_hourspw_1 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_hourspw_2 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_hourspw_3 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_hourspw_4 = median(nfe_hourspw) if nfe_hourspw~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_hourspw_5 = median(nfe_hourspw) if nfe_hourspw~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_hourspw_1 = max(temp_est_nfe_hourspw_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_hourspw_2 = max(temp_est_nfe_hourspw_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_hourspw_3 = max(temp_est_nfe_hourspw_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_hourspw_4 = max(temp_est_nfe_hourspw_4)
by nfe_CIC female rural, sort : egen float est_nfe_hourspw_5 = max(temp_est_nfe_hourspw_5)
drop temp*
* Build nfe_hourspw prediction variable if at least 10 observations of reported nfe_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_hourspw = est_nfe_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_3 if track==4
replace track = 5 if est_nfe_hourspw==.
replace est_nfe_hourspw = est_nfe_hourspw_5 if track==5
drop est_nfe_hourspw_* count_* track
**** Replace missing value of nfe_hourspw (22 changes)
replace nfe_hourspw = est_nfe_hourspw if nfe_hourspw==.
replace nfe_hourspw = 1 if nfe_hourspw==.

* Generate Full Time Equivalents
gen fte_nfe = nfe_weeks*nfe_hourspw/2016
mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
* 1 change
replace fte_nfe = 2 if fte_nfe>2 & fte_nfe~=.

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe~=0
mvdecode emp_nfe, mv(0)
mvencode emp_nfe, mv(0)

* Collapse to HH / ISIC level
collapse (sum) fte_nfe emp_nfe, by (hhid indiv)

* Bring in information for ISIC Groupings
joinby hhid indiv using "$clean/Nigeria12_member_demos.dta", unmatched(none)

ren emp_nfe nfe_o
ren fte_nfe nfe_f 

* Collapse to the household level
collapse (sum) nfe_o nfe_f, by (hhid indiv)

merge 1:1 hhid indiv using "$clean/intermediate/Nigeria12_nfe1_occCHEVAL.dta", nogen keep(1 2 3)

save "$clean/intermediate/Nigeria12_nfe_occCHEVAL.dta", replace

** Occupation by Wages
* Post Harvest //Post planting has no information on benefits 
* Primary Wage
use "$phhh/sect3a_harvestw2.dta", clear
keep if s3aq1==1 & s3aq12a!=10 & s3aq12a!=9 //keep only those working for someone else
* Rename variables
rename s3aq10b wage_CIC
g pension=(s3aq12b2==1 | s3aq12b3==1 | s3aq12b4==1 | s3aq12b1==1)
g health_insurance=(s3aq35==1) // asks whether individual contributes to national health insurance Scheme, no information about employer's contribution
g leave=(s3aq12b2==3 | s3aq12b3==3 | s3aq12b4==3 | s3aq12b1==3 | s3aq12b2==4 | s3aq12b3==4 | s3aq12b4==4 | s3aq12b1==4)
rename s3aq13 wage_months
rename s3aq14 wage_weeks
rename s3aq15 wage_hourspw
rename s3aq18a1 wage_payment
rename s3aq18a2 wage_payment_duration
rename s3aq20a wage_payment_inkind
rename s3aq20b wage_payment_inkind_duration

order hhid indiv wage_* pension health_insurance leave
keep hhid indiv wage_* pension health_insurance leave

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (19)
replace wage_hourspw = . if wage_hourspw>80
* Only 52 weeks in a year
replace wage_weeks = 52 if wage_weeks>52 & wage_weeks<.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (4 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (22 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 1 change
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance pension)

gen sw = 1

save "$clean/intermediate/Nigeria12_wage_occ_PH1.dta", replace

* Post Harvest 
* Secondary Wage
* Primary Wage
use "$phhh/sect3a_harvestw2.dta", clear
keep if s3aq21==1 & s3aq24a!=10 & s3aq24a!=9 //keep only those working for someone else
* Rename variables
rename s3aq22b wage_CIC
g pension=(s3aq24b2==1 | s3aq24b3==1 | s3aq24b4==1 | s3aq24b1==1)
g health_insurance=(s3aq35==1) //! asks whether individual contributes to national health insurance Scheme, no information about employer's contribution
g leave=(s3aq24b2==3 | s3aq24b3==3 | s3aq24b4==3 | s3aq24b1==3 | s3aq24b2==4 | s3aq24b3==4 | s3aq24b4==4 | s3aq24b1==4)
rename s3aq25 wage_months
rename s3aq26 wage_weeks
rename s3aq27 wage_hourspw
rename s3aq30a1 wage_payment
rename s3aq30a2 wage_payment_duration
rename s3aq32a wage_payment_inkind
rename s3aq32b wage_payment_inkind_duration

order hhid indiv wage_* pension health_insurance leave
keep hhid indiv wage_* pension health_insurance leave

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (19)
replace wage_hourspw = . if wage_hourspw>80
* Only 52 weeks in a year
replace wage_weeks = 52 if wage_weeks>52 & wage_weeks<.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (4 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (22 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

gen formalsp = 0 //added definition of "formality" due to SP
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 1 change
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance pension)

gen sw = 2

save "$clean/intermediate/Nigeria12_wage_occ_PH2.dta", replace

* Combine Wage files
use "$clean/intermediate/Nigeria12_wage_occ_PH1.dta", clear
append using "$clean/intermediate/Nigeria12_wage_occ_PH2.dta"

* Collapse to HH / ISIC level
collapse (sum) fte* emp* formal* (max) health_insurance pension, by (hhid indiv wage_CIC)

joinby hhid indiv using "$clean/Nigeria12_member_demos.dta", unmatched(none)
ren fte_wage w_f
ren fte_f0_wage w_f_f0
ren fte_f1_wage w_f_f1
ren fte_fsp0_wage w_f_spf0
ren fte_fsp1_wage w_f_spf1
ren emp_f0_wage w_o_f0
ren emp_f1_wage w_o_f1
ren emp_fsp0_wage w_o_spf0
ren emp_fsp1_wage w_o_spf1

* Collapse to the household level
collapse (sum) w_o* w_f* (max) health_insurance pension, by (hhid indiv)

save "$clean/intermediate/Nigeria12_wage_occCHEVAL.dta", replace

** Own Farm Employment
* Post Planting & Post Harvest Data
* Occupation cannot be listed more than once, but FTE is summed
* Post Planting
use "$ppag/sect11c1_plantingw2.dta", clear

local member "a b c d"
foreach m in `member' {
	rename s11c1q1`m'1 member_`m'
}
*

local member "a b c d"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d"
foreach m in `member' {
	replace farm_`m'_weeks = s11c1q1`m'2
	replace farm_`m'_days = s11c1q1`m'3
	replace farm_`m'_hourspd = s11c1q1`m'4
}
*  
  
* Convert data from horizontal to vertical
save "$clean/intermediate/Nigeria12_temp_farm_PP.dta", replace

local member "a b c d"
foreach m in `member' {
	use "$clean/intermediate/Nigeria12_temp_farm_PP.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "$clean/intermediate/Nigeria12_temp_farm_PP_`m'.dta", replace
}
*

use "$clean/intermediate/Nigeria12_temp_farm_PP_a.dta", clear
append using "$clean/intermediate/Nigeria12_temp_farm_PP_b.dta"
append using "$clean/intermediate/Nigeria12_temp_farm_PP_c.dta"
append using "$clean/intermediate/Nigeria12_temp_farm_PP_d.dta"

***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16
* Results in changing 67 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (3 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (5 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (69 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = 0
replace farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "$clean/intermediate/Nigeria12_farm_PP.dta", replace


* Pre-Harvest Labor
use "$phag/secta2_harvestw2.dta", clear

local member "a b c d"
foreach m in `member' {
	rename sa2q1`m'1 member_`m'
}
*

local member "a b c d"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d"
foreach m in `member' {
	replace farm_`m'_weeks = sa2q1`m'2
	replace farm_`m'_days = sa2q1`m'3
	replace farm_`m'_hourspd = sa2q1`m'4
}
*

* Convert data from horizontal to vertical
save "$clean/intermediate/Nigeria12_temp_farm_PH.dta", replace

local member "a b c d"
foreach m in `member' {
	use "$clean/intermediate/Nigeria12_temp_farm_PH.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "$clean/intermediate/Nigeria12_temp_farm_PH_`m'.dta", replace
}
*

use "$clean/intermediate/Nigeria12_temp_farm_PH_a.dta", clear
local member "b c d"
foreach m in `member' {
	append using "$clean/intermediate/Nigeria12_temp_farm_PH_`m'.dta"
}
*

***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16 & farm_hourspd~=.
* Results in changing 14 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (2 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (0 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (20 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "$clean/intermediate/Nigeria12_farm_PreH.dta", replace


** Harvest Own Farm Labor
use "$phag/secta2_harvestw2.dta", clear

local member "a b c d"
foreach m in `member' {
	rename sa2q1`m'1 member_`m'
}
*

local member "a b c d"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d"
foreach m in `member' {
	replace farm_`m'_weeks = sa2q1`m'2
	replace farm_`m'_days = sa2q1`m'3
	replace farm_`m'_hourspd = sa2q1`m'4
}
*

* Convert data from horizontal to vertical
save "$clean/intermediate/Nigeria12_temp_farm_PoH.dta", replace

local member "a b c d"
foreach m in `member' {
	use "$clean/intermediate/Nigeria12_temp_farm_PoH.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "$clean/intermediate/Nigeria12_temp_farm_PoH_`m'.dta", replace
}
*

use "$clean/intermediate/Nigeria12_temp_farm_PoH_a.dta", clear
local member "b c d"
foreach m in `member' {
	append using "$clean/intermediate/Nigeria12_temp_farm_PoH_`m'.dta"
}
*
***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16 & farm_hourspd~=.
* Results in changing 12 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "$clean/Nigeria12_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (3 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (0 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (17 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = 0
replace farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "$clean/intermediate/Nigeria12_farm_PoH.dta", replace


* Combine Own Farm Labor
use "$clean/intermediate/Nigeria12_farm_PP.dta", clear
append using "$clean/intermediate/Nigeria12_farm_PreH.dta"
append using "$clean/intermediate/Nigeria12_farm_PoH.dta"

* Own Farming at Member level
gen farm = 1
collapse (max) farm (sum) farm_hours, by (hhid indiv)
drop if farm_hours==0

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
*** Assume no farmer averages more than 80 hours per week for the entire year
replace farm_fte = 2 if farm_fte>2 & farm_fte~=.
*** 290 changes in 9069 observations
drop farm_hours

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

keep hhid indiv farm_o_c1 farm_f_c1

save "$clean/intermediate/Nigeria12_farm_occ.dta", replace

**** COMBINE ALL ****
use "$clean/intermediate/Nigeria12_nfe_occCHEVAL.dta", clear
joinby hhid indiv using "$clean/intermediate/Nigeria12_wage_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid indiv using "$clean/intermediate/Nigeria12_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Demographics
joinby hhid indiv using "$clean/Nigeria12_member_demos.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode nfe* w* farm*, mv(0)
mvencode nfe* w* farm*, mv(0)

** Generate Grouped Categorical Variables
forvalues f=0(1)2 {
	gen o_f`f' = 0
	gen o_spf`f' = 0
}

*

forvalues f=0(1)2 {
	gen f_f`f' = 0
	gen f_spf`f' = 0
}
*

forvalues f=0(1)1 {
	replace o_f`f' = w_o_f`f'
	replace o_f`f' = w_o_f`f'
	replace f_f`f' = w_f_f`f'
	replace o_spf`f' = w_o_spf`f'
	replace f_spf`f' = w_f_spf`f'
}
*

	replace o_f2 = nfe_o
	replace f_f2 = nfe_f

* Total (f3) Casual, Formal and Self Employed
	generate o_f3 = o_f0 + o_f1 + o_f2
	generate f_f3 = f_f0 + f_f1 + f_f2

gen total_jobs = 0
	replace total_jobs = total_jobs + o_f3

gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
	replace fte_total = fte_total + f_f3

*
tostring hhid,replace
joinby using "$clean/Nigeria12_iid.dta", unmatched(none)
keep hhid iid fte_total o_f* o_spf* f_f* f_spf*  nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete school_secondary school_level health_insurance pension
gen country="Nigeria"
g year=2012
save "$clean/NIGERIA12_FTEsCHEVAL.dta", replace

