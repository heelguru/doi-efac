********************************************************************************
*********************** TANZANIA 2014/2015: DEMOGRAPHICS ************************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Tanzania 2014/2015 National Panel Survey

The resulting data set is "Tanz1415_demographics"

Stata Version 15.1 (14.2 Ce)
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl TANZANIA	"$path/data\Tanzania_NPS_1415" 

gl DO   	    ${TANZANIA}/do
gl raw data   	${TANZANIA}/raw data 
gl clean data 	${TANZANIA}/clean data
gl FIG 		    ${TANZANIA}/graphs
gl TAB 	    	${TANZANIA}/tables
 
********************************************************************************
cd "$TANZANIA"


* Location
use "raw data/hh_sec_a.dta", clear
rename hh_a01_1 region
rename hh_a02_1 district
rename hh_a03_1 ward
rename hh_a03_3a village
rename hh_a04_1 ea
gen rural = 1 if clustertype==1
mvencode rural, mv(0)
rename y4_weights hhweight
order y4_hhid rural region district ward village ea domain strataid clusterid wardtype hhweight
keep y4_hhid rural region district ward village ea domain strataid clusterid wardtype hhweight
g admin_1 = region
g admin_2 = region*10 + district
g admin_3 = ward

save "clean data/Tanz1415_location.dta", replace

** Household Member Demographic information
use "raw data/hh_sec_b.dta", clear
rename hh_b02 gender
rename hh_b04 age
rename hh_b05 relationship
gen female = gender-1
gen head = 0
replace head = 1 if relationship==1
gen female_head = 0
replace female_head = 1 if head==1 & gender==2

/* Age Tiers
1 = 0-14
2 = 15-17
3 = 18-24
4 = 25-34
5 = 35-64
6 = 65+
*/
gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth youth_34, mv(0)

keep y4_hhid indidy4 age female head female_head age_tier youth youth_34
order y4_hhid indidy4 age age_tier female head female_head youth youth_34

* Bring in Education
joinby y4_hhid indidy4 using "raw data/hh_sec_c.dta", unmatched(both) _merge(_merge)

gen school_read = 1 if hh_c02==1
gen school_any = 1 if hh_c03==1
gen school_currently = 1 if hh_c05==1
gen school_complete = 1 if hh_c07>=17 & hh_c07~=.
gen school_secondary = 1 if hh_c07>=24 & hh_c07~=.
gen school_level = hh_c07
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep y4_hhid indidy4 age age_tier female head female_head youth youth_34 school_*

joinby y4_hhid using "clean data/Tanz1415_location.dta", unmatched(none)

* HH Size
by y4_hhid, sort : egen float hhsize = count(y4_hhid)

save "clean data/Tanz1415_member_demos.dta", replace


** Credit
use "Raw Data/hh_sec_p.dta", clear
drop if loancode==.
gen credit = 1 
collapse (max) credit, by (y4_hhid)

save "clean data/intermediate/Tanz1415_credit.dta", replace

** Land 
* Area measurements
use "raw data/ag_sec_2a.dta", clear
append using "raw data/ag_sec_2b.dta"
joinby using "raw data/ag_sec_3a.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "raw data/ag_sec_3b.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Identify as owned if: 1) inheritance, 2) gifted, 4) allocated by village, 5) purchased, 9) renting out 
gen land = 0
replace land = 1 if ag3a_25==1 | ag3a_25==2 | ag3a_25==4 | ag3a_25==5 | ag3a_25==9
replace land = 1 if ag3b_25==1 | ag3b_25==2 | ag3b_25==4 | ag3b_25==5 | ag3b_25==9

* Use area measurements calculated by GPS first and farmer estimate second
gen hectares = .
replace hectares = ag2a_09 if ag2a_09~=. & ag2a_09~=0 & hectares==.
replace hectares = ag2b_20 if ag2b_20~=. & ag2b_20~=0 & hectares==.
replace hectares = ag2a_04 if ag2a_04~=. & ag2a_04~=0 & hectares==.
replace hectares = ag2b_15 if ag2b_15~=. & ag2b_15~=0 & hectares==.
replace hectares = hectares * 0.404686

keep if land==1

collapse (sum) hectares (max) land, by (y4_hhid)
mvdecode hectares, mv(0)

drop land
rename hectares land_owned

save "clean data/intermediate/Tanz1415_land.dta", replace


* HH demographics
use "clean data/Tanz1415_member_demos.dta", clear
* Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
gen fem_share_24 = 1 if age>=15 & age<=24 & female==1 
gen fem_share_34 = 1 if age>=15 & age<=34 & female==1
gen mal_share_24 = 1 if age>=15 & age<=24 & female==0
gen mal_share_34 = 1 if age>=15 & age<=34 & female==0
mvencode youth_share_*, mv(0)

collapse (sum) youth_share_* fem_share_* mal_share* (max) admin_1 admin_2 admin_3 female_head school_secondary school_level, by (y4_hhid rural region district ward village ea domain strataid clusterid wardtype hhweight hhsize)

foreach v in youth fem mal { 						
replace `v'_share_24 = `v'_share_24/hhsize
replace `v'_share_34 = `v'_share_34/hhsize
}
rename school_secondary school_secondary_hh
rename school_level educ_max

joinby y4_hhid using "clean data/intermediate/Tanz1415_credit.dta", unmatched(both) _merge(_merge)
mvencode credit, mv(0)
drop _merge

joinby y4_hhid using "clean data/intermediate/Tanz1415_land.dta", unmatched(both) _merge(_merge)
mvencode land_owned, mv(0)
drop if _merge==2
drop _merge

gen popweight = hhweight*hhsize

save "clean data/Tanz1415_demographics.dta", replace


** Build hhid & iid
use "clean data/Tanz1415_member_demos.dta", clear
keep y4_hhid indidy4

gen hhid = y4_hhid
tostring indidy4, g(iid)
replace iid = "0" +iid if indidy4<10
replace iid = hhid+iid

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Tanz1415_iid.dta", replace
collapse (min) indidy4, by (y4_hhid hhid)
drop indidy4
save "clean data/Tanz1415_hhid.dta", replace
