********************************************************************************
*********************** NIGER 2014: DEMOGRAPHICS ************************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Niger 2014 General Household Survey-Panel Wave 3

The resulting data set is "Niger14_demographics"

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGER	"$path\data\Niger_ECVMA_14"

gl DO   	    ${NIGER}/do
gl raw data   	${NIGER}/raw data 
gl clean data 	${NIGER}/clean data
gl FIG 		    ${NIGER}/graphs
gl TAB 		    ${NIGER}/tables
 
********************************************************************************
cd "$NIGER"


* HH Weight
use "raw data/ECVMA2014_P1P2_Conso.dta", clear
collapse (sum) passage, by (GRAPPE MENAGE EXTENSION hhweight)
drop passage
save "clean data/Niger14_hhweight.dta", replace

* Location
use "raw data/ECVMA2_MS00P1.dta", clear
rename MS00Q10 region
rename MS00Q11 department
rename MS00Q12 town
rename MS00Q14 ea
g admin_1 = region
g admin_2 = department
g admin_3 = town
gen rural = 0
replace rural = 1 if MS00Q15==3
keep admin_1-admin_3 GRAPPE MENAGE EXTENSION region department town ea rural
joinby using "clean data/Niger14_hhweight.dta", unmatched(none)

save "clean data/Niger14_location.dta", replace

** Household Member Demographic information
use "raw data/ECVMA2_MS01P1.dta", clear
gen PID = MS01Q0

* drop if no longer a member of the household
drop if MS01Q00F==2

rename MS01Q06A age
gen female = MS01Q01-1
gen head = 0
replace head = 1 if MS01Q02==1
gen female_head = 0
replace female_head = 1 if head==1 & female==1

/* Age Tiers
1 = 0-14
2 = 15-17
3 = 18-24
4 = 25-34
5 = 35-64
6 = 65+
*/

gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth youth_34, mv(0)

keep GRAPPE MENAGE EXTENSION PID age age_tier female head female_head youth youth_34
order GRAPPE MENAGE EXTENSION PID age age_tier female head female_head youth youth_34

* Bring in Education
rename PID MS02Q00
joinby GRAPPE MENAGE EXTENSION MS02Q00 using "raw data/ECVMA2_MS02P1.dta", unmatched(none)
rename MS02Q00 PID
gen school_read = 1 if MS02Q01==1 | MS02Q02==1
gen school_any = 1 if MS02Q04==1 | MS02Q04==2 | MS02Q04==3
gen school_currently = 1 if MS02Q10==1
gen school_complete = 1 if MS02Q23>=2 & MS02Q23<.
gen school_secondary = 1 if MS02Q23>=3 & MS02Q23<.
gen school_level = MS02Q23
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep GRAPPE MENAGE EXTENSION PID age age_tier female head female_head youth youth_34 school_*
order GRAPPE MENAGE EXTENSION PID age age_tier female head female_head youth youth_34 school_*

joinby GRAPPE MENAGE EXTENSION using "clean data/Niger14_location.dta", unmatched(none)

* HH Size
by GRAPPE MENAGE EXTENSION, sort : egen float hhsize = count(GRAPPE)

save "clean data/Niger14_member_demos.dta", replace

** Credit
* Credit in time of crisis
use "raw data/ECVMA2_MS10P1.dta", clear
gen credit = 0
replace credit = 1 if MS10Q05A==13
replace credit = 1 if MS10Q05B==13
replace credit = 1 if MS10Q05C==13
collapse (max) credit, by (GRAPPE MENAGE EXTENSION)
save "clean data/intermediate/Niger14_temp_credit.dta", replace

* Ag section 2D
use "raw data/SectionAS2DP2.dta", clear
gen credit = 0
replace credit = 1 if AS02DQ06>1 & AS02DQ06<5
collapse (max) credit, by (GRAPPE MENAGE EXTENSION)
append using "clean data/intermediate/Niger14_temp_credit.dta"
collapse (max) credit, by (GRAPPE MENAGE EXTENSION)

* Credit for HH Enterprise (is mentioned regarding if was a problem to get, but not if they have credit)

save "clean data/intermediate/Niger14_credit.dta", replace

** Land 
* Area measurements
use "raw data/ECVMA2_AS1P1.dta", clear

* Identify as owned if: 1) purchased, 2) inheritance, 3) gifted 
gen land = 0
replace land = 1 if AS01Q18==1 | AS01Q18==2 | AS01Q18==3 

* Use area measurements calculated by GPS first and farmer estimate second
gen hectares = .
replace AS01Q07 = . if AS01Q07>900000
replace AS01Q06 = . if AS01Q06>900000
replace hectares = AS01Q07 if AS01Q07~=. & AS01Q07~=0 & hectares==.
replace hectares = AS01Q06 if AS01Q06~=. & AS01Q06~=0 & hectares==.
replace hectares = hectares/10000

keep if land==1

collapse (sum) hectares (max) land, by (GRAPPE MENAGE EXTENSION)
mvdecode hectares, mv(0)

drop land
rename hectares land_owned

save "clean data/intermediate/Niger14_land.dta", replace

* HH demographics
use "clean data/Niger14_member_demos.dta", clear
* Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
mvencode youth_share_*, mv(0)

collapse (sum) youth_share_* (max) admin_1 admin_2 admin_3 female_head school_secondary school_level, by (GRAPPE MENAGE EXTENSION region department town ea rural hhweight hhsize)

replace youth_share_24 = youth_share_24/hhsize
replace youth_share_34 = youth_share_34/hhsize
rename school_secondary school_secondary_hh
rename school_level educ_max

joinby GRAPPE MENAGE EXTENSION using "clean data/intermediate/Niger14_credit.dta", unmatched(both) _merge(_merge)
drop _merge

joinby GRAPPE MENAGE EXTENSION using "clean data/intermediate/Niger14_land.dta", unmatched(both) _merge(_merge)
mvencode land_owned, mv(0)
drop if _merge==2
drop _merge

gen popweight = hhweight*hhsize

drop if hhweight==.

save "clean data/Niger14_demographics.dta", replace

** Build hhid & iid
use "clean data/Niger14_member_demos.dta", clear
keep GRAPPE MENAGE EXTENSION PID

 foreach v in GRAPPE MENAGE EXTENSION {
	tostring `v', g(s_`v')
}
replace s_MENAGE = "0" + s_MENAGE if MENAGE<10
g hhid = s_GRAPPE + s_MENAGE + s_EXTENSION
drop s_*

tostring PID, g(iid)
replace iid = "0" +iid if PID<10
replace iid = hhid+iid

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Niger14_iid.dta", replace
collapse (min) PID, by (GRAPPE MENAGE EXTENSION hhid)
drop PID
save "clean data/Niger14_hhid.dta", replace
