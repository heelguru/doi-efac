************ CHEVAL : DEPTH OF INEQUALITY *************
*******************************************************
*******NIGERIA 2015/16: GENERATE EXPENDITURE *************
*******************************************************

/*
This file gets the total household expenditure and defines households as poor.

The dataset used for this is:
Nigeria 2015/16 General Household Survey-Panel Wave 3

Stata Version 16.1
-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "$clean/NIGERIAIA1516_expenditure_cheval.dta", replace

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\Nigeria_GHS_1516"

gl DO   	    ${NIGERIA}/do
gl raw   		${NIGERIA}/raw data 
gl clean 	"${NIGERIA}/clean data"
********************************************************************************

use "$raw/cons_agg_wave3_visit1.dta", clear

* Convert expenditures variables to  USD constant PPP
g pcexp= totcons
foreach u of varlist pcexp  {
	* Currency conversion from XE.com 31/12/2015
	gen USD_`u' = `u'*0.0050244944
	* Constant 2011 PPP conversion using World Bank GNI Data 
	* Average of 2015 and 2016 GNI per capita constant 2011 PPP / Average of 2015 and 2016 GNI per capita Atlas Method [((5527.085712+5325.568998)/2)/((2850+2450)/2)] (Source WDI)
	replace USD_`u' = USD_`u'*2.0476707
	drop `u'
	ren USD_`u' `u'
	replace `u' = `u'/365 //Daily expenditure to later define poverty
	}

keep hhid pcexp totcons

merge 1:1 hhid using "$raw/NGA_HouseholdGeovars_Y3", nogen keep(1 2 3)

joinby using "$clean/Nigeria1516_demographics.dta", unmatched(none)
g year=2015

save "$clean/NIGERIAIA1516_expenditure_cheval.dta", replace

 