********************************************************************************
***************** ETHIOPIA - 2015/2016: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Ethiopia 2015/2016 Socioeconomic Survey, Wave 3

generates 
save "$CLEAN\ETHIOPIA_FTEsCHEVAL.dta", replace

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl ETHIOPIA	"$path\data\Ethiopia_ESS_1516"

gl DO   	    ${ETHIOPIA}/do
gl raw data   	${ETHIOPIA}/raw data 
gl clean data 	${ETHIOPIA}/clean data
gl FIG 		    ${ETHIOPIA}/graphs
gl TAB 		    ${ETHIOPIA}/tables
 
********************************************************************************
cd "$ETHIOPIA"

	** HOURS WORKED BY SECTOR **
/*
For each individual we first compute the hours worked from each job and annualise it.
Then we sum the income by individual and sector (On-farm ASF, Off-farm ASF, non ASF).
Finally we aggregate sectoral income at household level. 

We assume: 12 months per year, 4.3 weeks per month, 40 hours per week
* Generate Full Time Equivalents
FTE = hours worked / 40 /52 by sector and occupation

*/


** Occupations in Non-Farm Own Enterprises
* First convert so that there is one observation per worker per NFE

local worker "a b c d e f"
foreach w in `worker' {
use "raw data/Household/sect11b_hh_w3", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
drop if HHID==.
rename hh_s11bq12_`w' member_id
drop if member_id==.
drop if member_id==0
rename hh_s11bq01_b nfe_CIC
**** Have to assume that all workers work the same number of days, during each month of operation
rename hh_s11bq09 nfe_months
rename hh_s11bq10 nfe_dayspm
drop if nfe_months==. & nfe_dayspm==.

gen worker = "`w'"

keep region zone ward town subcity FA ea HHID household_id2 member_id nfe_CIC nfe_months nfe_dayspm worker
order region zone ward town subcity FA ea HHID household_id2 member_id nfe_CIC nfe_months nfe_dayspm worker

save "clean data/intermediate/Ethiopia1516_temp_nfe_`w'.dta", replace
}
*
use "clean data/intermediate/Ethiopia1516_temp_nfe_a.dta", clear
append using "clean data/intermediate/Ethiopia1516_temp_nfe_b.dta"
append using "clean data/intermediate/Ethiopia1516_temp_nfe_c.dta"
append using "clean data/intermediate/Ethiopia1516_temp_nfe_d.dta"
append using "clean data/intermediate/Ethiopia1516_temp_nfe_e.dta"
append using "clean data/intermediate/Ethiopia1516_temp_nfe_f.dta"

**** Correct Clear errors
**** If worked months then assume to have worked days
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)
* Count observations with nfe_dayspm reported by geographic location.
by nfe_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward, sort : egen float count_5 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region zone, sort : egen float count_6 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural region, sort : egen float count_7 = count(1) if nfe_dayspm~=.
by nfe_CIC female rural, sort : egen float count_8 = count(1) if nfe_dayspm~=.
* Calculate median of nfe_dayspm by geographic location - later used for predictive purposes.
by nfe_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_nfe_dayspm_1 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_nfe_dayspm_2 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town subcity, sort : egen float temp_est_nfe_dayspm_3 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward town, sort : egen float temp_est_nfe_dayspm_4 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region zone ward, sort : egen float temp_est_nfe_dayspm_5 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region zone, sort : egen float temp_est_nfe_dayspm_6 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural region, sort : egen float temp_est_nfe_dayspm_7 = median(nfe_dayspm) if nfe_dayspm~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_dayspm_8 = median(nfe_dayspm) if nfe_dayspm~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural region zone ward town subcity FA ea, sort : egen float est_nfe_dayspm_1 = max(temp_est_nfe_dayspm_1)
by nfe_CIC female rural region zone ward town subcity FA, sort : egen float est_nfe_dayspm_2 = max(temp_est_nfe_dayspm_2)
by nfe_CIC female rural region zone ward town subcity, sort : egen float est_nfe_dayspm_3 = max(temp_est_nfe_dayspm_3)
by nfe_CIC female rural region zone ward town, sort : egen float est_nfe_dayspm_4 = max(temp_est_nfe_dayspm_4)
by nfe_CIC female rural region zone ward, sort : egen float est_nfe_dayspm_5 = max(temp_est_nfe_dayspm_5)
by nfe_CIC female rural region zone, sort : egen float est_nfe_dayspm_6 = max(temp_est_nfe_dayspm_6)
by nfe_CIC female rural region, sort : egen float est_nfe_dayspm_7 = max(temp_est_nfe_dayspm_7)
by nfe_CIC female rural, sort : egen float est_nfe_dayspm_8 = max(temp_est_nfe_dayspm_8)
drop temp*
* Build nfe_dayspm prediction variable if at least 10 observations of reported nfe_dayspm in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_dayspm = est_nfe_dayspm_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_7 if track==7
replace track = 8 if est_nfe_dayspm==.
replace est_nfe_dayspm = est_nfe_dayspm_8 if track==8
drop est_nfe_dayspm_* count_* track
**** Replace missing value of nfe_dayspm (2 changes)
replace nfe_dayspm = est_nfe_dayspm if nfe_dayspm==.

gen nfe_days = nfe_dayspm*nfe_months

* Gen FTE
**** Assuming full time work during each day that household worked
* FTE equals 21 days per month, 252 days per year.
gen fte_nfe = nfe_days/252

mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
replace fte_nfe = 2 if fte_nfe>2

* Simple count of employed in NFE.
gen emp_nfe = 1

* Collapse to HH / CIC level
replace nfe_CIC = 99999 if nfe_CIC==.
collapse (sum) fte_nfe emp_nfe, by (HHID household_id2 region zone ward town subcity FA ea member_id nfe_CIC)

* Bring in information for ISIC Groupings
rename nfe_CIC CIC
joinby using "clean data/Ethiopia1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop _merge
*** 43 observations are for hh members not found in the demographic information
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.722 if AFS_share==. & rural==1
replace AFS_share = 0.683 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*
forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*

* Four mixed CIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612
replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612

* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (HHID household_id2 region zone ward town subcity FA ea member_id)

save "clean data/intermediate/Ethiopia1516_nfe_occ.dta", replace


** Occupation by Wages 
* Transpose data so that 2 Wage occupations are in one set of variables
use "raw data/Household/sect4_hh_w3.dta", clear
* Wage 1
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id
rename hh_s4q11_b wage_CIC
rename hh_s4q12 wage_employer
rename hh_s4q16 wage_payment
rename hh_s4q17 wage_payment_duration
rename hh_s4q18 wage_inkind
rename hh_s4q19 wage_inkind_duration
rename hh_s4q13 wage_months
rename hh_s4q14 wage_weeks
rename hh_s4q15 wage_hourspw
mvdecode wage*, mv(0)
drop if wage_months==. & wage_weeks==. & wage_hourspw==. & wage_payment==. & wage_inkind==.
order HHID household_id2 region zone ward town subcity FA ea member_id wage_*
keep HHID household_id2 region zone ward town subcity FA ea member_id wage_*
gen worker = 1
save "clean data/intermediate/Ethiopia1516_temp_wage_1.dta", replace

* Wage 2
use "raw data/Household/sect4_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id
rename hh_s4q22_b wage_CIC
rename hh_s4q23 wage_employer
rename hh_s4q27 wage_payment
rename hh_s4q28 wage_payment_duration
rename hh_s4q28 wage_inkind
rename hh_s4q30 wage_inkind_duration
rename hh_s4q24 wage_months
rename hh_s4q25 wage_weeks
rename hh_s4q26 wage_hourspw
mvdecode wage*, mv(0)
drop if wage_months==. & wage_weeks==. & wage_hourspw==. & wage_payment==. & wage_inkind==.
order HHID household_id2 region zone ward town subcity FA ea member_id wage_*
keep HHID household_id2 region zone ward town subcity FA ea member_id wage_*
gen worker = 2
save "clean data/intermediate/Ethiopia1516_temp_wage_2.dta", replace

use "clean data/intermediate/Ethiopia1516_temp_wage_1.dta", clear
append using "clean data/intermediate/Ethiopia1516_temp_wage_2.dta"

* Clear Errors in data
* No occupation code, but reported earnings. 
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=.
* Only 12 months in a year (1 change)
replace wage_months = . if wage_months>12
* Only 4(5) weeks in a month (2 changes)
replace wage_weeks = . if wage_weeks>5
**** Assume that reported hours per day above 16 hours per day are in error (affects calculations of 42 of 1447 observations)
replace wage_hourspw = . if wage_hourspw>80
**** Assume if work during a month, the employee worked at least one hour in one day
**** If worked months then assume to have worked days
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_months~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_months~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_months_6 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_7 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_8 = median(wage_months) if wage_months~=.
by wage_CIC female, sort : egen float temp_est_wage_months_9 = median(wage_months) if wage_months~=.
by wage_CIC, sort : egen float temp_est_wage_months_10 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
by wage_CIC female rural region zone, sort : egen float est_wage_months_6 = max(temp_est_wage_months_6)
by wage_CIC female rural region, sort : egen float est_wage_months_7 = max(temp_est_wage_months_7)
by wage_CIC female rural, sort : egen float est_wage_months_8 = max(temp_est_wage_months_8)
by wage_CIC female, sort : egen float est_wage_months_9 = max(temp_est_wage_months_9)
by wage_CIC, sort : egen float est_wage_months_10 = max(temp_est_wage_months_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_9 if track==9
replace track = 10 if est_wage_months==.
replace est_wage_months = est_wage_months_10 if track==10
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (2, 1 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_weeks~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_weeks_6 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_7 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_8 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female, sort : egen float temp_est_wage_weeks_9 = median(wage_weeks) if wage_weeks~=.
by wage_CIC, sort : egen float temp_est_wage_weeks_10 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
by wage_CIC female rural region zone, sort : egen float est_wage_weeks_6 = max(temp_est_wage_weeks_6)
by wage_CIC female rural region, sort : egen float est_wage_weeks_7 = max(temp_est_wage_weeks_7)
by wage_CIC female rural, sort : egen float est_wage_weeks_8 = max(temp_est_wage_weeks_8)
by wage_CIC female, sort : egen float est_wage_weeks_9 = max(temp_est_wage_weeks_9)
by wage_CIC, sort : egen float est_wage_weeks_10 = max(temp_est_wage_weeks_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_9 if track==9
replace track = 10 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_10 if track==10
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (7, 1 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_hourspw~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_hourspw~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_hourspw_6 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hourspw_7 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_8 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female, sort : egen float temp_est_wage_hourspw_9 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC, sort : egen float temp_est_wage_hourspw_10 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
by wage_CIC female rural region zone, sort : egen float est_wage_hourspw_6 = max(temp_est_wage_hourspw_6)
by wage_CIC female rural region, sort : egen float est_wage_hourspw_7 = max(temp_est_wage_hourspw_7)
by wage_CIC female rural, sort : egen float est_wage_hourspw_8 = max(temp_est_wage_hourspw_8)
by wage_CIC female, sort : egen float est_wage_hourspw_9 = max(temp_est_wage_hourspw_9)
by wage_CIC, sort : egen float est_wage_hourspw_10 = max(temp_est_wage_hourspw_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_9 if track==9
replace track = 10 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_10 if track==10
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (46, 1 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.
drop est_*

* Formal Wage only possible definition in Ethiopian case!
gen formal = 0
replace formal = 1 if wage_payment_duration>=5 & wage_payment_duration<.
replace formal = 1 if wage_inkind_duration>=5 & wage_inkind_duration<.
replace formal = 1 if wage_employer==3 | wage_employer==4 | wage_employer==6 | wage_employer==7

* Formal Wage contract status only possible definition in Ethiopian case!
gen formalcon = 0
replace formalcon = 1 if wage_payment_duration>=5 & wage_payment_duration<.
replace formalcon = 1 if wage_inkind_duration>=5 & wage_inkind_duration<.

* added definition of "formality" due to SP
g formalsp = (wage_employer==3 | wage_employer==4 | wage_employer==6 | wage_employer==7) // if employer is government, state-owned firm, NGO/charity or political party, employment comes with social protection

* Generate Full Time Equivalents
gen fte_wage = wage_months*wage_weeks*wage_hours/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2

keep HHID household_id2 region zone ward town subcity FA ea member_id wage_CIC formal* fte_wage

* Bring in Member Demographic Info
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)

* FTEs by Formal
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*
** (!)inf2= the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* ! Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*

* Collapse to HH / CIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal* , by (HHID household_id2 region zone ward town subcity FA ea member_id wage_CIC)

* Bring in information for ISIC Groupings
rename wage_CIC CIC
joinby using "clean data/Ethiopia1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
forvalues w=1(1)2 {
	replace AFS_share = 0.722 if AFS_share==. & rural==1
	replace AFS_share = 0.683 if AFS_share==. & rural==0
}

** Generate occupation dummies
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}

*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
forvalues w=1(1)2 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
}
*

* Collapse to the household level
collapse (sum) w_o_* w_f_*, by (HHID household_id2 region zone ward town subcity FA ea member_id)

save "clean data/intermediate/Ethiopia1516_w_occCHEVAL.dta", replace

** Public Safety Net Program & Other Informal Labor
use "raw data/Household/sect4_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id

rename hh_s4q32 psnp_days
rename hh_s4q33 psnp_income
rename hh_s4q35 casual_days
rename hh_s4q36 casual_income

* Keep relevant data
keep HHID household_id2 region zone ward town subcity FA ea member_id psnp_* casual_*

* Calculate FTE (252 days in year is full time)
**** Assuming full time work during each day that household worked
* FTE equals 21 days per month, 252 days per year.
gen fte_psnp = psnp_days/252
gen fte_casual = casual_days/252

* Indicate if HH member completed any PSNP or Casual Labor
gen emp_psnp = 1 if fte_psnp>0 & fte_psnp~=.
gen emp_casual = 1 if fte_casual>0 & fte_casual~=.

drop if emp_psnp==. & emp_casual==.

mvdecode fte_* emp_*, mv(0)
mvencode fte_* emp_*, mv(0)

* Bring in Member Demographic Info
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/Ethiopia1516_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

*****
* Do not know the details of the type of work with either PSNP labor or Casual labor. Both considered CIC 612.
* Four mixed CIC Groups
gen AFS_share = .
replace AFS_share = 0.722 if rural==1
replace AFS_share = 0.683 if rural==0

local other "psnp casual"
foreach o in `other' {
	gen `o'_o_f0_c6 =.
	gen `o'_o_f0_c12 = .
	gen `o'_f_f0_c6 = .
	gen `o'_f_f0_c12 = .
}
*
local other "psnp casual"
foreach o in `other' {
	replace `o'_o_f0_c6 = AFS_share*emp_`o'
	replace `o'_o_f0_c12 = (1-AFS_share)*emp_`o'
	replace `o'_f_f0_c6 = AFS_share*fte_`o'
	replace `o'_f_f0_c12 = (1-AFS_share)*fte_`o'
}
*

keep HHID household_id2 region zone ward town subcity FA ea member_id psnp_o_* psnp_f_* casual_o_* casual_f_*

save "clean data/intermediate/Ethiopia1516_other_w_occ.dta", replace


** Own Farm Employment
* Post Planting & Post Harvest Data
* Occupation cannot be listed more than once, but FTE is summed
* Report one worker per observation
forvalues p = 1(1)4 {
* Post Planting
use "raw data/Post-Planting/sect3_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename pp_s3q27_a member_1
rename pp_s3q27_b farm_weeks_1
rename pp_s3q27_c farm_days_1
rename pp_s3q27_d farm_hourspd_1
rename pp_s3q27_e member_2
rename pp_s3q27_f farm_weeks_2
rename pp_s3q27_g farm_days_2
rename pp_s3q27_h farm_hourspd_2
rename pp_s3q27_i member_3
rename pp_s3q27_j farm_weeks_3
rename pp_s3q27_k farm_days_3
rename pp_s3q27_l farm_hourspd_3
rename pp_s3q27_m member_4
rename pp_s3q27_n farm_weeks_4
rename pp_s3q27_o farm_days_4
rename pp_s3q27_p farm_hourspd_4
keep rural region zone ward FA ea HHID household_id2 member_`p' farm_weeks_`p' farm_days_`p' farm_hourspd_`p'
rename member_`p' member_id
rename farm_weeks_`p' farm_weeks
rename farm_days_`p' farm_days
rename farm_hourspd_`p' farm_hourspd
drop if member_id==. | member_id==0
drop if farm_weeks==. & farm_days==. & farm_hourspd==.
gen worker = `p'

save "clean data/intermediate/Ethiopia1516_temp_farm_`p'.dta", replace
}
*
forvalues p = 5(1)8 {
* Post Harvest
use "raw data/Post-Harvest/sect10_ph_w3", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s10q02_a member_5
rename ph_s10q02_b farm_weeks_5
rename ph_s10q02_c farm_days_5
rename ph_s10q02_d farm_hourspd_5
rename ph_s10q02_e member_6
rename ph_s10q02_f farm_weeks_6
rename ph_s10q02_g farm_days_6
rename ph_s10q02_h farm_hourspd_6
rename ph_s10q02_i member_7
rename ph_s10q02_j farm_weeks_7
rename ph_s10q02_k farm_days_7
rename ph_s10q02_l farm_hourspd_7
rename ph_s10q02_m member_8
rename ph_s10q02_n farm_weeks_8
rename ph_s10q02_o farm_days_8
rename ph_s10q02_p farm_hourspd_8
keep rural region zone ward FA ea HHID household_id2 member_`p' farm_weeks_`p' farm_days_`p' farm_hourspd_`p'
rename member_`p' member_id
rename farm_weeks_`p' farm_weeks
rename farm_days_`p' farm_days
rename farm_hourspd_`p' farm_hourspd
drop if member_id==. | member_id==0
drop if farm_weeks==. & farm_days==. & farm_hourspd==.
gen worker = `p'

save "clean data/intermediate/Ethiopia1516_temp_farm_`p'.dta", replace
}
*
* Combine 
use "clean data/intermediate/Ethiopia1516_temp_farm_1.dta", clear
forvalues p = 2(1)8 {
	append using "clean data/intermediate/Ethiopia1516_temp_farm_`p'.dta"
}
*

***
*** Correct errors in data
*** if worked during a week/day/hour, the typical week/day/hour cannot be 0. (953 changes)
mvdecode farm_*, mv(0)
* Drop if no reported time working
drop if farm_weeks==. & farm_days==. & farm_hourspd==.
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16
* Results in changing 155 observations
* Assume that people do not average more than 7 days per week
replace farm_days = . if farm_days>7 
* Results in changing 151 observations
***** Assume if work during a week, the frmer worked at least one hour in one day
**** If worked weeks then assume to have worked days
**** Use median of weeks in wage job of those in the smallest geographic region with 10 observations of weekly wage labor (drops 93 observations)
joinby using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural region zone ward town subcity, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural region zone ward town, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural region zone ward, sort : egen float count_5 = count(1) if farm_weeks~=.
by female rural region zone, sort : egen float count_6 = count(1) if farm_weeks~=.
by female rural region, sort : egen float count_7 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_8 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural region zone ward town subcity FA ea, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural region zone ward town subcity FA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural region zone ward town subcity, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural region zone ward town, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural region zone ward, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
by female rural region zone, sort : egen float temp_est_farm_weeks_6 = median(farm_weeks) if farm_weeks~=.
by female rural region, sort : egen float temp_est_farm_weeks_7 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_8 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural region zone ward town subcity FA ea, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural region zone ward town subcity FA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural region zone ward town subcity, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural region zone ward town, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural region zone ward, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
by female rural region zone, sort : egen float est_farm_weeks_6 = max(temp_est_farm_weeks_6)
by female rural region, sort : egen float est_farm_weeks_7 = max(temp_est_farm_weeks_7)
by female rural, sort : egen float est_farm_weeks_8 = max(temp_est_farm_weeks_8)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_7 if track==7
replace track = 8 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_8 if track==8
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (117 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if farm_days~=.
by female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural region zone ward town subcity, sort : egen float count_3 = count(1) if farm_days~=.
by female rural region zone ward town, sort : egen float count_4 = count(1) if farm_days~=.
by female rural region zone ward, sort : egen float count_5 = count(1) if farm_days~=.
by female rural region zone, sort : egen float count_6 = count(1) if farm_days~=.
by female rural region, sort : egen float count_7 = count(1) if farm_days~=.
by female rural, sort : egen float count_8 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural region zone ward town subcity FA ea, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural region zone ward town subcity FA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural region zone ward town subcity, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural region zone ward town, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural region zone ward, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
by female rural region zone, sort : egen float temp_est_farm_days_6 = median(farm_days) if farm_days~=.
by female rural region, sort : egen float temp_est_farm_days_7 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_8 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural region zone ward town subcity FA ea, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural region zone ward town subcity FA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural region zone ward town subcity, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural region zone ward town, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural region zone ward, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
by female rural region zone, sort : egen float est_farm_days_6 = max(temp_est_farm_days_6)
by female rural region, sort : egen float est_farm_days_7 = max(temp_est_farm_days_7)
by female rural, sort : egen float est_farm_days_8 = max(temp_est_farm_days_8)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_7 if track==7
replace track = 8 if est_farm_days==.
replace est_farm_days = est_farm_days_8 if track==8
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (244 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural region zone ward town subcity, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural region zone ward town, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural region zone ward, sort : egen float count_5 = count(1) if farm_hourspd~=.
by female rural region zone, sort : egen float count_6 = count(1) if farm_hourspd~=.
by female rural region, sort : egen float count_7 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_8 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural region zone ward town subcity FA ea, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural region zone ward town subcity FA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural region zone ward town subcity, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural region zone ward town, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural region zone ward, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
by female rural region zone, sort : egen float temp_est_farm_hourspd_6 = median(farm_hourspd) if farm_hourspd~=.
by female rural region, sort : egen float temp_est_farm_hourspd_7 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_8 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural region zone ward town subcity FA ea, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural region zone ward town subcity FA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural region zone ward town subcity, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural region zone ward town, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural region zone ward, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
by female rural region zone, sort : egen float est_farm_hourspd_6 = max(temp_est_farm_hourspd_6)
by female rural region, sort : egen float est_farm_hourspd_7 = max(temp_est_farm_hourspd_7)
by female rural, sort : egen float est_farm_hourspd_8 = max(temp_est_farm_hourspd_8)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_7 if track==7
replace track = 8 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_8 if track==8
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (652 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate total hours worked
gen farm_hours = farm_weeks * farm_days * farm_hourspd

keep rural region zone ward town subcity FA ea HHID household_id2 member_id farm_hours

gen farm = 1 if farm_hours~=0

collapse (max) farm (sum) farm_hours, by (rural region zone ward town subcity FA ea HHID household_id2 member_id)

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
replace farm_fte = 2 if farm_fte>2
* Chages 11 of 8793 observations

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

* Keep Farming Data at the household level
keep HHID household_id2 region zone ward town subcity FA ea member_id farm_o_c1 farm_f_c1

save "clean data/intermediate/Ethiopia1516_farm_occ.dta", replace


**** COMBINE ALL ****
use "clean data/Ethiopia1516_member_demos.dta", clear
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/intermediate/Ethiopia1516_nfe_occ.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/intermediate/Ethiopia1516_w_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/intermediate/Ethiopia1516_other_w_occ.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID household_id2 region zone ward town subcity FA ea member_id using "clean data/intermediate/Ethiopia1516_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode nfe* w* psnp_* casual_* farm*, mv(0)
mvencode nfe* w* psnp_* casual_* farm*, mv(0)

** Generate Grouped Categorical Variables

** Generate Grouped Categorical Variables (!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*
*(!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}

forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
*

replace o_f2_c1 = o_f2_c1 + farm_o_c1

forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}
*

replace f_f2_c1 = f_f2_c1 + farm_f_c1

local other "psnp casual"
foreach o in `other' {
	replace o_f0_c6 = o_f0_c6 + `o'_o_f0_c6
	replace o_f0_c12 = o_f0_c12 + `o'_o_f0_c12
	replace f_f0_c6 = f_f0_c6 + `o'_f_f0_c6
	replace f_f0_c12 = f_f0_c12 + `o'_f_f0_c12
}
*

* Total (f3) Casual, Formal and Self Employed
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f0_c`c'
	replace total_jobs = total_jobs + o_f1_c`c'
	replace total_jobs = total_jobs + o_f2_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	
*

ren household_id2 hhid 

joinby hhid member_id using "clean data/Ethiopia1516_iid.dta", unmatched(none)

** keep only relevant variables:

keep member_id fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete school_secondary school_level hhid
tostring member_id, g(iid)
drop member_id
gen country="Ethiopia"

save "clean data/ETHIOPIA_FTEsCHEVAL.dta", replace
