********************************************************************************
***************** MALAWI 2016/2017: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)


The dataset used for this is:
Malawi 2016/2017 Fourth Integrated Household Survey

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")

Stata Version 15.1

-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "clean data/intermediate/Malawi1617_wage_incomeCHEVAL.dta", replace
save "clean data/MALAWI_incomesharesCHEVAL.dta", replace

Informality types:

-	Contract: Has a contract; duration is permanent, fixed term with duration more than 1X?, government
-	Social protection: Pension scheme and/or health insurance through job

Some glossary:
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force

*/

capture log close
clear
clear matrix
set more off


********************************************************************************
******************************* SET DRIVES *************************************
gl MALAWI	"$path\data\Malawi_IHS_1617"

gl DO   	    ${MALAWI}/do
gl raw data     ${MALAWI}/raw data 
gl clean data 	${MALAWI}/clean data
gl FIG 		    ${MALAWI}/graphs
gl TAB 		    ${MALAWI}/tables
 
********************************************************************************
cd "$MALAWI"

*** INCOME SHARES OF FARMING AND FARM PRODUCE SALES ***

*get from each module the value of production, sales, own consumption and aggregate at household level:
*** AG-INCOME = crops, crop products, forestry, livestock, livestock by-products
		*reference period for ag. income is past 12 months

** Occupations in Non-Farm Own Enterprises

**** BIG ASSUMPTION
* There are inconsistencies between N25 and N30, where N25 states that the NFE was non operational, but there 
* is household labor working during these months and there is reported profit. 
* Use the maximum number of months reported by any household member as the number of months of NFE operation.
* Consider the last month of profits as the average profit levels.
****

use "raw data/HH_MOD_N2.dta", clear
rename hh_n09c nfe_CIC
format %40.0g nfe_CIC
rename hh_n09a entid
gen nfe_months = 0
forvalues m=1(1)6 {
	replace nfe_months = hh_n30d`m' if hh_n30d`m'>nfe_months & hh_n30d`m'~=.
}
*
rename hh_n32 last_mo_sales
* Last month's costs
gen last_mo_costs = 0
mvdecode hh_n31d1 hh_n31d2 hh_n31d3 hh_n41a hh_n41b hh_n41c hh_n41d hh_n41e hh_n41f hh_n41g hh_n41h, mv(0)
mvencode hh_n31d1 hh_n31d2 hh_n31d3 hh_n41a hh_n41b hh_n41c hh_n41d hh_n41e hh_n41f hh_n41g hh_n41h, mv(0)
local costs "hh_n31d1 hh_n31d2 hh_n31d3 hh_n41a hh_n41b hh_n41c hh_n41d hh_n41e hh_n41f hh_n41g hh_n41h"
foreach c in `costs' {
	replace last_mo_costs = last_mo_costs + `c'
}
*
gen last_mo_net_inc = last_mo_sales - last_mo_costs
rename hh_n40 last_mo_profit

* Keep relevant data
keep case_id entid nfe_CIC nfe_months last_mo_profit last_mo_net_inc last_mo_sales last_mo_costs

drop if nfe_months==0 | last_mo_net_inc==.

* Bring in Demographic Info
joinby case_id using "clean data/Malawi1617_demographics.dta", unmatched(none)

gen nfe_ann_gross_inc = nfe_months*last_mo_sales
gen nfe_ann_costs = nfe_months*last_mo_costs
gen nfe_ann_net_inc = nfe_months*last_mo_net_inc

* Collapse to HH / CIC level
replace nfe_CIC = 99999 if nfe_CIC==0
collapse (sum) nfe_ann*, by (case_id nfe_CIC)

* Calculate Income Per Day
gen nfe_gross_inc = nfe_ann_gross_inc/365
gen nfe_costs = nfe_ann_costs/365
gen nfe_net_inc = nfe_ann_net_inc/365

* Bring in information for CIC Groupings
rename nfe_CIC CIC
joinby CIC using "clean data/Malawi1617_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace ISIC_group = 612 if ISIC_group==.
joinby case_id using "clean data/Malawi1617_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC nfe_CIC

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.644 if AFS_share==. & rural==1
replace AFS_share = 0.540 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612


* Keep relevant variables
order case_id nfe_CIC ISIC_group nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*
keep case_id nfe_CIC ISIC_group nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_net_inc nfe_gross_inc nfe_costs nfe_i_* nfe_gi_* nfe_c_*, by (case_id)

* Drop 14 observations with zero earning
drop if nfe_net_inc==0 & nfe_gross_inc==0 & nfe_costs==0

save "clean data/intermediate/Malawi1617_nfe_income.dta", replace

** Wage Income
* Transpose data so that 2 Wage occupations are in one set of variables
use "raw data/HH_MOD_E.dta", clear

**adding ANY emp type performed in last 12 months:
recode hh_e06_1a hh_e06_1b hh_e06_1c hh_e06_2 hh_e06_3 hh_e06_4 hh_e06_5 hh_e06_6 (2=0) //cleaning emp status over last 12 months 
* Rename variables
rename hh_e20b wage_CIC
rename hh_e25 wage_payment
rename hh_e26a wage_payment_duration
rename hh_e26b wage_payment_duration_unit
rename hh_e27 wage_inkind
rename hh_e28a wage_inkind_duration
rename hh_e28b wage_inkind_duration_unit
rename hh_e22 wage_months
rename hh_e23 wage_weeks
rename hh_e24 wage_hours
rename hh_e21_2 w_contract
rename hh_e21_3 pension
rename hh_e21_4 health_insurance
order case_id PID wage_* w_contract pension health_insurance 
keep case_id PID wage_* w_contract pension health_insurance 
gen wage_job = 1
save "clean data/intermediate/Malawi1617_temp_wage_a.dta", replace
use "raw data/HH_MOD_E.dta", clear
* Rename variables
rename hh_e34_code wage_CIC
rename hh_e35_2 w_contract
rename hh_e35_3 pension
rename hh_e35_4 health_insurance
rename hh_e36 wage_months
rename hh_e37 wage_weeks
rename hh_e38 wage_hours
rename hh_e39 wage_payment
rename hh_e40a wage_payment_duration
rename hh_e40b wage_payment_duration_unit
rename hh_e41 wage_inkind
rename hh_e42a wage_inkind_duration
rename hh_e42b wage_inkind_duration_unit
destring wage_payment_duration wage_inkind, replace
order case_id PID wage_* w_contract pension health_insurance
keep case_id PID wage_* w_contract pension health_insurance
gen wage_job = 2
save "clean data/intermediate/Malawi1617_temp_wage_b.dta", replace

use "clean data/intermediate/Malawi1617_temp_wage_a.dta", clear
append using "clean data/intermediate/Malawi1617_temp_wage_b.dta"

mvdecode wage_CIC wage_months wage_weeks wage_hours, mv(0)
drop if wage_CIC==. & wage_months==. & wage_weeks==. & wage_hours==.

* Clear Errors in data
* No occupation code, but reported earnings. (252 changes)
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=. | wage_CIC==. & wage_months~=. | wage_CIC==. & wage_weeks~=. | wage_CIC==. & wage_hours~=.
* Cannot work more than 5 weeks per month (1 change)
replace wage_weeks = . if wage_weeks>5 & wage_weeks~=.
**** Assume that reported hours per day above 16 hours per day are in error, so adjust to 16 hours per day. (affects calculations of 30 observations)
replace wage_hours = . if wage_hours>80
**** If worked within the past week, must have worked within the past month, etc
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Malawi1617_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region district, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
drop est_wage_months_* count_*
**** Replace missing value of wage_months (18 changes)
replace wage_months = est_wage_months if wage_months==. & wage_CIC~=.
replace wage_months = 1 if wage_months==. & wage_CIC~=.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region district, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
capture drop track
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_4 if track==4
drop est_wage_weeks_* count_*
* Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==. & wage_CIC~=.
replace wage_weeks = 1 if wage_weeks==. & wage_CIC~=.
* Count observations with wage_hours reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_hours~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_hours~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_hours~=.
* Calculate median of wage_hours by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_hours_1 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_hours_2 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_hours_3 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hours_4 = median(wage_hours) if wage_hours~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_hours_1 = max(temp_est_wage_hours_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_hours_2 = max(temp_est_wage_hours_2)
by wage_CIC female rural region district, sort : egen float est_wage_hours_3 = max(temp_est_wage_hours_3)
by wage_CIC female rural region, sort : egen float est_wage_hours_4 = max(temp_est_wage_hours_4)
drop temp*
* Build wage_hours prediction variable if at least 10 observations of reported wage_hours in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
capture drop track
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hours = est_wage_hours_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==3
replace track = 4 if est_wage_hours==.
replace est_wage_hours = est_wage_hours_4 if track==4
drop est_wage_hours_* count_*
* Replace missing value of wage_hours (32 changes)
replace wage_hours = est_wage_hours if wage_hours==. & wage_CIC~=.
replace wage_hours = 1 if wage_hours==. & wage_CIC~=.
* Calculate total wage payments per year
gen wage_total = 0
gen wage_total_cash = 0
gen wage_total_inkind = 0
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hours/8
replace est_wage_days = 7 if est_wage_days>7
replace wage_total_cash = (wage_payment/wage_payment_duration)*(est_wage_days)*wage_weeks*wage_months if wage_payment_duration_unit==3
replace wage_total_cash = (wage_payment/wage_payment_duration)*wage_weeks*wage_months if wage_payment_duration_unit==4
replace wage_total_cash = (wage_payment/wage_payment_duration)*wage_months if wage_payment_duration_unit==5
replace wage_total_inkind = (wage_inkind/wage_inkind_duration)*(est_wage_days)*wage_weeks*wage_months if wage_inkind_duration_unit==3
replace wage_total_inkind = (wage_inkind/wage_inkind_duration)*wage_weeks*wage_months if wage_inkind_duration_unit==4
replace wage_total_inkind = (wage_inkind/wage_inkind_duration)*wage_months if wage_inkind_duration_unit==5
replace wage_total = wage_total_cash if wage_total_cash~=.
replace wage_total = wage_total+wage_total_inkind if wage_total_inkind~=.

* Calculate total wage payments per day
gen wage_income = wage_total/365

* Casual vs Formal Wage; here the definition is a combination of (i) receive payments monthly, quarterly, etc (ii)gen formal = 0
g formal = 0
replace formal = 1 if wage_payment_duration_unit>=5 & wage_payment_duration_unit<.
replace formal = 1 if wage_inkind_duration_unit>=5 & wage_inkind_duration_unit<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

//added definition of "formality" due to contract structure 
g formalcon =  (w_contract==1) 

//added definition of "formality" due to SP
g formalsp = (pension==1 | health_insurance==1)

* Calculate farm income, non-farm income and total income per individual per day
* bring in AFS Shares
rename wage_CIC CIC
replace CIC = 99999 if CIC==.
joinby CIC using "clean data/Malawi1617_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Bring in Member Demographic Info
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(none)
* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.644 if AFS_share==. & rural==1
replace AFS_share = 0.540 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen wage_i_f`f'_c`c' = 0
	gen wage_i_fcon`f'_c`c' = 0 //(!)
	gen wage_i_fsp`f'_c`c' = 0 //(!)
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
	replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f' //(!)
	replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f' //(!)
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'
	*(!)
	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	*(!)
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}
*

g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0)


* keep only for those 16 and older: (!)
keep if age>=16

* Keep relevant variables
order case_id CIC ISIC_group wage_income wage_i_* wage_job w_contract pension health_insurance 
keep case_id CIC ISIC_group wage_income wage_i_* informal* formal* wage_job w_contract pension health_insurance 

tab wage_job,g(job) // do you have a wage job yes or no?
* Not possible to differentiate between permanent and temporary contract in Malawian data

recode pension health_insurance (2=0) 	//0-1 variable for counts


* Collapse to household level 
collapse (sum) wage_income wage_i_* job1 job2 Ninformal=informal N_pension=pension N_health_insurance=health_insurance (mean) informal* formal* sh_pension=pension sh_health_insurance=health_insurance , by (case_id)


**labels
foreach v in pension health_insurance {
la var sh_`v' "Share of HHmemb with `v'"
la var N_`v' "N HHmemb with `v'"
}
la var job1 		"N HHmemb with wagejob(primary)"
la var job2 		"N HHmemb with wagejob(secondary)"
la var informal 	"Informal sh of wagework(no contract,SP)"	// important variable
la var Ninformal	"N HHmemb with informal wagework"
la var formal		"Formal sh of wagework(any contract,SP)"
la var informalcon 	"Informal(no contract)"
la var formalcon  	"Formal sh of wagework(contract)"
la var informalsp 	"Informal sh of wagework(SP:insurance/pension)"
la var formalsp 	"Formal sh of wagework(SP:insurance/pension)"

global lab1 "OwnFarming"	
global lab2 "Farm Labor"	
global lab3 "Food&Ag Manufacturing w/in AFS"
global lab4 "FoodPrep"	
global lab5 "FoodSystem Marketing&Transport"
global lab6 "AFS Unclassified"	
global lab7 "Non-AFS Manufacturing"
global lab8 "Non-AFS Industry"	
global lab9 "Non-AFS Marketing&Transport"
global lab10 "Non-AFS Other"
global lab11 "Non-AFS PublicService"
global lab12 "Non-AFS Unclassifed"

forv f=1/12 {
la var wage_i_f0_c`f'    "Informal w from ${lab`f'}"
la var wage_i_fcon0_c`f' "Informal w from ${lab`f'}(contract)"
la var wage_i_fsp0_c`f'  "Informal w from ${lab`f'}(SP)"
la var wage_i_f1_c`f'    "Formal w from ${lab`f'}"
la var wage_i_fcon1_c`f' "Formal w from ${lab`f'}(contract)"
la var wage_i_fsp1_c`f'  "Formal w from ${lab`f'}(SP)"
}

save "clean data/intermediate/Malawi1617_wage_incomeCHEVAL.dta", replace


** Ganyu Labor
use "raw data/HH_MOD_E.dta", clear
rename hh_e56 g_months
rename hh_e57 g_weeks
rename hh_e58 g_days
rename hh_e59 g_wagepd

* Keep relevant data
keep case_id PID g_*
keep if g_wagepd~=.

* Correct error
replace g_days = . if g_days>7 

**** Estimate days based on median days of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Malawi1617_member_demos.dta", unmatched(none)
* Count observations with hours/week reported by geographic location.
by rural female region district TA ea, sort : egen float count_1 = count(1) if g_days~=.
by rural female region district TA, sort : egen float count_2 = count(1) if g_days~=.
by rural female region district, sort : egen float count_3 = count(1) if g_days~=.
by rural female region, sort : egen float count_4 = count(1) if g_days~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by rural female region district TA ea, sort : egen float temp_est_days_1 = median(g_days) if g_days~=.
by rural female region district TA, sort : egen float temp_est_days_2 = median(g_days) if g_days~=.
by rural female region district, sort : egen float temp_est_days_3 = median(g_days) if g_days~=.
by rural female region, sort : egen float temp_est_days_4 = median(g_days) if g_days~=.
mvencode temp_*, mv(0)

by rural female region district TA ea, sort : egen float est_days_1 = max(temp_est_days_1)
by rural female region district TA, sort : egen float est_days_2 = max(temp_est_days_2)
by rural female region district, sort : egen float est_days_3 = max(temp_est_days_3)
by rural female region, sort : egen float est_days_4 = max(temp_est_days_4)
drop temp*

* Build hours per week prediction variable if at least 10 observations of reported hours per week in location counted above.
*	Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_days = est_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_days==.
replace est_days = est_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_days==.
replace est_days = est_days_3 if track==3
replace track = 4 if est_days==.
replace est_days = est_days_4 if track==4
drop est_days_* count_*
replace g_days = est_days if g_days==.
* (1 change) 

* Calculate total days
gen g_days_total = g_months*g_weeks*g_days
drop if g_days_total==.

* Calculate total Ganyu income
gen g_income_annual = g_days_total*g_wagepd
drop if g_income_annual==0

* Calculate Ganyu income per day
gen g_income = g_income_annual/365
gen g_farm_income = g_income
gen g_nonfarm_income = 0

collapse (sum) g_income g_farm_income g_nonfarm_income, by (case_id)

save "clean data/intermediate/Malawi1617_ganyu_inc.dta", replace


*** Own Farm Income
* Rainy Season
* Prices
use "raw data/AG_MOD_I.dta", clear
rename ag_i02a crop_qty
rename ag_i02b unit
rename ag_i02b_oth unit_other
rename ag_i02c shelled
rename ag_i03 crop_sales
keep case_id crop_* unit unit_other shelled

* Estimate prices per standard unit for crop
drop if crop_code==.
drop if crop_sales==.

* Bring in the conversion data that I could find & apply
joinby crop_code unit unit_other shelled using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace crop_qty = crop_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.

* Estimate prices
gen revealed_price = crop_sales/crop_qty
replace revealed_price = . if revealed_price==0

**** Use median of revealed prices by crop in the smallest geographic with 10 observations of revealed prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
* Count observations with revealed_price reported by geographic location.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float count_4 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by crop_code unit unit_other shelled rural region district TA, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by crop_code unit unit_other shelled rural region district, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by crop_code unit unit_other shelled rural region, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_revealed_price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_3 if track==3
replace track = 4 if est_revealed_price==.
replace est_revealed_price = est_revealed_price_4 if track==4
drop est_revealed_price_* count_*

order rural region district TA ea crop_code unit unit_other shelled est_revealed_price
keep rural region district TA ea crop_code unit unit_other shelled est_revealed_price

gen count = 1
collapse (sum) count, by (rural region district TA ea crop_code unit unit_other shelled est_revealed_price)
drop count

save "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", replace

* Rainy Season Sales
use "raw data/AG_MOD_I.dta", clear
rename ag_i02a sales_crop_qty
rename ag_i02b unit
rename ag_i02b_oth unit_other
rename ag_i02c shelled
rename ag_i03 sales_crop_sales
rename ag_i10 farm_sales_r_transport
keep case_id crop_code sales_* farm_sales_r_transport unit unit_other shelled
drop if crop_code==.

* Bring in the conversion data that I could find & apply
joinby using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace sales_crop_qty = sales_crop_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.

* Bring in prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
joinby rural region district TA ea crop_code unit unit_other shelled using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
drop _merge

* Estimate Farm Sales
gen farm_sales_r = sales_crop_qty*est_revealed_price

* Collapse to household level
collapse (sum) farm_sales_r farm_sales_r_transport, by (case_id)
drop if farm_sales_r==0 & farm_sales_r_transport==0

replace farm_sales_r = farm_sales_r/365
replace farm_sales_r_transport = farm_sales_r_transport/365

save "clean data/intermediate/Malawi1617_farm_sales_rainy.dta", replace

* Dry Season
* Prices
use "raw data/AG_MOD_O.dta", clear
rename ag_o02a sales_crop_qty
rename ag_o02b unit
rename ag_o02c shelled
rename ag_o03 sales_crop_sales
keep case_id crop_code sales_* unit shelled

* Estimate prices per standard unit for crop
drop if crop_code==.
drop if sales_crop_sales==.

* Bring in the conversion data that I could find & apply
joinby crop_code unit shelled using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace sales_crop_qty = sales_crop_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.

* Estimate prices
gen revealed_price = sales_crop_sales/sales_crop_qty
replace revealed_price = . if revealed_price==0

**** Use median of revealed prices by crop in the smallest geographic with 10 observations of revealed prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
* Count observations with revealed_price reported by geographic location.
by crop_code unit shelled rural region district TA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code unit shelled rural region district TA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code unit shelled rural region district, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code unit shelled rural region, sort : egen float count_4 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by crop_code unit shelled rural region district TA ea, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by crop_code unit shelled rural region district TA, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by crop_code unit shelled rural region district, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by crop_code unit shelled rural region, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit shelled rural region district TA ea, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by crop_code unit shelled rural region district TA, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by crop_code unit shelled rural region district, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by crop_code unit shelled rural region, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic
generate track = 1 if count_1>=10 & count_1<100000
generate est_revealed_price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_3 if track==3
replace track = 4 if est_revealed_price==.
replace est_revealed_price = est_revealed_price_4 if track==4
drop est_revealed_price_* count_*

order rural region district TA ea crop_code unit shelled est_revealed_price
keep rural region district TA ea crop_code unit shelled est_revealed_price

gen count = 1
collapse (sum) count, by (rural region district TA ea crop_code unit shelled est_revealed_price)
drop count

save "clean data/intermediate/Malawi1617_crop_prices_dry.dta", replace

* Dry Season Sales
use "raw data/AG_MOD_O.dta", clear
rename ag_o02a sales_crop_qty
rename ag_o02b unit
rename ag_o02c shelled
rename ag_o03 sales_crop_sales
rename ag_o10 farm_sales_d_transport
keep case_id crop_code sales_* farm_sales_d_transport unit shelled
drop if crop_code==.

* Bring in the conversion data that I could find & apply
joinby crop_code unit shelled using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace sales_crop_qty = sales_crop_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.

* Bring in prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
joinby rural region district TA ea crop_code unit shelled using "clean data/intermediate/Malawi1617_crop_prices_dry.dta", unmatched(both) _merge(_merge)
drop _merge

* Estimate Farm Sales
gen farm_sales_d = sales_crop_qty*est_revealed_price

* Collapse to household level
collapse (sum) farm_sales_d farm_sales_d_transport, by (case_id)
drop if farm_sales_d==0 & farm_sales_d_transport==0

replace farm_sales_d = farm_sales_d/365
replace farm_sales_d_transport = farm_sales_d_transport/365

save "clean data/intermediate/Malawi1617_farm_sales_dry.dta", replace

* Permanent Crop
* Prices
use "raw data/AG_MOD_Q.dta", clear
rename ag_q02a perm_qty
rename ag_q02b perm_unit
rename ag_q02b_oth perm_unit_other
rename ag_q03 perm_sold
keep case_id crop_code perm_*

* Estimate prices per standard unit for perm
drop if crop_code==.
drop if perm_sold==.

* Bring in the conversion data that I could find & apply
joinby using "clean data/Malawi1617_kg_conversion_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace perm_qty = perm_qty*kg_conver if kg_conver~=.
replace perm_unit = 1 if kg_conver~=.
replace perm_unit_other = "" if kg_conver~=.

* Estimate prices
gen revealed_price = perm_sold/perm_qty
replace revealed_price = . if revealed_price==0

**** Use median of revealed prices by perm in the smallest geographic with 10 observations of revealed prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
* Count observations with revealed_price reported by geographic location.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float count_4 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by crop_code perm_unit perm_unit_other rural region district, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by crop_code perm_unit perm_unit_other rural region, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_revealed_price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_3 if track==3
replace track = 4 if est_revealed_price==.
replace est_revealed_price = est_revealed_price_4 if track==4
drop est_revealed_price_* count_*
** One crop (a piece of "other crop" in rural Zomba) with a price of zero

order rural region district TA ea crop_code perm_unit perm_unit_other est_revealed_price
keep rural region district TA ea crop_code perm_unit perm_unit_other est_revealed_price

gen count = 1
collapse (sum) count, by (rural region district TA ea crop_code perm_unit perm_unit_other est_revealed_price)
drop count

save "clean data/intermediate/Malawi1617_perm_prices.dta", replace

* Permanent Crop Sales
use "raw data/AG_MOD_Q.dta", clear
rename ag_q02a perm_qty
rename ag_q02b perm_unit
rename ag_q02b_oth perm_unit_other
rename ag_q03 perm_sold
rename ag_q10 perm_sales_transport
keep case_id crop_code perm_*
drop if crop_code==.
drop if perm_qty==.

* Bring in the conversion data that I could find & apply
joinby using "clean data/Malawi1617_kg_conversion_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace perm_qty = perm_qty*kg_conver if kg_conver~=.
replace perm_unit = 1 if kg_conver~=.
replace perm_unit_other = "" if kg_conver~=.

* Bring in prices
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
joinby rural region district TA ea crop_code perm_unit perm_unit_other using "clean data/intermediate/Malawi1617_perm_prices.dta", unmatched(both) _merge(_merge)
drop _merge

* Estimate perm Sales
gen perm_sales = perm_qty*est_revealed_price

* Collapse to household level
collapse (sum) perm_sales perm_sales_transport, by (case_id)
drop if perm_sales==0 & perm_sales_transport==0

replace perm_sales = perm_sales/365
replace perm_sales_transport = perm_sales_transport/365

save "clean data/intermediate/Malawi1617_perm_sales.dta", replace

* Harvest
* Rainy Crop
use "raw data/AG_MOD_G.dta", clear
rename ag_g13a harvest_qty
rename ag_g13b unit
rename ag_g13b_oth unit_other
rename ag_g13c shelled
keep case_id crop_code harvest_qty unit unit_other shelled
drop if crop_code==.
drop if harvest_qty==0
drop if harvest_qty==.
replace shelled = 3 if shelled==.

* Convert to shelled if not applicable and standard to be shelled
local notapp "1 2 3 4"
foreach na in `notapp' {
	replace shelled  = 1 if shelled==3 & crop_code==`na'
}
*

* Convert to not applicable if shelled and unshelled do not make sense
local notapp "5 6 7 8 9 10 42 43 44 45"
foreach na in `notapp' {
	replace shelled  = 3 if shelled==1 & crop_code==`na'
	replace shelled  = 3 if shelled==2 & crop_code==`na'
}
*

joinby crop_code unit unit_other shelled using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace harvest_qty = harvest_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.
drop kg_conver

joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)

joinby rural region district TA ea crop_code unit unit_other shelled using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other shelled rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other shelled rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other shelled rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other shelled rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other shelled, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track

gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*

replace price = . if price==0

* Count observations with price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_price_2 = median(price) if price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_price_3 = median(price) if price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_price_4 = median(price) if price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_price_5 = median(price) if price~=.
by crop_code unit unit_other, sort : egen float temp_est_price_6 = median(price) if price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_price_1 = max(temp_est_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_price_2 = max(temp_est_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_price_3 = max(temp_est_price_3)
by crop_code unit unit_other rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by crop_code unit unit_other rural, sort : egen float est_price_5 = max(temp_est_price_5)
by crop_code unit unit_other, sort : egen float est_price_6 = max(temp_est_price_6)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if est_price==.
replace est_price = est_price_6 if track==6
drop est_price_* count_* track

replace price = est_price if price==.
drop est_price

save "clean data/intermediate/Malawi1617_temp_harvest_rainy.dta", replace

* Rainy Crop Loss
use "raw data/AG_MOD_I.dta", clear
rename ag_i36a lost_qty
rename ag_i36b unit
rename ag_i36b_oth unit_other
rename ag_i36c shelled
keep case_id crop_code lost_qty unit unit_other shelled
replace shelled = 3 if shelled==.
mvdecode lost_qty, mv(0)
mvencode lost_qty, mv(0)
drop if lost_qty==0

* Convert to shelled if not applicable and standard to be shelled
local notapp "1 2 3 4"
foreach na in `notapp' {
	replace shelled  = 1 if shelled==3 & crop_code==`na'
}
*

* Convert to not applicable if shelled and unshelled do not make sense
local notapp "5 6 7 8 9 10 42 43 44 45"
foreach na in `notapp' {
	replace shelled  = 3 if shelled==1 & crop_code==`na'
	replace shelled  = 3 if shelled==2 & crop_code==`na'
}
*

joinby crop_code unit unit_other shelled using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace lost_qty = lost_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.
drop kg_conver

save "clean data/intermediate/Malawi1617_lostqty_harvest_rainy.dta", replace

* Rainy Crop Loss Percentage
use "raw data/AG_MOD_I.dta", clear
rename ag_i36d lost_percent
keep case_id crop_code lost_percent
mvdecode lost_percent, mv(0)
mvencode lost_percent, mv(0)
drop if lost_percent==0

save "clean data/intermediate/Malawi1617_lostper_harvest_rainy.dta", replace

use "clean data/intermediate/Malawi1617_temp_harvest_rainy.dta", clear
joinby case_id crop_code using "clean data/intermediate/Malawi1617_lostper_harvest_rainy.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
mvencode lost_percent, mv(0)

joinby case_id crop_code unit unit_other shelled using "clean data/intermediate/Malawi1617_lostqty_harvest_rainy.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace lost_qty = 0 if lost_qty==.

replace harvest_qty = harvest_qty - lost_qty
replace harvest_qty = 0 if harvest_qty<0
replace harvest_qty = harvest_qty * ((100-lost_percent)/100)

generate harvest_value_r = harvest_qty * price

collapse (sum) harvest_value_r, by (case_id)

replace harvest_value_r = harvest_value_r/365

save "clean data/intermediate/Malawi1617_harvest_rainy.dta", replace


* Harvest
* dry Crop
use "raw data/AG_MOD_M.dta", clear
rename ag_m11a harvest_qty
rename ag_m11b unit
rename ag_m11b_oth unit_other
rename ag_m11c shelled
keep case_id crop_code harvest_qty unit unit_other shelled
drop if crop_code==.
drop if harvest_qty==0
drop if harvest_qty==.
replace shelled = 3 if shelled==.

* Convert to shelled if not applicable and standard to be shelled
local notapp "1 2 3 4"
foreach na in `notapp' {
	replace shelled  = 1 if shelled==3 & crop_code==`na'
}
*

* Convert to not applicable if shelled and unshelled do not make sense
local notapp "5 6 7 8 9 10 42 43 44 45"
foreach na in `notapp' {
	replace shelled  = 3 if shelled==1 & crop_code==`na'
	replace shelled  = 3 if shelled==2 & crop_code==`na'
}
*

joinby crop_code unit unit_other shelled using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace harvest_qty = harvest_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.
drop kg_conver

joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)

joinby rural region district TA ea crop_code unit shelled using "clean data/intermediate/Malawi1617_crop_prices_dry.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other shelled rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other shelled rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other shelled rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other shelled rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other shelled, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track

gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*

replace price = . if price==0

* Count observations with price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_price_2 = median(price) if price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_price_3 = median(price) if price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_price_4 = median(price) if price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_price_5 = median(price) if price~=.
by crop_code unit unit_other, sort : egen float temp_est_price_6 = median(price) if price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_price_1 = max(temp_est_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_price_2 = max(temp_est_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_price_3 = max(temp_est_price_3)
by crop_code unit unit_other rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by crop_code unit unit_other rural, sort : egen float est_price_5 = max(temp_est_price_5)
by crop_code unit unit_other, sort : egen float est_price_6 = max(temp_est_price_6)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if est_price==.
replace est_price = est_price_6 if track==6
drop est_price_* count_* track

replace price = est_price if price==.
drop est_price

save "clean data/intermediate/Malawi1617_temp_harvest_dry.dta", replace

* dry Crop Loss
use "raw data/AG_MOD_O.dta", clear
rename ag_o36a lost_qty
rename ag_o36b unit
rename ag_o36b_oth unit_other
rename ag_o36c shelled
keep case_id crop_code lost_qty unit unit_other shelled
replace shelled = 3 if shelled==.
mvdecode lost_qty, mv(0)
mvencode lost_qty, mv(0)
drop if lost_qty==0

* Convert to shelled if not applicable and standard to be shelled
local notapp "1 2 3 4"
foreach na in `notapp' {
	replace shelled  = 1 if shelled==3 & crop_code==`na'
}
*

* Convert to not applicable if shelled and unshelled do not make sense
local notapp "5 6 7 8 9 10 42 43 44 45"
foreach na in `notapp' {
	replace shelled  = 3 if shelled==1 & crop_code==`na'
	replace shelled  = 3 if shelled==2 & crop_code==`na'
}
*

joinby crop_code unit unit_other shelled using "clean data/Malawi1617_kg_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge season
replace lost_qty = lost_qty*kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
replace unit_other = "" if kg_conver~=.
drop kg_conver

save "clean data/intermediate/Malawi1617_lostqty_harvest_dry.dta", replace

* dry Crop Loss Percentage
use "raw data/AG_MOD_O.dta", clear
rename ag_o36d lost_percent
keep case_id crop_code lost_percent
mvdecode lost_percent, mv(0)
mvencode lost_percent, mv(0)
drop if lost_percent==0

save "clean data/intermediate/Malawi1617_lostper_harvest_dry.dta", replace

use "clean data/intermediate/Malawi1617_temp_harvest_dry.dta", clear
joinby case_id crop_code using "clean data/intermediate/Malawi1617_lostper_harvest_dry.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
mvencode lost_percent, mv(0)

joinby case_id crop_code unit unit_other shelled using "clean data/intermediate/Malawi1617_lostqty_harvest_dry.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace lost_qty = 0 if lost_qty==.

replace harvest_qty = harvest_qty - lost_qty
replace harvest_qty = 0 if harvest_qty<0
replace harvest_qty = harvest_qty * ((100-lost_percent)/100)

generate harvest_value_d = harvest_qty * price

collapse (sum) harvest_value_d, by (case_id)

replace harvest_value_d = harvest_value_d/365

save "clean data/intermediate/Malawi1617_harvest_dry.dta", replace

* Harvest
* Permanent Crop
use "raw data/AG_MOD_P.dta", clear
rename ag_p09a harvest_qty
rename ag_p09b perm_unit
rename ag_p09b_oth perm_unit_other
rename ag_p0c crop_code
replace crop_code = 16 if crop_code==21
replace crop_code = 1 if crop_code==100
keep case_id crop_code harvest_qty perm_unit perm_unit_other
drop if crop_code>100
drop if harvest_qty==0
drop if harvest_qty==.
replace perm_unit_other = "90 KG BAG" if perm_unit_other == "90KG"
replace perm_unit_other = "90 KG BAG" if perm_unit_other == "90KGBAG"

joinby crop_code perm_unit perm_unit_other using "clean data/Malawi1617_kg_conversion_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace harvest_qty = harvest_qty*kg_conver if kg_conver~=.
replace perm_unit = 1 if kg_conver~=.
replace perm_unit_other = "" if kg_conver~=.
drop kg_conver

joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)

joinby rural region district TA ea crop_code perm_unit using "clean data/intermediate/Malawi1617_perm_prices.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use predicted prices of from households in other spatial perm_units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code perm_unit perm_unit_other, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code perm_unit perm_unit_other rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code perm_unit perm_unit_other rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code perm_unit perm_unit_other rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code perm_unit perm_unit_other, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track

gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*

replace price = . if price==0

* Count observations with price reported by geographic location.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float count_1 = count(1) if price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float count_2 = count(1) if price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float count_3 = count(1) if price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float count_4 = count(1) if price~=.
by crop_code perm_unit perm_unit_other rural, sort : egen float count_5 = count(1) if price~=.
by crop_code perm_unit perm_unit_other, sort : egen float count_6 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float temp_est_price_2 = median(price) if price~=.
by crop_code perm_unit perm_unit_other rural region district, sort : egen float temp_est_price_3 = median(price) if price~=.
by crop_code perm_unit perm_unit_other rural region, sort : egen float temp_est_price_4 = median(price) if price~=.
by crop_code perm_unit perm_unit_other rural, sort : egen float temp_est_price_5 = median(price) if price~=.
by crop_code perm_unit perm_unit_other, sort : egen float temp_est_price_6 = median(price) if price~=.
mvencode temp_*, mv(0)
by crop_code perm_unit perm_unit_other rural region district TA ea, sort : egen float est_price_1 = max(temp_est_price_1)
by crop_code perm_unit perm_unit_other rural region district TA, sort : egen float est_price_2 = max(temp_est_price_2)
by crop_code perm_unit perm_unit_other rural region district, sort : egen float est_price_3 = max(temp_est_price_3)
by crop_code perm_unit perm_unit_other rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by crop_code perm_unit perm_unit_other rural, sort : egen float est_price_5 = max(temp_est_price_5)
by crop_code perm_unit perm_unit_other, sort : egen float est_price_6 = max(temp_est_price_6)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if est_price==.
replace est_price = est_price_6 if track==6
drop est_price_* count_* track

replace price = est_price if price==.
drop est_price

save "clean data/intermediate/Malawi1617_temp_permanent.dta", replace

* Permanent Loss
use "raw data/AG_MOD_Q.dta", clear
rename ag_q35a lost_qty
rename ag_q35b perm_unit
rename ag_q35b_oth perm_unit_other
keep case_id crop_code lost_qty perm_unit perm_unit_other
mvdecode lost_qty, mv(0)
mvencode lost_qty, mv(0)
drop if lost_qty==0

joinby crop_code perm_unit perm_unit_other using "clean data/Malawi1617_kg_conversion_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace lost_qty = lost_qty*kg_conver if kg_conver~=.
replace perm_unit = 1 if kg_conver~=.
replace perm_unit_other = "" if kg_conver~=.
drop kg_conver

save "clean data/intermediate/Malawi1617_lostqty_permanent.dta", replace

* dry Crop Loss Percentage
use "raw data/AG_MOD_Q.dta", clear
rename ag_q35d lost_percent
keep case_id crop_code lost_percent
mvdecode lost_percent, mv(0)
mvencode lost_percent, mv(0)
drop if lost_percent==0

save "clean data/intermediate/Malawi1617_lostper_permanent.dta", replace

use "clean data/intermediate/Malawi1617_temp_permanent.dta", clear
joinby case_id crop_code using "clean data/intermediate/Malawi1617_lostper_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
mvencode lost_percent, mv(0)

joinby case_id crop_code perm_unit perm_unit_other using "clean data/intermediate/Malawi1617_lostqty_permanent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace lost_qty = 0 if lost_qty==.

replace harvest_qty = harvest_qty - lost_qty
replace harvest_qty = 0 if harvest_qty<0
replace harvest_qty = harvest_qty * ((100-lost_percent)/100)

generate harvest_value_p = harvest_qty * price

collapse (sum) harvest_value_p, by (case_id)

replace harvest_value_p = harvest_value_p/365

save "clean data/intermediate/Malawi1617_permanent.dta", replace

** Livestock
* General Livestock
use "raw data/AG_MOD_R1.dta", clear
rename ag_r0a livestock_code
rename ag_r14 livestock_givenaway
rename ag_r16 livestock_sold
rename ag_r17 livestock_sold_value
rename ag_r19 livestock_slaughtered
keep case_id livestock*
mvdecode livestock_*, mv(0)
drop if livestock_givenaway==. & livestock_sold==. & livestock_sold_value==. & livestock_slaughtered==.

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_sold_value / livestock_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Malawi1617_demographics.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by livestock_code rural region district TA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_code rural region district TA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_code rural region district, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_code rural region, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_code rural, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by livestock_code rural region district TA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_code rural region district TA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_code rural region district, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_code rural region, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_code rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by livestock_code, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by livestock_code rural region district TA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_code rural region district TA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_code rural region district, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_code rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_code rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by livestock_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price
drop if predict_price==.

mvencode livestock_*, mv(0)
gen livestock_to_income = livestock_givenaway+livestock_sold+livestock_slaughtered

gen livestock_income_yr = livestock_to_income*predict_price
****
**** Do not know the number of slaughtered that were sold, so have to take the slaughtered value as given
gen livestock_sold_yr = livestock_sold*predict_price

keep case_id livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (case_id)

save "clean data/intermediate/Malawi1617_livestock.dta", replace

* Livestock Products
use "raw data/AG_MOD_S.dta", clear
rename ag_s0a livestock_product
* Cannot correctly allocate "other product"
drop if livestock_product==408
rename ag_s02 livestock_months
rename ag_s03a livestock_qty_prod
rename ag_s03b livestock_unit_prod
rename ag_s03b_oth livestock_unit_prod_other
rename ag_s05a livestock_sold
rename ag_s05b livestock_unit
rename ag_s05b_oth livestock_unit_other
rename ag_s06 livestock_sold_value
keep case_id livestock*
mvdecode livestock_*, mv(0)
drop if livestock_months==. | livestock_qty_prod==.


* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_sold_value / livestock_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Malawi1617_demographics.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by livestock_product livestock_unit livestock_unit_other rural region district TA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region district TA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region district, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by livestock_product livestock_unit livestock_unit_other rural region district TA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region district TA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region district, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural region, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by livestock_product livestock_unit livestock_unit_other, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by livestock_product livestock_unit_prod livestock_unit_prod_other rural region district TA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_product livestock_unit_prod livestock_unit_prod_other rural region district TA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_product livestock_unit_prod livestock_unit_prod_other rural region district, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_product livestock_unit_prod livestock_unit_prod_other rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_product livestock_unit_prod livestock_unit_prod_other rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by livestock_product livestock_unit_prod livestock_unit_prod_other, sort : egen float predict_price_6 = max(temp_predict_price_6)
by livestock_product livestock_unit livestock_unit_other rural region district TA ea, sort : egen float s_predict_price_1 = max(temp_predict_price_1)
by livestock_product livestock_unit livestock_unit_other rural region district TA, sort : egen float s_predict_price_2 = max(temp_predict_price_2)
by livestock_product livestock_unit livestock_unit_other rural region district, sort : egen float s_predict_price_3 = max(temp_predict_price_3)
by livestock_product livestock_unit livestock_unit_other rural region, sort : egen float s_predict_price_4 = max(temp_predict_price_4)
by livestock_product livestock_unit livestock_unit_other rural, sort : egen float s_predict_price_5 = max(temp_predict_price_5)
by livestock_product livestock_unit livestock_unit_other, sort : egen float s_predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_* s_predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
generate s_track = 1 if count_1>=10 & count_1<100000
generate s_predict_price = s_predict_price_1 if s_track==1
replace s_track = 2 if count_2>=10 & count_2<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_2 if s_track==2
replace s_track = 3 if count_3>=10 & count_3<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_3 if s_track==3
replace s_track = 4 if count_4>=10 & count_4<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_4 if s_track==4
replace s_track = 5 if count_5>=10 & count_5<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_5 if s_track==5
replace s_track = 6 if s_predict_price==.
replace s_predict_price = s_predict_price_6 if s_track==6
drop predict_price_* count_* s_predict_price_* revealed_price
mvencode livestock_* predict_price s_predict_price, mv(0)

gen livestock_product_income_yr = livestock_months*livestock_qty_prod*predict_price
gen livestock_product_sold_yr = livestock_sold*s_predict_price

keep case_id livestock_product_income_yr livestock_product_sold_yr

gen livestock_product_income = livestock_product_income_yr/365
gen livestock_product_sold = livestock_product_sold_yr/365

collapse (sum) livestock_*, by (case_id)

save "clean data/intermediate/Malawi1617_livestock_product.dta", replace

* Fishery Income High
* Convert from 2 columns to one
use "raw data/FS_MOD_E1.dta", clear
rename fs_e02 fish_species
replace fish_species = 12 if fish_species>10
rename fs_e03 fish_weeks
* if report catches, must have fished
replace fish_weeks = 1 if fish_weeks==0
rename fs_e04a fish_qty
rename fs_e04b fish_packaging
rename fs_e04d fish_processing
rename fs_e08a fish_sales_qty
rename fs_e08b fish_sales_packaging
rename fs_e08d fish_sales_processing
rename fs_e08f fish_sales_price
rename fs_e09 fish_sales_weeks
* if report sales, must have sold
replace fish_sales_weeks = 1 if fish_sales_weeks==0 & fish_sales_qty~=0
keep case_id fish_*
mvdecode fish_qty fish_sales_qty, mv(0)
drop if fish_qty==. & fish_sales_qty==.
save "clean data/intermediate/Malawi1617_fish_high_a.dta", replace

use "raw data/FS_MOD_E1.dta", clear
rename fs_e02 fish_species
replace fish_species = 12 if fish_species>10
rename fs_e03 fish_weeks
* if report catches, must have fished
replace fish_weeks = 1 if fish_weeks==0
rename fs_e04g fish_qty
rename fs_e04h fish_packaging
rename fs_e04j fish_processing
rename fs_e08g fish_sales_qty
rename fs_e08h fish_sales_packaging
rename fs_e08j fish_sales_processing
rename fs_e08l fish_sales_price
rename fs_e09 fish_sales_weeks
* if report sales, must have sold
replace fish_sales_weeks = 1 if fish_sales_weeks==0 & fish_sales_qty~=0
keep case_id fish_*
mvdecode fish_qty fish_sales_qty, mv(0)
drop if fish_qty==. & fish_sales_qty==.
save "clean data/intermediate/Malawi1617_fish_high_b.dta", replace

use "clean data/intermediate/Malawi1617_fish_high_a.dta", clear
append using "clean data/intermediate/Malawi1617_fish_high_b.dta"
replace fish_sales_price = . if fish_sales_price==0

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Malawi1617_demographics.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float count_1 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float count_2 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float count_3 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float count_4 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float count_5 = count(1) if fish_sales_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float temp_predict_price_1 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float temp_predict_price_2 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float temp_predict_price_3 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float temp_predict_price_4 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float temp_predict_price_5 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing, sort : egen float temp_predict_price_6 = median(fish_sales_price) if fish_sales_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by fish_species fish_packaging fish_processing rural region district TA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by fish_species fish_packaging fish_processing rural region district TA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by fish_species fish_packaging fish_processing rural region district, sort : egen float predict_price_3 = max(temp_predict_price_3)
by fish_species fish_packaging fish_processing rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by fish_species fish_packaging fish_processing rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by fish_species fish_packaging fish_processing, sort : egen float predict_price_6 = max(temp_predict_price_6)
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float s_predict_price_1 = max(temp_predict_price_1)
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float s_predict_price_2 = max(temp_predict_price_2)
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float s_predict_price_3 = max(temp_predict_price_3)
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float s_predict_price_4 = max(temp_predict_price_4)
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float s_predict_price_5 = max(temp_predict_price_5)
by fish_species fish_sales_packaging fish_sales_processing, sort : egen float s_predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_* s_predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
generate s_track = 1 if count_1>=10 & count_1<100000
generate s_predict_price = s_predict_price_1 if s_track==1
replace s_track = 2 if count_2>=10 & count_2<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_2 if s_track==2
replace s_track = 3 if count_3>=10 & count_3<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_3 if s_track==3
replace s_track = 4 if count_4>=10 & count_4<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_4 if s_track==4
replace s_track = 5 if count_5>=10 & count_5<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_5 if s_track==5
replace s_track = 6 if s_predict_price==.
replace s_predict_price = s_predict_price_6 if s_track==6
drop predict_price_* count_* s_predict_price_* fish_sales_price

gen fish_income_yr = fish_weeks*fish_qty*predict_price
gen fish_sold_yr = fish_sales_weeks*fish_sales_qty*s_predict_price

keep case_id fish_income_yr fish_sold_yr

gen fish_income = fish_income_yr/365
gen fish_sold = fish_sold_yr/365

collapse (sum) fish_*, by (case_id)

****
**** Cannot sell more fish than catch, per the survey. Used same price estimate structure, so fix sales=<income
replace fish_sold_yr = fish_income_yr if fish_sold_yr>=fish_income_yr
replace fish_sold = fish_income if fish_sold>=fish_income

drop if fish_income==0

save "clean data/intermediate/Malawi1617_fish_high.dta", replace

* Fishery Income Low
* Convert from 2 columns to one
use "raw data/FS_MOD_I1.dta", clear
replace fs_i02 = fs_i02a if fs_i02==.
rename fs_i02 fish_species
replace fish_species = 12 if fish_species>10
rename fs_i03 fish_weeks
* if report catches, must have fished
replace fish_weeks = 1 if fish_weeks==0
rename fs_i04a fish_qty
rename fs_i04b fish_packaging
rename fs_i04d fish_processing
rename fs_i08a fish_sales_qty
rename fs_i08b fish_sales_packaging
rename fs_i08d fish_sales_processing
rename fs_i08f fish_sales_price
rename fs_i09 fish_sales_weeks
* if report sales, must have sold
replace fish_sales_weeks = 1 if fish_sales_weeks==0 & fish_sales_qty~=0
keep case_id fish_*
mvdecode fish_qty fish_sales_qty, mv(0)
drop if fish_qty==. & fish_sales_qty==.
save "clean data/intermediate/Malawi1617_fish_low_a.dta", replace

use "raw data/FS_MOD_I1.dta", clear
rename fs_i02 fish_species
replace fish_species = 12 if fish_species>10
rename fs_i03 fish_weeks
* if report catches, must have fished
replace fish_weeks = 1 if fish_weeks==0
rename fs_i04g fish_qty
rename fs_i04h fish_packaging
rename fs_i04j fish_processing
rename fs_i08g fish_sales_qty
rename fs_i08h fish_sales_packaging
rename fs_i08j fish_sales_processing
rename fs_i08l fish_sales_price
rename fs_i09 fish_sales_weeks
* if report sales, must have sold
replace fish_sales_weeks = 1 if fish_sales_weeks==0 & fish_sales_qty~=0
keep case_id fish_*
mvdecode fish_qty fish_sales_qty, mv(0)
drop if fish_qty==. & fish_sales_qty==.
save "clean data/intermediate/Malawi1617_fish_low_b.dta", replace

use "clean data/intermediate/Malawi1617_fish_low_a.dta", clear
append using "clean data/intermediate/Malawi1617_fish_low_b.dta"
replace fish_sales_price = . if fish_sales_price==0

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Malawi1617_demographics.dta", unmatched(none)

* Count observations with calculated prices by geographic location.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float count_1 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float count_2 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float count_3 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float count_4 = count(1) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float count_5 = count(1) if fish_sales_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float temp_predict_price_1 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float temp_predict_price_2 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float temp_predict_price_3 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float temp_predict_price_4 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float temp_predict_price_5 = median(fish_sales_price) if fish_sales_price~=.
by fish_species fish_sales_packaging fish_sales_processing, sort : egen float temp_predict_price_6 = median(fish_sales_price) if fish_sales_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by fish_species fish_packaging fish_processing rural region district TA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by fish_species fish_packaging fish_processing rural region district TA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by fish_species fish_packaging fish_processing rural region district, sort : egen float predict_price_3 = max(temp_predict_price_3)
by fish_species fish_packaging fish_processing rural region, sort : egen float predict_price_4 = max(temp_predict_price_4)
by fish_species fish_packaging fish_processing rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
by fish_species fish_packaging fish_processing, sort : egen float predict_price_6 = max(temp_predict_price_6)
by fish_species fish_sales_packaging fish_sales_processing rural region district TA ea, sort : egen float s_predict_price_1 = max(temp_predict_price_1)
by fish_species fish_sales_packaging fish_sales_processing rural region district TA, sort : egen float s_predict_price_2 = max(temp_predict_price_2)
by fish_species fish_sales_packaging fish_sales_processing rural region district, sort : egen float s_predict_price_3 = max(temp_predict_price_3)
by fish_species fish_sales_packaging fish_sales_processing rural region, sort : egen float s_predict_price_4 = max(temp_predict_price_4)
by fish_species fish_sales_packaging fish_sales_processing rural, sort : egen float s_predict_price_5 = max(temp_predict_price_5)
by fish_species fish_sales_packaging fish_sales_processing, sort : egen float s_predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_* s_predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
generate s_track = 1 if count_1>=10 & count_1<100000
generate s_predict_price = s_predict_price_1 if s_track==1
replace s_track = 2 if count_2>=10 & count_2<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_2 if s_track==2
replace s_track = 3 if count_3>=10 & count_3<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_3 if s_track==3
replace s_track = 4 if count_4>=10 & count_4<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_4 if s_track==4
replace s_track = 5 if count_5>=10 & count_5<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_5 if s_track==5
replace s_track = 6 if s_predict_price==.
replace s_predict_price = s_predict_price_6 if s_track==6
drop predict_price_* count_* s_predict_price_* fish_sales_price

gen fish_income_low_yr = fish_weeks*fish_qty*predict_price
gen fish_sold_low_yr = fish_sales_weeks*fish_sales_qty*s_predict_price

keep case_id fish_income_low_yr fish_sold_low_yr

gen fish_income_low = fish_income_low_yr/365
gen fish_sold_low = fish_sold_low_yr/365

collapse (sum) fish_*, by (case_id)

****
**** Cannot sell more fish than catch, per the survey. Used same price estimate structure, so fix sales=<income
replace fish_sold_low_yr = fish_income_low_yr if fish_sold_low_yr>=fish_income_low_yr
replace fish_sold_low = fish_income_low if fish_sold_low>=fish_income_low

drop if fish_income_low==0

save "clean data/intermediate/Malawi1617_fish_low.dta", replace

*** Non Employment Income 

** Household Remittances
use "raw data/HH_MOD_O.dta", clear
rename hh_o17 hhmem_remittances
keep if hhmem_remittances~=.
collapse (sum) hhmem_remittances, by (case_id)

replace hhmem_remittances = hhmem_remittances/365
drop if hhmem_remittances==0

save "clean data/intermediate/Malawi1617_hhmem_remittances.dta", replace

** Non-Employment General Income
use "raw data/HH_MOD_P.dta", clear
rename hh_p02 non_emp_inc_general
mvdecode non_emp_inc_general hh_p03a hh_p03b hh_p03c, mv(0)
mvencode non_emp_inc_general hh_p03a hh_p03b hh_p03c, mv(0)
replace non_emp_inc_general = non_emp_inc_general + hh_p03a + hh_p03b + hh_p03c
collapse (sum) non_emp_inc_general, by (case_id)

replace non_emp_inc_general = non_emp_inc_general/365
drop if non_emp_inc_general<1
drop if non_emp_inc_general>100000

save "clean data/intermediate/Malawi1617_non_emp_inc_general.dta", replace

** Social Safety Nets
use "raw data/HH_MOD_R.dta", clear
mvdecode hh_r02a hh_r02b hh_r02c, mv(0)
mvencode hh_r02a hh_r02b hh_r02c, mv(0)
rename hh_r02a cash
rename hh_r02b inkind
rename hh_r02c maize_kg
keep case_id cash inkind maize_kg
* Maize price is median prize of kg of maize during the dry season
gen maize_price = 160

gen safety_net = cash + inkind + maize_kg * 160

collapse (sum) safety_net, by (case_id)

replace safety_net = safety_net/365
drop if safety_net==0

save "clean data/intermediate/Malawi1617_safety_net.dta", replace


** Costs
* Coupon (cost for seed or fertilizer)
use "raw data/AG_MOD_E2.dta", clear
mvdecode ag_e04 ag_e14 ag_e15 ag_e20, mv(0)
mvencode ag_e04 ag_e14 ag_e15 ag_e20, mv(0)
gen cost_with_coupon = ag_e04 + ag_e14 + ag_e15 - ag_e20
collapse (sum) cost_*, by (case_id)
drop if cost_with_coupon==0
replace cost_with_coupon = cost_with_coupon/365
save "clean data/intermediate/Malawi1617_cost_with_coupon.dta", replace

* Seeds during rainy season
use "raw data/AG_MOD_H.dta", clear
mvdecode ag_h09 ag_h10 ag_h40, mv(0)
mvencode ag_h09 ag_h10 ag_h40, mv(0)
gen cost_seeds = ag_h09 + ag_h10 + ag_h40
collapse (sum) cost_*, by (case_id)
drop if cost_seeds==0
replace cost_seeds = cost_seeds/365
save "clean data/intermediate/Malawi1617_seeds_rainy.dta", replace

* Seeds during dry season
use "raw data/AG_MOD_N.dta", clear
mvdecode ag_n09 ag_n10 ag_n40, mv(0)
mvencode ag_n09 ag_n10 ag_n40, mv(0)
gen cost_seeds = ag_n09 + ag_n10 + ag_n40
collapse (sum) cost_*, by (case_id)
drop if cost_seeds==0
replace cost_seeds = cost_seeds/365
save "clean data/intermediate/Malawi1617_seeds_dry.dta", replace

* Fertilizer during rainy season
use "raw data/AG_MOD_F.dta", clear
mvdecode ag_f09 ag_f10 ag_f40, mv(0)
mvencode ag_f09 ag_f10 ag_f40, mv(0)
gen cost_fertilizer = ag_f09 + ag_f10 + ag_f40
collapse (sum) cost_*, by (case_id)
drop if cost_fertilizer==0
replace cost_fertilizer = cost_fertilizer/365
save "clean data/intermediate/Malawi1617_fert_rainy.dta", replace

* Fertilizer during dry season
use "raw data/AG_MOD_L.dta", clear
mvdecode ag_l09 ag_l10 ag_l40, mv(0)
mvencode ag_l09 ag_l10 ag_l40, mv(0)
gen cost_fertilizer = ag_l09 + ag_l10 + ag_l40
collapse (sum) cost_*, by (case_id)
drop if cost_fertilizer==0
replace cost_fertilizer = cost_fertilizer/365
save "clean data/intermediate/Malawi1617_fert_dry.dta", replace

* Hired Crop Labor rainy season - Wages
use "raw data/AG_MOD_D.dta", clear
gen c_wages_men = ag_d46a1 * ag_d46b1
gen c_wages_women = ag_d46a2 * ag_d46b2
gen c_wages_children = ag_d46a3 * ag_d46b3
gen pp_wages_men = ag_d47a1 * ag_d47b1
gen pp_wages_women = ag_d47a2 * ag_d47b2
gen pp_wages_children = ag_d47a3 * ag_d47b3
gen ph_wages_men = ag_d48a1 * ag_d48b1
gen ph_wages_women = ag_d48a2 * ag_d48b2
gen ph_wages_children = ag_d48a3 * ag_d48b3
mvdecode c_wages_* pp_wages_* ph_wages_*, mv(0)
mvencode c_wages_* pp_wages_* ph_wages_*, mv(0)
* Skip command was supposed to separate cross sectional from panel households, assume cross sectional questions to capture full data.
gen cross_wages = c_wages_men + c_wages_women + c_wages_children
gen panel_wages = pp_wages_men + pp_wages_women + pp_wages_children + ph_wages_men + ph_wages_women + ph_wages_children
collapse (sum) cross_wages panel_wages, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_1.dta", replace

* Hired Crop Labor rainy season - In kind
use "raw data/AG_MOD_D.dta", clear
rename ag_d46c qty_paid
rename ag_d46f crop_code
rename ag_d46d unit
rename ag_d46e shelled
keep if crop_code~=.
keep case_id crop_code qty_paid unit shelled
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
drop if unit==13
joinby using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace qty_paid = qty_paid * kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
joinby using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track
gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*
replace price = . if price==0
drop if _merge==2
drop _merge
gen cross_in_kind = price * qty_paid
collapse (sum) cross_in_kind, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_2.dta", replace

use "raw data/AG_MOD_D.dta", clear
rename ag_d47c qty_paid
rename ag_d47f crop_code
rename ag_d47d unit
rename ag_d47e shelled
keep if crop_code~=.
keep case_id crop_code qty_paid unit shelled
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
drop if unit==13
joinby using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace qty_paid = qty_paid * kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
joinby using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track
gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*
replace price = . if price==0
drop if _merge==2
drop _merge
gen panel_in_kind = price * qty_paid
collapse (sum) panel_in_kind, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_3.dta", replace

use "raw data/AG_MOD_D.dta", clear
rename ag_d48c qty_paid
rename ag_d48f crop_code
rename ag_d48d unit
rename ag_d48e shelled
keep if crop_code~=.
keep case_id crop_code qty_paid unit shelled
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
drop if unit==13
joinby using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace qty_paid = qty_paid * kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
joinby using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track
gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*
replace price = . if price==0
drop if _merge==2
drop _merge
gen panel_in_kind = price * qty_paid
collapse (sum) panel_in_kind, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_4.dta", replace

* Hired Crop Labor dry season - Wages
use "raw data/AG_MOD_K.dta", clear
gen c_wages_men = ag_k46a1 * ag_k46b1
gen c_wages_women = ag_k46a2 * ag_k46b2
gen c_wages_children = ag_k46a3 * ag_k46b3
mvdecode c_wages_*, mv(0)
mvencode c_wages_*, mv(0)
gen cost_labor_wages = c_wages_men + c_wages_women + c_wages_children
collapse (sum) cost_labor_wages, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_5.dta", replace

* Hired Crop Labor dry season - In kind
use "raw data/AG_MOD_K.dta", clear
rename ag_k46d qty_paid
rename ag_k46c crop_code
rename ag_k46e unit
rename ag_k46f shelled
keep if crop_code~=.
keep case_id crop_code qty_paid unit shelled
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
drop if unit==13
joinby using "clean data/Malawi1617_kg_conversion_no_other.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace qty_paid = qty_paid * kg_conver if kg_conver~=.
replace unit = 1 if kg_conver~=.
joinby using "clean data/intermediate/Malawi1617_crop_prices_dry.dta", unmatched(both) _merge(_merge)
* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track
gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*
replace price = . if price==0
drop if _merge==2
drop _merge
gen cost_labor_in_kind = price * qty_paid
collapse (sum) cost_labor_in_kind, by (case_id)
save "clean data/intermediate/Malawi1617_temp_costs_labor_6.dta", replace

use "clean data/intermediate/Malawi1617_temp_costs_labor_1.dta", clear
joinby using "clean data/intermediate\Malawi1617_temp_costs_labor_2.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate\Malawi1617_temp_costs_labor_3.dta", unmatched(both) _merge(_merge)
drop _merge
append using "clean data/intermediate\Malawi1617_temp_costs_labor_4.dta"
collapse (sum) cross_wages panel_wages cross_in_kind panel_in_kind, by (case_id)
gen cost_labor = cross_wages + cross_in_kind
gen panel_labor = panel_wages + panel_in_kind
replace cost_labor = panel_labor if cost_labor==0
keep case_id cost_labor
joinby case_id using "clean data/intermediate\Malawi1617_temp_costs_labor_5.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate\Malawi1617_temp_costs_labor_6.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
replace cost_labor = cost_labor + cost_labor_wages + cost_labor_in_kind
collapse (sum) cost_labor, by (case_id)
replace cost_labor = cost_labor/365
drop if cost_labor==0
save "clean data/intermediate/Malawi1617_costs_crop_labor.dta", replace

* Animal/Fish Labor
use "raw data/AG_MOD_R2.dta", clear
rename ag_r25 cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_animal_labor==0
save "clean data/intermediate/Malawi1617_costs_animal_labor_1.dta", replace

use "raw data/FS_MOD_D3.dta", clear
mvdecode fs_d14* fs_d16* fs_d20* fs_d21*, mv(0)
mvencode fs_d14* fs_d16* fs_d20* fs_d21*, mv(0)
gen weekly_pay_adult = fs_d16a + fs_d21a
gen weekly_pay_child = fs_d16b + fs_d21b
gen adult_high_pay = fs_d14a*fs_d14a*weekly_pay_adult + fs_d20a
gen child_high_pay = fs_d14b*fs_d14b*weekly_pay_child + fs_d20b
gen cost_animal_labor = adult_high_pay + child_high_pay
collapse (sum) cost_*, by (case_id)
drop if cost_animal_labor==0
save "clean data/intermediate/Malawi1617_costs_animal_labor_2.dta", replace

use "raw data/FS_MOD_H3.dta", clear
mvdecode fs_h14* fs_h16* fs_h20* fs_h21*, mv(0)
mvencode fs_h14* fs_h16* fs_h20* fs_h21*, mv(0)
gen weekly_pay_adult = fs_h16a + fs_h21a
gen weekly_pay_child = fs_h16b + fs_h21b
gen adult_low_pay = fs_h14a*fs_h14a*weekly_pay_adult + fs_h20a
gen child_low_pay = fs_h14b*fs_h14b*weekly_pay_child + fs_h20b
gen cost_animal_labor = adult_low_pay + child_low_pay
collapse (sum) cost_*, by (case_id)
drop if cost_animal_labor==0

append using "clean data/intermediate/Malawi1617_costs_animal_labor_2.dta"
append using "clean data/intermediate/Malawi1617_costs_animal_labor_1.dta"
collapse (sum) cost_*, by (case_id)
replace cost_animal_labor = cost_animal_labor/365
save "clean data/intermediate/Malawi1617_costs_animal_labor.dta", replace

* Animal/Fish fuel and transport
use "raw data/FS_MOD_D2.dta", clear
mvdecode fs_d13, mv(0)
mvencode fs_d13, mv(0)
rename fs_d13 cost_animal_fuel
collapse (sum) cost_*, by (case_id)
drop if cost_animal_fuel==0
save "clean data/intermediate/Malawi1617_costs_animal_fuel_1.dta", replace

use "raw data/FS_MOD_H2.dta", clear
mvdecode fs_h13, mv(0)
mvencode fs_h13, mv(0)
rename fs_h13 cost_animal_fuel
collapse (sum) cost_*, by (case_id)
drop if cost_animal_fuel==0

append using "clean data/intermediate/Malawi1617_costs_animal_fuel_1.dta"
collapse (sum) cost_*, by (case_id)
replace cost_animal_fuel = cost_animal_fuel/365
save "clean data/intermediate/Malawi1617_costs_animal_fuel.dta", replace

* Animal Feed
use "raw data/AG_MOD_R2.dta", clear
rename ag_r26 cost_feed
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_feed==0
replace cost_feed = cost_feed/365
save "clean data/intermediate/Malawi1617_costs_feed.dta", replace

* Land Rent (Crop and by value)
use "raw data/AG_MOD_B2.dta", clear
rename ag_b208a crop_code
rename ag_b208b crop_qty
rename ag_b208c unit
rename ag_b208c_oth unit_other
rename ag_b208d shelled
keep case_id crop_code crop_qty unit unit_other shelled
drop if crop_qty==. | crop_qty==0
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
joinby using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other shelled rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other shelled rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other shelled rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other shelled rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other shelled, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track

gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*

replace price = . if price==0

* Count observations with price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_price_2 = median(price) if price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_price_3 = median(price) if price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_price_4 = median(price) if price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_price_5 = median(price) if price~=.
by crop_code unit unit_other, sort : egen float temp_est_price_6 = median(price) if price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_price_1 = max(temp_est_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_price_2 = max(temp_est_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_price_3 = max(temp_est_price_3)
by crop_code unit unit_other rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by crop_code unit unit_other rural, sort : egen float est_price_5 = max(temp_est_price_5)
by crop_code unit unit_other, sort : egen float est_price_6 = max(temp_est_price_6)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if est_price==.
replace est_price = est_price_6 if track==6
drop est_price_* count_* track

replace price = est_price if price==.
drop est_price

gen cost_land_rent = price*crop_qty
mvencode cost_land_rent, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_land_rent==0
save "clean data/intermediate/Malawi1617_costs_land_rent_1.dta", replace

use "raw data/AG_MOD_B2.dta", clear
mvdecode ag_b209a ag_b209b, mv(0)
mvencode ag_b209a ag_b209b, mv(0)
gen cost_land_rent = ag_b209a + ag_b209b
collapse (sum) cost_*, by (case_id)
drop if cost_land_rent==0
save "clean data/intermediate/Malawi1617_costs_land_rent_2.dta", replace

use "raw data/AG_MOD_I2.dta", clear
rename ag_i208a crop_code
rename ag_i208b crop_qty
rename ag_i208c unit
rename ag_i208c_oth unit_other
rename ag_i208d shelled
keep case_id crop_code crop_qty unit unit_other shelled
drop if crop_qty==. | crop_qty==0
joinby using "clean data/Malawi1617_location.dta", unmatched(none)
joinby using "clean data/intermediate/Malawi1617_crop_prices_rainy.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use predicted prices of from households in other spatial units to estimate missing prices
* Count observations with est_revealed_price reported by geographic location.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float count_1 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float count_2 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float count_3 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float count_4 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float count_5 = count(1) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float count_6 = count(1) if est_revealed_price~=.
* Calculate median of est_revealed_price by geographic location - later used for predictive purposes.
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float temp_est_est_revealed_price_1 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district TA, sort : egen float temp_est_est_revealed_price_2 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region district, sort : egen float temp_est_est_revealed_price_3 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural region, sort : egen float temp_est_est_revealed_price_4 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled rural, sort : egen float temp_est_est_revealed_price_5 = median(est_revealed_price) if est_revealed_price~=.
by crop_code unit unit_other shelled, sort : egen float temp_est_est_revealed_price_6 = median(est_revealed_price) if est_revealed_price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other shelled rural region district TA ea, sort : egen float est_est_revealed_price_1 = max(temp_est_est_revealed_price_1)
by crop_code unit unit_other shelled rural region district TA, sort : egen float est_est_revealed_price_2 = max(temp_est_est_revealed_price_2)
by crop_code unit unit_other shelled rural region district, sort : egen float est_est_revealed_price_3 = max(temp_est_est_revealed_price_3)
by crop_code unit unit_other shelled rural region, sort : egen float est_est_revealed_price_4 = max(temp_est_est_revealed_price_4)
by crop_code unit unit_other shelled rural, sort : egen float est_est_revealed_price_5 = max(temp_est_est_revealed_price_5)
by crop_code unit unit_other shelled, sort : egen float est_est_revealed_price_6 = max(temp_est_est_revealed_price_6)
drop temp*
* Build est_revealed_price prediction variable if at least 10 observations of reported est_revealed_price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_revealed_price = est_est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_5 if track==5
replace track = 6 if est_est_revealed_price==.
replace est_est_revealed_price = est_est_revealed_price_6 if track==6
drop est_est_revealed_price_* count_* track

gen price = est_revealed_price
replace price = est_est_revealed_price if price==.
drop est*

replace price = . if price==0

* Count observations with price reported by geographic location.
by crop_code unit unit_other rural region district TA ea, sort : egen float count_1 = count(1) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float count_2 = count(1) if price~=.
by crop_code unit unit_other rural region district, sort : egen float count_3 = count(1) if price~=.
by crop_code unit unit_other rural region, sort : egen float count_4 = count(1) if price~=.
by crop_code unit unit_other rural, sort : egen float count_5 = count(1) if price~=.
by crop_code unit unit_other, sort : egen float count_6 = count(1) if price~=.
* Calculate median of price by geographic location - later used for predictive purposes.
by crop_code unit unit_other rural region district TA ea, sort : egen float temp_est_price_1 = median(price) if price~=.
by crop_code unit unit_other rural region district TA, sort : egen float temp_est_price_2 = median(price) if price~=.
by crop_code unit unit_other rural region district, sort : egen float temp_est_price_3 = median(price) if price~=.
by crop_code unit unit_other rural region, sort : egen float temp_est_price_4 = median(price) if price~=.
by crop_code unit unit_other rural, sort : egen float temp_est_price_5 = median(price) if price~=.
by crop_code unit unit_other, sort : egen float temp_est_price_6 = median(price) if price~=.
mvencode temp_*, mv(0)
by crop_code unit unit_other rural region district TA ea, sort : egen float est_price_1 = max(temp_est_price_1)
by crop_code unit unit_other rural region district TA, sort : egen float est_price_2 = max(temp_est_price_2)
by crop_code unit unit_other rural region district, sort : egen float est_price_3 = max(temp_est_price_3)
by crop_code unit unit_other rural region, sort : egen float est_price_4 = max(temp_est_price_4)
by crop_code unit unit_other rural, sort : egen float est_price_5 = max(temp_est_price_5)
by crop_code unit unit_other, sort : egen float est_price_6 = max(temp_est_price_6)
drop temp*
* Build price prediction variable if at least 10 observations of reported price in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_price = est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_price==.
replace est_price = est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_price==.
replace est_price = est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_price==.
replace est_price = est_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_price==.
replace est_price = est_price_5 if track==5
replace track = 6 if est_price==.
replace est_price = est_price_6 if track==6
drop est_price_* count_* track

replace price = est_price if price==.
drop est_price

gen cost_land_rent = price*crop_qty
mvencode cost_land_rent, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_land_rent==0
save "clean data/intermediate/Malawi1617_costs_land_rent_3.dta", replace

use "raw data/AG_MOD_I2.dta", clear
mvdecode ag_i209a ag_i209b, mv(0)
mvencode ag_i209a ag_i209b, mv(0)
gen cost_land_rent = ag_i209a + ag_i209b
collapse (sum) cost_*, by (case_id)
drop if cost_land_rent==0

append using "clean data/intermediate/Malawi1617_costs_land_rent_1.dta"
append using "clean data/intermediate/Malawi1617_costs_land_rent_2.dta"
append using "clean data/intermediate/Malawi1617_costs_land_rent_3.dta"
collapse (sum) cost_*, by (case_id)
replace cost_land_rent = cost_land_rent/365
save "clean data/intermediate/Malawi1617_costs_land_rent.dta", replace

* Irrigation
* NA

* Machinery and Animal Rent
* NA

* Transport to Market
use "raw data/AG_MOD_I.dta", clear
rename ag_i10 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_transport==0
save "clean data/intermediate/Malawi1617_costs_transport_1.dta", replace

use "raw data/AG_MOD_O.dta", clear
rename ag_o10 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_transport==0
save "clean data/intermediate/Malawi1617_costs_transport_2.dta", replace

use "raw data/AG_MOD_Q.dta", clear
rename ag_q10 cost_transport
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
drop if cost_transport==0

append using "clean data/intermediate/Malawi1617_costs_transport_1.dta"
append using "clean data/intermediate/Malawi1617_costs_transport_2.dta"
collapse (sum) cost_*, by (case_id)
replace cost_transport = cost_transport/365
save "clean data/intermediate/Malawi1617_costs_transport.dta", replace

* Combine costs (careful when to merge and when to append)
use "clean data/intermediate/Malawi1617_cost_with_coupon.dta", clear
joinby case_id using "clean data/intermediate/Malawi1617_fert_rainy.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_seeds_rainy.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_crop_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_animal_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_animal_fuel.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_feed.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_land_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id using "clean data/intermediate/Malawi1617_costs_transport.dta", unmatched(both) _merge(_merge)
drop _merge
append using "clean data/intermediate/Malawi1617_fert_dry.dta"
append using "clean data/intermediate/Malawi1617_seeds_dry.dta"
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (case_id)
save "clean data/intermediate/Malawi1617_costs.dta", replace


*****************
* EXPENDITURE
*****************

* FOOD EXPENDITURE

use "raw data/HH_MOD_G1.dta", clear
merge m:1 case_id using  "raw data/HH_MOD_A_FILT.dta", nogen


ren hh_g03a q_tot
ren hh_g03b unit_tot
ren hh_g04a q_purch
ren hh_g04b unit 
ren hh_g05 v_purch
ren  hh_g02 item_cd
g price = v_purch/q_purch

preserve

drop if price==.

bys ea_id district region reside item_cd unit: egen m_price1 = median (price)

collapse (mean) m_price1, by (ea_id district region reside item_cd unit)

tempfile m_price1
save `m_price1',replace 
restore

preserve

drop if price==.
bys ea_id district region item_cd unit: egen m_price2 = median (price)

collapse (mean) m_price2, by (ea_id district region item_cd unit)

tempfile m_price2
save `m_price2',replace 
restore

preserve

drop if price==.

bys ea_id district item_cd unit: egen m_price3 = median (price)

collapse (mean) m_price3, by (ea_id district item_cd unit)

tempfile m_price3
save `m_price3',replace 
restore

preserve

drop if price==.

bys ea_id item_cd unit: egen m_price4 = median (price)

collapse (mean) m_price4, by (ea_id item_cd unit)

tempfile m_price4
save `m_price4',replace 
restore

preserve

drop if price==.

bys  item_cd unit: egen m_price5 = median (price)

collapse (mean) m_price5, by ( item_cd unit)

tempfile m_price5
save `m_price5',replace 
restore

use "raw data/HH_MOD_G1.dta", clear
merge m:1 case_id using  "raw data/HH_MOD_A_FILT.dta", nogen

ren hh_g03a q_tot
ren hh_g03b unit
ren  hh_g02 item_cd

merge m:1 ea_id district region reside item_cd unit using `m_price1', nogen keep (1 3)
merge m:1 ea_id district region item_cd unit using `m_price2', nogen keep (1 3)
merge m:1 ea_id district item_cd unit using `m_price3', nogen keep (1 3)
merge m:1 ea_id item_cd unit using `m_price4', nogen keep (1 3)
merge m:1 item_cd unit using `m_price5', nogen keep (1 3)

g unit_price = m_price1
replace unit_price = m_price2 if unit_price==.
replace unit_price= m_price3 if unit_price==.
replace unit_price = m_price4 if unit_price==.
replace unit_price = m_price5 if unit_price==.


g foodexp = q_tot*unit_price

count if foodexp==. & q_tot!=. & q_tot!=0  // 497

collapse (sum) foodexp, by (case_id) 


replace foodexp = foodexp/7

label var foodexp "Household food consumption expenditure per day"

tempfile foodexp
save `foodexp', replace

* NON - FOOD EXPENDITURE

* 7 days recall

use "raw data/HH_MOD_I1.dta", clear

g nonfoodexp_7 = hh_i03

collapse (sum) nonfoodexp_7, by (case_id) 

replace nonfoodexp_7 = nonfoodexp_7/7

label var nonfoodexp_7 " 7 days recall - hh non-food expenditure per day "

tempfile nonfoodexp_7
save `nonfoodexp_7', replace

* 1 month recall

use "raw data/HH_MOD_I2.dta", clear

g nonfoodexp_30 = hh_i06

collapse (sum) nonfoodexp_30, by (case_id) 

replace nonfoodexp_30 = nonfoodexp_30/30.4

label var nonfoodexp_30 " 1 month recall - hh non-food expenditure per day "

tempfile nonfoodexp_30
save `nonfoodexp_30', replace


* 3 months recall

use "raw data/HH_MOD_J.dta", clear

g nonfoodexp_3 = hh_j03

collapse (sum) nonfoodexp_3, by (case_id) 

replace nonfoodexp_3 = nonfoodexp_3/(30.4*3)

label var nonfoodexp_3 " 3 months recall - hh non-food expenditure per day "

tempfile nonfoodexp_3
save `nonfoodexp_3', replace


* 12 months recall

use "raw data/HH_MOD_K1.dta", clear
append using "raw data/HH_MOD_K2.dta"

g nonfoodexp_12 = hh_k03

collapse (sum) nonfoodexp_12, by (case_id) 

replace nonfoodexp_12 = nonfoodexp_12/365

label var nonfoodexp_12 " 12 months recall - hh non-food expenditure per day "

tempfile nonfoodexp_12
save `nonfoodexp_12', replace

* Durable goods

use "raw data/HH_MOD_L.dta", clear

g durexp = hh_l07

collapse (sum) durexp, by (case_id)

replace durexp= durexp/365

label var durexp " 12 months recall - hh durable goods expenditure per day "

tempfile durexp
save `durexp', replace

* Rental of the house

use "raw data/HH_MOD_F.dta", clear

g rentexp = hh_f04a if hh_f04b==3
replace rentexp = hh_f04a/30.4 if hh_f04b==5
replace rentexp = hh_f04a/365 if hh_f04b==6

collapse (sum) rentexp, by (case_id)

label var rentexp " hh house rent expenditure per day "

tempfile rentexp
save `rentexp', replace


*Merge all non-food expenditure dataset

use  `nonfoodexp_7', clear
merge 1:1 case_id using `nonfoodexp_30', nogen
merge 1:1 case_id using `nonfoodexp_3', nogen
merge 1:1 case_id using `nonfoodexp_12', nogen
merge 1:1 case_id using `durexp', nogen
merge 1:1 case_id using `rentexp', nogen

egen nonfoodexp = rsum (nonfoodexp_7 nonfoodexp_30 nonfoodexp_3 nonfoodexp_12 durexp rentexp)

label var nonfoodexp "Household non-food expenditure per day "

merge 1:1 case_id using `foodexp', nogen


egen hhexp = rsum ( foodexp nonfoodexp)

label var hhexp "Household total expenditure per day "

save "clean data/Expenditure.dta", replace 



** Combine all
use "clean data/Malawi1617_demographics.dta", clear
joinby case_id using "clean data/intermediate/Malawi1617_nfe_income.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_wage_incomeCHEVAL.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_ganyu_inc.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_harvest_rainy.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_farm_sales_rainy.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_harvest_dry.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_farm_sales_dry.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_permanent.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_perm_sales.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_livestock.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_livestock_product.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_fish_high.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_fish_low.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_hhmem_remittances.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_non_emp_inc_general.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_safety_net.dta", unmatched(both) _merge(_merge)
drop _m
joinby case_id using "clean data/intermediate/Malawi1617_costs.dta", unmatched(both) _merge(_merge)
drop _m

merge 1:1 case_id using "clean data/Expenditure.dta", nogen keep (1 3)

joinby using "clean data/Malawi1617_hhid.dta", unmatched(none)

gen remit = 1 if hhmem_remittances>0
replace remit = 0 if remit==. | hhmem_remittances==.


mvdecode nfe_* wage_* g_* harvest_* farm_* perm_* livestock_* fish_* hhmem_remittances non_emp_inc_general safety_net cost_*, mv(0)
mvencode nfe_* wage_* g_* harvest_* farm_* perm_* livestock_* fish_* hhmem_remittances non_emp_inc_general safety_net cost_*, mv(0)

* Conversion of expenditure variables in USD constant PPP
g pcexp = hhexp/hhsize

foreach u of varlist hhexp pcexp {
	gen USD_`u' = `u'*0.001376426
	replace USD_`u' = USD_`u'*3.2755
	drop `u'
	ren USD_`u' `u'
}

* Convert to Per Capita USD constant PPP
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income g_income g_farm_income g_nonfarm_income harvest_value_r farm_sales_r farm_sales_r_transport harvest_value_d farm_sales_d farm_sales_d_transport harvest_value_p perm_sales perm_sales_transport livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_income_low fish_sold_low hhmem_remittances non_emp_inc_general safety_net cost_with_coupon cost_fertilizer cost_seeds cost_labor cost_animal_labor cost_animal_fuel cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 12/1/2016
	gen USD_`u' = `u'*0.001376426
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2017 data not available
	* 2016 GNI per capita constant 2011 PPP / 2016 GNI per capita Atlas Method
	replace USD_`u' = USD_`u'*3.2755
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 12/1/2016
	gen USD_`usd'`u' = `usd'`u'*0.001376426
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2017 data not available
	* 2016 GNI per capita constant 2011 PPP / 2016 GNI per capita Atlas Method
	replace USD_`usd'`u' = USD_`usd'`u'*3.2755
}
}
*	
* Drop former variables
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income g_income g_farm_income g_nonfarm_income harvest_value_r farm_sales_r farm_sales_r_transport harvest_value_d farm_sales_d farm_sales_d_transport harvest_value_p perm_sales perm_sales_transport livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_income_low fish_sold_low hhmem_remittances non_emp_inc_general safety_net cost_with_coupon cost_fertilizer cost_seeds cost_labor cost_animal_labor cost_animal_fuel cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	
* Rename
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income g_income g_farm_income g_nonfarm_income harvest_value_r farm_sales_r farm_sales_r_transport harvest_value_d farm_sales_d farm_sales_d_transport harvest_value_p perm_sales perm_sales_transport livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_income_low fish_sold_low hhmem_remittances non_emp_inc_general safety_net cost_with_coupon cost_fertilizer cost_seeds cost_labor cost_animal_labor cost_animal_fuel cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	

* Impute farm income components before creating the aggregate variable
local varlist harvest_value_r harvest_value_d harvest_value_p livestock_income livestock_product_income fish_income
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}

replace farm_sales_r=harvest_value_r  if farm_sales_r>harvest_value_r & farm_sales_r!=.
replace farm_sales_d=harvest_value_d  if farm_sales_d>harvest_value_d  & farm_sales_d!=.
replace perm_sales=harvest_value_p  if perm_sales>harvest_value_p & perm_sales!=.
replace livestock_sold=livestock_income if livestock_sold>livestock_income & livestock_sold!=.
replace livestock_product_sold=livestock_product_income  if livestock_product_sold>livestock_product_income  & livestock_product_sold!=.
replace fish_sold=fish_income  if fish_sold>fish_income  & fish_sold!=.


** Combine various income variables into a total income by general type and categories
gen farm_sales = farm_sales_r + farm_sales_d + perm_sales + livestock_sold + livestock_product_sold + fish_sold
gen own_farm_crop_inc_costs = cost_with_coupon + cost_fertilizer + cost_seeds + cost_labor + cost_land_rent + cost_transport
gen own_farm_livestock_inc_costs = cost_animal_labor + cost_animal_fuel + cost_feed
gen own_farm_crop_inc_gross = harvest_value_r + harvest_value_d + harvest_value_p
gen own_farm_livestock_inc_gross = livestock_income + livestock_product_income + fish_income
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross
* Bring in Ganyu
replace i_gi_f0_c2 = i_gi_f0_c2 + g_farm_income
replace i_gi_f0_c12 = i_gi_f0_c12 + g_nonfarm_income

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net
* Bring in Ganyu
replace i_ni_f0_c2 = i_ni_f0_c2 + g_farm_income
replace i_ni_f0_c12 = i_ni_f0_c12 + g_nonfarm_income

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet
* Bring in Ganyu
replace i_nnni_f0_c2 = i_nnni_f0_c2 + g_farm_income
replace i_nnni_f0_c12 = i_nnni_f0_c12 + g_nonfarm_income

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + hhmem_remittances + non_emp_inc_general + safety_net
}
*

keep hhid rural hhweight hhsize youth_share_24 youth_share_34 female_head school_secondary_hh educ_max credit land_owned job1 job2 Ninformal N_pension N_health_insurance informal informalcon informalsp formal formalcon formalsp sh_pension sh_health_insurance remit pcexp nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales own_farm* i_gi* i_ni* i_nnni* nfe_gross_inc nfe_costs nfe_net_inc farm_sales
	
save "clean data/MALAWI_incomesharesCHEVAL.dta", replace

