********************************************************************************
***************** TANZANIA 2014/2015: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)


The dataset used for this is:
Tanzania 2014/2015 National Panel Survey

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")

Stata Version 15.1 (14.2 Cecilia)

-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "clean data/intermediate/Tanz1415_wage_incomeCHEVAL.dta", replace
save "clean data/TANZANIA_incomesharesCHEVAL.dta", replace


NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl TANZANIA	"$path/data\Tanzania_NPS_1415"

gl DO   	    ${TANZANIA}/do
gl raw data   	${TANZANIA}/raw data 
gl clean data 	${TANZANIA}/clean data
gl FIG 		    ${TANZANIA}/graphs
gl TAB 	    	${TANZANIA}/tables
 
********************************************************************************
cd "$TANZANIA"

*** INCOME SHARES OF FARMING AND FARM PRODUCE SALES ***

*get from each module the value of production, sales, own consumption and aggregate at household level:
*** AG-INCOME = crops, crop products, forestry, livestock, livestock by-products
		*reference period for ag. income is past 12 months
	
** Income from Non-Farm Own Enterprises
use "raw data/hh_sec_n.dta", clear
rename hh_n19 nfe_months
rename hh_n02_3 nfe_CIC
format %40.0g nfe_CIC
* To include costs & gross income, must assume last month is the typical month
gen nfe_lastmo_gross_inc = hh_n13_2
replace nfe_lastmo_gross_inc = nfe_lastmo_gross_inc*4.3 if hh_n13_1==1
gen nfe_lastmo_costs = 0
replace nfe_lastmo_costs = nfe_lastmo_costs + hh_n16 if hh_n16~=.
replace nfe_lastmo_costs = nfe_lastmo_costs + hh_n17 if hh_n17~=.
replace nfe_lastmo_costs = nfe_lastmo_costs + hh_n18 if hh_n18~=.
gen nfe_lastmo_net_inc = nfe_lastmo_gross_inc - nfe_lastmo_cost

* Clean out extra data
drop if entid==.

* Keep relevant data
keep y4_hhid entid nfe_CIC nfe_months nfe_lastmo_net_inc nfe_lastmo_gross_inc nfe_lastmo_cost

* Clear Errors in data
drop if nfe_months==0 & nfe_lastmo_net_inc==0 & nfe_lastmo_gross_inc==0 & nfe_lastmo_cost==0
replace nfe_months = 12 if nfe_months==13
drop if nfe_months==.
**** If reported income but not months, assume that this enterprise was operational.
**** Impute 10 monthly values with the median operational months from observations of the same non farm enterprise, spatial unit at national level
by nfe_CIC, sort : egen float est_months = median(nfe_months)
replace nfe_months = est_months if nfe_months==0

* Generate Annual Income Variable
gen nfe_ann_gross_inc = nfe_months*nfe_lastmo_gross_inc
gen nfe_ann_costs = nfe_months*nfe_lastmo_costs
gen nfe_ann_net_inc = nfe_months*nfe_lastmo_net_inc

* Calculate Income Per Day
gen nfe_gross_inc = nfe_ann_gross_inc/365
gen nfe_costs = nfe_ann_costs/365
gen nfe_net_inc = nfe_ann_net_inc/365

* Bring in ISIC Group Information
rename nfe_CIC CIC
replace CIC = 99999 if CIC==.
joinby CIC using "clean data/Tanz1415_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC nfe_CIC

* Calculate income by agriculture vs non-agriculture activities
* Bring in Demographic Info
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(none)
* Replace missing AFS Share Values with Country Specific AFS factors
* Currently use AFS Shares calculated with this method from Tanzania 10/11
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.579 if AFS_share==. & rural==1
replace AFS_share = 0.527 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612

* Keep relevant variables
order y4_hhid nfe_CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*
keep y4_hhid nfe_CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*, by (y4_hhid)

save "clean data/intermediate/Tanz1415_nfe_income.dta", replace


** Gross Income from Wage Earnings
** Occupation by Wages 
* Transpose data so that 2 Wage occupations are in one set of variables
use "raw data/hh_sec_e.dta", clear
* Rename variables
**adding ANY emp type performed in last 12 months:
recode hh_e04ab hh_e04cd hh_e04e (2=0) (-999999999999/-1 .a/.y =.) 	//cleaning emp status over last 12 months (!)

rename hh_e20_2 wage_tasco 						
rename hh_e21_2 wage_CIC
rename hh_e24 paid
rename hh_e26_1 wage_payment
rename hh_e26_2 wage_payment_duration
rename hh_e28_1 wage_payment_inkind
rename hh_e28_2 wage_payment_inkind_duration
rename hh_e29 wage_months
rename hh_e30 wage_weeks
rename hh_e31 wage_hours
rename hh_e34a w_contract
rename hh_e35_1 offer_materpater 
rename hh_e35_2 withold_taxes
rename hh_e35_3 health_insurance
rename hh_e62a tradeunion 
gen wage_job = 1
* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hours==.
order y4_hhid indidy4 wage_CIC wage_tasco wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_months wage_weeks wage_hours w_contract withold_taxes offer_materpater health_insurance wage_job tradeunion 
keep y4_hhid indidy4 wage_CIC wage_tasco wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_months wage_weeks wage_hours w_contract withold_taxes offer_materpater health_insurance wage_job tradeunion 
save "clean data/intermediate/Tanz1415_temp_wage_1.dta", replace
use "raw data/hh_sec_e.dta", clear
* Rename variables
rename hh_e39_2 wage_CIC
rename hh_e24 paid
rename hh_e44_1 wage_payment
rename hh_e44_2 wage_payment_duration
rename hh_e46_1 wage_payment_inkind
rename hh_e46_2 wage_payment_inkind_duration
rename hh_e50 wage_hours
rename hh_e52a w_contract
gen wage_job = 2
* Drop no data households
drop if wage_payment==. & wage_hours==.
order y4_hhid indidy4 wage_CIC wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_hours w_contract wage_job
keep y4_hhid indidy4 wage_CIC wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_hours w_contract wage_job
save "clean data/intermediate/Tanz1415_temp_wage_2.dta", replace

use "clean data/intermediate/Tanz1415_temp_wage_1.dta", clear
append using "clean data/intermediate/Tanz1415_temp_wage_2.dta"
mvdecode wage_CIC wage_months wage_weeks wage_hours, mv(0)

* Clear Errors in data
* Obvious typo where hour was entered as duration, and month was far more likely
replace wage_payment_duration = 5 if y4_hhid=="2983-001" & wage_payment_duration==1
replace wage_payment_duration = 5 if y4_hhid=="4325-001" & wage_payment_duration==1
replace wage_payment_duration = 5 if y4_hhid=="1918-001" & wage_payment_duration==1
* Obvious typo where daily income payment would not be 2x monthly salary
replace wage_payment_inkind_duration = 5 if y4_hhid=="4555-001" & wage_payment_inkind_duration==2 & wage_payment_duration==5
* Clear Errors in data
*** If weeks is greater than 5, convert to missing (1 change)
replace wage_weeks = . if wage_weeks>5
*** Assume that reported hours per day above 16 hours per day are in error. (287)
replace wage_hours =. if wage_hours>80

**** If worked hours then assume to have worked weeks, and assume worked during months
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Tanz1415_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_6 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region district, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural region, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
by wage_CIC female rural, sort : egen float est_wage_months_6 = max(temp_est_wage_months_6)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
replace track = 6 if est_wage_months==.
replace est_wage_months = est_wage_months_6 if track==6
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (228 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_6 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region district, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural region, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
by wage_CIC female rural, sort : egen float est_wage_weeks_6 = max(temp_est_wage_weeks_6)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==5
replace track = 6 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_6 if track==6
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (228 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hours reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_hours~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_hours~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_hours~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_hours~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_hours~=.
* Calculate median of wage_hours by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_hours_1 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_hours_2 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_hours_3 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_hours_4 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hours_5 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hours_6 = median(wage_hours) if wage_hours~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_hours_1 = max(temp_est_wage_hours_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_hours_2 = max(temp_est_wage_hours_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_hours_3 = max(temp_est_wage_hours_3)
by wage_CIC female rural region district, sort : egen float est_wage_hours_4 = max(temp_est_wage_hours_4)
by wage_CIC female rural region, sort : egen float est_wage_hours_5 = max(temp_est_wage_hours_5)
by wage_CIC female rural, sort : egen float est_wage_hours_6 = max(temp_est_wage_hours_6)
drop temp*
* Build wage_hours prediction variable if at least 10 observations of reported wage_hours in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hours = est_wage_hours_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==5
replace track = 6 if est_wage_hours==.
replace est_wage_hours = est_wage_hours_6 if track==6
drop est_wage_hours_* count_* track
**** Replace missing value of wage_hours (440 changes)
replace wage_hours = est_wage_hours if wage_hours==.
replace wage_hours = 1 if wage_hours==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hours)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==1 
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hours/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.
* Payment in kind
* Duration in hours
replace wage_payment_inkind = ((wage_payment_inkind*wage_hours)/7)*(wage_weeks/4.2)*(wage_months/12) if wage_payment_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_payment_inkind = ((wage_payment_inkind*est_wage_days)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_inkind_duration==2
* Duration in weeks
replace wage_payment_inkind = wage_payment_inkind*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_inkind_duration==3
* Duration in fortnights
replace wage_payment_inkind = wage_payment_inkind*(1/2)*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_inkind_duration==4
* Duration in months
replace wage_payment_inkind = wage_payment_inkind*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==5
* Duration in quarters
replace wage_payment_inkind = wage_payment_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==6
* Duration in half years
replace wage_payment_inkind = wage_payment_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==7
* Duration in years
replace wage_payment_inkind = wage_payment_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==8
replace wage_payment_inkind = 0 if wage_payment_inkind_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_payment_inkind if wage_payment_inkind~=.
drop if wage_income==0

* Casual vs Formal Wage : here the definition is a combination of (i) receive payments monthly, quarterly, etc (ii)
gen formal = 0
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if withold_taxes==1
replace formal = 1 if health_insurance==1
replace formal = 1 if offer_materpater==1 

gen formalcon = 0 //added definition of "formality" due to contract structure 
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP
replace formalsp = 1 if withold_taxes==1
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if offer_materpater==1 


* Calculate farm income, non-farm income and total income per individual per day
* bring in AFS Shares
rename wage_CIC CIC
replace CIC = 99999 if CIC==.
joinby CIC using "clean data/Tanz1415_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Bring in Member Demographic Info
joinby y4_hhid indidy4 using "clean data/Tanz1415_member_demos.dta", unmatched(none)
* Replace missing AFS Share Values with Country Specific AFS factors
* Currently use AFS Shares calculated with this method from Tanzania 10/11
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.579 if AFS_share==. & rural==1
replace AFS_share = 0.527 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen wage_i_f`f'_c`c' = 0
	gen wage_i_fcon`f'_c`c' = 0 //(!)
	gen wage_i_fsp`f'_c`c' = 0 //(!)
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
	replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f' //(!)
	replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f'
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'

	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}


* Keep relevant variables
g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0)


order y4_hhid CIC ISIC_group wage_income wage_i_* wage_job tradeunion w_contract withold_taxes offer_materpater health_insurance 
keep y4_hhid CIC ISIC_group wage_income wage_i_* informal* formal* wage_job tradeunion w_contract withold_taxes offer_materpater health_insurance 
tab wage_job,g(job) // do you have a wage job yes or no?
g contract_permanent=(w_contract==1)	//has permanent contract
g contract_temporary=(w_contract>1&w_contract<.)	//has temporary contract
recode withold_taxes offer_materpater health_insurance (2=0) 	//0-1 variable for counts

* Collapse to household level
collapse (sum) wage_income wage_i_* tradeunion job1 job2 contract_* Ninformal=informal ///
N_withold_taxes=withold_taxes N_offer_materpater=offer_materpater N_health_insurance=health_insurance (mean) informal* formal* ///
sh_withold_taxes=withold_taxes sh_offer_materpater=offer_materpater sh_health_insurance=health_insurance , by (y4_hhid) 


**labels
foreach v in withold_taxes offer_materpater health_insurance {
la var sh_`v' "Share of HHmemb with `v'"
la var N_`v' "N HHmemb with `v'"
}
la var job1 		"N HHmemb with wagejob(primary)"
la var job2 		"N HHmemb with wagejob(secondary)"
la var informal 	"Informal sh of wagework(no contract,SP)"	// important variable
la var Ninformal	"N HHmemb with informal wagework"
la var formal		"Formal sh of wagework(any contract,SP)"
la var informalcon 	"Informal(no contract)"
la var formalcon  	"Formal sh of wagework(contract)"
la var informalsp 	"Informal sh of wagework(SP:insurance/matpat)"
la var formalsp 	"Formal sh of wagework(SP:insurance/matpat)"
la var tradeunion 	"Any HHwageworker in trade union"

global lab1 "OwnFarming"	
global lab2 "Farm Labor"	
global lab3 "Food&Ag Manufacturing w/in AFS"
global lab4 "FoodPrep"	
global lab5 "FoodSystem Marketing&Transport"
global lab6 "AFS Unclassified"	
global lab7 "Non-AFS Manufacturing"
global lab8 "Non-AFS Industry"	
global lab9 "Non-AFS Marketing&Transport"
global lab10 "Non-AFS Other"
global lab11 "Non-AFS PublicService"
global lab12 "Non-AFS Unclassifed"

forv f=1/12 {
la var wage_i_f0_c`f'    "Informal w from ${lab`f'}"
la var wage_i_fcon0_c`f' "Informal w from ${lab`f'}(contract)"
la var wage_i_fsp0_c`f'  "Informal w from ${lab`f'}(SP)"
la var wage_i_f1_c`f'    "Formal w from ${lab`f'}"
la var wage_i_fcon1_c`f' "Formal w from ${lab`f'}(contract)"
la var wage_i_fsp1_c`f'  "Formal w from ${lab`f'}(SP)"
}

save "clean data/intermediate/Tanz1415_wage_incomeCHEVAL.dta", replace 


** Farm Production

* Input costs
* Input costs from Plot Details (section 3)

* Long Season
use "raw data/ag_sec_3a.dta", clear
rename ag3a_33 cost_plot_rent
rename ag3a_45 cost_org_fert
rename ag3a_51 cost_inorg_fert
rename ag3a_58 cost_inorg_fert2
rename ag3a_63 cost_herbicides
rename ag3a_65c cost_pesticides
rename ag3a_74_4 cost_labor_prep
rename ag3a_74_8 cost_labor_growing
rename ag3a_74_16 cost_labor_harvest
* Do not include costs of inputs on credit to avoid double counting

local costs "plot_rent org_fert inorg_fert inorg_fert2 herbicides pesticides labor_prep labor_growing labor_harvest"

**** Replace missing costs with zero
foreach c in `costs' {
	replace cost_`c' = 0 if cost_`c'==.
}
*

* Sum costs from this section
gen ag3a_costs = 0
foreach c in `costs' {
	replace ag3a_costs = ag3a_costs+cost_`c'
}
*

* Collapse to household level
collapse (sum) ag3a_costs, by (y4_hhid)
drop if ag3a_costs==0

save "clean data/intermediate/Tanz1415_ag3a_costs.dta", replace

* Short Season
use "raw data/ag_sec_3b.dta", clear
rename ag3b_33 cost_plot_rent
rename ag3b_45 cost_org_fert
rename ag3b_51 cost_inorg_fert
rename ag3b_58 cost_inorg_fert2
rename ag3b_63 cost_herbicides
rename ag3b_65c cost_pesticides
rename ag3b_74_4 cost_labor_prep
rename ag3b_74_8 cost_labor_growing
rename ag3b_74_16 cost_labor_harvest
* Do not include costs of inputs on credit to avoid double counting

local costs "plot_rent org_fert inorg_fert inorg_fert2 herbicides pesticides labor_prep labor_growing labor_harvest"

* Replace missing costs with zero
foreach c in `costs' {
	replace cost_`c' = 0 if cost_`c'==.
}
*

* Sum costs from this section
gen ag3b_costs = 0
foreach c in `costs' {
	replace ag3b_costs = ag3b_costs+cost_`c'
}
*

* Collapse to household level
collapse (sum) ag3b_costs, by (y4_hhid)
drop if ag3b_costs==0

save "clean data/intermediate/Tanz1415_ag3b_costs.dta", replace

* Input costs from Crops (section 4)

* Long Season
use "raw data/ag_sec_4a.dta", clear
rename ag4a_12 cost_seeds

* Replace missing costs with zero
replace cost_seeds = 0 if cost_seeds==.

rename cost_seeds ag4a_costs

* Collapse to household level
collapse (sum) ag4a_costs, by (y4_hhid)
drop if ag4a_costs==0

save "clean data/intermediate/Tanz1415_ag4a_costs.dta", replace

* Short Season
use "raw data/ag_sec_4b.dta", clear
rename ag4b_12 cost_seeds

* Replace missing costs with zero
replace cost_seeds = 0 if cost_seeds==.

rename cost_seeds ag4b_costs

* Collapse to household level
collapse (sum) ag4b_costs, by (y4_hhid)
drop if ag4b_costs==0

save "clean data/intermediate/Tanz1415_ag4b_costs.dta", replace

* Input costs from Crops Household Totals (section 5)

* Long Season
use "raw data/ag_sec_5a.dta", clear
rename ag5a_22 cost_transport

* Replace missing costs with zero
replace cost_transport = 0 if cost_transport==.

rename cost_transport ag5a_costs

* Collapse to household level
collapse (sum) ag5a_costs, by (y4_hhid)
drop if ag5a_costs==0

save "clean data/intermediate/Tanz1415_ag5a_costs.dta", replace

* Short Season
use "raw data/ag_sec_5b.dta", clear
rename ag5b_22 cost_transport

* Replace missing costs with zero
replace cost_transport = 0 if cost_transport==.

rename cost_transport ag5b_costs

* Collapse to household level
collapse (sum) ag5b_costs, by (y4_hhid)
drop if ag5b_costs==0

save "clean data/intermediate/Tanz1415_ag5b_costs.dta", replace


* Input costs from Farm Implements & Machines (section 11)
use "raw data/ag_sec_11.dta", clear
rename ag11_09 ag11_costs

* Replace missing costs with zero
replace ag11_costs = 0 if ag11_costs==.

* Collapse to household level
collapse (sum) ag11_costs, by (y4_hhid)
drop if ag11_costs==0

save "clean data/intermediate/Tanz1415_ag11_costs.dta", replace


* Input costs from Extension (section 12)
use "raw data/ag_sec_12a.dta", clear
rename ag12a_06 ag12a_costs

* Replace missing costs with zero
replace ag12a_costs = 0 if ag12a_costs==.

* Collapse to household level
collapse (sum) ag12a_costs, by (y4_hhid)
drop if ag12a_costs==0

save "clean data/intermediate/Tanz1415_ag12a_costs.dta", replace

use "raw data/ag_sec_12b.dta", clear
rename ag12b_10 ag12b_costs

* Replace missing costs with zero
replace ag12b_costs = 0 if ag12b_costs==.

* Collapse to household level
collapse (sum) ag12b_costs, by (y4_hhid)
drop if ag12b_costs==0

save "clean data/intermediate/Tanz1415_ag12b_costs.dta", replace


* Aggregate Agricultural Costs
use "clean data/intermediate/Tanz1415_ag3a_costs.dta", clear
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag3b_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag4a_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag4b_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag5a_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag5b_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag11_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag12a_costs.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag12b_costs.dta", unmatched(both) _merge(_merge)
drop _merge

* Convert Missing to zero
mvencode ag3a_costs ag3b_costs ag4a_costs ag4b_costs ag5a_costs ag5b_costs ag11_costs ag12a_costs ag12b_costs, mv(0)

* Sum Agricultural Costs
egen float ag_costs = rowtotal(ag3a_costs ag3b_costs ag4a_costs ag4b_costs ag5a_costs ag5b_costs ag11_costs ag12a_costs ag12b_costs)

keep y4_hhid ag_costs

save "clean data/intermediate/Tanz1415_ag_costs.dta", replace


* Calculate Quantity of Agriculture Harvest

* Long Season
use "raw data/ag_sec_4a.dta", clear
rename ag4a_28 L_harvest_kg
rename ag4a_29 L_harvest_est_value

* Drop extra observations
drop if L_harvest_kg==.
drop if L_harvest_kg==0

* Collapse to household crop level
collapse (sum) L_harvest_kg L_harvest_est_value, by (y4_hhid zaocode)

save "clean data/intermediate/Tanz1415_ag4a_harvest.dta", replace

* Post Harvest Loss (not to be included in total production for income purposes)
use "raw data/ag_sec_5a.dta", clear
* Calculate percentage of post L_harvest loss
gen ag5a_post_L_harvest_loss = 0
replace ag5a_post_L_harvest_loss = ag5a_31/10 if ag5a_31~=.
keep if ag5a_post_L_harvest_loss~=0

keep y4_hhid zaocode ag5a_post_L_harvest_loss
* One household (3851-001) with total loss

save "clean data/intermediate/Tanz1415_ag5a_harvest_loss.dta", replace

* Adjust L_harvest for loss
use "clean data/intermediate/Tanz1415_ag4a_harvest.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_ag5a_harvest_loss.dta", unmatched(both) _merge(_merge)
drop _merge
mvencode ag5a_post_L_harvest_loss, mv(0)
gen L_harvest_kg_net = L_harvest_kg*(1-ag5a_post_L_harvest_loss)
drop ag5a_post_L_harvest_loss

save "clean data/intermediate/Tanz1415_ag4a_harvest.dta", replace

* Short Season
use "raw data/ag_sec_4b.dta", clear
rename ag4b_28 S_harvest_kg
rename ag4b_29 S_harvest_est_value

* Drop extra observations
drop if S_harvest_kg==.
drop if S_harvest_kg==0

* Collapse to household crop level
collapse (sum) S_harvest_kg S_harvest_est_value, by (y4_hhid zaocode)

save "clean data/intermediate/Tanz1415_ag4b_harvest.dta", replace

* Post S_harvest Loss (not to be included in total production for income purposes)
use "raw data/ag_sec_5b.dta", clear
* Calculate percentage of post S_harvest loss
gen ag5b_post_S_harvest_loss = 0
replace ag5b_post_S_harvest_loss = ag5b_31/10 if ag5b_31~=.
keep if ag5b_post_S_harvest_loss~=0

keep y4_hhid zaocode ag5b_post_S_harvest_loss
* One household (6153-001) with total loss

save "clean data/intermediate/Tanz1415_ag5b_harvest_loss.dta", replace

* Adjust S_harvest for loss
use "clean data/intermediate/Tanz1415_ag4b_harvest.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_ag5b_harvest_loss.dta", unmatched(both) _merge(_merge)
drop _merge
mvencode ag5b_post_S_harvest_loss, mv(0)
gen S_harvest_kg_net = S_harvest_kg*(1-ag5b_post_S_harvest_loss)
drop ag5b_post_S_harvest_loss

save "clean data/intermediate/Tanz1415_ag4b_harvest.dta", replace


* Combine Harvest Quantities

* Merge datasets
use "clean data/intermediate/Tanz1415_ag4a_harvest.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_ag4b_harvest.dta", unmatched(both) _merge(_merge)
drop _merge

* Convert Missing to Zero
mvdecode L_harvest_kg L_harvest_est_value L_harvest_kg_net S_harvest_kg S_harvest_est_value S_harvest_kg_net, mv(0)
mvencode L_harvest_kg L_harvest_est_value L_harvest_kg_net S_harvest_kg S_harvest_est_value S_harvest_kg_net, mv(0)

save "clean data/intermediate/Tanz1415_harvest.dta", replace

* Build Price Look-ups

* Long Season
use "raw data/ag_sec_5a.dta", clear
rename ag5a_02 ag5a_kg_sales
rename ag5a_03 ag5a_value_sales

* Drop extra data
keep y4_hhid zaocode ag5a_kg_sales ag5a_value_sales

* Keep observation with sales
keep if ag5a_value_sales~=.
drop if ag5a_value_sales==0

* estimate prices

* revealed prices per kg
gen revealed_price = ag5a_value_sales/ag5a_kg_sales

* Bring in all production to cover all spatial breakdowns
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_harvest.dta", unmatched(both) _merge(_merge)
keep y4_hhid zaocode revealed_price

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(master) _merge(_merge)
drop _merge

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by zaocode region district ward village ea, sort : egen float count_1 = count(1) if revealed_price~=.
by zaocode region district ward village, sort : egen float count_2 = count(1) if revealed_price~=.
by zaocode region district ward, sort : egen float count_3 = count(1) if revealed_price~=.
by zaocode region district, sort : egen float count_4 = count(1) if revealed_price~=.
by zaocode region, sort : egen float count_5 = count(1) if revealed_price~=.
**** NO ITEM IS SOLD MORE THAN 8 TIMES AT THE REGION DISTRICT WARD LEVEL ****
**** ALL PREDICTED PRICES WILL BE AT THE DISTRICT LEVEL OR GREATER ****

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by zaocode region district ward village ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by zaocode region district ward village, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by zaocode region district ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by zaocode region district, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by zaocode region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by zaocode, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by zaocode region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by zaocode region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by zaocode region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by zaocode region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by zaocode region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by zaocode, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

keep zaocode rural region district ward village ea track predict_price

rename track L_price_level
rename predict_price L_predict_price

mvencode L_predict_price, mv(0)

* Collapse to the "zaocode region district ward village ea" level
collapse (max) L_predict_price L_price_level, by (zaocode region district ward village ea)
drop if L_predict_price==0

save "clean data/intermediate/Tanz1415_price_1a.dta", replace

* Short Season
use "raw data/ag_sec_5b.dta", clear
rename ag5b_02 ag5b_kg_sales
rename ag5b_03 ag5b_value_sales

* Drop extra data
keep y4_hhid zaocode ag5b_kg_sales ag5b_value_sales

* Keep observation with sales
keep if ag5b_value_sales~=.

* estimate prices

* revealed prices per kg
gen revealed_price = ag5b_value_sales/ag5b_kg_sales

* Bring in all production to cover all spatial breakdowns
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_harvest.dta", unmatched(both) _merge(_merge)
keep y4_hhid zaocode revealed_price S_harvest_est_value

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(master) _merge(_merge)
drop _merge

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by zaocode region district ward village ea, sort : egen float count_1 = count(1) if revealed_price~=.
by zaocode region district ward village, sort : egen float count_2 = count(1) if revealed_price~=.
by zaocode region district ward, sort : egen float count_3 = count(1) if revealed_price~=.
by zaocode region district, sort : egen float count_4 = count(1) if revealed_price~=.
by zaocode region, sort : egen float count_5 = count(1) if revealed_price~=.
**** NO ITEM IS SOLD MORE THAN 6 TIMES AT THE REGION DISTRICT WARD LEVEL ****
**** ALL PREDICTED PRICES WILL BE AT THE DISTRICT LEVEL OR GREATER ****

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by zaocode region district ward village ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by zaocode region district ward village, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by zaocode region district ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by zaocode region district, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by zaocode region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by zaocode, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by zaocode region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by zaocode region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by zaocode region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by zaocode region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by zaocode region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by zaocode, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price
* Five observations have no predicted price. Predicted price value = 0

keep zaocode rural region district ward village ea track predict_price

rename track S_price_level
rename predict_price S_predict_price

mvdecode S_predict_price, mv(0)
* Collapse to the "zaocode region district ward village ea" level
collapse (max) S_predict_price S_price_level, by (zaocode region district ward village ea)

save "clean data/intermediate/Tanz1415_price_1b.dta", replace



* Bring in Predicted Prices
use "clean data/intermediate/Tanz1415_harvest.dta", clear
* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Predicted Prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_1a.dta", unmatched(both) _merge(_merge)
drop _merge
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_1b.dta", unmatched(both) _merge(_merge)
drop _merge
drop if y4_hhid==""

* Clean missing values
mvdecode L_harvest_kg_net L_predict_price S_harvest_kg_net S_predict_price, mv(0)
mvencode L_harvest_kg_net L_predict_price S_harvest_kg_net S_predict_price, mv(0)

* Calculate Net Harvest Value
gen L_harvest_value = L_harvest_kg_net*L_predict_price
gen S_harvest_value = S_harvest_kg_net*S_predict_price
mvdecode L_harvest_value S_harvest_value, mv(0)
mvencode L_harvest_value S_harvest_value, mv(0)
gen harvest_value = L_harvest_value + S_harvest_value
gen harvest_kg = L_harvest_kg + S_harvest_kg
gen harvest_kg_net = L_harvest_kg_net + S_harvest_kg_net

* Three observations have no predicted price during long season. Predicted price value = 0
* Nine observations have no predicted price during short season. Predicted price value = 0

* Collapse harvest value to household level
collapse (sum) harvest_value, by (y4_hhid)

save "clean data/intermediate/Tanz1415_harvest_value.dta", replace


* Valuation of gross harvest sold
* Long Season
use "raw data/ag_sec_5a.dta", clear
rename ag5a_02 ag5a_kg_sales

keep y4_hhid zaocode ag5a_kg_sales
drop if zaocode==.
drop if ag5a_kg_sales==.

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in median prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_1a.dta", unmatched(both) _merge(_merge)
drop _merge
drop if y4_hhid==""
drop if zaocode==.

* Calculate Sales Value
gen L_crop_gross_sales = ag5a_kg_sales*L_predict_price

* Collapse to hh level
collapse (sum) L_crop_gross_sales, by (y4_hhid)

save "clean data/intermediate/Tanz1415_L_crop_gross_sales.dta", replace

* Short Season
use "raw data/ag_sec_5b.dta", clear
rename ag5b_02 ag5b_kg_sales

keep y4_hhid zaocode ag5b_kg_sales
drop if zaocode==.
drop if ag5b_kg_sales==.

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in median prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_1b.dta", unmatched(both) _merge(_merge)
drop _merge
drop if y4_hhid==""
drop if zaocode==.

* Calculate Sales Value
gen S_crop_gross_sales = ag5b_kg_sales*S_predict_price

* Collapse to hh level
collapse (sum) S_crop_gross_sales, by (y4_hhid)

save "clean data/intermediate/Tanz1415_S_crop_gross_sales.dta", replace



** Permanent Crops
* Fruit
use "raw data/ag_sec_6a.dta", clear
rename ag6a_09 ag6a_kg_harvest
drop if zaocode==.
* Collapse to household crop level
replace ag6a_kg_harvest = 0 if ag6a_kg_harvest==.
collapse (sum) ag6a_kg_harvest, by (y4_hhid zaocode)

save "clean data/intermediate/Tanz1415_permanent_a.dta", replace

* General Permanent Crops
use "raw data/ag_sec_6b.dta", clear
rename ag6b_09 ag6b_kg_harvest
drop if zaocode==.
* Collapse to household crop level
replace ag6b_kg_harvest = 0 if ag6b_kg_harvest==.
collapse (sum) ag6b_kg_harvest, by (y4_hhid zaocode)

save "clean data/intermediate/Tanz1415_permanent_b.dta", replace

* Post Harvest loss of permanent crops
* Fruit
use "raw data/ag_sec_7a.dta", clear
* Calculate percentage of post L_harvest loss
gen ag7a_post_harvest_loss = 0
replace ag7a_post_harvest_loss = ag7a_15/10 if ag7a_15~=.
keep if ag7a_post_harvest_loss~=0

keep y4_hhid zaocode ag7a_post_harvest_loss
* Four households (2616-001 6111-001 2616-001 3114-001) with total loss of one crop

save "clean data/intermediate/Tanz1415_ag7a_harvest_loss.dta", replace

* General Permanent Crops
use "raw data/ag_sec_7b.dta", clear
* Calculate percentage of post S_harvest loss
gen ag7b_post_harvest_loss = 0
replace ag7b_post_harvest_loss = ag7b_15/10 if ag7b_15~=.
keep if ag7b_post_harvest_loss~=0

keep y4_hhid zaocode ag7b_post_harvest_loss
* One household (3914-001) with total loss of one crop

save "clean data/intermediate/Tanz1415_ag7b_harvest_loss.dta", replace

* Adjust Permanent Crops for loss

* Fruit
use "clean data/intermediate/Tanz1415_permanent_a.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_ag7a_harvest_loss.dta", unmatched(both) _merge(_merge)
drop _merge
mvencode ag7a_post_harvest_loss, mv(0)
gen L_permanent_kg_net = ag6a_kg_harvest*(1-ag7a_post_harvest_loss)
keep y4_hhid zaocode L_permanent_kg_net

save "clean data/intermediate/Tanz1415_permanent_net_a.dta", replace

* General Permanent Crops
use "clean data/intermediate/Tanz1415_permanent_b.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_ag7b_harvest_loss.dta", unmatched(both) _merge(_merge)
drop _merge
mvencode ag7b_post_harvest_loss, mv(0)
gen S_permanent_kg_net = ag6b_kg_harvest*(1-ag7b_post_harvest_loss)
keep y4_hhid zaocode S_permanent_kg_net

save "clean data/intermediate/Tanz1415_permanent_net_b.dta", replace

* Sales Prices for Permanent Crops

* Sales value of permanent crops
* Fruit
use "raw data/ag_sec_7a.dta", clear
rename ag7a_03 ag7a_kg_sales
rename ag7a_04 ag7a_value_sales

* Drop extra data
keep y4_hhid zaocode ag7a_kg_sales ag7a_value_sales

* Keep observation with sales
keep if ag7a_value_sales~=.
keep if ag7a_value_sales~=0

* estimate prices

* revealed prices per kg
gen revealed_price = ag7a_value_sales/ag7a_kg_sales

* Bring in all production to cover all spatial breakdowns
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_permanent_net_a.dta", unmatched(both) _merge(_merge)
keep y4_hhid zaocode revealed_price

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(master) _merge(_merge)
drop _merge

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by zaocode region district ward village ea, sort : egen float count_1 = count(1) if revealed_price~=.
by zaocode region district ward village, sort : egen float count_2 = count(1) if revealed_price~=.
by zaocode region district ward, sort : egen float count_3 = count(1) if revealed_price~=.
by zaocode region district, sort : egen float count_4 = count(1) if revealed_price~=.
by zaocode region, sort : egen float count_5 = count(1) if revealed_price~=.
**** NO ITEM IS SOLD MORE THAN 8 TIMES AT THE REGION DISTRICT WARD LEVEL ****
**** ALL PREDICTED PRICES WILL BE AT THE DISTRICT LEVEL OR GREATER ****

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by zaocode region district ward village ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by zaocode region district ward village, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by zaocode region district ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by zaocode region district, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by zaocode region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by zaocode, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.

mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by zaocode region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by zaocode region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by zaocode region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by zaocode region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by zaocode region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by zaocode, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*

mvdecode predict_price_*, mv (0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price
* 48 observations have no predicted price. Predicted price value = 0
mvencode predict_price, mv (0)

keep zaocode rural region district ward village ea track predict_price

rename track L_price_level
rename predict_price L_predict_price

* Collapse to the "zaocode region district ward village ea" level
collapse (max) L_predict_price L_price_level, by (zaocode region district ward village ea)

save "clean data/intermediate/Tanz1415_price_permanent_a.dta", replace

* General Permanent Crops
use "raw data/ag_sec_7b.dta", clear
rename ag7b_03 ag7b_kg_sales
rename ag7b_04 ag7b_value_sales

* Drop extra data
keep y4_hhid zaocode ag7b_kg_sales ag7b_value_sales

* Keep observation with sales
keep if ag7b_value_sales~=.
keep if ag7b_value_sales~=0

* estimate prices

* revealed prices per kg
gen revealed_price = ag7b_value_sales/ag7b_kg_sales

* Bring in all production to cover all spatial breakdowns
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_permanent_net_b.dta", unmatched(both) _merge(_merge)
keep y4_hhid zaocode revealed_price

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(master) _merge(_merge)
drop _merge

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by zaocode region district ward village ea, sort : egen float count_1 = count(1) if revealed_price~=.
by zaocode region district ward village, sort : egen float count_2 = count(1) if revealed_price~=.
by zaocode region district ward, sort : egen float count_3 = count(1) if revealed_price~=.
by zaocode region district, sort : egen float count_4 = count(1) if revealed_price~=.
by zaocode region, sort : egen float count_5 = count(1) if revealed_price~=.
**** NO ITEM IS SOLD MORE THAN 7 TIMES AT THE REGION DISTRICT WARD LEVEL ****
**** ALL PREDICTED PRICES WILL BE AT THE DISTRICT LEVEL OR GREATER ****

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by zaocode region district ward village ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by zaocode region district ward village, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by zaocode region district ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by zaocode region district, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by zaocode region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by zaocode, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.

mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by zaocode region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by zaocode region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by zaocode region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by zaocode region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by zaocode region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by zaocode, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*

mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price
* 42 observations have no predicted price. Predicted price value = 0
mvencode predict_price, mv(0)

keep zaocode rural region district ward village ea track predict_price

rename track S_price_level
rename predict_price S_predict_price

* Collapse to the "zaocode region district ward village ea" level
collapse (max) S_predict_price S_price_level, by (zaocode region district ward village ea)

save "clean data/intermediate/Tanz1415_price_permanent_b.dta", replace


* Combine all Permanent Crop Production Information
use "clean data/intermediate/Tanz1415_permanent_net_a.dta", clear
joinby y4_hhid zaocode using "clean data/intermediate/Tanz1415_permanent_net_b.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Bring in Predicted Prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_permanent_a.dta", unmatched(both) _merge(_merge)
drop _merge
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_permanent_b.dta", unmatched(both) _merge(_merge)
drop _merge

* Calculate Net Harvest Value
gen L_permanent_value = L_permanent_kg_net*L_predict_price
gen S_permanent_value = S_permanent_kg_net*S_predict_price
mvdecode L_permanent_value S_permanent_value, mv(0)
mvencode L_permanent_value S_permanent_value, mv(0)
gen permanent_value = L_permanent_value + S_permanent_value

* Collapse harvest value to household level
collapse (sum) permanent_value, by (y4_hhid)

save "clean data/intermediate/Tanz1415_permanent_value.dta", replace

* Gross Sales of Permanent Crops
* Fruit
use "raw data/ag_sec_7a.dta", clear
rename ag7a_03 ag7a_kg_sales

keep y4_hhid zaocode ag7a_kg_sales

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in median prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_permanent_a.dta", unmatched(both) _merge(_merge)
drop _merge
drop if y4_hhid==""
drop if zaocode==.

* Clean missing values
mvdecode ag7a_kg_sales L_predict_price, mv(0)
mvencode ag7a_kg_sales L_predict_price, mv(0)

* Calculate Sales Value
gen L_permanent_gross_sales = ag7a_kg_sales*L_predict_price

* Collapse to hh level
collapse (sum) L_permanent_gross_sales, by (y4_hhid)

save "clean data/intermediate/Tanz1415_L_permanent_gross_sales.dta", replace

* General Permanent Crops
use "raw data/ag_sec_7b.dta", clear
rename ag7b_03 ag7b_kg_sales

keep y4_hhid zaocode ag7b_kg_sales

* Bring in location data
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in median prices
joinby zaocode region district ward village ea using "clean data/intermediate/Tanz1415_price_permanent_b.dta", unmatched(both) _merge(_merge)
drop _merge
drop if y4_hhid==""
drop if ag7b_kg_sales==.

* Calculate Sales Value
gen S_permanent_gross_sales = ag7b_kg_sales*S_predict_price

* Collapse to hh level
collapse (sum) S_permanent_gross_sales, by (y4_hhid)

save "clean data/intermediate/Tanz1415_S_permanent_gross_sales.dta", replace


** Livestock
* General Livestock
use "Raw Data/lf_sec_02.dta", clear
rename lvstckid livestock_code
rename lf02_13 livestock_givenaway
rename lf02_25 livestock_sold
rename lf02_26 livestock_sold_value
rename lf02_30 livestock_slaughtered
rename lf02_32 livestock_slaughtered_sold
rename lf02_33 livestock_slaughtered_sold_value
keep y4_hhid livestock*
drop if livestock_givenaway==. & livestock_sold==. & livestock_sold_value==. & livestock_slaughtered==. & livestock_slaughtered_sold==. & livestock_slaughtered_sold_value==.
replace livestock_slaughtered_sold = . if livestock_slaughtered_sold==0

* estimate value of livestock income with estimated prices from sales
gen revealed_price = livestock_sold_value / livestock_sold
gen revealed_price_slaughtered = livestock_slaughtered_sold_value / livestock_slaughtered_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby y4_hhid using "clean data/Tanz1415_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by livestock_code region district ward village ea, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_code region district ward village, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_code region district ward, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_code region district, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_code region, sort : egen float count_5 = count(1) if revealed_price~=.
* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by livestock_code region district ward village ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_code region district ward village, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_code region district ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_code region district, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_code region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by livestock_code, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)
by livestock_code region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_code region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_code region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_code region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_code region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by livestock_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)
* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price
mvencode predict_price, mv(0)

* Not enough observations for spatial medians of slaughtered sold prices
by livestock_code, sort : egen float temp_est_price = median (revealed_price_slaughtered) if revealed_price_slaughtered~=.
mvencode temp_*, mv(0)
by livestock_code, sort : egen float predict_price_slaughtered = max(temp_est_price)

mvdecode livestock_* predict_price predict_price_slaughtered, mv(0)
mvencode livestock_* predict_price predict_price_slaughtered, mv(0)

gen livestock_income_yr = (livestock_givenaway+livestock_sold)*predict_price+livestock_slaughtered*predict_price_slaughtered
gen livestock_sold_yr = livestock_sold*predict_price+livestock_slaughtered_sold*predict_price_slaughtered

keep y4_hhid livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (y4_hhid)
drop if livestock_income==0 & livestock_sold==0

save "clean data/intermediate/Tanz1415_livestock.dta", replace

* Milk
use "Raw Data/lf_sec_06.dta", clear
rename lvstckcat milk_code
rename lf06_01 milk_num_animals
rename lf06_02 milk_months_prod
rename lf06_03 milk_qty_prod
rename lf06_08 milk_qty_sold
rename lf06_09 milk_qty_input
rename lf06_10 milk_proc_qty_sold
rename lf06_11 milk_value_sold
keep y4_hhid milk*
mvdecode milk*, mv(0)
drop if milk_qty_prod==. & milk_qty_sold==. & milk_proc_qty_sold==. & milk_value_sold==.

* estimate value of milk income with estimated prices from sales (cannot separate qty processed and unprocessed milk). This will underestimate processed, and over estimate unprocessed.
mvencode milk_qty_sold milk_proc_qty_sold, mv(0)
gen milk_sold_price = milk_value_sold / (milk_qty_sold+milk_proc_qty_sold)

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby y4_hhid using "clean data/Tanz1415_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by milk_code region district ward village ea, sort : egen float count_1 = count(1) if milk_sold_price~=.
by milk_code region district ward village, sort : egen float count_2 = count(1) if milk_sold_price~=.
by milk_code region district ward, sort : egen float count_3 = count(1) if milk_sold_price~=.
by milk_code region district, sort : egen float count_4 = count(1) if milk_sold_price~=.
by milk_code region, sort : egen float count_5 = count(1) if milk_sold_price~=.
* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by milk_code region district ward village ea, sort : egen float temp_predict_price_1 = median(milk_sold_price) if milk_sold_price~=.
by milk_code region district ward village, sort : egen float temp_predict_price_2 = median(milk_sold_price) if milk_sold_price~=.
by milk_code region district ward, sort : egen float temp_predict_price_3 = median(milk_sold_price) if milk_sold_price~=.
by milk_code region district, sort : egen float temp_predict_price_4 = median(milk_sold_price) if milk_sold_price~=.
by milk_code region, sort : egen float temp_predict_price_5 = median(milk_sold_price) if milk_sold_price~=.
by milk_code, sort : egen float temp_predict_price_6 = median(milk_sold_price) if milk_sold_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)
by milk_code region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by milk_code region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by milk_code region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by milk_code region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by milk_code region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by milk_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)
* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* milk_sold_price track
mvencode predict_price, mv(0)

mvdecode milk_* predict_price, mv(0)
mvencode milk_* predict_price, mv(0)

gen milk_income_yr = (milk_num_animals*milk_qty_prod-milk_qty_input+milk_proc_qty_sold)*milk_months_prod*30.4*predict_price
gen milk_sold_yr = (milk_qty_sold-milk_qty_input+milk_proc_qty_sold)*milk_months_prod*30.4*predict_price
replace milk_sold_yr = 0 if milk_sold_yr<0

keep y4_hhid milk_income_yr milk_sold_yr

gen milk_income = milk_income_yr/365
gen milk_sold = milk_sold_yr/365

collapse (sum) milk_*, by (y4_hhid)
drop if milk_income==0 & milk_sold==0

save "clean data/intermediate/Tanz1415_milk.dta", replace

* Animal Other
use "Raw Data/lf_sec_07.dta", clear
rename lf07_03 livestock_dung_yr
keep y4_hhid livestock_dung_yr
mvdecode livestock_dung_yr, mv(0)
mvencode livestock_dung_yr, mv(0)
drop if livestock_dung_yr==0

rename livestock_dung_yr livestock_other_nfegi_yr

collapse (sum) livestock_other_nfegi_yr, by (y4_hhid)

gen livestock_other_nfegi = livestock_other_nfegi_yr/365

save "clean data/intermediate/Tanz1415_livestock_other.dta", replace

* Livestock byproduct
use "Raw Data/lf_sec_08.dta", clear
rename productid byproduct_code
rename lf08_02 byproduct_months_prod
rename lf08_03_1 byproduct_qty_prod
rename lf08_03_2 byproduct_unit_prod
rename lf08_05_1 byproduct_qty_sold
rename lf08_05_2 byproduct_unit_sold
rename lf08_06 byproduct_value_sold
keep y4_hhid byproduct*
mvdecode byproduct*, mv(0)
drop if byproduct_qty_prod==. & byproduct_qty_sold==. & byproduct_value_sold==.

* estimate value of byproduct income with estimated prices from sales 
gen byproduct_sold_price = byproduct_value_sold / byproduct_qty_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby y4_hhid using "clean data/Tanz1415_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by byproduct_code byproduct_unit_prod region district ward village ea, sort : egen float count_1 = count(1) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district ward village, sort : egen float count_2 = count(1) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district ward, sort : egen float count_3 = count(1) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district, sort : egen float count_4 = count(1) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region, sort : egen float count_5 = count(1) if byproduct_sold_price~=.
* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by byproduct_code byproduct_unit_prod region district ward village ea, sort : egen float temp_predict_price_1 = median(byproduct_sold_price) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district ward village, sort : egen float temp_predict_price_2 = median(byproduct_sold_price) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district ward, sort : egen float temp_predict_price_3 = median(byproduct_sold_price) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region district, sort : egen float temp_predict_price_4 = median(byproduct_sold_price) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod region, sort : egen float temp_predict_price_5 = median(byproduct_sold_price) if byproduct_sold_price~=.
by byproduct_code byproduct_unit_prod, sort : egen float temp_predict_price_6 = median(byproduct_sold_price) if byproduct_sold_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)
by byproduct_code byproduct_unit_prod region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by byproduct_code byproduct_unit_prod region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by byproduct_code byproduct_unit_prod region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by byproduct_code byproduct_unit_prod region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by byproduct_code byproduct_unit_prod region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by byproduct_code byproduct_unit_prod, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)
* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* byproduct_sold_price track
mvencode predict_price, mv(0)

mvdecode byproduct_* predict_price, mv(0)
mvencode byproduct_* predict_price, mv(0)

gen byproduct_income_yr = byproduct_qty_prod*byproduct_months_prod*predict_price
gen byproduct_sold_yr = byproduct_qty_sold*predict_price
replace byproduct_sold_yr = 0 if byproduct_sold_yr<0

keep y4_hhid byproduct_income_yr byproduct_sold_yr

gen byproduct_income = byproduct_income_yr/365
gen byproduct_sold = byproduct_sold_yr/365

collapse (sum) byproduct_*, by (y4_hhid)
drop if byproduct_income==0 & byproduct_sold==0

save "clean data/intermediate/Tanz1415_livestock_byproduct.dta", replace

* Fish
use "Raw Data/lf_sec_12.dta", clear
rename fishid fish_code
rename lf12_07 fish_weeks_prod
rename lf12_08_1 fish_qty_prod
rename lf12_08_2 fish_unit_prod
rename lf12_11 fish_weeks_sold
rename lf12_12_1 fish_qty_sold
rename lf12_12_2 fish_unit_sold
rename lf12_12_3 fish_unit_form
rename lf12_12_4 fish_unit_price
keep y4_hhid fish*
mvdecode fish*, mv(0)
drop if fish_qty_prod==. & fish_qty_sold==.
save "clean data/intermediate/Tanz1415_temp_fish.dta", replace
use "Raw Data/lf_sec_12.dta", clear
rename fishid fish_code
rename lf12_11 fish_weeks_sold
rename lf12_12_5 fish_qty_sold
rename lf12_12_6 fish_unit_sold
rename lf12_12_7 fish_unit_form
rename lf12_12_8 fish_unit_price
keep y4_hhid fish*
mvdecode fish*, mv(0)
drop if fish_qty_sold==.
append using "clean data/intermediate/Tanz1415_temp_fish.dta"
drop if fish_code==.

* estimate value of fish income with estimated prices from sales 
* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby y4_hhid using "clean data/Tanz1415_location.dta", unmatched(none)
* Count observations with calculated prices by geographic location.
by fish_code fish_unit_sold fish_unit_form region district ward village ea, sort : egen float count_1 = count(1) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district ward village, sort : egen float count_2 = count(1) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district ward, sort : egen float count_3 = count(1) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district, sort : egen float count_4 = count(1) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region, sort : egen float count_5 = count(1) if fish_unit_price~=.
* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by fish_code fish_unit_sold fish_unit_form region district ward village ea, sort : egen float temp_predict_price_1 = median(fish_unit_price) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district ward village, sort : egen float temp_predict_price_2 = median(fish_unit_price) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district ward, sort : egen float temp_predict_price_3 = median(fish_unit_price) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region district, sort : egen float temp_predict_price_4 = median(fish_unit_price) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form region, sort : egen float temp_predict_price_5 = median(fish_unit_price) if fish_unit_price~=.
by fish_code fish_unit_sold fish_unit_form, sort : egen float temp_predict_price_6 = median(fish_unit_price) if fish_unit_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)
by fish_code fish_unit_sold fish_unit_form region district ward village ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by fish_code fish_unit_sold fish_unit_form region district ward village, sort : egen float predict_price_2 = max(temp_predict_price_2)
by fish_code fish_unit_sold fish_unit_form region district ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by fish_code fish_unit_sold fish_unit_form region district, sort : egen float predict_price_4 = max(temp_predict_price_4)
by fish_code fish_unit_sold fish_unit_form region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by fish_code fish_unit_sold fish_unit_form, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)
* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* fish_unit_price track
mvencode predict_price, mv(0)

mvdecode fish_* predict_price, mv(0)
mvencode fish_* predict_price, mv(0)

gen fish_income_yr = fish_weeks_prod*fish_qty_prod*predict_price
gen fish_sold_yr = fish_weeks_sold*fish_qty_sold*predict_price

keep y4_hhid fish_income_yr fish_sold_yr

gen fish_income = fish_income_yr/365
gen fish_sold = fish_sold_yr/365

collapse (sum) fish_*, by (y4_hhid)
drop if fish_income==0 & fish_sold==0

save "clean data/intermediate/Tanz1415_fish.dta", replace

* Remittance Income
use "raw data/hh_sec_q2.dta", clear
rename hh_q23 remittance_cash
rename hh_q26 remittance_inkind

* Clean missing values
mvdecode remittance_cash remittance_inkind, mv(0)
mvencode remittance_cash remittance_inkind, mv(0)

* Collapse to hh level
collapse (sum) remittance_cash remittance_inkind, by (y4_hhid)
gen remittances = (remittance_cash+remittance_inkind)/365

keep y4_hhid remittances

save "clean data/intermediate/Tanz1415_remittances.dta", replace


** Other Assistance
use "raw data/hh_sec_o1.dta", clear
rename hh_o03 assistance_cash
rename hh_o04 assistance_food
rename hh_o05 assistance_other

* Clean missing values
mvdecode assistance_*, mv(0)
mvencode assistance_*, mv(0)

* Collapse to hh level
collapse (sum) assistance_*, by (y4_hhid)
gen assistance = (assistance_cash+assistance_food+assistance_other)/365

keep y4_hhid assistance

save "clean data/intermediate/Tanz1415_assistance.dta", replace


* Rent of land
* Long Season
use "raw data/ag_sec_3a.dta", clear
rename ag3a_04 rent_long
replace rent_long = 0 if rent_long==.
replace rent_long = rent_long/365

collapse (max) rent_long, by (y4_hhid)

save "clean data/intermediate/Tanz1415_rent_long.dta", replace

* Short Season
use "raw data/ag_sec_3b.dta", clear
rename ag3b_04 rent_short
replace rent_short = 0 if rent_short==.
replace rent_short = rent_short/365

collapse (max) rent_short, by (y4_hhid)

save "clean data/intermediate/Tanz1415_rent_short.dta", replace


* Costs

* Seeds
* Long Season
use "raw data/ag_sec_4a.dta", clear
rename ag4a_12 cost_seeds
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
* Collapse to household level
collapse (sum) cost_seeds, by (y4_hhid)
save "clean data/intermediate/Tanz1415_seeds_1.dta", replace
* Short Season
use "raw data/ag_sec_4b.dta", clear
rename ag4b_12 cost_seeds
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
* Collapse to household level
collapse (sum) cost_seeds, by (y4_hhid)
save "clean data/intermediate/Tanz1415_seeds_2.dta", replace

* Fertilizer, Crop Labor & Land Rent
* Long Season
use "raw data/ag_sec_3a.dta", clear
rename ag3a_33 cost_plot_rent
rename ag3a_45 cost_org_fert
rename ag3a_51 cost_inorg_fert
rename ag3a_58 cost_inorg_fert2
rename ag3a_63 cost_herbicides
rename ag3a_65c cost_pesticides
rename ag3a_74_4 cost_labor_prep
rename ag3a_74_8 cost_labor_growing
rename ag3a_74_16 cost_labor_harvest
* Do not include costs of inputs on credit to avoid double counting
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
gen cost_fertilizer = cost_org_fert + cost_inorg_fert + cost_inorg_fert2 + cost_herbicides + cost_pesticides
gen cost_labor = cost_labor_prep + cost_labor_growing + cost_labor_harvest
* Collapse to household level
collapse (sum) cost_fertilizer cost_labor cost_plot_rent, by (y4_hhid)
save "clean data/intermediate/Tanz1415_costs_fert_labor_land_1.dta", replace
* Short Season
use "raw data/ag_sec_3b.dta", clear
rename ag3b_33 cost_plot_rent
rename ag3b_45 cost_org_fert
rename ag3b_51 cost_inorg_fert
rename ag3b_58 cost_inorg_fert2
rename ag3b_63 cost_herbicides
rename ag3b_65c cost_pesticides
rename ag3b_74_4 cost_labor_prep
rename ag3b_74_8 cost_labor_growing
rename ag3b_74_16 cost_labor_harvest
* Do not include costs of inputs on credit to avoid double counting
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
gen cost_fertilizer = cost_org_fert + cost_inorg_fert + cost_inorg_fert2 + cost_herbicides + cost_pesticides
gen cost_labor = cost_labor_prep + cost_labor_growing + cost_labor_harvest
* Collapse to household level
collapse (sum) cost_fertilizer cost_labor cost_plot_rent, by (y4_hhid)
save "clean data/intermediate/Tanz1415_fert_labor_land_2.dta", replace

* Hired Animal & Fish Labor
**** FISH LABOR DATA (lf_sec_10) IS MISSING
use "raw data/lf_sec_05.dta", clear
rename lf05_07 cost_animal_labor
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (y4_hhid)
drop if cost_animal_labor==0
save "clean data/intermediate/Tanz1415_costs_animal_labor.dta", replace

* Animal/Fish fuel and transport
use "raw data/lf_sec_11a.dta", clear
mvdecode lf11_03 lf11_07, mv(0)
mvencode lf11_03 lf11_07, mv(0)
gen cost_animal_fuel = lf11_03 * lf11_07
collapse (sum) cost_*, by (y4_hhid)
drop if cost_animal_fuel==0
save "clean data/intermediate/Tanz1415_costs_animal_fuel.dta", replace

* Animal Feed
use "raw data/lf_sec_04.dta", clear
mvdecode lf04_04 lf04_09, mv(0)
mvencode lf04_04 lf04_09, mv(0)
gen cost_feed = lf04_04 + lf04_09
collapse (sum) cost_*, by (y4_hhid)
drop if cost_feed==0
save "clean data/intermediate/Tanz1415_costs_feed.dta", replace

* Land Rent
* Above

* Machine & Animal Rent
use "raw data/ag_sec_11.dta", clear
mvdecode ag11_09, mv(0)
mvencode ag11_09, mv(0)
gen cost_machine = ag11_09
collapse (sum) cost_*, by (y4_hhid)
drop if cost_machine==0
save "clean data/intermediate/Tanz1415_costs_machine.dta", replace

* Transport Costs
use "raw data/ag_sec_5a.dta", clear
rename ag5a_22 cost_transport
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
* Collapse to household level
collapse (sum) cost_transport, by (y4_hhid)
drop if cost_transport==0
save "clean data/intermediate/Tanz1415_transport_1.dta", replace
* Short Season
use "raw data/ag_sec_5b.dta", clear
rename ag5b_22 cost_transport
mvdecode cost*, mv(0)
mvencode cost*, mv(0)
* Collapse to household level
collapse (sum) cost_transport, by (y4_hhid)
drop if cost_transport==0
save "clean data/intermediate/Tanz1415_transport_2.dta", replace


* Aggregate Costs
use "clean data/intermediate/Tanz1415_seeds_1.dta", clear
joinby using "clean data/intermediate/Tanz1415_costs_fert_labor_land_1.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Tanz1415_transport_1.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Tanz1415_costs_machine.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Tanz1415_costs_feed.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Tanz1415_costs_animal_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Tanz1415_costs_animal_fuel.dta", unmatched(both) _merge(_merge)
drop _merge
append using "clean data/intermediate/Tanz1415_seeds_2.dta"
append using "clean data/intermediate/Tanz1415_fert_labor_land_2.dta"
append using "clean data/intermediate/Tanz1415_transport_2.dta"
* Convert Missing to zero
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (y4_hhid)
local costs "cost_seeds cost_fertilizer cost_labor cost_plot_rent cost_transport cost_machine cost_feed cost_animal_labor cost_animal_fuel"
foreach c in `costs' {
	replace `c' = `c'/365
}
*

save "clean data/intermediate/Tanz1415_costs.dta", replace


**** COMBINE ALL ****
* Begin with harvest value
use "clean data/intermediate/Tanz1415_harvest_value.dta", clear

* Bring in permanent crops
joinby y4_hhid using "clean data/intermediate/Tanz1415_permanent_value.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in harvest costs
joinby y4_hhid using "clean data/intermediate/Tanz1415_ag_costs.dta", unmatched(both) _merge(_merge)
drop _merge
replace ag_costs = 0 if ag_costs==.
**** drop harvest costs if household does not report harvest production 
drop if ag_costs~=0 & harvest_value+permanent_value==0
mvdecode ag_costs harvest_value permanent_value, mv(0)
mvencode ag_costs harvest_value permanent_value, mv(0)

* Generate Daily net Ag Production income
*gen net_AgProduction_value = (harvest_value+permanent_value-ag_costs)/365
*gen gross_AgProduction_value = (harvest_value+permanent_value)/365
gen AgProduction_value = (harvest_value+permanent_value)/365

* Bring in Gross Sales
joinby y4_hhid using "clean data/intermediate/Tanz1415_L_crop_gross_sales.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_S_crop_gross_sales.dta", unmatched(both) _merge(_merge)
drop _merge
replace L_crop_gross_sales = L_crop_gross_sales/365
replace S_crop_gross_sales = S_crop_gross_sales/365
gen crop_gross_sales = 0
replace crop_gross_sales = crop_gross_sales + L_crop_gross_sales if L_crop_gross_sales~=.
replace crop_gross_sales = crop_gross_sales + S_crop_gross_sales if S_crop_gross_sales~=.
joinby y4_hhid using "clean data/intermediate/Tanz1415_L_permanent_gross_sales.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby y4_hhid using "clean data/intermediate/Tanz1415_S_permanent_gross_sales.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace L_permanent_gross_sales = L_permanent_gross_sales/365
replace S_permanent_gross_sales = S_permanent_gross_sales/365
gen permanent_gross_sales = 0
replace permanent_gross_sales = permanent_gross_sales + L_permanent_gross_sales if L_permanent_gross_sales~=.
replace permanent_gross_sales = permanent_gross_sales + S_permanent_gross_sales if S_permanent_gross_sales~=.
drop L_* S_*
generate farm_sales = crop_gross_sales+permanent_gross_sales

*gen f_i_net_c1 = net_AgProduction_value
*gen f_i_gross_c1 = gross_AgProduction_value
gen f_i_c1 = AgProduction_value

* Bring in Non-Farm Own Enterprises
joinby y4_hhid using "clean data/intermediate/Tanz1415_nfe_income.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Wage Income
joinby y4_hhid using "clean data/intermediate/Tanz1415_wage_incomeCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Livestock
joinby y4_hhid using "clean data/intermediate/Tanz1415_livestock.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Milk
joinby y4_hhid using "clean data/intermediate/Tanz1415_milk.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Livestock Other
joinby y4_hhid using "clean data/intermediate/Tanz1415_livestock_other.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Livestock Byproducts
joinby y4_hhid using "clean data/intermediate/Tanz1415_livestock_byproduct.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Fish
joinby y4_hhid using "clean data/intermediate/Tanz1415_fish.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Remittances
joinby y4_hhid using "clean data/intermediate/Tanz1415_remittances.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Assistance
joinby y4_hhid using "clean data/intermediate/Tanz1415_assistance.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Rent from land
* Long
joinby y4_hhid using "clean data/intermediate/Tanz1415_rent_long.dta", unmatched(both) _merge(_merge)
drop _merge
* Short
joinby y4_hhid using "clean data/intermediate/Tanz1415_rent_short.dta", unmatched(both) _merge(_merge)
drop _merge

keep y4_hhid f_i_* nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_* wage_i_* wage_income livestock_* milk_* byproduct_* fish_* remittances assistance rent* farm_sales informal* formal* job1 job2 tradeunion ///
Ninformal N_health_insurance informal informalcon informalsp ///
formal formalcon formalsp sh_health_insurance remit ///
nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales nfe_gross_inc nfe_costs nfe_net_inc farm_sales 

* Bring in Demographics
joinby y4_hhid using "clean data/Tanz1415_demographics.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Costs
joinby y4_hhid using "clean data/intermediate/Tanz1415_costs.dta", unmatched(both) _merge(_merge)
drop _merge


mvdecode f_i_* nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_* wage_i_* wage_income livestock_* milk_* byproduct_* fish_* remittances assistance rent* farm_sales cost_* informal* formal* job1 job2 tradeunion, mv(0) // changed (!)
mvencode f_i_* nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_* wage_i_* wage_income livestock_* milk_* byproduct_* fish_* remittances assistance rent* farm_sales cost_* informal* formal* job1 job2 tradeunion, mv(0) // changed (!)
gen pop_people = hhsize*hhweight
gen pop_youth = hhsize*youth_share_24*hhweight
gen data_obs = 1
gen hh_represented = hhweight

* Convert to Per Capita USD constant PPP
local USD "farm_sales nfe_gross_inc nfe_costs nfe_net_inc wage_income livestock_income livestock_sold milk_income milk_sold livestock_other_nfegi byproduct_income byproduct_sold fish_income fish_sold remittances assistance rent_long rent_short f_i_c1 cost_seeds cost_fertilizer cost_labor cost_plot_rent cost_transport cost_machine cost_feed cost_animal_labor cost_animal_fuel"
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 2/15/2015
	gen USD_`u' = `u'*0.0005494505
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (3/11 * 2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method) + (8/11 * 2015 GNI per capita constant 2011 PPP / 2015 GNI per capita Atlas Method)
	replace USD_`u' = USD_`u'*2.662
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 2/15/2015
	gen USD_`usd'`u' = `usd'`u'*0.0005494505
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (3/11 * 2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method) + (8/11 * 2015 GNI per capita constant 2011 PPP / 2015 GNI per capita Atlas Method)
	replace USD_`usd'`u' = USD_`usd'`u'*2.662
}
}
*	
* Drop former variables
local USD "farm_sales nfe_gross_inc nfe_costs nfe_net_inc wage_income livestock_income livestock_sold milk_income milk_sold livestock_other_nfegi byproduct_income byproduct_sold fish_income fish_sold remittances assistance rent_long rent_short f_i_c1 cost_seeds cost_fertilizer cost_labor cost_plot_rent cost_transport cost_machine cost_feed cost_animal_labor cost_animal_fuel"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	
* Rename
local USD "farm_sales nfe_gross_inc nfe_costs nfe_net_inc wage_income livestock_income livestock_sold milk_income milk_sold livestock_other_nfegi byproduct_income byproduct_sold fish_income fish_sold remittances assistance rent_long rent_short f_i_c1 cost_seeds cost_fertilizer cost_labor cost_plot_rent cost_transport cost_machine cost_feed cost_animal_labor cost_animal_fuel"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	

* Impute farm income components before creating the aggregate variable
local varlist f_i_c1 livestock_income milk_income byproduct_income fish_income
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}


replace farm_sales=f_i_c1 if farm_sales>f_i_c1 & farm_sales!=.
replace livestock_sold=livestock_income if livestock_sold>livestock_income & livestock_sold!=.
replace milk_sold=milk_income if milk_sold>milk_income & milk_sold!=.
replace byproduct_sold=byproduct_income if byproduct_sold>byproduct_income & byproduct_sold!=.
replace fish_sold=fish_income if fish_sold>fish_income & fish_sold!=.


replace farm_sales = farm_sales + livestock_sold + milk_sold + byproduct_sold + fish_sold
gen own_farm_crop_inc_costs = cost_seeds + cost_fertilizer + cost_labor + cost_plot_rent + cost_transport + cost_machine
gen own_farm_livestock_inc_costs = cost_feed + cost_animal_labor + cost_animal_fuel
gen own_farm_crop_inc_gross = f_i_c1
gen own_farm_livestock_inc_gross = livestock_income + milk_income + byproduct_income + fish_income
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

* Bring in livestock enterprise income
local nfe "nfe_gross_inc nfe_gi_c2 nfe_net_inc nfe_i_c2"
foreach n in `nfe' {
	replace `n' = `n' + livestock_other_nfegi
}
*

** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + remittances + assistance + rent_long + rent_short
}
*

joinby using "clean data/Tanz1415_hhid.dta", unmatched(none)


gen remit = 1 if remit>0
replace remit = 0 if remit==.

merge 1:1 y4_hhid using "raw data/consumptionnps4.dta" , nogen

g hhexp = expmR/365
g pcexp = hhexp/hhsize

foreach u of varlist hhexp pcexp {
    gen USD_`u' = `u'*0.0005494505
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (3/11 * 2014 GNI per capita constant 2011 PPP / 2014 GNI per capita Atlas Method) + (8/11 * 2015 GNI per capita constant 2011 PPP / 2015 GNI per capita Atlas Method)
	replace USD_`u' = USD_`u'*2.662
	drop `u'
	ren USD_`u' `u'
}


g N_pension=.
g sh_pension=.
keep hhid clusterid rural hhweight hhsize youth_share_24 youth_share_34 female_head school_secondary_hh ///
educ_max credit land_owned job1 job2 /// 
Ninformal N_pension N_health_insurance  ///
informal informalcon informalsp formal formalcon formalsp ///
sh_pension sh_health_insurance remit pcexp nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales ///
own_farm* i_gi* i_ni* i_nnni* nfe_gross_inc nfe_costs nfe_net_inc farm_sales ///
admin_1 admin_2 admin_3

save "clean data/TANZANIA_incomesharesCHEVAL.dta", replace

