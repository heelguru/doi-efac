
*** This do-file generates labels for all remaining variables ***

//FTE shares
	lab var fte_wage_formal "Formal wage FTEs, hh total"
	lab var fte_wage_formal_sp "Formal wage FTEs (social protection), hh total" 
	lab var sh_fte_formal "Share of formal FTEs"
	lab var sh_fte_formal_sp "Share of formal (social protection) FTEs"

//job shares
	lab var occ_wage_f "Formal wage occupation, hh total"
	lab var occ_wage_sp "Formal (social protection) wage occupation, hh total"
	lab var sh_occ_formal "Share of formal occupations"
	lab var sh_occ_formal_sp "Share of formal (social protection) occupations"

//other variables	
lab var log_pcexp				"Log(Per capita daily expenditure)"
label var dep_ratio 			"Dependency ratio"
label var dep_n 				"N. hh members 0-14 & +65"
label var indep_n 				"N. hh members 15-64"
//lab var ea_id 					"Location ID (to match with spatial data)"
lab var hhweight 				"Household weight"
lab var fte_total 				"FTE all occupations, hh total"
lab var occ_total 				"Occupations, all occupations, hh total"
lab var occ_ownfarm 			"Occupations, own farm, hh total"
lab var occ_wage 				"Occupations, wage work, hh total"
lab var occ_enterprise  		"Occupations, own enterprise, hh total"
lab var land_owned 				"Land owned, hectares"
lab var credit     				"Dummy of credit"
//lab var remit      				"Remittances received, dummy"
lab var land_d  				"Land ownership, dummy"
lab var hhsize 					"Household size"
//lab var nfe_costs 				"Non-farm enterprise costs"
lab var pcexp 					"Daily per capita expenditure, constant $US PPP 2011"
//lab var wage_formal 			"Wage inc, formal"
//lab var wage_formalsp 			"Wage inc, formal SP"
//lab var nfe_net_inc 		"business inc, net"
//lab var nfe_gross_inc 		"business inc, gross"
lab var poor 					"Poor (international poverty line US$ 1.90 per day"
lab var total_active_mem 		"Number of active hh members"
lab var wa_share "Share of working-age hh members"
lab var total_active_youth 		"Number of active youth in hh"
lab var N_inschool 				"Number of hh members currently in school"
lab var N_out_labor_mkt 		"Number of hh members out of labor market"
lab var sh_NEET 				"NEET, share of hh members"
lab var sh_NEET_34 				"NEET 15-34, share of hh members"
lab var sh_out_labor_mkt 		"Share of hh members out of labor market"
lab var avg_total_jobs 			"Number of jobs, avg."
lab var sh_school_secondary 	"Share of hh members with secondary schooling"
lab var age_mean 				"Age, avg."
//lab var total_income 			"Total income, gross"
//lab var tot_inc_net 			"Total income, net"
//lab var total_nonAFS_income_net	"Non-AFS inc, net" 
lab var rural 					"Rural, administrative"
lab var total_jobs				"Total number of jobs, hh"
lab var avg_total_jobs			"Average number of jobs, hh"
//lab var inc_count 				"Income diversification, count"
//lab var c_wage					"Someone in hh earns wage income"

foreach v in dist_popcenter dist_admctr dist_road dist_market dist_borderpost{
		lab var terc_`v' "Terciles of `v'"
	}

ex




