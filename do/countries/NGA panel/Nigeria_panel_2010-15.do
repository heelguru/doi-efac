
capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl raw	"$path/data\Nigeria_GHS_1516\raw data"
gl nga1	"$path/data\NGA_2010_GHSP-W1_v03_M_STATA\clean data"
gl nga2	"$path/data\NGA_2012_GHSP-W2_v02_M_STATA\clean data"
gl nga3	"$path/data\Nigeria_GHS_1516\clean data"

gl DO1 "$path/do/countries\NGA panel"
gl clean "$path/data"
 ********************************************************************************

tempfile temp0
use "$nga3/NIGERIA_incomesharesCHEVAL", clear
keep HHID hhweight hhsize female_head educ_max school_secondary_hh credit land_owned N_health_insurance N_pension sh_health_insurance sh_pension rural
ren HHID hhid
merge 1:1 hhid using "$nga3/NIGERIAIA1516_expenditure_cheval", nogen keep(1 3)
ren hhid HHID
tostring HHID, g(hhid)
save `temp0'

tempfile temp1 
use "$nga3/NIGERIA_FTEsCHEVAL_2015_panel", clear
do "$DO1/CHEVAL_Occ_clean_NGA.do"
gcollapse (sum) occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep_n=dep indep_n=indep total_active_mem=active_mem ///
		total_active_youth=active_youth_24 N_inschool=school_currently N_out_labor_mkt=out_labor_mkt ///
		(mean) sh_NEET=NEET sh_NEET_34=NEET_34 sh_out_labor_mkt=out_labor_mkt avg_total_jobs=total_jobs ///
			sh_school_secondary=school_secondary female_head age_mean=age ///			
		(max) age_head head_f head_sp ///
		, by(hhid country) labelformat(#sourcelabel#)
g year=2015
merge 1:1 hhid using `temp0', nogen keep(3)
g ccode = "NGA"
g hhid_unique = ccode + hhid
	lab var hhid_unique "Household ID, unique"
save `temp1'

tempfile temp2
use "$nga1/Nigeria10_FTEsCHEVAL", clear
do "$DO1/CHEVAL_Occ_clean_NGA_2010.do"
gcollapse (sum) occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep_n=dep indep_n=indep total_active_mem=active_mem ///
		total_active_youth=active_youth_24 N_inschool=school_currently N_out_labor_mkt=out_labor_mkt ///
		(mean) sh_health_insurance=health_insurance sh_NEET=NEET sh_NEET_34=NEET_34 sh_out_labor_mkt=out_labor_mkt avg_total_jobs=total_jobs ///
			sh_school_secondary=school_secondary female_head age_mean=age ///			
		(max) age_head head_f head_sp ///
		, by(hhid country) labelformat(#sourcelabel#)
g year=2010
destring hhid, replace
merge 1:1 hhid using "$nga1/NIGERIA10_expenditure", nogen keep(3)
tostring hhid, replace
g ccode = "NGA"
g hhid_unique = ccode + hhid
	lab var hhid_unique "Household ID, unique"
save `temp2'

* Generate panel *
use "$nga2/NIGERIA12_FTEsCHEVAL", clear
do "$DO1/CHEVAL_Occ_clean_NGA.do"
gcollapse (sum) occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep_n=dep indep_n=indep total_active_mem=active_mem ///
		total_active_youth=active_youth_24 N_inschool=school_currently N_out_labor_mkt=out_labor_mkt ///
		(mean) sh_pension=pension sh_health_insurance=health_insurance sh_NEET=NEET sh_NEET_34=NEET_34 sh_out_labor_mkt=out_labor_mkt avg_total_jobs=total_jobs ///
			sh_school_secondary=school_secondary female_head age_mean=age ///
		(max) age_head head_f  head_sp ///
		, by(hhid country ) labelformat(#sourcelabel#)
g year=2012
destring hhid, replace
merge 1:1 hhid using "$nga2/NIGERIA12_expenditure", nogen keep(3)
tostring hhid, replace
g ccode = "NGA"
g hhid_unique = ccode + hhid
	lab var hhid_unique "Household ID, unique"
append using `temp1'
append using `temp2'

order hhid_unique hhid year country ccode *

*1) Clean variables *

*clean outliers of income components and then generate income aggregates from these 'clean' income components
	local varlist pcexp 
	foreach v of local varlist{
		recode `v' .=0 
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
	}

* 2) generate FTE shares:
ren fte_wage_f fte_wage_formal 
ren fte_wage_sp fte_wage_formal_sp

g sh_fte_formal = (fte_wage_formal/fte_total)
g sh_fte_formal_sp = (fte_wage_formal_sp/fte_total)
	*limit shares btw [0,1]:
foreach v in sh_fte_formal sh_fte_formal_sp {
		replace `v'=1 if `v'>1 & `v'!=.
		replace `v'=0 if `v'<0 & `v'!=.
		recode `v' .=0
	}

* 3) generate ccupation count share:

g sh_occ_formal = (occ_wage_f/total_jobs)
g sh_occ_formal_sp = (occ_wage_sp/total_jobs)
	
	*limit shares btw [0,1]:
foreach v in sh_occ_formal sh_occ_formal_sp {
		replace `v'=1 if `v'>1 & `v'!=.
		replace `v'=0 if `v'<0 & `v'!=.
		recode `v' .=0
	}

* 4) generate further variables:

*DEMOGRAPHICS:
g dep_ratio = dep_n/indep_n
g wa_share = indep_n/hhsize

*LOG transformations:
g log_pcexp = log(pcexp)
*Poverty:
g poor = (pcexp<1.9)

*land ownership
g land_d=(land_owned>0 & land_owned!=.)

*Geographic information:
replace dist_road=dist_road2 if dist_road==.
replace dist_popcenter=dist_popcenter2 if dist_popcenter==.
replace dist_borderpost=dist_border2 if dist_borderpost==.
drop dist_road2 dist_popcenter2 dist_border2

*terciles of distances:
local x 1
foreach v in dist_road dist_popcenter dist_market dist_admctr dist_borderpost{
	xtile terc_`v' = `v' , n(3)
	local ++x
}

*label all remaining variables
do "$DO1/CHEVAL_labels_nga.do"
	
order country ccode rural hhid_unique hhweight year rural zone state sector lga ea pcexp poor female_head age_head age_mean dep_ratio wa_share youth_share_24 total_active_youth school_secondary_hh educ_max N_inschool sh_school_secondary NEET NEET_34 sh_NEET sh_NEET_34 out_labor_mkt credit land_d land_owned fte_* occ_* sh_* total_*

** create balanced panel:
bys hhid: egen obs=count(hhid_unique)
keep if obs==3
	
*Correct consumption and poverty variables:
/*
World Bank: Poverty Reduction in Nigeria in the Last Decade 2016
http://documents1.worldbank.org/curated/en/103491483646246005/pdf/ACS19141-REVISED-PUBLIC-Pov-assessment-final.pdf

2004 poverty line of ₦28,830 a year

The poverty line is deflated temporally using the national consumer price index provided by the International Monetary Fund:

IMF: CPI, All items, 2009M11 as base category
2005M01 60.92 28,830 
2010M01 103.10 = 1.692383453709783 = 48,791.41497045304
2012M01 130.20 = 2.137229152987525 = 61,616.31648063035
2015M01 165.77 = 2.721109652002626 = 78,449.59126723572
*/

g poor_n=0
replace poor_n=1 if totcons<=(48791.41497045304) & year==2010
replace poor_n=1 if totcons<=(61616.31648063035) & year==2012
replace poor_n=1 if totcons<=(78449.59126723572) & year==2015

g pcexp_n=.
replace pcexp_n = totcons*1.692383453709783/365/hhsize if year==2010
replace pcexp_n = totcons*2.137229152987525/365/hhsize if year==2012
replace pcexp_n = totcons*2.721109652002626/365/hhsize if year==2015

	
save "$clean\NGA_panel", replace

exit
*
