
*** This do-file generates occupation dummies and FTEs for each occupation ***

* Name / Create Pooled Variables for FTE and Occupation Count
* Own Farming
gen fte_ownfarm = farm_f
gen occ_ownfarm = farm_o

* On-Farm AFS Wage Work
gen fte_wage = f_f0 + f_f1
gen occ_wage = o_f0 + o_f1
	*!EVA: formal on-farm wage work
gen fte_wage_f = f_f1
gen occ_wage_f = o_f1
gen fte_wage_sp = f_spf1
gen occ_wage_sp = o_spf1

* Enterprise
gen fte_enterprise = nfe_f
gen occ_enterprise = nfe_o

* Cap individual FTE at 2
gen fte_scale = 1
replace fte_scale = 2/fte_total if fte_total>2
foreach fv in fte_total fte_ownfarm fte_wage fte_enterprise fte_wage_f fte_wage_sp f_f0 f_f1 f_f2 f_f3{
	replace `fv' = `fv'*fte_scale
}


** Calculate Out of Labor Market by those without a job who are not unemployed
gen out_labor_mkt = 1
*replace out_labor_mkt = 0 if unemployed==1
replace out_labor_mkt = 0 if total_jobs>0
replace out_labor_mkt = 0 if age_tier==1
mvdecode out_labor_mkt, mv(0)
mvencode out_labor_mkt, mv(0)
gen occ_out = out_labor_mkt

* Calculate FTE of Out of Labor Market (!) EVA: I would delete this...useless
gen fte_out = 1 - fte_total
replace fte_out = 0 if age_tier==1

* Calculate share of econ active youth in econ active hh members
gen active_mem = 1 if fte_total>0
gen active_youth_24 = 1 if fte_total>0 & age_tier==2
replace active_youth_24 = 1 if fte_total>0 & age_tier==3
gen active_youth_34 = active_youth_24
replace active_youth_34 = 1 if fte_total>0 & age_tier==4
mvencode active*, mv(0)

* Calculate a NEET variable
gen NEET = out_labor_mkt
replace NEET = 0 if school_currently==1
replace NEET = 0 if age_tier==1
replace NEET = 0 if age_tier>=4

gen NEET_34 = out_labor_mkt
replace NEET_34 = 0 if school_currently==1
replace NEET_34 = 0 if age_tier==1
replace NEET_34 = 0 if age_tier>=5

foreach d in ownfarm wage enterprise wage_f wage_sp {
	gen d_`d' = 1 if occ_`d'>0
}
*
mvencode d_*, mv(0)

*DEMOGRAPHICS:
**!EVA: household level variables that require individual level information:
* !EVA: Dependency ratio 
g dep = (age>=0 & age<=14 | age>=65) if age!=.
g indep = (age>=15 & age<=64) if age!=.

* !EVA: Adult with at least secondary school 
g adult_school = (school_secondary==1  & age_tier>2 & age_tier!=.)
g adult = (age_tier>2 & age_tier!=.)

*!EVA: household head

*characteristics of household head (sex, age, education)

egen d_in = rmax (d_ownfarm d_wage d_enterprise )
g d_out = (d_in==0) //out of labor market

lab var d_ownfarm "Occupied, own farm"
lab var d_wage "Occupied, wage work"
lab var d_enterprise  "Occupied, own enterprise"
lab var adult_school "Adult individual with at least the secondary school"
label var adult "Adult individual ( age>=18)"

foreach v in f sp {
	g head_`v' = ( head==1 & d_wage_`v'==1)
}
lab var head_f "Head has formal wage job"
lab var head_sp "Head has formal wage job, social protection"

g age_head = age if head==1
	lab var age_head "Age of household head"
	
*keep only relevant variables:
keep hhid country occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep indep active_mem ///
		active_youth_24 school_currently out_labor_mkt school_secondary female_head age_head age head_* pension health_insurance 

ex
