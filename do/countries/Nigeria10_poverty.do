************ CHEVAL : DEPTH OF INEQUALITY *************
*******************************************************
*******NIGERIA 2010: GENERATE EXPENDITURE *************
*******************************************************

/*
This file gets the total household expenditure and defines households as poor.

The dataset used for this is:
Nigeria 2010 General Household Survey-Panel Wave 1

Stata Version 16.1
-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "$clean/NIGERIAIA10_expenditure.dta", replace

*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\NGA_2010_GHSP-W1_v03_M_STATA"

gl DO   	    ${NIGERIA}/do
gl raw   		${NIGERIA}/raw data 
gl clean 	"${NIGERIA}/clean data"
********************************************************************************

use "$raw/cons_agg_wave1_visit1.dta", clear

* Convert expenditures variables to  USD constant PPP
g pcexp= totcons 

foreach u of varlist pcexp  {
	* Currency conversion from XE.com 31/12/2010
	gen USD_`u' = `u'*0.0065789474
	* Constant 2011 PPP conversion using World Bank GNI Data 
	* Average of 2010 and 2011 GNI per capita constant 2011 PPP / Average of 2010 and 2011 GNI per capita Atlas Method [((4861.650345+4969.914953)/2) / ((1470+1730)/2)] (Source WDI)
	replace USD_`u' = USD_`u'*3.0723642  
	drop `u'
	ren USD_`u' `u'
	replace `u' = `u'/365 //Daily expenditure to later define poverty
	}

keep hhid pcexp totcons

merge 1:1 hhid using "$raw/Geodata/NGA_HouseholdGeovariables_Y1", nogen keep(1 2 3)

joinby using "$clean/Nigeria10_demographics.dta", unmatched(none)
g year=2010

save "$clean/NIGERIA10_expenditure.dta", replace

 