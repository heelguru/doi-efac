********************************************************************************
***************** TANZANIA 2014/2015: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Tanzania 2014/2015 National Panel Survey

-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
generates 
save "clean data/intermediate/Tanz1415_wage_occCHEVAL.dta", replace
save "clean data/TANZANIA_FTEsCHEVAL.dta", replace

Stata Version 15.1 

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl TANZANIA	"$path/data\Tanzania_NPS_1415"

gl DO   	    ${TANZANIA}/do
gl raw data   	${TANZANIA}/raw data 
gl clean data 	${TANZANIA}/clean data
gl FIG 		    ${TANZANIA}/graphs
gl TAB 	    	${TANZANIA}/tables
 
********************************************************************************
cd "$TANZANIA"


** Occupations in Non-Farm Own Enterprises
* First convert so that there is one observation per worker per NFE
forvalues w=1(1)6 {
use "raw data/hh_sec_n.dta", clear
rename hh_n19 nfe_months
rename hh_n02_3 nfe_CIC
format %40.0g nfe_CIC
rename hh_n20 nfe_avg_net_moinc
rename hh_n03_`w' indidy4

* Clean out extra data
drop if entid==.
drop if indidy==.

* Keep relevant data
keep y4_hhid indidy4 entid nfe_CIC nfe_months nfe_avg_net_moinc

gen worker = `w'

save "clean data/intermediate/Tanz1415_temp_nfe_`w'.dta", replace
}
*
use "clean data/intermediate/Tanz1415_temp_nfe_1.dta", clear
forvalues w=2(1)6 {
	append using "clean data/intermediate/Tanz1415_temp_nfe_`w'.dta"
}
*

* Clear Errors in data
* Replace zero/missing months active with 1 month active if income does not equal zero
drop if nfe_months==0 & nfe_avg_net_moinc==0
drop if nfe_months==. & nfe_avg_net_moinc==.
replace nfe_months = . if nfe_months==13
replace nfe_months = . if nfe_months==0
**** If reported income but not months, assume that this enterprise was operational.
joinby using "clean data/Tanz1415_member_demos.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC female rural region district ward village, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC female rural region district ward, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC female rural region district, sort : egen float count_4 = count(1) if nfe_months~=.
by nfe_CIC female rural region, sort : egen float count_5 = count(1) if nfe_months~=.
by nfe_CIC female rural, sort : egen float count_6 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC female rural region district ward village ea, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural region district ward village, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural region district ward, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural region district, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural region, sort : egen float temp_est_nfe_months_5 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_months_6 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural region district ward village ea, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC female rural region district ward village, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC female rural region district ward, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC female rural region district, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
by nfe_CIC female rural region, sort : egen float est_nfe_months_5 = max(temp_est_nfe_months_5)
by nfe_CIC female rural, sort : egen float est_nfe_months_6 = max(temp_est_nfe_months_6)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_5 if track==5
replace track = 6 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_6 if track==6
drop est_nfe_months_* count_* track
**** Replace missing value of nfe_months (11 changes)
replace nfe_months = est_nfe_months if nfe_months==.
replace nfe_months = 1 if nfe_months==.

********* Assume that when active in NFE that the individual worked full time.
gen fte_nfe = nfe_months/12

mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
replace fte_nfe = 2 if fte_nfe>2

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe~=0

mvdecode emp_nfe, mv(0)
mvencode emp_nfe, mv(0)

* Collapse to HH / CIC level
replace nfe_CIC = 99999 if nfe_CIC==.
collapse (sum) fte_nfe emp_nfe, by (y4_hhid indidy4 nfe_CIC)

* Bring in information for CIC Groupings
rename nfe_CIC CIC
joinby CIC using "clean data/Tanz1415_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Bring in Member Demographic Info
joinby y4_hhid indidy4 using "clean data/Tanz1415_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
* Currently use AFS Shares calculated with this method from Tanzania 10/11
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.579 if AFS_share==. & rural==1
replace AFS_share = 0.527 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*

forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612

replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612


* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (y4_hhid indidy4)

save "clean data/intermediate/Tanz1415_nfe_occ.dta", replace


** Occupation by Wages 
*** Variables hh_e64 hh_e66 capture hours worked in past week in unpaid NFE and HH ag. These values would be captured in other modules.
* Transpose data so that 2 Wage occupations are in one set of variables
use "raw data/hh_sec_e.dta", clear
* Rename variables
rename hh_e20_2 wage_tasco //added!
rename hh_e21_2 wage_CIC
rename hh_e26_1 wage_payment
rename hh_e26_2 wage_payment_duration
rename hh_e28_1 wage_payment_inkind
rename hh_e28_2 wage_payment_inkind_duration
rename hh_e29 wage_months
rename hh_e30 wage_weeks
rename hh_e31 wage_hours
rename hh_e34a w_contract
rename hh_e35_1 offer_materpater //added!
rename hh_e35_2 withold_taxes
rename hh_e35_3 health_insurance
rename hh_e62a tradeunion //added!
gen wage_job = 1
mvdecode wage_CIC wage_tasco wage_months wage_weeks wage_hours, mv(0) //
* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hours==.
order y4_hhid indidy4 wage_CIC wage_tasco wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_months wage_weeks wage_hours w_contract withold_taxes offer_materpater health_insurance wage_job tradeunion
keep y4_hhid indidy4 wage_CIC wage_tasco wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_months wage_weeks wage_hours w_contract withold_taxes offer_materpater health_insurance wage_job tradeunion
save "clean data/intermediate/Tanz1415_temp_wage_1.dta", replace
use "raw data/hh_sec_e.dta", clear
* Rename variables
rename hh_e39_2 wage_CIC
rename hh_e44_1 wage_payment
rename hh_e44_2 wage_payment_duration
rename hh_e46_1 wage_payment_inkind
rename hh_e46_2 wage_payment_inkind_duration
rename hh_e50 wage_hours
rename hh_e52a w_contract
gen wage_job = 2
mvdecode wage_CIC wage_hours, mv(0)
* Drop no data households
drop if wage_payment==. & wage_hours==.
order y4_hhid indidy4 wage_CIC wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_hours w_contract wage_job
keep y4_hhid indidy4 wage_CIC wage_payment wage_payment_duration wage_payment_inkind wage_payment_inkind_duration 	///
	wage_hours w_contract wage_job
save "clean data/intermediate/Tanz1415_temp_wage_2.dta", replace

use "clean data/intermediate/Tanz1415_temp_wage_1.dta", clear
append using "clean data/intermediate/Tanz1415_temp_wage_2.dta"
mvdecode wage_CIC wage_months wage_weeks wage_hours, mv(0)

* Clear Errors in data
*** If weeks is greater than 5, convert to missing (1 change)
replace wage_weeks = . if wage_weeks>5
*** Assume that reported hours per day above 16 hours per day are in error. (287)
replace wage_hours =. if wage_hours>80

**** If worked hours then assume to have worked weeks, and assume worked during months
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Tanz1415_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_6 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region district, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural region, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
by wage_CIC female rural, sort : egen float est_wage_months_6 = max(temp_est_wage_months_6)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
replace track = 6 if est_wage_months==.
replace est_wage_months = est_wage_months_6 if track==6
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (228 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_6 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region district, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural region, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
by wage_CIC female rural, sort : egen float est_wage_weeks_6 = max(temp_est_wage_weeks_6)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==5
replace track = 6 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_6 if track==6
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (228 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hours reported by geographic location.
by wage_CIC female rural region district ward village ea, sort : egen float count_1 = count(1) if wage_hours~=.
by wage_CIC female rural region district ward village, sort : egen float count_2 = count(1) if wage_hours~=.
by wage_CIC female rural region district ward, sort : egen float count_3 = count(1) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float count_4 = count(1) if wage_hours~=.
by wage_CIC female rural region, sort : egen float count_5 = count(1) if wage_hours~=.
by wage_CIC female rural, sort : egen float count_6 = count(1) if wage_hours~=.
* Calculate median of wage_hours by geographic location - later used for predictive purposes.
by wage_CIC female rural region district ward village ea, sort : egen float temp_est_wage_hours_1 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district ward village, sort : egen float temp_est_wage_hours_2 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district ward, sort : egen float temp_est_wage_hours_3 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_hours_4 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hours_5 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hours_6 = median(wage_hours) if wage_hours~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district ward village ea, sort : egen float est_wage_hours_1 = max(temp_est_wage_hours_1)
by wage_CIC female rural region district ward village, sort : egen float est_wage_hours_2 = max(temp_est_wage_hours_2)
by wage_CIC female rural region district ward, sort : egen float est_wage_hours_3 = max(temp_est_wage_hours_3)
by wage_CIC female rural region district, sort : egen float est_wage_hours_4 = max(temp_est_wage_hours_4)
by wage_CIC female rural region, sort : egen float est_wage_hours_5 = max(temp_est_wage_hours_5)
by wage_CIC female rural, sort : egen float est_wage_hours_6 = max(temp_est_wage_hours_6)
drop temp*
* Build wage_hours prediction variable if at least 10 observations of reported wage_hours in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hours = est_wage_hours_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==5
replace track = 6 if est_wage_hours==.
replace est_wage_hours = est_wage_hours_6 if track==6
drop est_wage_hours_* count_* track
**** Replace missing value of wage_hours (440 changes)
replace wage_hours = est_wage_hours if wage_hours==.
replace wage_hours = 1 if wage_hours==.

* Casual vs Formal Wage
gen formal = 0
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if withold_taxes==1
replace formal = 1 if health_insurance==1
replace formal = 1 if offer_materpater==1 

gen formalcon = 0 //added definition of "formality" due to contract structure 
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP
replace formalsp = 1 if withold_taxes==1
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if offer_materpater==1 

* Generate Full Time Equivalents
gen fte_wage = wage_months*wage_weeks*wage_hours/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2

keep y4_hhid indidy4 wage_CIC formal formalcon formalsp fte_wage health_insurance 
recode health_insurance (2=0)
* Bring in Member Demographic Info
joinby y4_hhid indidy4 using "clean data/Tanz1415_member_demos.dta", unmatched(none)

* Demographic FTEs
forvalues f=0(1)1 {
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*
* Simple count of Wages earned.
forvalues f=0(1)1 {
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

** (!)inf2= the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
*
* Collapse to HH / CIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal* (max) health_insurance, by (y4_hhid indidy4 wage_CIC) 

* Bring in information for CIC Groupings
rename wage_CIC CIC
joinby CIC using "clean data/Tanz1415_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby y4_hhid indidy4 using "clean data/Tanz1415_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
* Currently use AFS Shares calculated with this method from Tanzania 10/11
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
	replace AFS_share = 0.579 if AFS_share==. & rural==1 
	replace AFS_share = 0.527 if AFS_share==. & rural==0

** Generate Categorical Variables
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}

*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*
* Four mixed CIC Groups (!CE copypaste Malawi loop)
forvalues f=0(1)1 {
forvalues w=1(1)2 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
}

collapse (sum) w_o_* w_f_* (max) health_insurance, by (y4_hhid indidy4)

save "clean data/intermediate/Tanz1415_wage_occCHEVAL.dta", replace


** Own Farm Employment
* Report one worker per observation
* Long Season
* Prep
forvalues p=1(1)6 {
use "raw data/ag_sec_3a.dta", clear
rename ag3a_72_id`p' indidy4
rename ag3a_72_`p' farm_prep_days
rename ag3a_72_25 farm_prep_hourspd
keep y4_hhid indidy4 farm_prep_days farm_prep_hourspd
drop if indidy4==. & farm_prep_days==.
drop if farm_prep_days==0
gen farm_prep = `p'
save "clean data/intermediate/Tanz1415_temp_farm_long_prep_`p'.dta", replace
}
*
* Grow
forvalues p=7(1)12 {
use "raw data/ag_sec_3a.dta", clear
rename ag3a_72_id`p' indidy4
rename ag3a_72_`p' farm_grow_days
rename ag3a_72_26 farm_grow_hourspd
keep y4_hhid indidy4 farm_grow_days farm_grow_hourspd
drop if indidy4==. & farm_grow_days==.
drop if farm_grow_days==0
gen farm_grow = `p'
save "clean data/intermediate/Tanz1415_temp_farm_long_grow_`p'.dta", replace
}
*
* Harvest
forvalues p=13(1)18 {
use "raw data/ag_sec_3a.dta", clear
rename ag3a_72_id`p' indidy4
rename ag3a_72_`p' farm_harvest_days
rename ag3a_72_28 farm_harvest_hourspd
keep y4_hhid indidy4 farm_harvest_days farm_harvest_hourspd
drop if indidy4==. & farm_harvest_days==.
drop if farm_harvest_days==0
gen farm_harvest = `p'
save "clean data/intermediate/Tanz1415_temp_farm_long_harvest_`p'.dta", replace
}
*

use "clean data/intermediate/Tanz1415_temp_farm_long_prep_1.dta", clear
forvalues p=2(1)6 {
append using "clean data/intermediate/Tanz1415_temp_farm_long_prep_`p'.dta"
}
forvalues p=7(1)12 {
append using "clean data/intermediate/Tanz1415_temp_farm_long_grow_`p'.dta"
}
forvalues p=13(1)18 {
append using "clean data/intermediate/Tanz1415_temp_farm_long_harvest_`p'.dta"
}
*

***
* Correct error in data
*** if worked during a day, the typical day cannot be 0. (10 changes)
mvdecode farm_*, mv(0)
*** Cannot average more than 16 hours a day (1 change)
replace farm_harvest_hourspd = . if farm_harvest_hourspd>16
* Assume if people work one hour, they worked at least one day, and they worked at least one week...
joinby using "clean data/Tanz1415_member_demos.dta", unmatched(none)
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_days reported by geographic location.
	by rural female region district ward village ea, sort : egen float count_1 = count(1) if farm_`f'_days~=.
	by rural female region district ward village, sort : egen float count_2 = count(1) if farm_`f'_days~=.
	by rural female region district ward, sort : egen float count_3 = count(1) if farm_`f'_days~=.
	by rural female region district, sort : egen float count_4 = count(1) if farm_`f'_days~=.
	by rural female region, sort : egen float count_5 = count(1) if farm_`f'_days~=.
	by rural female, sort : egen float count_6 = count(1) if farm_`f'_days~=.
	* Calculate median of farm_`f'_days by geographic location - later used for predictive purposes.
	by rural female region district ward village ea, sort : egen float temp_est_farm_`f'_days_1 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district ward village, sort : egen float temp_est_farm_`f'_days_2 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district ward, sort : egen float temp_est_farm_`f'_days_3 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district, sort : egen float temp_est_farm_`f'_days_4 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region, sort : egen float temp_est_farm_`f'_days_5 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female, sort : egen float temp_est_farm_`f'_days_6 = median(farm_`f'_days) if farm_`f'_days~=.
	mvencode temp_*, mv(0)
	by rural female region district ward village ea, sort : egen float est_farm_`f'_days_1 = max(temp_est_farm_`f'_days_1)
	by rural female region district ward village, sort : egen float est_farm_`f'_days_2 = max(temp_est_farm_`f'_days_2)
	by rural female district ward, sort : egen float est_farm_`f'_days_3 = max(temp_est_farm_`f'_days_3)
	by rural female district, sort : egen float est_farm_`f'_days_4 = max(temp_est_farm_`f'_days_4)
	by rural female, sort : egen float est_farm_`f'_days_5 = max(temp_est_farm_`f'_days_5)
	by rural female, sort : egen float est_farm_`f'_days_6 = max(temp_est_farm_`f'_days_6)
	drop temp*
	* Build farm_`f'_days prediction variable if at least 10 observations of reported farm_`f'_days in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_days = est_farm_`f'_days_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_3 if track==3
	replace track = 4 if count_4>=10 & count_4<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_4 if track==4
	replace track = 5 if count_5>=10 & count_5<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_5 if track==5
	replace track = 6 if est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_6 if track==6
	drop est_farm_`f'_days_* count_* track
	* Replace missing value of farm_`f'_days (10, 9, 8 changes)
	replace farm_`f'_days = est_farm_`f'_days if farm_`f'_days==. & farm_`f'~=.
	replace farm_`f'_days = 1 if farm_`f'_days==. & farm_`f'~=.
}
*
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_hourspd reported by geographic location.
	by rural female region district ward village ea, sort : egen float count_1 = count(1) if farm_`f'_hourspd~=.
	by rural female region district ward village, sort : egen float count_2 = count(1) if farm_`f'_hourspd~=.
	by rural female region district ward, sort : egen float count_3 = count(1) if farm_`f'_hourspd~=.
	by rural female region district, sort : egen float count_4 = count(1) if farm_`f'_hourspd~=.
	by rural female region, sort : egen float count_5 = count(1) if farm_`f'_hourspd~=.
	by rural female, sort : egen float count_6 = count(1) if farm_`f'_hourspd~=.
	* Calculate median of farm_`f'_hourspd by geographic location - later used for predictive purposes.
	by rural female region district ward village ea, sort : egen float temp_est_farm_`f'_hourspd_1 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district ward village, sort : egen float temp_est_farm_`f'_hourspd_2 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district ward, sort : egen float temp_est_farm_`f'_hourspd_3 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district, sort : egen float temp_est_farm_`f'_hourspd_4 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region, sort : egen float temp_est_farm_`f'_hourspd_5 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female, sort : egen float temp_est_farm_`f'_hourspd_6 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	mvencode temp_*, mv(0)
	by rural female region district ward village ea, sort : egen float est_farm_`f'_hourspd_1 = max(temp_est_farm_`f'_hourspd_1)
	by rural female region district ward village, sort : egen float est_farm_`f'_hourspd_2 = max(temp_est_farm_`f'_hourspd_2)
	by rural female region district ward, sort : egen float est_farm_`f'_hourspd_3 = max(temp_est_farm_`f'_hourspd_3)
	by rural female region district, sort : egen float est_farm_`f'_hourspd_4 = max(temp_est_farm_`f'_hourspd_4)
	by rural female region, sort : egen float est_farm_`f'_hourspd_5 = max(temp_est_farm_`f'_hourspd_5)
	by rural female, sort : egen float est_farm_`f'_hourspd_6 = max(temp_est_farm_`f'_hourspd_6)
	drop temp*
	* Build farm_`f'_hourspd prediction variable if at least 10 observations of reported farm_`f'_hourspd in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_hourspd = est_farm_`f'_hourspd_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_3 if track==3
	replace track = 4 if count_4>=10 & count_4<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_4 if track==4
	replace track = 5 if count_5>=10 & count_5<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_5 if track==5
	replace track = 6 if est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_6 if track==6
	drop est_farm_`f'_hourspd_* count_* track
	* Replace missing value of farm_`f'_hourspd (4, 8, 8 changes)
	replace farm_`f'_hourspd = est_farm_`f'_hourspd if farm_`f'_hourspd==. & farm_`f'~=.
	replace farm_`f'_hourspd = 1 if farm_`f'_hourspd==. & farm_`f'~=.
}
*
mvdecode farm_*, mv(0)
mvencode farm_*, mv(0)

* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	gen farm_`f'_hours = 0
}
*
* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	replace farm_`f'_hours = farm_`f'_days*farm_`f'_hourspd
}
*

* Collapse to HH Member
collapse (sum) farm_prep_hours farm_grow_hours farm_harvest_hours, by (y4_hhid indidy4)

* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_prep_hours>0 | farm_grow_hours>0 | farm_harvest_hours>0

* Total farm hours by member
gen farm_hours = farm_prep_hours + farm_grow_hours + farm_harvest_hours

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
*

keep y4_hhid indidy4 farm_worker farm_fte

* Indicate that this is Long Season
rename farm_worker farm_L_work
rename farm_fte farm_L_fte

save "clean data/intermediate/Tanz1415_farm_long_occ.dta", replace


* Short Season
* Prep
forvalues p=1(1)6 {
use "raw data/ag_sec_3b.dta", clear
rename ag3b_72_id`p' indidy4
rename ag3b_72_`p' farm_prep_days
rename ag3b_72_25 farm_prep_hourspd
keep y4_hhid indidy4 farm_prep_days farm_prep_hourspd
drop if indidy4==. & farm_prep_days==.
drop if farm_prep_days==0
gen farm_prep = `p'
save "clean data/intermediate/Tanz1415_temp_farm_short_prep_`p'.dta", replace
}
*
* Grow
forvalues p=7(1)12 {
use "raw data/ag_sec_3b.dta", clear
rename ag3b_72_id`p' indidy4
rename ag3b_72_`p' farm_grow_days
rename ag3b_72_26 farm_grow_hourspd
keep y4_hhid indidy4 farm_grow_days farm_grow_hourspd
drop if indidy4==. & farm_grow_days==.
drop if farm_grow_days==0
gen farm_grow = `p'
save "clean data/intermediate/Tanz1415_temp_farm_short_grow_`p'.dta", replace
}
*
* Harvest
forvalues p=13(1)18 {
use "raw data/ag_sec_3b.dta", clear
rename ag3b_72_id`p' indidy4
rename ag3b_72_`p' farm_harvest_days
rename ag3b_72_28 farm_harvest_hourspd
keep y4_hhid indidy4 farm_harvest_days farm_harvest_hourspd
drop if indidy4==. & farm_harvest_days==.
drop if farm_harvest_days==0
gen farm_harvest = `p'
save "clean data/intermediate/Tanz1415_temp_farm_short_harvest_`p'.dta", replace
}
*

use "clean data/intermediate/Tanz1415_temp_farm_short_prep_1.dta", clear
forvalues p=2(1)6 {
append using "clean data/intermediate/Tanz1415_temp_farm_short_prep_`p'.dta"
}
forvalues p=7(1)12 {
append using "clean data/intermediate/Tanz1415_temp_farm_short_grow_`p'.dta"
}
forvalues p=13(1)18 {
append using "clean data/intermediate/Tanz1415_temp_farm_short_harvest_`p'.dta"
}
*

***
* Correct error in data
*** if worked during a day, the typical day cannot be 0. (7 changes)
mvdecode farm_*, mv(0)
*** Cannot average more than 16 hours a day (2 changes)
replace farm_harvest_hourspd = . if farm_harvest_hourspd>16 
* Assume if people work one hour, they worked at least one day, and they worked at least one week...
joinby using "clean data/Tanz1415_member_demos.dta", unmatched(none)
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_days reported by geographic location.
	by rural female region district ward village ea, sort : egen float count_1 = count(1) if farm_`f'_days~=.
	by rural female region district ward village, sort : egen float count_2 = count(1) if farm_`f'_days~=.
	by rural female region district ward, sort : egen float count_3 = count(1) if farm_`f'_days~=.
	by rural female region district, sort : egen float count_4 = count(1) if farm_`f'_days~=.
	by rural female region, sort : egen float count_5 = count(1) if farm_`f'_days~=.
	by female rural, sort : egen float count_6 = count(1) if farm_`f'_days~=.
	* Calculate median of farm_`f'_days by geographic location - later used for predictive purposes.
	by rural female region district ward village ea, sort : egen float temp_est_farm_`f'_days_1 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district ward village, sort : egen float temp_est_farm_`f'_days_2 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district ward, sort : egen float temp_est_farm_`f'_days_3 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region district, sort : egen float temp_est_farm_`f'_days_4 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural female region, sort : egen float temp_est_farm_`f'_days_5 = median(farm_`f'_days) if farm_`f'_days~=.
	by female rural, sort : egen float temp_est_farm_`f'_days_6 = median(farm_`f'_days) if farm_`f'_days~=.
	mvencode temp_*, mv(0)
	by rural female region district ward village ea, sort : egen float est_farm_`f'_days_1 = max(temp_est_farm_`f'_days_1)
	by rural female region district ward village, sort : egen float est_farm_`f'_days_2 = max(temp_est_farm_`f'_days_2)
	by rural female region district ward, sort : egen float est_farm_`f'_days_3 = max(temp_est_farm_`f'_days_3)
	by rural female region district, sort : egen float est_farm_`f'_days_4 = max(temp_est_farm_`f'_days_4)
	by rural female region, sort : egen float est_farm_`f'_days_5 = max(temp_est_farm_`f'_days_5)
	by female rural, sort : egen float est_farm_`f'_days_6 = max(temp_est_farm_`f'_days_6)
	drop temp*
	* Build farm_`f'_days prediction variable if at least 10 observations of reported farm_`f'_days in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_days = est_farm_`f'_days_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_3 if track==3
	replace track = 4 if count_4>=10 & count_4<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_4 if track==4
	replace track = 5 if count_5>=10 & count_5<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_5 if track==5
	replace track = 6 if est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_6 if track==6
	drop est_farm_`f'_days_* count_* track
	* Replace missing value of farm_`f'_days (3, 2, 5 changes)
	replace farm_`f'_days = est_farm_`f'_days if farm_`f'_days==. & farm_`f'~=.
	replace farm_`f'_days = 1 if farm_`f'_days==. & farm_`f'~=.
}
*
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_hourspd reported by geographic location.
	by rural female region district ward village ea, sort : egen float count_1 = count(1) if farm_`f'_hourspd~=.
	by rural female region district ward village, sort : egen float count_2 = count(1) if farm_`f'_hourspd~=.
	by rural female region district ward, sort : egen float count_3 = count(1) if farm_`f'_hourspd~=.
	by rural female region district, sort : egen float count_4 = count(1) if farm_`f'_hourspd~=.
	by rural female region, sort : egen float count_5 = count(1) if farm_`f'_hourspd~=.
	by female rural, sort : egen float count_6 = count(1) if farm_`f'_hourspd~=.
	* Calculate median of farm_`f'_hourspd by geographic location - later used for predictive purposes.
	by rural female region district ward village ea, sort : egen float temp_est_farm_`f'_hourspd_1 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district ward village, sort : egen float temp_est_farm_`f'_hourspd_2 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district ward, sort : egen float temp_est_farm_`f'_hourspd_3 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region district, sort : egen float temp_est_farm_`f'_hourspd_4 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural female region, sort : egen float temp_est_farm_`f'_hourspd_5 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by female rural, sort : egen float temp_est_farm_`f'_hourspd_6 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	mvencode temp_*, mv(0)
	by rural female region district ward village ea, sort : egen float est_farm_`f'_hourspd_1 = max(temp_est_farm_`f'_hourspd_1)
	by rural female region district ward village, sort : egen float est_farm_`f'_hourspd_2 = max(temp_est_farm_`f'_hourspd_2)
	by rural female region district ward, sort : egen float est_farm_`f'_hourspd_3 = max(temp_est_farm_`f'_hourspd_3)
	by rural female region district, sort : egen float est_farm_`f'_hourspd_4 = max(temp_est_farm_`f'_hourspd_4)
	by rural female region, sort : egen float est_farm_`f'_hourspd_5 = max(temp_est_farm_`f'_hourspd_5)
	by female rural, sort : egen float est_farm_`f'_hourspd_6 = max(temp_est_farm_`f'_hourspd_6)
	drop temp*
	* Build farm_`f'_hourspd prediction variable if at least 10 observations of reported farm_`f'_hourspd in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_hourspd = est_farm_`f'_hourspd_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_3 if track==3
	replace track = 4 if count_4>=10 & count_4<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_4 if track==4
	replace track = 5 if count_5>=10 & count_5<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_5 if track==5
	replace track = 6 if est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_6 if track==6
	drop est_farm_`f'_hourspd_* count_* track
	* Replace missing value of farm_`f'_hourspd (4, 6, 11 changes)
	replace farm_`f'_hourspd = est_farm_`f'_hourspd if farm_`f'_hourspd==. & farm_`f'~=.
	replace farm_`f'_hourspd = 1 if farm_`f'_hourspd==. & farm_`f'~=.
}
*
mvdecode farm_*, mv(0)
mvencode farm_*, mv(0)

* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	gen farm_`f'_hours = 0
}
*
* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	replace farm_`f'_hours = farm_`f'_days*farm_`f'_hourspd
}
*

* Collapse to HH Member
collapse (sum) farm_prep_hours farm_grow_hours farm_harvest_hours, by (y4_hhid indidy4)

* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_prep_hours>0 | farm_grow_hours>0 | farm_harvest_hours>0

* Total farm hours by member
gen farm_hours = farm_prep_hours + farm_grow_hours + farm_harvest_hours

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
*

keep y4_hhid indidy4 farm_worker farm_fte

* Indicate that this is short Season
rename farm_worker farm_S_work
rename farm_fte farm_S_fte

save "clean data/intermediate/Tanz1415_farm_short_occ.dta", replace

* Combine Long and Short Farming
use "clean data/intermediate/Tanz1415_farm_long_occ.dta", clear
joinby y4_hhid indidy4 using "clean data/intermediate/Tanz1415_farm_short_occ.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode farm*, mv(0)
mvencode farm*, mv(0)

* Combine Long & Short Seasons
gen farm_o_c1 = 1 if farm_L_work>0 | farm_S_work>0
gen farm_f_c1 = farm_L_fte + farm_S_fte

mvdecode farm*, mv(0)
mvencode farm*, mv(0)
replace farm_f_c1 = 2 if farm_f_c1>2

* Collapse to HH
collapse (sum) farm_o_c1 farm_f_c1, by (y4_hhid indidy4)

* All Own Farming is 100% AFS and self employed labor

* Keep Farming Data at the household member level
keep y4_hhid indidy4 farm_o_c1 farm_f_c1

save "clean data/intermediate/Tanz1415_farm_occ.dta", replace


** Permanent Crops
* No Labor Information on Permanent Crops
* other than who negotiates price, and who decided what to do with revenue

** Processing of Crops
* No Labor Information on Processing of Crops
* Assume this is included in NFE if conducted as employment

**** COMBINE ALL ****

use "clean data/intermediate/Tanz1415_nfe_occ.dta", clear
joinby y4_hhid indidy4 using "clean data/intermediate/Tanz1415_wage_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby y4_hhid indidy4 using "clean data/intermediate/Tanz1415_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Demographics
joinby y4_hhid indidy4 using "clean data/Tanz1415_member_demos.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode nfe* w* farm*, mv(0)
mvencode nfe* w* farm*, mv(0)

** Generate Grouped Categorical Variables (!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*
*(!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}
*
*non-farm enterprise
forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}
*

*farm enterprise
replace o_f2_c1 = o_f2_c1 + farm_o_c1
replace f_f2_c1 = f_f2_c1 + farm_f_c1

* Total (f3) Casual, Formal and Self Employed
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f3_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	

gen rur_farm = 1 if rural==1 & o_f2_c1>0
replace rur_farm = 0 if rur_farm==.
by y4_hhid, sort : egen float rural_farm = max(rur_farm)


joinby using "clean data/Tanz1415_iid.dta", unmatched(none)

keep hhid iid fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete school_secondary school_level health_insurance
g pension=.
gen country="Tanzania"
save "clean data/TANZANIA_FTEsCHEVAL.dta", replace

