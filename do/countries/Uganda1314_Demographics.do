********************************************************************************
*********************** UGANDA 2013/2014: DEMOGRAPHICS ************************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Uganda 2013/2014 National Panel Survey

The resulting data set is "Uganda1314_demographics"

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl UGANDA	"$path/data/Uganda_NPS_1314"

gl DO   	    ${UGANDA}/do
gl raw data   	${UGANDA}/raw data 
gl clean data 	${UGANDA}/clean data
gl FIG 	    	${UGANDA}/graphs
gl TAB 		    ${UGANDA}/tables
 
********************************************************************************
cd "$UGANDA"


* Location
use "raw data/GSEC1.dta", clear
rename sregion subregion
rename h1aq1a district
rename h1aq3a subcounty
rename h1aq4a parish
rename wgt_X hhweight
rename wgt panel_hhweight
replace panel_hhweight = 0 if panel_hhweight==.
gen rural = 0
replace rural = 1 if urban==0
order HHID rural region subregion district subcounty parish ea hhweight panel_hhweight
keep HHID rural region subregion district subcounty parish ea hhweight panel_hhweight
joinby HHID using "clean data/Uganda1314_HHID_String_Long.dta", unmatched(none)

save "clean data/Uganda1314_location.dta", replace

** Household Member Demographic information
use "raw data/GSEC2.dta", clear
gen female = h2q3-1
gen head = 0
replace head = 1 if h2q4==1
gen female_head = 0
replace female_head = 1 if head==1 & female==1

/* Age Tiers
1 = 0-14
2 = 15-17
3 = 18-24
4 = 25-34
5 = 35-64
6 = 65+
*/
rename h2q8 age
* Many newborn children have missing age, but those are 0.
replace age = 0 if age==. & h2q6==1
replace age = 0 if age==. & h2q9c==2014
* two missing ages, but one is a spouse (assume same age as spouse) and other has a year born of 201_ (give age as 5, as this would be under 15)
replace age = 5 if age==. & h2q9c==201
replace age = 34 if age==. & HHID=="H32909-04-01"
gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth youth_34, mv(0)

keep HHID PID age age_tier female head female_head youth youth_34
order HHID PID age age_tier female head female_head youth youth_34

* Bring in Education
joinby HHID PID using "raw data/GSEC4.dta", unmatched(both) _merge(_merge)

gen school_read = 1 if h4q4==2 | h4q4==4
gen school_any = 1 if h4q5==2 | h4q5==3
gen school_currently = 1 if h4q5==3
gen school_complete = 1 if h4q7>=17 & h4q7~=99 & h4q7~=.
gen school_secondary = 1 if h4q7>=34 & h4q7~=99 & h4q7~=.
gen school_level = h4q7
replace school_level = . if h4q7==99
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep HHID PID age age_tier female head female_head youth youth_34 school_*

joinby HHID using "clean data/Uganda1314_location.dta", unmatched(both) _merge(_merge)
drop _merge

* HH Size
by HHID, sort : egen float hhsize = count(HHID)

save "clean data/Uganda1314_member_demos.dta", replace

** Credit
* Credit in time of crisis
use "raw data/GSEC16.dta", clear
gen credit = 0
replace credit = 1 if h16q4a==9
replace credit = 1 if h16q4b==9
replace credit = 1 if h16q4c==9
collapse (max) credit, by (HHID)
save "clean data/intermediate/Uganda1314_temp_credit.dta", replace

* Credit for HH Enterprise (NO mention of credit in agriclture)
use "raw data/gsec12.dta", clear
gen credit = 0
replace credit = 1 if h12q9==1
collapse (max) credit, by (hhid)
rename hhid HHID

append using "clean data/intermediate/Uganda1314_temp_credit.dta"
collapse (max) credit, by (HHID)

save "clean data/intermediate/Uganda1314_credit.dta", replace

** Land 
* Area measurements
use "raw data/AGSEC2A.dta", clear

* Identify as owned if: 1) purchased, 2) inheritance / gifted 
gen land = 0
replace land = 1 if a2aq8==1 | a2aq8==2 

* Use area measurements calculated by GPS first and farmer estimate second
gen hectares = .
replace hectares = a2aq4 if a2aq4~=. & a2aq4~=0 & hectares==.
replace hectares = a2aq5 if a2aq5~=. & a2aq5~=0 & hectares==.
replace hectares = hectares * 0.404686

keep if land==1

collapse (sum) hectares (max) land, by (HHID)
mvdecode hectares, mv(0)

rename HHID ag_HHID

drop land
rename hectares land_owned

save "clean data/intermediate/Uganda1314_land.dta", replace

* HH demographics
use "clean data/Uganda1314_member_demos.dta", clear
* Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
mvencode youth_share_*, mv(0)

collapse (sum) youth_share_* (max) female_head school_secondary school_level (first) rural region subregion subcounty parish ea hhweight panel_hhweight ag_HHID hhsize , by (HHID )

replace youth_share_24 = youth_share_24/hhsize
replace youth_share_34 = youth_share_34/hhsize
rename school_secondary school_secondary_hh
rename school_level educ_max

joinby HHID using "clean data/intermediate/Uganda1314_credit.dta", unmatched(both) _merge(_merge)
drop _merge

joinby ag_HHID using "clean data/intermediate/Uganda1314_land.dta", unmatched(both) _merge(_merge)
mvencode land_owned, mv(0)
drop if _merge==2
drop _merge

gen popweight = hhweight*hhsize
gen panel_popweight = panel_hhweight*hhsize

save "clean data/Uganda1314_demographics.dta", replace

** Build hhid & iid
use "clean data/Uganda1314_member_demos.dta", clear
keep HHID PID

split HHID, p(H)
split HHID2, p(-)
split PID, p(P)
split PID2, p(-)

gen hhid = HHID21+HHID22+HHID23
gen iid = PID21+PID22

keep HHID PID hhid iid

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Uganda1314_iid.dta", replace
gen a = 1
collapse (min) a, by (HHID hhid)
drop a
save "clean data/Uganda1314_hhid.dta", replace

