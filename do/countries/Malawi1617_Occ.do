********************************************************************************
***************** MALAWI 2016/2017: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Malawi 2016/2017 Fourth Integrated Household Survey

generates 
save "$CLEAN\MALAWI_FTEsCHEVAL.dta", replace

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl MALAWI	"$path\data\Malawi_IHS_1617"

gl DO   	    ${MALAWI}/do
gl raw data     ${MALAWI}/raw data 
gl clean data 	${MALAWI}/clean data
gl FIG 		    ${MALAWI}/graphs
gl TAB 		    ${MALAWI}/tables
 
********************************************************************************
cd "$MALAWI"


** Occupations in Non-Farm Own Enterprises
* First convert so that there is one observation per worker per NFE

forvalues w=1(1)6 {
use "raw data/HH_MOD_N2.dta", clear
rename hh_n09c nfe_CIC
format %40.0g nfe_CIC
rename hh_n09a entid
forvalues n=1(1)6 {
	rename hh_n30a`n' nfe_PID_`n'
	rename hh_n30b`n' nfe_days_`n'
	rename hh_n30c`n' nfe_hourspd_`n'
	rename hh_n30d`n' nfe_months_`n'
}
*

* Keep relevant data
keep case_id entid nfe_CIC nfe_PID_`w' nfe_days_`w' nfe_hourspd_`w' nfe_months_`w'

rename nfe_PID_`w' PID
rename nfe_days_`w' nfe_days
rename nfe_hourspd_`w' nfe_hourspd
rename nfe_months_`w' nfe_months

replace nfe_days = . if nfe_days==0
replace nfe_hourspd = . if nfe_hourspd==0
replace nfe_months = . if nfe_months==0

drop if nfe_days==. & nfe_hourspd==. & nfe_months==.

gen worker = `w'

save "clean data/intermediate/Malawi1617_temp_nfe_`w'.dta", replace
}
*

use "clean data/intermediate/Malawi1617_temp_nfe_1.dta", clear
append using "clean data/intermediate/Malawi1617_temp_nfe_2.dta"
append using "clean data/intermediate/Malawi1617_temp_nfe_3.dta"
append using "clean data/intermediate/Malawi1617_temp_nfe_4.dta"
append using "clean data/intermediate/Malawi1617_temp_nfe_5.dta"
append using "clean data/intermediate/Malawi1617_temp_nfe_6.dta"

* Bring in Member Demographic Info
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(none)

*** Correct errors in data
drop if nfe_months<0
**** Assume that people do not average more than 16 hours per day (125)
replace nfe_hourspd = . if nfe_hourspd>16
**** If worked within the past week, must have worked within the past month, etc
**** Use median of months in nfe job of those in the smallest geographic with 10 observations of monthly nfe labor
joinby using "clean data/Malawi1617_member_demos.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC female rural district TA ea, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC female rural district TA, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC female rural district, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC female rural, sort : egen float count_4 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC female rural district TA ea, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural district TA, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural district, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural district TA ea, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC female rural district TA, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC female rural district, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC female rural, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
drop est_nfe_months_* count_*
**** Replace missing value of nfe_months (18 changes)
replace nfe_months = est_nfe_months if nfe_months==. & nfe_CIC~=.
replace nfe_months = 1 if nfe_months==. & nfe_CIC~=.

gen nfe_hours = nfe_days*nfe_hourspd*nfe_months

* Gen FTE
gen fte_nfe = nfe_hours/2016
mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
replace fte_nfe = 2 if fte_nfe>2

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe~=0

mvdecode emp_nfe, mv(0)
mvencode emp_nfe, mv(0)

* Collapse to HH / CIC level
replace nfe_CIC = 99999 if nfe_CIC==0
collapse (sum) fte_nfe emp_nfe, by (case_id PID nfe_CIC)

* Bring in information for CIC Groupings
rename nfe_CIC CIC
joinby CIC using "clean data/Malawi1617_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace ISIC_group = 612 if ISIC_group==.
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.644 if AFS_share==. & rural==1
replace AFS_share = 0.540 if AFS_share==. & rural==0

** Generate Categorical Variables

forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*

forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*

* Four mixed CIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612
replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612

* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (case_id PID)

save "clean data/intermediate/Malawi1617_nfe_occ.dta", replace


** Occupation by Wages 
* Transpose data so that 2 Wage occupations are in one set of variables
use "raw data/HH_MOD_E.dta", clear

**adding ANY emp type performed in last 12 months:
recode hh_e06_1a hh_e06_1b hh_e06_1c hh_e06_2 hh_e06_3 hh_e06_4 hh_e06_5 hh_e06_6 (2=0) //cleaning emp status over last 12 months 

* Rename variables
rename hh_e20b wage_CIC
rename hh_e25 wage_payment
rename hh_e26a wage_payment_duration
rename hh_e26b wage_payment_duration_unit
rename hh_e27 wage_inkind
rename hh_e28a wage_inkind_duration
rename hh_e28b wage_inkind_duration_unit
rename hh_e22 wage_months
rename hh_e23 wage_weeks
rename hh_e24 wage_hours
rename hh_e21_2 w_contract
rename hh_e21_3 pension
rename hh_e21_4 health_insurance
order case_id PID wage_* w_contract pension health_insurance
keep case_id PID wage_* w_contract pension health_insurance
gen worker = 1
save "clean data/intermediate/Malawi1617_temp_wage_a.dta", replace
use "raw data/HH_MOD_E.dta", clear
* Rename variables
rename hh_e34_code wage_CIC
rename hh_e35_2 w_contract
rename hh_e35_3 pension
rename hh_e35_4 health_insurance
rename hh_e36 wage_months
rename hh_e37 wage_weeks
rename hh_e38 wage_hours
rename hh_e39 wage_payment
rename hh_e40a wage_payment_duration
rename hh_e40b wage_payment_duration_unit
rename hh_e41 wage_inkind
rename hh_e42a wage_inkind_duration
rename hh_e42b wage_inkind_duration_unit
destring wage_payment_duration wage_inkind, replace
order case_id PID wage_* w_contract pension health_insurance
keep case_id PID wage_* w_contract pension health_insurance
gen worker = 2
save "clean data/intermediate/Malawi1617_temp_wage_b.dta", replace

use "clean data/intermediate/Malawi1617_temp_wage_a.dta", clear
append using "clean data/intermediate/Malawi1617_temp_wage_b.dta"

mvdecode wage_CIC wage_months wage_weeks wage_hours, mv(0)
drop if wage_CIC==. & wage_months==. & wage_weeks==. & wage_hours==.

* Clear Errors in data
* No occupation code, but reported earnings. (252 changes)
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=. | wage_CIC==. & wage_months~=. | wage_CIC==. & wage_weeks~=. | wage_CIC==. & wage_hours~=.
* Cannot work more than 5 weeks per month (1 change)
replace wage_weeks = . if wage_weeks>5 & wage_weeks~=.
**** Assume that reported hours per day above 16 hours per day are in error, so adjust to 16 hours per day. (affects calculations of 30 observations)
replace wage_hours = . if wage_hours>80
**** If worked within the past week, must have worked within the past month, etc
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Malawi1617_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region district, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
drop est_wage_months_* count_*
**** Replace missing value of wage_months (18 changes)
replace wage_months = est_wage_months if wage_months==. & wage_CIC~=.
replace wage_months = 1 if wage_months==. & wage_CIC~=.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region district, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
capture drop track
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_4 if track==4
drop est_wage_weeks_* count_*
* Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==. & wage_CIC~=.
replace wage_weeks = 1 if wage_weeks==. & wage_CIC~=.
* Count observations with wage_hours reported by geographic location.
by wage_CIC female rural region district TA ea, sort : egen float count_1 = count(1) if wage_hours~=.
by wage_CIC female rural region district TA, sort : egen float count_2 = count(1) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float count_3 = count(1) if wage_hours~=.
by wage_CIC female rural region, sort : egen float count_4 = count(1) if wage_hours~=.
* Calculate median of wage_hours by geographic location - later used for predictive purposes.
by wage_CIC female rural region district TA ea, sort : egen float temp_est_wage_hours_1 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district TA, sort : egen float temp_est_wage_hours_2 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region district, sort : egen float temp_est_wage_hours_3 = median(wage_hours) if wage_hours~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hours_4 = median(wage_hours) if wage_hours~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region district TA ea, sort : egen float est_wage_hours_1 = max(temp_est_wage_hours_1)
by wage_CIC female rural region district TA, sort : egen float est_wage_hours_2 = max(temp_est_wage_hours_2)
by wage_CIC female rural region district, sort : egen float est_wage_hours_3 = max(temp_est_wage_hours_3)
by wage_CIC female rural region, sort : egen float est_wage_hours_4 = max(temp_est_wage_hours_4)
drop temp*
* Build wage_hours prediction variable if at least 10 observations of reported wage_hours in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
capture drop track
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hours = est_wage_hours_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hours==.
replace est_wage_hours = est_wage_hours_3 if track==3
replace track = 4 if est_wage_hours==.
replace est_wage_hours = est_wage_hours_4 if track==4
drop est_wage_hours_* count_*
* Replace missing value of wage_hours (35 changes)
replace wage_hours = est_wage_hours if wage_hours==. & wage_CIC~=.
replace wage_hours = 1 if wage_hours==. & wage_CIC~=.

* Casual vs Formal Wage; here the definition is a combination of (i) receive payments monthly, quarterly, etc (ii)gen formal = 0
g formal = 0
replace formal = 1 if wage_payment_duration_unit>=5 & wage_payment_duration_unit<.
replace formal = 1 if wage_inkind_duration_unit>=5 & wage_inkind_duration_unit<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

//added definition of "formality" due to contract structure
g formalcon =  w_contract==1 

//added definition of "formality" due to SP
g formalsp = (pension==1 | health_insurance==1)


* Generate Full Time Equivalents
gen fte_wage = wage_months*wage_weeks*wage_hours/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2

* Bring in Member Demographic Info
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(none)

* FTEs by Formal
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

** (!)inf2= the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
*
recode pension health_insurance (2=0)

* Collapse to HH / CIC level
collapse (sum) fte* emp* formal* (max) pension health_insurance,  by (case_id PID wage_CIC) // changed (!)

* Bring in information for CIC Groupings
rename wage_CIC CIC
joinby CIC using "clean data/Malawi1617_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.644 if AFS_share==. & rural==1
replace AFS_share = 0.540 if AFS_share==. & rural==0

** Generate Categorical Variables
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}

*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*
* For mixed CIC Groups
forvalues f=0(1)1 {
forvalues w=1(1)2 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
}
*

* Collapse to the household level
collapse (sum) w_o_* w_f_* (max) pension health_insurance, by (case_id PID)

save "clean data/intermediate/Malawi1617_wage_occCHEVAL.dta", replace

** Apprenticeships
use "raw data/HH_MOD_E.dta", clear
* Rename variables
rename hh_e48b app_CIC
rename hh_e50 app_months
rename hh_e51 app_weeks
rename hh_e52 app_hours
order case_id PID app_*
keep case_id PID app_*

mvdecode app_CIC app_months app_weeks app_hours, mv(0)
drop if app_CIC==. & app_months==. & app_weeks==. & app_hours==.

* Clear Errors in data
* No occupation code, but reported earnings. (31 changes)
replace app_CIC = 99999 if app_CIC==.

* Generate Full Time Equivalents
gen fte_app = app_months*app_weeks*app_hours/2016
mvdecode fte_app, mv(0)
mvencode fte_app, mv(0)
replace fte_app = 2 if fte_app>2

* Bring in Member Demographic Info
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(none)

**** FTEs for Apprenticeships are considered informal *
gen fte_f0_app = fte_app

* Simple count of Wages earned.
gen emp_f0_app = 1 if fte_app~=0
mvencode emp_f0_app, mv(0)

* Collapse to HH / CIC level
collapse (sum) fte* emp*, by (case_id PID app_CIC)

* Bring in information for CIC Groupings
rename app_CIC CIC
joinby CIC using "clean data/Malawi1617_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.644 if AFS_share==. & rural==1
replace AFS_share = 0.540 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen a_o_f0_c`c' = 0
}
*

forvalues c=1(1)12 {
	gen a_f_f0_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace a_o_f0_c`c' = emp_f0_app if ISIC_group==`c'
	replace a_f_f0_c`c' = fte_f0_app if ISIC_group==`c'
}
*

* Four mixed CIC Groups
forvalues w=1(1)2 {
	replace a_o_f0_c3 = AFS_share*emp_f0_app if ISIC_group==37
	replace a_o_f0_c7 = (1-AFS_share)*emp_f0_app if ISIC_group==37
	replace a_o_f0_c5 = AFS_share*emp_f0_app if ISIC_group==509
	replace a_o_f0_c9 = (1-AFS_share)*emp_f0_app if ISIC_group==509
	replace a_o_f0_c4 = AFS_share*emp_f0_app if ISIC_group==410
	replace a_o_f0_c10 = (1-AFS_share)*emp_f0_app if ISIC_group==410
	replace a_o_f0_c6 = AFS_share*emp_f0_app if ISIC_group==612
	replace a_o_f0_c12 = (1-AFS_share)*emp_f0_app if ISIC_group==612
	replace a_f_f0_c3 = AFS_share*fte_f0_app if ISIC_group==37
	replace a_f_f0_c7 = (1-AFS_share)*fte_f0_app if ISIC_group==37
	replace a_f_f0_c5 = AFS_share*fte_f0_app if ISIC_group==509
	replace a_f_f0_c9 = (1-AFS_share)*fte_f0_app if ISIC_group==509
	replace a_f_f0_c4 = AFS_share*fte_f0_app if ISIC_group==410
	replace a_f_f0_c10 = (1-AFS_share)*fte_f0_app if ISIC_group==410
	replace a_f_f0_c6 = AFS_share*fte_f0_app if ISIC_group==612
	replace a_f_f0_c12 = (1-AFS_share)*fte_f0_app if ISIC_group==612
}
*

* Collapse to the household level
collapse (sum) a_o_* a_f_*, by (case_id PID)

save "clean data/intermediate/Malawi1617_app_occ.dta", replace

** Ganyu Labor
use "raw data/HH_MOD_E.dta", clear
rename hh_e56 g_months
rename hh_e57 g_weeks
rename hh_e58 g_days
rename hh_e59 g_wage
rename hh_e10 g_hours_last_week

* Keep relevant data
keep case_id PID g_*
keep if g_months~=.

* Correct error
replace g_days = . if g_days>7

*****
* The assumption of full days work in Ganyu leads to unrealistic levels of Ganyu labor.
* Treat question asking for last week's hours in casual, part-time or ganyu as average hours per week worked in ganyu.
* This assumption will naturally lead to overstating of ganyu labor, but less so than assuming 8 hours per day
* Option 1
* Calculate FTE - Full time is 252 days per year
gen fte_ganyu_JUST_GANYU_SECTION = g_months*g_weeks*g_days/252
* Option 2
**** Assume that any value greater than zero for g_hours_last_week is for casual or part time labor (collected elsewhere in data) if no reported weeks working in ganyu labor
replace g_hours_last_week = . if g_weeks==.
**** It is possible that ganyu labor during the year did not happen during the last week, 
**** and since the hours per week variable is from a different part of the survey, need to
**** assume the average hours of other households worked during the past week is the amount
**** of hours that the laborer with zero hours last week would have worked if they worked.
**** 5595 of 13168 individuals who reported working ganyu this past year worked last week. (avg hours = 13.43)

* Calculate median hours worked per week by geographic regions. 
replace g_hours_last_week = . if g_hours_last_week==0

joinby using "clean data/Malawi1617_member_demos.dta", unmatched(none)

**** Estimate hours/week based on median hours/week of minimum 10 observations by smallest spatial variable possible

* Count observations with hours/week reported by geographic location.
by rural female region district TA ea, sort : egen float count_1 = count(1) if g_hours_last_week~=.
by rural female region district TA, sort : egen float count_2 = count(1) if g_hours_last_week~=.
by rural female region district, sort : egen float count_3 = count(1) if g_hours_last_week~=.
by rural female region, sort : egen float count_4 = count(1) if g_hours_last_week~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by rural female region district TA ea, sort : egen float temp_est_hrs_1 = median(g_hours_last_week) if g_hours_last_week~=.
by rural female region district TA, sort : egen float temp_est_hrs_2 = median(g_hours_last_week) if g_hours_last_week~=.
by rural female region district, sort : egen float temp_est_hrs_3 = median(g_hours_last_week) if g_hours_last_week~=.
by rural female region, sort : egen float temp_est_hrs_4 = median(g_hours_last_week) if g_hours_last_week~=.
mvencode temp_*, mv(0)

by rural female region district TA ea, sort : egen float est_hrs_1 = max(temp_est_hrs_1)
by rural female region district TA, sort : egen float est_hrs_2 = max(temp_est_hrs_2)
by rural female region district, sort : egen float est_hrs_3 = max(temp_est_hrs_3)
by rural female region, sort : egen float est_hrs_4 = max(temp_est_hrs_4)
drop temp*

* Build hours per week prediction variable if at least 10 observations of reported hours per week in location counted above.
*	Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_hrs = est_hrs_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_hrs==.
replace est_hrs = est_hrs_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_hrs==.
replace est_hrs = est_hrs_3 if track==3
replace track = 4 if est_hrs==.
replace est_hrs = est_hrs_4 if track==4
drop est_hrs_* count_*
* (7,573 changes) 
replace g_hours_last_week = est_hrs if g_hours_last_week==.

* Calculate FTE - Full time is 2016 hours per year
gen fte_ganyu = g_months*g_weeks*g_hours_last_week/2016

* Indicate if HH member completed any Ganyu labor
gen emp_ganyu = 1 if fte_ganyu>0 & fte_ganyu~=.

drop if emp_ganyu==.

* Bring in Member Demographic Info
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Demographic FTEs
gen fte_g = fte_ganyu
mvdecode fte_g, mv(0)
mvencode fte_g, mv(0)
replace fte_g = 2 if fte_g>2

** Generate Categorical Variables
gen g_o_c2 = 0
gen g_f_c2 = 0
replace g_o_c2 = 1 if fte_g>0
replace g_f_c2 = fte_g

keep case_id PID g_o_* g_f_* g_wage

save "clean data/intermediate/Malawi1617_ganyu_occ.dta", replace


** Own Farm Employment
* Rainy Season
* Report one worker per observation
* prep
forvalues p=1(1)12 {
use "raw data/AG_MOD_D.dta", clear
rename ag_d42a`p' PID
rename ag_d42b`p' farm_prep_weeks
rename ag_d42c`p' farm_prep_days
rename ag_d42d`p' farm_prep_hourspd
keep case_id PID farm_prep_weeks farm_prep_days farm_prep_hourspd
drop if PID==. & farm_prep_weeks==. & farm_prep_days==. & farm_prep_hourspd==.
gen farm_prep = `p'
save "clean data/intermediate/Malawi1617_temp_farm_rain_prep_`p'.dta", replace
}
*
* grow
forvalues p=1(1)12 {
use "raw data/AG_MOD_D.dta", clear
rename ag_d43a`p' PID
rename ag_d43b`p' farm_grow_weeks
rename ag_d43c`p' farm_grow_days
rename ag_d43d`p' farm_grow_hourspd
keep case_id PID farm_grow_weeks farm_grow_days farm_grow_hourspd
drop if PID==. & farm_grow_weeks==. & farm_grow_days==. & farm_grow_hourspd==.
gen farm_grow = `p'
save "clean data/intermediate/Malawi1617_temp_farm_rain_grow_`p'.dta", replace
}
*
* harvest
forvalues p=1(1)12 {
use "raw data/AG_MOD_D.dta", clear
rename ag_d44a`p' PID
rename ag_d44b`p' farm_harvest_weeks
rename ag_d44c`p' farm_harvest_days
rename ag_d44d`p' farm_harvest_hourspd
keep case_id PID farm_harvest_weeks farm_harvest_days farm_harvest_hourspd
drop if PID==. & farm_harvest_weeks==. & farm_harvest_days==. & farm_harvest_hourspd==.
gen farm_harvest = `p'
save "clean data/intermediate/Malawi1617_temp_farm_rain_harvest_`p'.dta", replace
}
*

use "clean data/intermediate/Malawi1617_temp_farm_rain_prep_1.dta", clear
forvalues p=2(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_rain_prep_`p'.dta"
}
forvalues p=1(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_rain_grow_`p'.dta"
}
forvalues p=1(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_rain_harvest_`p'.dta"
}
*

mvdecode farm_*, mv(0)

***
*** Correct errors in data
* Cannot work more than 7 days a week
* Assume that people do not average more than 16 hours per day
local farm "prep grow harvest"
foreach f in `farm' {
	replace farm_`f'_hourspd = . if farm_`f'_hourspd>16
	* Results in changing the following # of observations: 235/34,347 prep, 236/33,937 grow, 162/33,923 harvest
	replace farm_`f'_days = . if farm_`f'_days>7
	* Results in changing the following # of observations: 255/34,391 prep, 211/33,979 grow, 67/33,995 harvest
}
*
* Assume if people work one hour, they worked at least one day, and they worked at least one week...
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_days reported by geographic location.
	by rural region district TA ea, sort : egen float count_1 = count(1) if farm_`f'_days~=.
	by rural region district TA, sort : egen float count_2 = count(1) if farm_`f'_days~=.
	by rural region district, sort : egen float count_3 = count(1) if farm_`f'_days~=.
	by rural region, sort : egen float count_4 = count(1) if farm_`f'_days~=.
	* Calculate median of farm_`f'_days by geographic location - later used for predictive purposes.
	by rural region district TA ea, sort : egen float temp_est_farm_`f'_days_1 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region district TA, sort : egen float temp_est_farm_`f'_days_2 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region district, sort : egen float temp_est_farm_`f'_days_3 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region, sort : egen float temp_est_farm_`f'_days_4 = median(farm_`f'_days) if farm_`f'_days~=.
	mvencode temp_*, mv(0)
	by rural region district TA ea, sort : egen float est_farm_`f'_days_1 = max(temp_est_farm_`f'_days_1)
	by rural region district TA, sort : egen float est_farm_`f'_days_2 = max(temp_est_farm_`f'_days_2)
	by rural region district, sort : egen float est_farm_`f'_days_3 = max(temp_est_farm_`f'_days_3)
	by rural region, sort : egen float est_farm_`f'_days_4 = max(temp_est_farm_`f'_days_4)
	drop temp*
	* Build farm_`f'_days prediction variable if at least 10 observations of reported farm_`f'_days in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_days = est_farm_`f'_days_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_3 if track==3
	replace track = 4 if est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_4 if track==4
	drop est_farm_`f'_days_* count_* track
	* Replace missing value of farm_`f'_days (257, 215, 67 changes)
	replace farm_`f'_days = est_farm_`f'_days if farm_`f'_days==. & farm_`f'~=.
	replace farm_`f'_days = 1 if farm_`f'_days==. & farm_`f'~=.
}
*
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_hourspd reported by geographic location.
	by rural region district TA ea, sort : egen float count_1 = count(1) if farm_`f'_hourspd~=.
	by rural region district TA, sort : egen float count_2 = count(1) if farm_`f'_hourspd~=.
	by rural region district, sort : egen float count_3 = count(1) if farm_`f'_hourspd~=.
	by rural region, sort : egen float count_4 = count(1) if farm_`f'_hourspd~=.
	* Calculate median of farm_`f'_hourspd by geographic location - later used for predictive purposes.
	by rural region district TA ea, sort : egen float temp_est_farm_`f'_hourspd_1 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region district TA, sort : egen float temp_est_farm_`f'_hourspd_2 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region district, sort : egen float temp_est_farm_`f'_hourspd_3 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region, sort : egen float temp_est_farm_`f'_hourspd_4 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	mvencode temp_*, mv(0)
	by rural region district TA ea, sort : egen float est_farm_`f'_hourspd_1 = max(temp_est_farm_`f'_hourspd_1)
	by rural region district TA, sort : egen float est_farm_`f'_hourspd_2 = max(temp_est_farm_`f'_hourspd_2)
	by rural region district, sort : egen float est_farm_`f'_hourspd_3 = max(temp_est_farm_`f'_hourspd_3)
	by rural region, sort : egen float est_farm_`f'_hourspd_4 = max(temp_est_farm_`f'_hourspd_4)
	drop temp*
	* Build farm_`f'_hourspd prediction variable if at least 10 observations of reported farm_`f'_hourspd in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_hourspd = est_farm_`f'_hourspd_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_3 if track==3
	replace track = 4 if est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_4 if track==4
	drop est_farm_`f'_hourspd_* count_* track
	* Replace missing value of farm_`f'_hourspd (281, 282, 236 changes)
	replace farm_`f'_hourspd = est_farm_`f'_hourspd if farm_`f'_hourspd==. & farm_`f'~=.
	replace farm_`f'_hourspd = 1 if farm_`f'_hourspd==. & farm_`f'~=.
}
*
mvencode farm_*, mv(0)

* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	gen farm_`f'_hours = farm_`f'_weeks*farm_`f'_days*farm_`f'_hourspd
}
*

* Collapse to HH Member
collapse (sum) farm_prep_hours farm_grow_hours farm_harvest_hours, by (case_id PID)

* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_prep_hours>0 | farm_grow_hours>0 | farm_harvest_hours>0

* Total farm hours by member
gen farm_hours = farm_prep_hours + farm_grow_hours + farm_harvest_hours

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
replace farm_fte = 2 if farm_fte>2

keep case_id PID farm_worker farm_fte

* Indicate that this is rainy Season
rename farm_worker farm_L_work
rename farm_fte farm_L_fte

save "clean data/intermediate/Malawi1617_farm_rainy_occ.dta", replace


* dry Season
* Report one worker per observation
* prep
forvalues p=1(1)7 {
use "raw data/AG_MOD_K.dta", clear
rename ag_k43a`p' PID
rename ag_k43b`p' farm_prep_weeks
rename ag_k43c`p' farm_prep_days
rename ag_k43d`p' farm_prep_hourspd
keep case_id PID farm_prep_weeks farm_prep_days farm_prep_hourspd
drop if PID==. & farm_prep_weeks==. & farm_prep_days==. & farm_prep_hourspd==.
gen farm_prep = `p'
save "clean data/intermediate/Malawi1617_temp_farm_dry_prep_`p'.dta", replace
}
*
* grow
forvalues p=1(1)7 {
use "raw data/AG_MOD_K.dta", clear
rename ag_k44a`p' PID
rename ag_k44b`p' farm_grow_weeks
rename ag_k44c`p' farm_grow_days
rename ag_k44d`p' farm_grow_hourspd
keep case_id PID farm_grow_weeks farm_grow_days farm_grow_hourspd
drop if PID==. & farm_grow_weeks==. & farm_grow_days==. & farm_grow_hourspd==.
gen farm_grow = `p'
save "clean data/intermediate/Malawi1617_temp_farm_dry_grow_`p'.dta", replace
}
*
* harvest
forvalues p=1(1)7 {
use "raw data/AG_MOD_K.dta", clear
rename ag_k45a`p' PID
rename ag_k45b`p' farm_harvest_weeks
rename ag_k45c`p' farm_harvest_days
rename ag_k45d`p' farm_harvest_hourspd
keep case_id PID farm_harvest_weeks farm_harvest_days farm_harvest_hourspd
drop if PID==. & farm_harvest_weeks==. & farm_harvest_days==. & farm_harvest_hourspd==.
gen farm_harvest = `p'
save "clean data/intermediate/Malawi1617_temp_farm_dry_harvest_`p'.dta", replace
}
*

use "clean data/intermediate/Malawi1617_temp_farm_dry_prep_1.dta", clear
forvalues p=2(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_dry_prep_`p'.dta"
}
forvalues p=1(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_dry_grow_`p'.dta"
}
forvalues p=1(1)4 {
append using "clean data/intermediate/Malawi1617_temp_farm_dry_harvest_`p'.dta"
}
*

mvdecode farm_*, mv(0)
	
***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
local farm "prep grow harvest"
foreach f in `farm' {
	replace farm_`f'_hourspd = . if farm_`f'_hourspd>16
	* Results in changing the following # of observations: 2 prep, 2 grow, 0 harvest
	replace farm_`f'_days = . if farm_`f'_days>7
	* Results in changing the following # of observations: 13 prep, 0 grow, 1 harvest
}
*
* Assume if people work one hour, they worked at least one day, and they worked at least one week...
joinby case_id using "clean data/Malawi1617_location.dta", unmatched(none)
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_weeks reported by geographic location.
	by rural region district TA ea, sort : egen float count_1 = count(1) if farm_`f'_weeks~=.
	by rural region district TA, sort : egen float count_2 = count(1) if farm_`f'_weeks~=.
	by rural region district, sort : egen float count_3 = count(1) if farm_`f'_weeks~=.
	by rural region, sort : egen float count_4 = count(1) if farm_`f'_weeks~=.
	* Calculate median of farm_`f'_weeks by geographic location - later used for predictive purposes.
	by rural region district TA ea, sort : egen float temp_est_farm_`f'_weeks_1 = median(farm_`f'_weeks) if farm_`f'_weeks~=.
	by rural region district TA, sort : egen float temp_est_farm_`f'_weeks_2 = median(farm_`f'_weeks) if farm_`f'_weeks~=.
	by rural region district, sort : egen float temp_est_farm_`f'_weeks_3 = median(farm_`f'_weeks) if farm_`f'_weeks~=.
	by rural region, sort : egen float temp_est_farm_`f'_weeks_4 = median(farm_`f'_weeks) if farm_`f'_weeks~=.
	mvencode temp_*, mv(0)
	by rural region district TA ea, sort : egen float est_farm_`f'_weeks_1 = max(temp_est_farm_`f'_weeks_1)
	by rural region district TA, sort : egen float est_farm_`f'_weeks_2 = max(temp_est_farm_`f'_weeks_2)
	by rural region district, sort : egen float est_farm_`f'_weeks_3 = max(temp_est_farm_`f'_weeks_3)
	by rural region, sort : egen float est_farm_`f'_weeks_4 = max(temp_est_farm_`f'_weeks_4)
	drop temp*
	* Build farm_`f'_weeks prediction variable if at least 10 observations of reported farm_`f'_weeks in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_weeks = est_farm_`f'_weeks_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_weeks==.
	replace est_farm_`f'_weeks = est_farm_`f'_weeks_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_weeks==.
	replace est_farm_`f'_weeks = est_farm_`f'_weeks_3 if track==3
	replace track = 4 if est_farm_`f'_weeks==.
	replace est_farm_`f'_weeks = est_farm_`f'_weeks_4 if track==4
	drop est_farm_`f'_weeks_* count_* track
	* Replace missing value of farm_`f'_weeks (1, 1, 0 changes)
	replace farm_`f'_weeks = est_farm_`f'_weeks if farm_`f'_weeks==. & farm_`f'~=.
	replace farm_`f'_weeks = 1 if farm_`f'_weeks==. & farm_`f'~=.
}
*
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_days reported by geographic location.
	by rural region district TA ea, sort : egen float count_1 = count(1) if farm_`f'_days~=.
	by rural region district TA, sort : egen float count_2 = count(1) if farm_`f'_days~=.
	by rural region district, sort : egen float count_3 = count(1) if farm_`f'_days~=.
	by rural region, sort : egen float count_4 = count(1) if farm_`f'_days~=.
	* Calculate median of farm_`f'_days by geographic location - later used for predictive purposes.
	by rural region district TA ea, sort : egen float temp_est_farm_`f'_days_1 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region district TA, sort : egen float temp_est_farm_`f'_days_2 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region district, sort : egen float temp_est_farm_`f'_days_3 = median(farm_`f'_days) if farm_`f'_days~=.
	by rural region, sort : egen float temp_est_farm_`f'_days_4 = median(farm_`f'_days) if farm_`f'_days~=.
	mvencode temp_*, mv(0)
	by rural region district TA ea, sort : egen float est_farm_`f'_days_1 = max(temp_est_farm_`f'_days_1)
	by rural region district TA, sort : egen float est_farm_`f'_days_2 = max(temp_est_farm_`f'_days_2)
	by rural region district, sort : egen float est_farm_`f'_days_3 = max(temp_est_farm_`f'_days_3)
	by rural region, sort : egen float est_farm_`f'_days_4 = max(temp_est_farm_`f'_days_4)
	drop temp*
	* Build farm_`f'_days prediction variable if at least 10 observations of reported farm_`f'_days in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_days = est_farm_`f'_days_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_3 if track==3
	replace track = 4 if est_farm_`f'_days==.
	replace est_farm_`f'_days = est_farm_`f'_days_4 if track==4
	drop est_farm_`f'_days_* count_* track
	* Replace missing value of farm_`f'_days (14, 0, 1 changes)
	replace farm_`f'_days = est_farm_`f'_days if farm_`f'_days==. & farm_`f'~=.
	replace farm_`f'_days = 1 if farm_`f'_days==. & farm_`f'~=.
}
*
local farm "prep grow harvest"
foreach f in `farm' {
	* Count observations with farm_`f'_hourspd reported by geographic location.
	by rural region district TA ea, sort : egen float count_1 = count(1) if farm_`f'_hourspd~=.
	by rural region district TA, sort : egen float count_2 = count(1) if farm_`f'_hourspd~=.
	by rural region district, sort : egen float count_3 = count(1) if farm_`f'_hourspd~=.
	by rural region, sort : egen float count_4 = count(1) if farm_`f'_hourspd~=.
	* Calculate median of farm_`f'_hourspd by geographic location - later used for predictive purposes.
	by rural region district TA ea, sort : egen float temp_est_farm_`f'_hourspd_1 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region district TA, sort : egen float temp_est_farm_`f'_hourspd_2 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region district, sort : egen float temp_est_farm_`f'_hourspd_3 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	by rural region, sort : egen float temp_est_farm_`f'_hourspd_4 = median(farm_`f'_hourspd) if farm_`f'_hourspd~=.
	mvencode temp_*, mv(0)
	by rural region district TA ea, sort : egen float est_farm_`f'_hourspd_1 = max(temp_est_farm_`f'_hourspd_1)
	by rural region district TA, sort : egen float est_farm_`f'_hourspd_2 = max(temp_est_farm_`f'_hourspd_2)
	by rural region district, sort : egen float est_farm_`f'_hourspd_3 = max(temp_est_farm_`f'_hourspd_3)
	by rural region, sort : egen float est_farm_`f'_hourspd_4 = max(temp_est_farm_`f'_hourspd_4)
	drop temp*
	* Build farm_`f'_hourspd prediction variable if at least 10 observations of reported farm_`f'_hourspd in location counted above.
	*	Start with smallest qualifying geographic region
	mvdecode est_*, mv(0)
	capture drop track
	generate track = 1 if count_1>=10 & count_1<100000
	generate est_farm_`f'_hourspd = est_farm_`f'_hourspd_1 if track==1
	replace track = 2 if count_2>=10 & count_2<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_2 if track==2
	replace track = 3 if count_3>=10 & count_3<100000 & est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_3 if track==3
	replace track = 4 if est_farm_`f'_hourspd==.
	replace est_farm_`f'_hourspd = est_farm_`f'_hourspd_4 if track==4
	drop est_farm_`f'_hourspd_* count_* track
	* Replace missing value of farm_`f'_hourspd (5, 4, 10 changes)
	replace farm_`f'_hourspd = est_farm_`f'_hourspd if farm_`f'_hourspd==. & farm_`f'~=.
	replace farm_`f'_hourspd = 1 if farm_`f'_hourspd==. & farm_`f'~=.
}
*
mvencode farm_*, mv(0)

* Calculate total hours per hh member
local farm "prep grow harvest"
foreach f in `farm' {
	replace farm_`f'_hours = farm_`f'_weeks*farm_`f'_days*farm_`f'_hourspd
}
*

* Collapse to HH Member
collapse (sum) farm_prep_hours farm_grow_hours farm_harvest_hours, by (case_id PID)

* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_prep_hours>0 | farm_grow_hours>0 | farm_harvest_hours>0

* Total farm hours by member
gen farm_hours = farm_prep_hours + farm_grow_hours + farm_harvest_hours

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
replace farm_fte = 2 if farm_fte>2

keep case_id PID farm_worker farm_fte

* Indicate that this is dry Season
rename farm_worker farm_S_work
rename farm_fte farm_S_fte

save "clean data/intermediate/Malawi1617_farm_dry_occ.dta", replace

* Combine rainy and dry Farming
use "clean data/intermediate/Malawi1617_farm_rainy_occ.dta", clear
joinby case_id PID using "clean data/intermediate/Malawi1617_farm_dry_occ.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode farm*, mv(0)
mvencode farm*, mv(0)

* Combine rainy & dry Seasons
gen farm = 1 if farm_L_work>0 | farm_S_work>0
gen farm_fte = farm_L_fte + farm_S_fte

mvdecode farm*, mv(0)
mvencode farm*, mv(0)

* Collapse to HH
collapse (sum) farm farm_fte, by (case_id PID)
replace farm_fte = 2 if farm_fte>2

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

* Keep Farming Data at the household level
keep case_id PID farm_o_c1 farm_f_c1

save "clean data/intermediate/Malawi1617_farm_occ.dta", replace

**** COMBINE ALL ****

use "clean data/intermediate/Malawi1617_nfe_occ.dta", clear
joinby case_id PID using "clean data/intermediate/Malawi1617_wage_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id PID using "clean data/intermediate/Malawi1617_app_occ.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id PID using "clean data/intermediate/Malawi1617_ganyu_occ.dta", unmatched(both) _merge(_merge)
drop _merge
joinby case_id PID using "clean data/intermediate/Malawi1617_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Demographics
joinby case_id PID using "clean data/Malawi1617_member_demos.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode nfe* w* a_* g_* farm*, mv(0)
mvencode nfe* w* a_* g_* farm*, mv(0)

** Generate Grouped Categorical Variables (!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*
*(!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}
*
*add apprenticeship work (informal)://!EVA: added on 22/05/2019
forvalues c=1(1)12 {
	replace o_f0_c`c' = o_f0_c`c' + a_o_f0_c`c'
	replace f_f0_c`c' = f_f0_c`c' + a_f_f0_c`c'
}

*non-farm enterprise
forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}

*
*farm enterprise
replace o_f2_c1 = o_f2_c1 + farm_o_c1

replace f_f2_c1 = f_f2_c1 + farm_f_c1

* Total (f3) Casual, Formal and Self Employed !EVA: This seems rather useless?!
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f3_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	
* 

joinby using "clean data/Malawi1617_iid.dta", unmatched(none)

*keep only relevant variables:

keep PID fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete  pension health_insurance school_secondary school_level case_id
ren case_id hhid
tostring PID, g(iid)
drop PID
gen country="Malawi"

save "clean data/MALAWI_FTEsCHEVAL.dta", replace
