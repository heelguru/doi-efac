********************************************************************************
***************** NIGERIA 2015/2016: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Nigeria 2015/2016 General Household Survey-Panel Wave 3


-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
generates 
save "clean data/intermediate/Nigeria1516_wage_occCHEVAL.dta", replace
save "clean data/NIGERIA_FTEsCHEVAL.dta", replace

Stata Version 15.1 

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force
*/

capture log close
clear
clear matrix
set more off


********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\Nigeria_GHS_1516" 

gl DO   	    ${NIGERIA}/do
gl raw data   	${NIGERIA}/raw data 
gl clean data 	${NIGERIA}/clean data
gl FIG 	    	${NIGERIA}/graphs
gl TAB 		    ${NIGERIA}/tables
 
********************************************************************************
cd "$NIGERIA"


** Occupations in Non-Farm Own Enterprises (ONLY in POST HARVEST)
use "raw data/sect9_harvestw3.dta", clear
rename s9q1b nfe_CIC
rename s9q13a1 nfe_hourspd
rename s9q13a nfe_mem_a
rename s9q13b nfe_mem_b
rename s9q13c nfe_mem_c
rename s9q13d nfe_mem_d
rename s9q13e nfe_mem_e
rename s9q13f nfe_mem_f
rename s9q13_2 nfe_days_a
rename s9q13_4 nfe_days_b
rename s9q13_6 nfe_days_c
rename s9q13_8 nfe_days_d
rename s9q13_10 nfe_days_e
rename s9q13_12 nfe_days_f
local alph "a b c d e f g h i j k l m n o"
foreach a in `alph' {
	replace s9q10`a' = "1" if s9q10`a'=="X"
}
*
destring s9q10*, replace
mvencode s9q10*, mv(0)
egen float nfe_months = rowtotal(s9q10*)
* question asks for the last 12 months, so must assume that those who reported greater 
* than 12 months did not understand the question. It is possible that those who understood
* the question only reported 12 months even if they worked more than 12 of past 15.
replace nfe_months = 12 if nfe_months>12

* Keep relevant data
keep hhid nfe_*
replace nfe_hourspd = 0 if nfe_hourspd==. & nfe_days_a==. & nfe_days_b==. & nfe_days_c==. & nfe_days_d==. & nfe_days_e==. & nfe_days_f==.
drop if nfe_hourspd==0 & nfe_months==0

save "clean data/intermediate/Nigeria1516_nfe_occ_temp.dta", replace

* Convert data to show one observation per worker per enterprise
local alph "a b c d e f"
foreach a in `alph' {
	use "clean data/intermediate/Nigeria1516_nfe_occ_temp.dta", clear
	keep hhid nfe_CIC nfe_mem_`a' nfe_days_`a' nfe_hourspd nfe_months
	rename nfe_mem_`a' indiv
	rename nfe_days_`a' nfe_days
	drop if indiv==.
	save "clean data/intermediate/Nigeria1516_nfe_occ_temp_`a'.dta", replace
}
*
use "clean data/intermediate/Nigeria1516_nfe_occ_temp_a.dta", clear
local alph "b c d e f"
foreach a in `alph' {
	append using "clean data/intermediate/Nigeria1516_nfe_occ_temp_`a'.dta"
}
*

***** Assume, not possible to average working greater than 16 hours per day (55 observations replaced)
replace nfe_hourspd = . if nfe_hourspd>16
***** Assume if no reported days worked, but reported worker, that the worker worked the median amount of days as other workers of the same industry (3 observations replaced)
replace nfe_days = . if nfe_days>31
mvdecode nfe_months nfe_days nfe_hourspd, mv(0)
* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_months~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_months_5 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
by nfe_CIC female rural, sort : egen float est_nfe_months_5 = max(temp_est_nfe_months_5)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
replace track = 5 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_5 if track==5
drop est_nfe_months_* count_* track
**** Replace missing value of nfe_months (20 changes)
replace nfe_months = est_nfe_months if nfe_months==.
* Count observations with nfe_days reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_days~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_days~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_days~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_days~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_days~=.
* Calculate median of nfe_days by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_days_1 = median(nfe_days) if nfe_days~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_days_2 = median(nfe_days) if nfe_days~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_days_3 = median(nfe_days) if nfe_days~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_days_4 = median(nfe_days) if nfe_days~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_days_5 = median(nfe_days) if nfe_days~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_days_1 = max(temp_est_nfe_days_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_days_2 = max(temp_est_nfe_days_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_days_3 = max(temp_est_nfe_days_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_days_4 = max(temp_est_nfe_days_4)
by nfe_CIC female rural, sort : egen float est_nfe_days_5 = max(temp_est_nfe_days_5)
drop temp*
* Build nfe_days prediction variable if at least 10 observations of reported nfe_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_days = est_nfe_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_days==.
replace est_nfe_days = est_nfe_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_days==.
replace est_nfe_days = est_nfe_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_days==.
replace est_nfe_days = est_nfe_days_3 if track==4
replace track = 5 if est_nfe_days==.
replace est_nfe_days = est_nfe_days_5 if track==5
drop est_nfe_days_* count_* track
**** Replace missing value of nfe_days (104 changes)
replace nfe_days = est_nfe_days if nfe_days==.
* Count observations with nfe_hourspd reported by geographic location.
by nfe_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_hourspd~=.
by nfe_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_hourspd~=.
by nfe_CIC female rural Zone State, sort : egen float count_3 = count(1) if nfe_hourspd~=.
by nfe_CIC female rural Zone, sort : egen float count_4 = count(1) if nfe_hourspd~=.
by nfe_CIC female rural, sort : egen float count_5 = count(1) if nfe_hourspd~=.
* Calculate median of nfe_hourspd by geographic location - later used for predictive purposes.
by nfe_CIC female rural Zone State LGA EA, sort : egen float temp_est_nfe_hourspd_1 = median(nfe_hourspd) if nfe_hourspd~=.
by nfe_CIC female rural Zone State LGA, sort : egen float temp_est_nfe_hourspd_2 = median(nfe_hourspd) if nfe_hourspd~=.
by nfe_CIC female rural Zone State, sort : egen float temp_est_nfe_hourspd_3 = median(nfe_hourspd) if nfe_hourspd~=.
by nfe_CIC female rural Zone, sort : egen float temp_est_nfe_hourspd_4 = median(nfe_hourspd) if nfe_hourspd~=.
by nfe_CIC female rural, sort : egen float temp_est_nfe_hourspd_5 = median(nfe_hourspd) if nfe_hourspd~=.
mvencode temp_*, mv(0)
by nfe_CIC female rural Zone State LGA EA, sort : egen float est_nfe_hourspd_1 = max(temp_est_nfe_hourspd_1)
by nfe_CIC female rural Zone State LGA, sort : egen float est_nfe_hourspd_2 = max(temp_est_nfe_hourspd_2)
by nfe_CIC female rural Zone State, sort : egen float est_nfe_hourspd_3 = max(temp_est_nfe_hourspd_3)
by nfe_CIC female rural Zone, sort : egen float est_nfe_hourspd_4 = max(temp_est_nfe_hourspd_4)
by nfe_CIC female rural, sort : egen float est_nfe_hourspd_5 = max(temp_est_nfe_hourspd_5)
drop temp*
* Build nfe_hourspd prediction variable if at least 10 observations of reported nfe_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_hourspd = est_nfe_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_hourspd==.
replace est_nfe_hourspd = est_nfe_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_hourspd==.
replace est_nfe_hourspd = est_nfe_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_hourspd==.
replace est_nfe_hourspd = est_nfe_hourspd_3 if track==4
replace track = 5 if est_nfe_hourspd==.
replace est_nfe_hourspd = est_nfe_hourspd_5 if track==5
drop est_nfe_hourspd_* count_* track
**** Replace missing value of nfe_hourspd (67 changes)
replace nfe_hourspd = est_nfe_hourspd if nfe_hourspd==.
* If cannot use other observations to estimate, assume at least one month, one week, one hour (Changes: 0, 1, 0)
replace nfe_months = 1 if nfe_months==.
replace nfe_days = 1 if nfe_days==.
replace nfe_hourspd = 1 if nfe_hourspd==.

* Calculate total hours worked
gen nfe_hours = 0
replace nfe_hours = nfe_months*nfe_days*nfe_hourspd

* Gen FTE
gen fte_nfe = nfe_hours/2016
mvdecode fte_nfe, mv(0)
mvencode fte_nfe, mv(0)
* 82 changes
replace fte_nfe = 2 if fte_nfe>2

* Simple count of employed in NFE.
gen emp_nfe = 1 if fte_nfe~=0
mvdecode emp_nfe, mv(0)
mvencode emp_nfe, mv(0)

* Collapse to HH / ISIC level
collapse (sum) fte_nfe emp_nfe, by (hhid indiv nfe_CIC)

* Bring in information for ISIC Groupings
rename nfe_CIC CIC
joinby CIC using "clean data/Nigeria1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace ISIC_group = 612 if ISIC_group==.
joinby hhid indiv using "clean data/Nigeria1516_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.607 if AFS_share==. & rural==1
replace AFS_share = 0.525 if AFS_share==. & rural==0

** Generate Categorical Variables

forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*

forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*

* Four mixed ISIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612
replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612

* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (hhid indiv)

save "clean data/intermediate/Nigeria1516_nfe_occCHEVAL.dta", replace


** Occupation by Wages (BOTH POST PLANTING AND POST HARVEST)
* Post Planting 
* Primary Wage
use "raw data/sect3_plantingw3.dta", clear

* Rename variables
rename s3q13b wage_CIC
rename s3q15e w_contract
rename s3q15d pension
rename s3q15f health_insurance
rename s3q16 wage_months
rename s3q17 wage_weeks
rename s3q18 wage_hourspw
rename s3q21a wage_payment
rename s3q21b wage_payment_duration
rename s3q24a wage_payment_inkind
rename s3q24b wage_payment_inkind_duration

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (19)
replace wage_hourspw = . if wage_hourspw>80
* Only 52 weeks in a year
replace wage_weeks = 52 if wage_weeks>52 & wage_weeks<.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (4 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (22 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
//replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
//replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 1 change
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

** (!)inf2= the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance w_contract pension)

gen sw = 1

save "clean data/intermediate/Nigeria1516_wage_occ_PP1.dta", replace

* Post Planting 
* Secondary Wage
use "raw data/sect3_plantingw3.dta", clear

* Rename variables
rename s3q26b wage_CIC
rename s3q28e w_contract
rename s3q28d pension
rename s3q28f health_insurance
rename s3q29 wage_months
rename s3q30 wage_weeks
rename s3q31 wage_hourspw
rename s3q34a wage_payment
rename s3q34b wage_payment_duration
rename s3q37a wage_payment_inkind
rename s3q37b wage_payment_inkind_duration

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 0 changes
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*
** the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance w_contract pension)
gen sw = 2

save "clean data/intermediate/Nigeria1516_wage_occ_PP2.dta", replace

* Post Harvest 
* Primary Wage
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q13b wage_CIC
rename s3q15e w_contract
rename s3q15d pension
rename s3q15f health_insurance
rename s3q16 wage_months
rename s3q17 wage_weeks
rename s3q18 wage_hourspw
rename s3q21a wage_payment
rename s3q21b wage_payment_duration
rename s3q24a wage_payment_inkind
rename s3q24b wage_payment_inkind_duration

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (71)
replace wage_hourspw = . if wage_hourspw>80 & wage_hourspw~=.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (1 change)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (71 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 0 changes
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*
** the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance w_contract pension)
gen sw = 3

save "clean data/intermediate/Nigeria1516_wage_occ_PH1.dta", replace

* Post Harvest 
* Secondary Wage
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q26b wage_CIC
rename s3q28e w_contract
rename s3q28d pension
rename s3q28f health_insurance
rename s3q29 wage_months
rename s3q30 wage_weeks
rename s3q31 wage_hourspw
rename s3q34a wage_payment
rename s3q34b wage_payment_duration
rename s3q37a wage_payment_inkind
rename s3q37b wage_payment_inkind_duration

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1
* Generate Full Time Equivalents
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
* 0 changes
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*
** the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
* Collapse to HH / ISIC level
replace wage_CIC = 99999 if wage_CIC==.
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC  health_insurance w_contract pension)
gen sw = 4

save "clean data/intermediate/Nigeria1516_wage_occ_PH2.dta", replace

* Post Harvest 
* Wage if NOT worked in past 7 days
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q43b wage_CIC
rename s3q45 wage_months
rename s3q46 wage_weeks
rename s3q47 wage_hourspw
rename s3q49a wage_payment
rename s3q49b wage_payment_duration

order hhid indiv wage_*
keep hhid indiv wage_*

* Clear Errors in data
* No occupation code, but reported earnings. 
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=.
* Assume that reported hours per day above 16 hours per day are in error. (3)
replace wage_hourspw = . if wage_hourspw>80 & wage_hourspw~=.

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.
* No occupation code, no reported earnings, but reported hours.
replace wage_payment = 0 if wage_payment==.

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (3 change)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.

* Generate Full Time Equivalents - USE WAGE WEEKS
gen fte_wage = wage_weeks*wage_hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.

* Demographic FTEs
forvalues f=0(1)1{
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f'_wage, mv(0)
	mvencode fte_f`f'_wage, mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1{
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f'_wage, mv(0)
	mvencode emp_f`f'_wage, mv(0)
}
*

* Collapse to HH / ISIC level
collapse (sum) fte* emp* formal*, by (hhid indiv wage_CIC)

gen sw = 5

save "clean data/intermediate/Nigeria1516_wage_occ_PH3.dta", replace



* Combine Wage files
use "clean data/intermediate/Nigeria1516_wage_occ_PP1.dta", clear
append using "clean data/intermediate/Nigeria1516_wage_occ_PP2.dta"
append using "clean data/intermediate/Nigeria1516_wage_occ_PH1.dta"
append using "clean data/intermediate/Nigeria1516_wage_occ_PH2.dta"
append using "clean data/intermediate/Nigeria1516_wage_occ_PH3.dta"

* If same occupation code occured twice in one season, sum. (3 cases)
* If same occupation code occured twice in two seasons, use the maximum values of FT & Occupational activity to not double count. (393)
gen season = 1 if sw==1 | sw==2
replace season = 2 if sw==3 | sw==4 | sw==5
collapse (sum) fte* emp*, by (hhid indiv wage_CIC formal season formal formalcon formalsp health_insurance w_contract pension)
collapse (max) fte* emp*, by (hhid indiv wage_CIC formal formalcon formalsp health_insurance w_contract pension)

recode health_insurance pension (2=0)
* Collapse to HH / ISIC level
collapse (sum) fte* emp* formal* (max) health_insurance pension, by (hhid indiv wage_CIC)

* Bring in information for ISIC Groupings
rename wage_CIC CIC
joinby CIC using "clean data/Nigeria1516_Alt_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby hhid indiv using "clean data/Nigeria1516_member_demos.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.607 if AFS_share==. & rural==1
replace AFS_share = 0.525 if AFS_share==. & rural==0

** Generate Categorical Variables

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*

* Four mixed ISIC Groups
forvalues f=0(1)1 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f' if ISIC_group==37
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
*

* Collapse to the household level
collapse (sum) w_o_* w_f_* (max) health_insurance pension, by (hhid indiv)

save "clean data/intermediate/Nigeria1516_wage_occCHEVAL.dta", replace

** Own Farm Employment
* Post Planting & Post Harvest Data
* Occupation cannot be listed more than once, but FTE is summed

* Post Planting
use "raw data/sect11c1_plantingw3.dta", clear

local member "a b c d"
foreach m in `member' {
	rename s11c1q1`m'1 member_`m'
}
*

local member "a b c d"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d"
foreach m in `member' {
	replace farm_`m'_weeks = s11c1q1`m'2
	replace farm_`m'_days = s11c1q1`m'3
	replace farm_`m'_hourspd = s11c1q1`m'4
}
*

* Convert data from horizontal to vertical
save "clean data/intermediate/Nigeria1516_temp_farm_PP.dta", replace

local member "a b c d"
foreach m in `member' {
	use "clean data/intermediate/Nigeria1516_temp_farm_PP.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "clean data/intermediate/Nigeria1516_temp_farm_PP_`m'.dta", replace
}
*

use "clean data/intermediate/Nigeria1516_temp_farm_PP_a.dta", clear
append using "clean data/intermediate/Nigeria1516_temp_farm_PP_b.dta"
append using "clean data/intermediate/Nigeria1516_temp_farm_PP_c.dta"
append using "clean data/intermediate/Nigeria1516_temp_farm_PP_d.dta"

***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16
* Results in changing 67 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (3 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (5 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (69 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = 0
replace farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "clean data/intermediate/Nigeria1516_farm_PP.dta", replace


* Pre-Harvest Labor
use "raw data/secta2_harvestw3.dta", clear

local member "a b c d e f g h"
foreach m in `member' {
	rename sa2q1b_`m'1 member_`m'
}
*

local member "a b c d e f g h"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d e f g h"
foreach m in `member' {
	replace farm_`m'_weeks = sa2q1b_`m'2
	replace farm_`m'_days = sa2q1b_`m'3
	replace farm_`m'_hourspd = sa2q1b_`m'4
}
*

* Convert data from horizontal to vertical
save "clean data/intermediate/Nigeria1516_temp_farm_PH.dta", replace

local member "a b c d e f g h"
foreach m in `member' {
	use "clean data/intermediate/Nigeria1516_temp_farm_PH.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "clean data/intermediate/Nigeria1516_temp_farm_PH_`m'.dta", replace
}
*

use "clean data/intermediate/Nigeria1516_temp_farm_PH_a.dta", clear
local member "b c d e f g h"
foreach m in `member' {
	append using "clean data/intermediate/Nigeria1516_temp_farm_PH_`m'.dta"
}
*

***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16 & farm_hourspd~=.
* Results in changing 14 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (2 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (0 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (20 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "clean data/intermediate/Nigeria1516_farm_PreH.dta", replace


** Harvest Own Farm Labor
use "raw data/secta2_harvestw3.dta", clear

local member "a b c d e f g h"
foreach m in `member' {
	rename sa2q1`m'1 member_`m'
}
*

local member "a b c d e f g h"
foreach m in `member' {
	gen farm_`m'_weeks = 0
	gen farm_`m'_days = 0
	gen farm_`m'_hourspd = 0
}
*

local member "a b c d e f g h"
foreach m in `member' {
	replace farm_`m'_weeks = sa2q1`m'2
	replace farm_`m'_days = sa2q1`m'3
	replace farm_`m'_hourspd = sa2q1`m'4
}
*

* Convert data from horizontal to vertical
save "clean data/intermediate/Nigeria1516_temp_farm_PoH.dta", replace

local member "a b c d e f g h"
foreach m in `member' {
	use "clean data/intermediate/Nigeria1516_temp_farm_PoH.dta", clear
	keep hhid member_`m' farm_`m'_weeks farm_`m'_days farm_`m'_hourspd
	rename member_`m' indiv
	rename farm_`m'_weeks farm_weeks
	rename farm_`m'_days farm_days
	rename farm_`m'_hourspd farm_hourspd
	drop if indiv==.
	save "clean data/intermediate/Nigeria1516_temp_farm_PoH_`m'.dta", replace
}
*

use "clean data/intermediate/Nigeria1516_temp_farm_PoH_a.dta", clear
local member "b c d e f g h"
foreach m in `member' {
	append using "clean data/intermediate/Nigeria1516_temp_farm_PoH_`m'.dta"
}
*
***
*** Correct errors in data
* Assume that people do not average more than 16 hours per day
replace farm_hourspd = . if farm_hourspd>16 & farm_hourspd~=.
* Results in changing 12 observations
**** If worked hours then assume to have worked days, and assume worked weeks
mvdecode farm_weeks farm_days farm_hourspd, mv(0)
**** Use median of weeks in farm job of those in the smallest geographic region with 10 observations of weekly farm labor
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with farm_weeks reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_weeks~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_weeks~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_weeks~=.
by female rural, sort : egen float count_5 = count(1) if farm_weeks~=.
* Calculate median of farm_weeks by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_weeks_1 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_weeks_2 = median(farm_weeks) if farm_weeks~=.
by female rural Zone State, sort : egen float temp_est_farm_weeks_3 = median(farm_weeks) if farm_weeks~=.
by female rural Zone, sort : egen float temp_est_farm_weeks_4 = median(farm_weeks) if farm_weeks~=.
by female rural, sort : egen float temp_est_farm_weeks_5 = median(farm_weeks) if farm_weeks~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_weeks_1 = max(temp_est_farm_weeks_1)
by female rural Zone State LGA, sort : egen float est_farm_weeks_2 = max(temp_est_farm_weeks_2)
by female rural Zone State, sort : egen float est_farm_weeks_3 = max(temp_est_farm_weeks_3)
by female rural Zone, sort : egen float est_farm_weeks_4 = max(temp_est_farm_weeks_4)
by female rural, sort : egen float est_farm_weeks_5 = max(temp_est_farm_weeks_5)
drop temp*
* Build farm_weeks prediction variable if at least 10 observations of reported farm_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_weeks = est_farm_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_4 if track==4
replace track = 5 if est_farm_weeks==.
replace est_farm_weeks = est_farm_weeks_5 if track==5
drop est_farm_weeks_* count_* track
**** Replace missing value of farm_weeks (3 changes)
replace farm_weeks = est_farm_weeks if farm_weeks==.
replace farm_weeks = 1 if farm_weeks==.
* Count observations with farm_days reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_days~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_days~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_days~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_days~=.
by female rural, sort : egen float count_5 = count(1) if farm_days~=.
* Calculate median of farm_days by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_days_1 = median(farm_days) if farm_days~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_days_2 = median(farm_days) if farm_days~=.
by female rural Zone State, sort : egen float temp_est_farm_days_3 = median(farm_days) if farm_days~=.
by female rural Zone, sort : egen float temp_est_farm_days_4 = median(farm_days) if farm_days~=.
by female rural, sort : egen float temp_est_farm_days_5 = median(farm_days) if farm_days~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_days_1 = max(temp_est_farm_days_1)
by female rural Zone State LGA, sort : egen float est_farm_days_2 = max(temp_est_farm_days_2)
by female rural Zone State, sort : egen float est_farm_days_3 = max(temp_est_farm_days_3)
by female rural Zone, sort : egen float est_farm_days_4 = max(temp_est_farm_days_4)
by female rural, sort : egen float est_farm_days_5 = max(temp_est_farm_days_5)
drop temp*
* Build farm_days prediction variable if at least 10 observations of reported farm_days in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_days = est_farm_days_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_days==.
replace est_farm_days = est_farm_days_3 if track==4
replace track = 5 if est_farm_days==.
replace est_farm_days = est_farm_days_5 if track==5
drop est_farm_days_* count_* track
**** Replace missing value of farm_days (0 changes)
replace farm_days = est_farm_days if farm_days==.
replace farm_days = 1 if farm_days==.
* Count observations with farm_hourspd reported by geographic location.
by female rural Zone State LGA EA, sort : egen float count_1 = count(1) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float count_2 = count(1) if farm_hourspd~=.
by female rural Zone State, sort : egen float count_3 = count(1) if farm_hourspd~=.
by female rural Zone, sort : egen float count_4 = count(1) if farm_hourspd~=.
by female rural, sort : egen float count_5 = count(1) if farm_hourspd~=.
* Calculate median of farm_hourspd by geographic location - later used for predictive purposes.
by female rural Zone State LGA EA, sort : egen float temp_est_farm_hourspd_1 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State LGA, sort : egen float temp_est_farm_hourspd_2 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone State, sort : egen float temp_est_farm_hourspd_3 = median(farm_hourspd) if farm_hourspd~=.
by female rural Zone, sort : egen float temp_est_farm_hourspd_4 = median(farm_hourspd) if farm_hourspd~=.
by female rural, sort : egen float temp_est_farm_hourspd_5 = median(farm_hourspd) if farm_hourspd~=.
mvencode temp_*, mv(0)
by female rural Zone State LGA EA, sort : egen float est_farm_hourspd_1 = max(temp_est_farm_hourspd_1)
by female rural Zone State LGA, sort : egen float est_farm_hourspd_2 = max(temp_est_farm_hourspd_2)
by female rural Zone State, sort : egen float est_farm_hourspd_3 = max(temp_est_farm_hourspd_3)
by female rural Zone, sort : egen float est_farm_hourspd_4 = max(temp_est_farm_hourspd_4)
by female rural, sort : egen float est_farm_hourspd_5 = max(temp_est_farm_hourspd_5)
drop temp*
* Build farm_hourspd prediction variable if at least 10 observations of reported farm_hourspd in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_farm_hourspd = est_farm_hourspd_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_3 if track==4
replace track = 5 if est_farm_hourspd==.
replace est_farm_hourspd = est_farm_hourspd_5 if track==5
drop est_farm_hourspd_* count_* track
**** Replace missing value of farm_hourspd (17 changes)
replace farm_hourspd = est_farm_hourspd if farm_hourspd==.
replace farm_hourspd = 1 if farm_hourspd==.

* Calculate Total Hours
gen farm_hours = 0
replace farm_hours = farm_weeks * farm_days * farm_hourspd
replace farm_hours = 0 if farm_hours==.

keep hhid indiv farm_hours

save "clean data/intermediate/Nigeria1516_farm_PoH.dta", replace


* Combine Own Farm Labor
use "clean data/intermediate/Nigeria1516_farm_PP.dta", clear
append using "clean data/intermediate/Nigeria1516_farm_PreH.dta"
append using "clean data/intermediate/Nigeria1516_farm_PoH.dta"

* Own Farming at Member level
gen farm = 1
collapse (max) farm (sum) farm_hours, by (hhid indiv)
drop if farm_hours==0

* Generate Full Time Equivalents
gen farm_fte = farm_hours/2016
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
*** Assume no farmer averages more than 80 hours per week for the entire year
replace farm_fte = 2 if farm_fte>2 & farm_fte~=.
*** 290 changes in 9069 observations
drop farm_hours

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

keep hhid indiv farm_o_c1 farm_f_c1

save "clean data/intermediate/Nigeria1516_farm_occ.dta", replace

**** COMBINE ALL ****
use "clean data/intermediate/Nigeria1516_nfe_occCHEVAL.dta", clear
joinby hhid indiv using "clean data/intermediate/Nigeria1516_wage_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid indiv using "clean data/intermediate/Nigeria1516_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge
* Bring in Demographics
joinby hhid indiv using "clean data/Nigeria1516_member_demos.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode nfe* w* farm*, mv(0)
mvencode nfe* w* farm*, mv(0)

** Generate Grouped Categorical Variables
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}
*

forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}
*farm enterprise
replace o_f2_c1 = o_f2_c1 + farm_o_c1
replace f_f2_c1 = f_f2_c1 + farm_f_c1

* Total (f3) Casual, Formal and Self Employed
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f3_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	
*

gen rur_farm = 1 if rural==1 & o_f2_c1>0
replace rur_farm = 0 if rur_farm==.
by hhid, sort : egen float rural_farm = max(rur_farm)

tostring hhid,replace
joinby using "clean data/Nigeria1516_iid.dta", unmatched(none)

keep hhid iid fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete school_secondary school_level health_insurance pension
gen country="Nigeria"
save "clean data/NIGERIA_FTEsCHEVAL.dta", replace

