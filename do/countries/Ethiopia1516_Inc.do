********************************************************************************
***************** ETHIOPIA - 2015/2016: GENERATE INCOME COMPONENTS *************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)

The dataset used for this is:
Ethiopia 2015/2016 Socioeconomic Survey, Wave 3

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")

Stata Version 15.1

*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "clean data/intermediate/Ethiopia1516_wage_incomeCHEVAL.dta", replace
save "clean data/ETHIOPIA_incomesharesCHEVAL.dta", replace


Informality types:

-	Contract: Has a contract; duration is permanent, fixed term with duration more than 1X?, government
-	Social protection: Pension scheme and/or health insurance through job


*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl ETHIOPIA	"$path\data\Ethiopia_ESS_1516"

gl DO   	    ${ETHIOPIA}/do
gl RAW	  		${ETHIOPIA}/raw data 
gl clean data 	${ETHIOPIA}/clean data

********************************************************************************
cd "$ETHIOPIA"

*** INCOME SHARES OF FARMING AND FARM PRODUCE SALES ***

*get from each module the value of production, sales, own consumption and aggregate at household level:
*** AG-INCOME = crops, crop products, forestry, livestock, livestock by-products
		*reference period for ag. income is past 12 months


** Occupations in Non-Farm Own Enterprises
use "$RAW/Household/sect11b_hh_w3", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
drop if HHID==.
rename hh_s11bq01_b nfe_CIC
rename hh_s11bq09 nfe_months
rename hh_s11bq10 nfe_dayspm
drop if nfe_months==. & nfe_dayspm==.
rename hh_s11bq13 nfe_mo_gross_inc
mvdecode hh_s11bq14_a hh_s11bq14_b hh_s11bq14_c hh_s11bq14_d hh_s11bq14_e, mv(0)
mvencode hh_s11bq14_a hh_s11bq14_b hh_s11bq14_c hh_s11bq14_d hh_s11bq14_e, mv(0)
egen float nfe_mo_costs = rowtotal(hh_s11bq14_a hh_s11bq14_b hh_s11bq14_c hh_s11bq14_d hh_s11bq14_e)
gen nfe_mo_net_income = nfe_mo_gross_inc - nfe_mo_costs
gen nfe_ann_costs = nfe_mo_costs*nfe_months
gen nfe_ann_gross_income = nfe_mo_gross_inc*nfe_months
gen nfe_ann_net_income = nfe_mo_net_inc*nfe_months
gen nfe_costs = nfe_ann_costs/365
gen nfe_gross_inc = nfe_ann_gross_income/365
gen nfe_net_inc = nfe_ann_net_income/365
drop if nfe_net_inc==.

keep region zone ward town subcity FA ea household_id2 HHID nfe_CIC nfe_gross_inc nfe_costs nfe_net_inc nfe_months
order region zone ward town subcity FA ea household_id2 HHID nfe_CIC nfe_gross_inc nfe_costs nfe_net_inc nfe_months

* Collapse to HH / CIC level
replace nfe_CIC = 99999 if nfe_CIC==.

* Bring in information for ISIC Groupings
rename nfe_CIC CIC
joinby using "clean data/Ethiopia1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC nfe_CIC
joinby household_id2 HHID region zone ward town subcity FA ea using "clean data/Ethiopia1516_demographics.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
***********************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
***********************************************************
replace AFS_share = 0.722 if AFS_share==. & rural==1
replace AFS_share = 0.683 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612

* Keep relevant variables
order region zone ward town subcity FA ea household_id2 HHID nfe_CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*
keep region zone ward town subcity FA ea household_id2 HHID nfe_CIC ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*, by (region zone ward town subcity FA ea household_id2 HHID)

* Drop 5 observations with zero earning
drop if nfe_gross_inc==0 & nfe_costs==0 & nfe_net_inc==0

save "clean data/intermediate/Ethiopia1516_nfe_inc.dta", replace


** Occupation by Wages 
* Transpose data so that 2 Wage occupations are in one set of variables
use "$RAW/Household/sect4_hh_w3.dta", clear

*add: dummies for different activities:
g t_PSNP  =(hh_s4q31==1)  
g t_wagepaid	=	((hh_s4q07>=1 & hh_s4q07!=.) | (hh_s4q06>=1 & hh_s4q06!=.) ) 
g t_nonfarmbiz	= 	(hh_s4q05>=1 & hh_s4q05!=.)		
g t_farmbiz 	=	(hh_s4q04>=1 & hh_s4q04!=. )					
g t_wageunpaid	=	((hh_s4q08>=1 & hh_s4q08!=. ) | hh_s4q37==1) 
g t_any_w_paidunpaid = (t_wagepaid==1 | t_wageunpaid==1) 

* Wage 1
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id
rename hh_s4q11_b wage_CIC
rename hh_s4q12 wage_employer
rename hh_s4q16 wage_payment
rename hh_s4q17 wage_payment_duration
rename hh_s4q18 wage_inkind
rename hh_s4q19 wage_inkind_duration
rename hh_s4q13 wage_months
rename hh_s4q14 wage_weeks
rename hh_s4q15 wage_hourspw
mvdecode wage*, mv(0)
drop if wage_months==. & wage_weeks==. & wage_hourspw==. & wage_payment==. & wage_inkind==.
order HHID household_id2 region zone ward town subcity FA ea member_id wage_* t_* 
keep HHID household_id2 region zone ward town subcity FA ea member_id wage_* t_* 
gen wage_job = 1 
save "clean data/intermediate/Ethiopia1516_temp_wage_1.dta", replace

* Wage 2
use "$RAW/Household/sect4_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id
rename hh_s4q22_b wage_CIC
rename hh_s4q23 wage_employer
rename hh_s4q27 wage_payment
rename hh_s4q28 wage_payment_duration
rename hh_s4q28 wage_inkind
rename hh_s4q30 wage_inkind_duration
rename hh_s4q24 wage_months
rename hh_s4q25 wage_weeks
rename hh_s4q26 wage_hourspw
mvdecode wage*, mv(0)
drop if wage_months==. & wage_weeks==. & wage_hourspw==. & wage_payment==. & wage_inkind==.
order HHID household_id2 region zone ward town subcity FA ea member_id wage_*
keep HHID household_id2 region zone ward town subcity FA ea member_id wage_*
gen wage_job = 2 
save "clean data/intermediate/Ethiopia1516_temp_wage_2.dta", replace

use "clean data/intermediate/Ethiopia1516_temp_wage_1.dta", clear
append using "clean data/intermediate/Ethiopia1516_temp_wage_2.dta"

* Clear Errors in data
* No occupation code, but reported earnings. 
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=.
* Only 12 months in a year (1 change)
replace wage_months = . if wage_months>12
* Only 4(5) weeks in a month (2 changes)
replace wage_weeks = . if wage_weeks>5
**** Assume that reported hours per day above 16 hours per day are in error (affects calculations of 42 of 1447 observations)
replace wage_hourspw = . if wage_hourspw>80
**** Assume if work during a month, the employee worked at least one hour in one day
**** If worked months then assume to have worked days
**** Use median of months in wage job of those in the smallest geographic region with 10 observations of monthly wage labor
joinby using "clean data/Ethiopia1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_months~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_months~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_months~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_months_6 = median(wage_months) if wage_months~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_months_7 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_8 = median(wage_months) if wage_months~=.
by wage_CIC female, sort : egen float temp_est_wage_months_9 = median(wage_months) if wage_months~=.
by wage_CIC, sort : egen float temp_est_wage_months_10 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
by wage_CIC female rural region zone, sort : egen float est_wage_months_6 = max(temp_est_wage_months_6)
by wage_CIC female rural region, sort : egen float est_wage_months_7 = max(temp_est_wage_months_7)
by wage_CIC female rural, sort : egen float est_wage_months_8 = max(temp_est_wage_months_8)
by wage_CIC female, sort : egen float est_wage_months_9 = max(temp_est_wage_months_9)
by wage_CIC, sort : egen float est_wage_months_10 = max(temp_est_wage_months_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_9 if track==9
replace track = 10 if est_wage_months==.
replace est_wage_months = est_wage_months_10 if track==10
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (2, 1 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_weeks~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_weeks~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_weeks_6 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_weeks_7 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_8 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female, sort : egen float temp_est_wage_weeks_9 = median(wage_weeks) if wage_weeks~=.
by wage_CIC, sort : egen float temp_est_wage_weeks_10 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
by wage_CIC female rural region zone, sort : egen float est_wage_weeks_6 = max(temp_est_wage_weeks_6)
by wage_CIC female rural region, sort : egen float est_wage_weeks_7 = max(temp_est_wage_weeks_7)
by wage_CIC female rural, sort : egen float est_wage_weeks_8 = max(temp_est_wage_weeks_8)
by wage_CIC female, sort : egen float est_wage_weeks_9 = max(temp_est_wage_weeks_9)
by wage_CIC, sort : egen float est_wage_weeks_10 = max(temp_est_wage_weeks_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_9 if track==9
replace track = 10 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_10 if track==10
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (7, 1 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward town, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone ward, sort : egen float count_5 = count(1) if wage_hourspw~=.
by wage_CIC female rural region zone, sort : egen float count_6 = count(1) if wage_hourspw~=.
by wage_CIC female rural region, sort : egen float count_7 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_8 = count(1) if wage_hourspw~=.
by wage_CIC female, sort : egen float count_9 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity FA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town subcity, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward town, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone ward, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region zone, sort : egen float temp_est_wage_hourspw_6 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural region, sort : egen float temp_est_wage_hourspw_7 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_8 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female, sort : egen float temp_est_wage_hourspw_9 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC, sort : egen float temp_est_wage_hourspw_10 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural region zone ward town subcity FA ea, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural region zone ward town subcity FA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural region zone ward town subcity, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural region zone ward town, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural region zone ward, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
by wage_CIC female rural region zone, sort : egen float est_wage_hourspw_6 = max(temp_est_wage_hourspw_6)
by wage_CIC female rural region, sort : egen float est_wage_hourspw_7 = max(temp_est_wage_hourspw_7)
by wage_CIC female rural, sort : egen float est_wage_hourspw_8 = max(temp_est_wage_hourspw_8)
by wage_CIC female, sort : egen float est_wage_hourspw_9 = max(temp_est_wage_hourspw_9)
by wage_CIC, sort : egen float est_wage_hourspw_10 = max(temp_est_wage_hourspw_10)
mvdecode est_*, mv(0)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_7 if track==7
replace track = 8 if count_8>=10 & count_8<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_8 if track==8
replace track = 9 if count_9>=10 & count_9<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_9 if track==9
replace track = 10 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_10 if track==10
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (46, 1 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.
drop est_*

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hours)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hours/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
* Payment in kind
* Duration in hours
replace wage_inkind = ((wage_inkind*wage_hours)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_inkind = ((wage_inkind*est_wage_days)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_inkind_duration==2
* Duration in weeks
replace wage_inkind = wage_inkind*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_inkind_duration==3
* Duration in fortnights
replace wage_inkind = wage_inkind*(1/2)*(1/7)*(wage_weeks/4.3)*(wage_months/12) if wage_inkind_duration==4
* Duration in months
replace wage_inkind = wage_inkind*(1/30.4)*(wage_months/12) if wage_inkind_duration==5
* Duration in quarters
replace wage_inkind = wage_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_inkind_duration==6
* Duration in half years
replace wage_inkind = wage_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_inkind_duration==7
* Duration in years
replace wage_inkind = wage_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_inkind_duration==8

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_inkind if wage_inkind~=.
drop if wage_income==0

* Formal Wage only possible definition in Ethiopian case!
gen formal = 0
replace formal = 1 if wage_payment_duration>=5 & wage_payment_duration<.
replace formal = 1 if wage_inkind_duration>=5 & wage_inkind_duration<.
replace formal = 1 if wage_employer==3 | wage_employer==4 | wage_employer==6 | wage_employer==7

* Formal Wage contract status only possible definition in Ethiopian case!
gen formalcon = 0
replace formalcon = 1 if wage_payment_duration>=5 & wage_payment_duration<.
replace formalcon = 1 if wage_inkind_duration>=5 & wage_inkind_duration<.

* added definition of "formality" due to SP(!)
g formalsp = (wage_employer==3 | wage_employer==4 | wage_employer==6 | wage_employer==7) // if employer is government, state-owned firm, NGO/charity or political party, employment comes with social protection

* Bring in information for ISIC Groupings
replace wage_CIC = 99999 if wage_CIC==.
rename wage_CIC CIC
joinby using "clean data/Ethiopia1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
******************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
forvalues w=1(1)2 {
	replace AFS_share = 0.722 if AFS_share==. & rural==1
	replace AFS_share = 0.683 if AFS_share==. & rural==0
}

** Generate Categorical Variables
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen wage_i_f`f'_c`c' = 0
	gen wage_i_fcon`f'_c`c' = 0 //(!)
	gen wage_i_fsp`f'_c`c' = 0 //(!)
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
	replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f' //(!)
	replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f' //(!)
}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'
	*(!)
	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	*(!)
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}
*

*define informality
g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0) 

// the share of inf work in agri & share of inf work non agri
g informal_agri=(informal==1 & CIC<=2)				
g informal_nonagri=(informal==1 & informal_agri==0)		
g formal_agri=(informal==0& CIC<=2) 
g formal_nonagri=(informal==0& formal_agri==0) 

* keep only for those 16 and older: (!)
keep if age>=16

* Keep relevant variables
order HHID household_id2 rural region zone ward town subcity FA ea CIC ISIC_group wage_income wage_i_* wage_job t_* formal* informal*  
keep HHID household_id2 rural region zone ward town subcity FA ea CIC ISIC_group wage_income wage_i_* wage_job t_* formal* informal*  

tab wage_job,g(job) // do you have a wage job yes or no?

* Collapse to household level
collapse (sum) wage_income wage_i_* job1 job2 ///
Ninformal_agri=informal_agri Ninformal_nonagri=informal_nonagri Nformal_agri=formal_agri Nformal_nonagri=formal_nonagri Ninformal=informal ///
  (mean) t_* informal* formal* ///
 , by (HHID household_id2 rural region zone ward town subcity FA ea)

save "clean data/intermediate/Ethiopia1516_wage_incomeCHEVAL.dta", replace


** Public Safety Net Program & Other Informal Labor
use "$RAW/Household/sect4_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s4q00 member_id

rename hh_s4q32 psnp_days
rename hh_s4q33 psnp_ann_income
rename hh_s4q35 casual_days
rename hh_s4q36 casual_ann_income

* Keep relevant data
keep HHID household_id2 region zone ward town subcity FA ea member_id psnp_* casual_*

* Convert to daily income
gen psnp_income = psnp_ann_income/365
gen casual_income = casual_ann_income/365

mvdecode psnp_income casual_income, mv(0)
mvencode psnp_income casual_income, mv(0)

drop if psnp_income==0 & casual_income==0

* Bring in Member Demographic Info
joinby HHID household_id2 region zone ward town subcity FA ea using "clean data/Ethiopia1516_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

*****
* Do not know the details of the type of work with either PSNP labor or Casual labor. Both considered CIC 612.
* Four mixed CIC Groups
gen AFS_share = .
replace AFS_share = 0.722 if rural==1
replace AFS_share = 0.683 if rural==0

local other "psnp casual"
foreach o in `other' {
	gen `o'_i_f0_c6 =.
	gen `o'_i_f0_c12 = .
}
*
local other "psnp casual"
foreach o in `other' {
	replace `o'_i_f0_c6 = AFS_share*`o'_income
	replace `o'_i_f0_c12 = (1-AFS_share)*`o'_income
}
*

keep HHID household_id2 region zone ward town subcity FA ea member_id psnp_i* casual_i*

collapse (sum) psnp_i* casual_i*, by (HHID household_id2 region zone ward town subcity FA ea)

save "clean data/intermediate/Ethiopia1516_other_w_inc.dta", replace

** Own Farm Employment
* Post Planting provides estimates of future while Post Harvest provide sales and production. Used Post Harvest.

* Price estimates to be built by sales data
* Crop Production
use "$RAW/Post-Harvest/sect11_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s11q03_a sales_qty
rename ph_s11q03_b unit_cd
rename ph_s11q04 sales_value

keep rural region zone ward FA ea HHID household_id2 crop_name crop_code sales_qty unit_cd sales_value
drop if sales_qty==0 | sales_qty==. | sales_value==0 | sales_value==. | unit_cd==900

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace sales_qty = sales_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.

* revealed prices
gen revealed_price = sales_value / sales_qty

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone, sort : egen float count_4 = count(1) if revealed_price~=.
by crop_code unit_cd rural region, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code unit_cd rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code unit_cd rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code unit_cd rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code unit_cd rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code unit_cd rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code unit_cd, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

keep rural region zone ward FA ea crop_name crop_code unit_cd predict_price

collapse (median) predict_price, by (rural region zone ward FA ea crop_code unit_cd)

save "clean data/intermediate/Ethiopia1516_crop_price_a.dta", replace

* Crop Sales
use "$RAW/Post-Harvest/sect11_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s11q03_a sales_qty
rename ph_s11q03_b unit_cd

keep rural region zone ward FA ea HHID household_id2 crop_name crop_code sales_qty unit_cd
drop if sales_qty==0 | sales_qty==.

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace sales_qty = sales_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.

* Bring in Prices
joinby using "clean data/intermediate/Ethiopia1516_crop_price_a.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Calculate Crop Sales Value
gen crop_sales_value = sales_qty * predict_price

* Collapse to Household Level
collapse (sum) crop_sales_value, by (rural region zone ward FA ea household_id2 HHID)

replace crop_sales_value = crop_sales_value/365

save "clean data/intermediate/Ethiopia1516_crop_sales_value.dta", replace


* Price estimates to be built by sales data
* Permanent Crop Production
use "$RAW/Post-Harvest/sect12_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s12q07 sales_qty
rename ph_s12q0b unit_cd
rename ph_s12q08 sales_value

keep rural region zone ward FA ea household_id2 HHID crop_name crop_code sales_qty unit_cd sales_value
drop if sales_qty==0 | sales_qty==. | sales_value==0 | sales_value==. | unit_cd==900

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace sales_qty = sales_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.

* revealed prices
gen revealed_price = sales_value / sales_qty

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_code unit_cd rural region zone, sort : egen float count_4 = count(1) if revealed_price~=.
by crop_code unit_cd rural region, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd rural region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by crop_code unit_cd, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code unit_cd rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code unit_cd rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code unit_cd rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code unit_cd rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code unit_cd rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code unit_cd, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

keep rural region zone ward FA ea crop_name crop_code unit_cd predict_price

collapse (median) predict_price, by (rural region zone ward FA ea crop_code unit_cd)

save "clean data/intermediate/Ethiopia1516_crop_price_perm.dta", replace

* Permanent Crop Sales
use "$RAW/Post-Harvest/sect12_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s12q07 sales_qty
rename ph_s12q0b unit_cd

keep rural region zone ward FA ea household_id2 HHID crop_name crop_code sales_qty unit_cd
drop if sales_qty==0 | sales_qty==.

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace sales_qty = sales_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.

* Bring in Prices
joinby using "clean data/intermediate/Ethiopia1516_crop_price_perm.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Calculate Permanent Crop Sales Value
gen perm_crop_sales_value = sales_qty * predict_price

* Collapse to Household Level
collapse (sum) perm_crop_sales_value, by (rural region zone ward FA ea household_id2 HHID)

replace perm_crop_sales_value = perm_crop_sales_value/365

save "clean data/intermediate/Ethiopia1516_perm_crop_sales_value.dta", replace


* Crop Harvest (Including Trees and roots)
use "$RAW/Post-Harvest/sect9_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s9q04_a harvest_qty
rename ph_s9q04_b unit_cd

keep rural region zone ward FA ea household_id2 HHID crop_code harvest_qty unit_cd parcel_id
drop if harvest_qty==. | unit_cd==900

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace harvest_qty = harvest_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.

collapse (sum) harvest_qty, by (rural region zone ward FA ea household_id2 HHID crop_code unit_cd parcel_id)

save "clean data/intermediate/Ethiopia1516_crop_harvest_for_rent_temp.dta", replace

* Collapse to Household Level
collapse (sum) harvest_qty, by (rural region zone ward FA ea household_id2 HHID crop_code unit_cd)

save "clean data/intermediate/Ethiopia1516_crop_harvest_temp.dta", replace

* Crop Post Harvest Loss By quantity
use "$RAW/Post-Harvest/sect11_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s11q15_1 loss_qty
rename ph_s11q15_2 unit_cd
rename ph_s11q15_4 loss_percent
drop if loss_percent~=0

keep rural region zone ward FA ea household_id2 HHID crop_code loss_qty unit_cd

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace loss_qty = loss_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.
keep if unit_cd~=.

collapse (sum) loss_qty, by (rural region zone ward FA ea household_id2 HHID crop_code unit_cd)

save "clean data/intermediate/Ethiopia1516_crop_harvest_loss_qty.dta", replace

* Crop Post Harvest Loss By Percent
use "$RAW/Post-Harvest/sect11_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s11q15_4 loss_percent

keep rural region zone ward FA ea household_id2 HHID crop_code loss_percent

collapse (max) loss_percent, by (rural region zone ward FA ea household_id2 HHID crop_code)

save "clean data/intermediate/Ethiopia1516_crop_harvest_loss_percent.dta", replace

use "clean data/intermediate/Ethiopia1516_crop_harvest_temp.dta", clear
joinby using "clean data/intermediate/Ethiopia1516_crop_harvest_loss_percent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby using "clean data/intermediate/Ethiopia1516_crop_harvest_loss_qty.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Adjust for Post Harvest Loss
replace harvest_qty = harvest_qty - loss_qty if loss_qty~=.
replace harvest_qty = 0 if harvest_qty<0
mvdecode loss_percent, mv(0)
mvencode loss_percent, mv(0)
replace harvest_qty = harvest_qty * (1-(loss_percent/100))

joinby using "clean data/intermediate/Ethiopia1516_crop_price_a.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename predict_price predict_price_a
joinby using "clean data/intermediate/Ethiopia1516_crop_price_perm.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

replace predict_price = predict_price_a if predict_price_a~=.
drop predict_price_a

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float count_1 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float count_2 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float count_3 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float count_4 = count(1) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by crop_code unit_cd, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code unit_cd rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code unit_cd rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code unit_cd rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code unit_cd rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code unit_cd rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code unit_cd, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*
*** 867 / 15783 do not have a price, therefore do not have value

* Calculate Crop Sales Value
gen crop_harvest_value = harvest_qty * predict_price
replace crop_harvest_value = 0 if crop_harvest_value==.

collapse (sum) crop_harvest_value , by (rural region zone ward FA ea household_id2 HHID)

replace crop_harvest_value = crop_harvest_value/365

save "clean data/intermediate/Ethiopia1516_crop_harvest.dta", replace

* Harvest of Permanent Crops (not above)
use "$RAW/Post-Harvest/sect12_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s12q03_a harvest_qty
rename ph_s12q03_b unit_cd

keep rural region zone ward FA ea household_id2 HHID crop_code harvest_qty unit_cd
drop if harvest_qty==. | unit_cd==900

* Bring in conversion codes
joinby crop_code unit_cd using "$RAW/Food and Crop Conversion Factors/Crop_CF_Wave3.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
gen conversion = .
replace conversion = mean_cf1 if region==1 & mean_cf1~=.
replace conversion = mean_cf2 if region==2 & mean_cf2~=.
replace conversion = mean_cf3 if region==3 & mean_cf3~=.
replace conversion = mean_cf4 if region==4 & mean_cf4~=.
replace conversion = mean_cf99 if region==5 & mean_cf99~=.
replace conversion = mean_cf6 if region==6 & mean_cf6~=.
replace conversion = mean_cf7 if region==7 & mean_cf7~=.
replace conversion = mean_cf12 if region==8 & mean_cf12~=.
replace conversion = mean_cf99 if region==9 & mean_cf99~=.
replace conversion = mean_cf99 if region==10 & mean_cf99~=.
replace conversion = mean_cf_nat if conversion==. & mean_cf_nat~=.

replace harvest_qty = harvest_qty*conversion if conversion~=.
replace unit_cd = 1 if conversion~=.
drop if unit_cd==.

* Collapse to Household Level
collapse (sum) harvest_qty, by (rural region zone ward FA ea household_id2 HHID crop_code unit_cd)

save "clean data/intermediate/Ethiopia1516_perm_crop_harvest_temp.dta", replace

* Perm Crop Post Harvest Loss By Percent
use "$RAW/Post-Harvest/sect12_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s12q12 loss_percent

keep rural region zone ward FA ea household_id2 HHID crop_code loss_percent

collapse (max) loss_percent, by (rural region zone ward FA ea household_id2 HHID crop_code)

save "clean data/intermediate/Ethiopia1516_perm_crop_harvest_loss_percent.dta", replace

use "clean data/intermediate/Ethiopia1516_perm_crop_harvest_temp.dta", clear
joinby using "clean data/intermediate/Ethiopia1516_crop_harvest_loss_percent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Adjust for Post Harvest Loss
mvdecode loss_percent, mv(0)
mvencode loss_percent, mv(0)
replace harvest_qty = harvest_qty * (1-(loss_percent/100))

joinby using "clean data/intermediate/Ethiopia1516_crop_price_perm.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename predict_price predict_price_a
joinby using "clean data/intermediate/Ethiopia1516_crop_price_a.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

replace predict_price = predict_price_a if predict_price_a~=.
drop predict_price_a

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float count_1 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float count_2 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float count_3 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float count_4 = count(1) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by crop_code unit_cd, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code unit_cd rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code unit_cd rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code unit_cd rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code unit_cd rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code unit_cd rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code unit_cd, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*
*** 501 / 6960 do not have a price, therefore do not have value

* Calculate Crop Sales Value
gen perm_crop_harvest_value = harvest_qty * predict_price
replace perm_crop_harvest_value = 0 if perm_crop_harvest_value==.

collapse (sum) perm_crop_harvest_value , by (rural region zone ward FA ea household_id2 HHID)

replace perm_crop_harvest_value = perm_crop_harvest_value/365

save "clean data/intermediate/Ethiopia1516_perm_crop_harvest.dta", replace

** Livestock
* General Livestock
use "$RAW/Livestock/sect8_2_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename ls_code livestock_code
rename ls_sec_8_2aq09 livestock_givenaway
rename ls_sec_8_2aq13 livestock_sold
rename ls_sec_8_2aq14 livestock_sold_value
rename ls_sec_8_2aq16 livestock_slaughtered
rename ls_sec_8_2aq18 livestock_slaughtered_value
keep rural region zone ward FA ea household_id2 HHID livestock*
drop if livestock_givenaway==. & livestock_sold==. & livestock_sold_value==. & livestock_slaughtered==. & livestock_slaughtered_value==.
* No animal is sold for less than $0.05, so we know these observations are erroneous.
drop if livestock_sold > livestock_sold_value
drop if livestock_slaughtered > livestock_slaughtered_value

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_sold_value / livestock_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by livestock_code rural region zone ward FA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_code rural region zone ward FA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_code rural region zone ward, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_code rural region zone, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_code rural region, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by livestock_code rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone ward FA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_code rural region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by livestock_code, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by livestock_code rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_code rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_code rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_code rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_code rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by livestock_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

mvencode livestock_*, mv(0)
gen livestock_to_income = livestock_givenaway+livestock_sold+livestock_slaughtered

gen livestock_income_yr = livestock_to_income*predict_price
****
**** Do not know the number of slaughtered that were sold, so have to take the slaughtered value as given
gen livestock_sold_yr = livestock_sold*predict_price+livestock_slaughtered_value

keep region zone ward FA ea HHID household_id2 livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (region zone ward FA ea household_id2 HHID)

joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea HHID household_id2 livestock*

save "clean data/intermediate/Ethiopia1516_livestock_general.dta", replace


* Milk Income from Livestock
use "$RAW/Livestock/sect8_6_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename ls_code livestock_code
rename ls_sec_8_6aq02 livestock_months_milking
rename ls_sec_8_6aq04 livestock_lt_perday
rename ls_sec_8_6aq09 livestock_lt_sold
rename ls_sec_8_6aq10 livestock_lt_sold_value
rename ls_sec_8_6aq12 livestock_gross_rev_milk_prod
keep rural region zone ward FA ea household_id2 HHID livestock*
drop if livestock_months_milking==. & livestock_lt_perday==. & livestock_lt_sold==. & livestock_lt_sold_value==. & livestock_gross_rev_milk_prod==.
* Cannot average more than 25 liters per day of milk production
drop if livestock_lt_perday > 25

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_lt_sold_value / livestock_lt_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by livestock_code rural region zone ward FA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_code rural region zone ward FA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_code rural region zone ward, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_code rural region zone, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_code rural region, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by livestock_code rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone ward FA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_code rural region zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_code rural region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
by livestock_code, sort : egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by livestock_code rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_code rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_code rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_code rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_code rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by livestock_code, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

mvdecode livestock_*, mv(0)
mvencode livestock_*, mv(0)

gen livestock_milk_income_yr = livestock_months_milking*livestock_lt_perday*30.4*predict_price
gen livestock_milk_sold_yr = livestock_lt_sold*4.3*livestock_months_milking*predict_price
gen livestock_milk_nfe_gross_yr = livestock_gross_rev_milk_prod*52

keep region zone ward FA ea HHID household_id2 livestock_milk_income_yr livestock_milk_sold_yr livestock_milk_nfe_gross_yr

gen livestock_milk_income = livestock_milk_income_yr/365
gen livestock_milk_sold = livestock_milk_sold_yr/365
gen livestock_milk_nfe_gross = livestock_milk_nfe_gross_yr/365

collapse (sum) livestock_*, by (region zone ward FA ea household_id2 HHID)

joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea household_id2 HHID livestock*

save "clean data/intermediate/Ethiopia1516_livestock_milk.dta", replace

* Egg Income from Livestock
use "$RAW/Livestock/sect8_6_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename ls_sec_8_6bq14 livestock_clutching_periods 
rename ls_sec_8_6bq15 livestock_eggs_clutching_period 
rename ls_sec_8_6bq16 livestock_layers 
rename ls_sec_8_6bq18 livestock_eggs_sold
rename ls_sec_8_6bq19 livestock_eggs_sold_value
keep rural region zone ward FA ea household_id2 HHID livestock*
drop if livestock_clutching_periods==. & livestock_eggs_clutching_period==. & livestock_layers==. & livestock_eggs_sold==. & livestock_eggs_sold_value==.

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_eggs_sold_value / livestock_eggs_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible

* Count observations with calculated prices by geographic location.
by rural region zone ward FA ea, sort : egen float count_1 = count(1) if revealed_price~=.
by rural region zone ward FA, sort : egen float count_2 = count(1) if revealed_price~=.
by rural region zone ward, sort : egen float count_3 = count(1) if revealed_price~=.
by rural region zone, sort : egen float count_4 = count(1) if revealed_price~=.
by rural region, sort : egen float count_5 = count(1) if revealed_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by rural region zone ward FA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by rural region zone ward, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by rural region zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by rural region, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
egen float temp_predict_price_6 = median(revealed_price) if revealed_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6
drop predict_price_* count_* revealed_price

mvdecode livestock_*, mv(0)
mvencode livestock_*, mv(0)

gen livestock_egg_income_yr = livestock_clutching_periods*livestock_eggs_clutching_period*livestock_layers*predict_price
gen livestock_egg_sold_yr = livestock_eggs_sold*predict_price
keep region zone ward FA ea HHID household_id2 livestock_egg_income_yr livestock_egg_sold_yr

gen livestock_egg_income = livestock_egg_income_yr/365
gen livestock_egg_sold = livestock_egg_sold_yr/365

collapse (sum) livestock_*, by (region zone ward FA ea household_id2 HHID)

joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea household_id2 HHID livestock*

save "clean data/intermediate/Ethiopia1516_livestock_egg.dta", replace

* Livestock other income
*Services
use "$RAW/Livestock/sect8_7a_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename ls_sec_8_7q04 livestock_services_yr
mvdecode livestock_services_yr, mv(0)
mvencode livestock_services_yr, mv(0)
drop if livestock_services_yr==0
collapse (sum) livestock_services_yr, by (region zone ward FA ea household_id2 HHID)

gen livestock_services = livestock_services_yr/365

joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea household_id2 HHID livestock*

save "clean data/intermediate/Ethiopia1516_livestock_services.dta", replace

* Dung
use "$RAW/Livestock/sect8_7b_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename ls_sec_8_7q08 livestock_dung_yr
mvdecode livestock_dung_yr, mv(0)
mvencode livestock_dung_yr, mv(0)
drop if livestock_dung_yr==0
collapse (sum) livestock_dung_yr, by (region zone ward FA ea household_id2 HHID)

gen livestock_dung = livestock_dung_yr/365

joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea household_id2 HHID livestock*

save "clean data/intermediate/Ethiopia1516_livestock_dung.dta", replace

* Other Income (includes rental income)
use "$RAW/Household/sect12_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename hh_s12q02 other_income_source
replace other_income = 0 if other_income_source==.

gen d_remittances = 1 if hh_s12q00==101 & other_income_source>0
replace d_remittances = 0 if d_remittances==.

keep region zone ward town subcity FA ea household_id2 HHID other_income_source d_remittances

collapse (sum) other_income_source (max) d_remittances, by (region zone ward town subcity FA ea household_id2 HHID)
drop if other_income_source==0

replace other_income_source = other_income_source/365

save "clean data/intermediate/Ethiopia1516_other_income.dta", replace

* Assistance
use "$RAW/Household/sect13_hh_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID

gen assistance = 0
replace assistance = assistance + hh_s13q03 if hh_s13q03~=.
replace assistance = assistance + hh_s13q04 if hh_s13q04~=.
replace assistance = assistance + hh_s13q05 if hh_s13q05~=.

collapse (sum) assistance, by (region zone ward town subcity FA ea household_id2 HHID)
drop if assistance==0

replace assistance = assistance/365

save "clean data/intermediate/Ethiopia1516_assistance_income.dta", replace


* Costs
* Seed
use "$RAW/Post-Planting/sect4_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename pp_s4q11c cost_seed
collapse (sum) cost_seed, by (rural region zone ward FA ea household_id2 HHID)
save "clean data/intermediate/Ethiopia1516_seed_cost_1.dta", replace

use "$RAW/Post-Planting/sect5_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
mvdecode pp_s5q07 pp_s5q08 pp_s5q16, mv(0)
mvencode pp_s5q07 pp_s5q08 pp_s5q16, mv(0)
gen cost_seed = pp_s5q07 + pp_s5q08 + pp_s5q16
collapse (sum) cost_seed, by (rural region zone ward FA ea household_id2 HHID)
drop if cost_seed==0
save "clean data/intermediate/Ethiopia1516_seed_cost_2.dta", replace

* Fertilizer
use "$RAW/Post-Planting/sect3_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
mvencode pp_s3q16d pp_s3q19d pp_s3q20c, mv(0)
gen cost_fertilizer = pp_s3q16d + pp_s3q19d + pp_s3q20c
collapse (sum) cost_fertilizer, by (rural region zone ward FA ea household_id2 HHID)
save "clean data/intermediate/Ethiopia1516_fertilizer_cost.dta", replace

* Crop Labor
use "$RAW/Post-Planting/sect3_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
gen labor_men = pp_s3q28_a * pp_s3q28_b * pp_s3q28_c
gen labor_women = pp_s3q28_d * pp_s3q28_e * pp_s3q28_f
gen labor_children = pp_s3q28_g * pp_s3q28_h * pp_s3q28_i
mvdecode labor_*, mv(0)
mvencode labor_*, mv(0)
gen cost_labor = labor_men + labor_women + labor_children
collapse (sum) cost_labor, by (rural region zone ward FA ea household_id2 HHID)
save "clean data/intermediate/Ethiopia1516_labor_cost_1.dta", replace

use "$RAW/Post-Harvest/sect10_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
gen labor_men = ph_s10q01_a * ph_s10q01_b * ph_s10q01_c
gen labor_women = ph_s10q01_d * ph_s10q01_e * ph_s10q01_f
gen labor_children = ph_s10q01_g * ph_s10q01_h * ph_s10q01_i
mvdecode labor_*, mv(0)
mvencode labor_*, mv(0)
gen cost_labor = labor_men + labor_women + labor_children
collapse (sum) cost_labor, by (rural region zone ward FA ea household_id2 HHID)
save "clean data/intermediate/Ethiopia1516_labor_cost_2.dta", replace

* Animal/Fish Labor
* NA

* Animal/Fish fuel and transport
*NA

* Livestock Feed
use "$RAW/Livestock/sect8_4_ls_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
mvdecode ls_sec_8_4q05 ls_sec_8_4q08, mv(0)
mvencode ls_sec_8_4q05 ls_sec_8_4q08, mv(0)
gen cost_feed = ls_sec_8_4q05 + ls_sec_8_4q08
collapse (sum) cost_feed, by (rural region zone ward FA ea household_id2 HHID)
save "clean data/intermediate/Ethiopia1516_feed_cost.dta", replace

* Land Rent (first in provided values, then in paid as a percent of harvest)
use "$RAW/Post-Planting/sect2_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
mvdecode pp_s2q07_a pp_s2q07_b, mv(0)
mvencode pp_s2q07_a pp_s2q07_b, mv(0)
gen cost_land_rent = pp_s2q07_a + pp_s2q07_b
collapse (sum) cost_land_rent, by (rural region zone ward FA ea household_id2 HHID)
drop if cost_land_rent==0
save "clean data/intermediate/Ethiopia1516_land_rent_cost_1.dta", replace

use "$RAW/Post-Planting/sect2_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
mvdecode pp_s2q07_c, mv(0)
mvencode pp_s2q07_c, mv(0)
rename pp_s2q07_c share_to_rent
* any values above 100 are not logical
replace share_to_rent = 0 if share_to_rent>100
drop if share_to_rent==0
keep rural region zone ward FA ea household_id2 HHID parcel_id share_to_rent
joinby using "clean data/intermediate/Ethiopia1516_crop_harvest_for_rent_temp.dta", unmatched(none)
replace harvest_qty = harvest_qty*(share_to_rent/100)
collapse (sum) harvest_qty, by (rural region zone ward FA ea household_id2 HHID crop_code unit_cd)
joinby using "clean data/intermediate/Ethiopia1516_crop_price_a.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename predict_price predict_price_a
joinby using "clean data/intermediate/Ethiopia1516_crop_price_perm.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

replace predict_price = predict_price_a if predict_price_a~=.
drop predict_price_a

* Apply prices to smallest geographic region possible
* Count observations with calculated prices by geographic location.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float count_1 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float count_2 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float count_3 = count(1) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float count_4 = count(1) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float count_5 = count(1) if predict_price~=.

* Calculate median of calculated prices by geographic location - later used for predictive purposes.
by crop_code unit_cd rural region zone ward FA ea, sort : egen float temp_predict_price_1 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward FA, sort : egen float temp_predict_price_2 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone ward, sort : egen float temp_predict_price_3 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region zone, sort : egen float temp_predict_price_4 = median(predict_price) if predict_price~=.
by crop_code unit_cd rural region, sort : egen float temp_predict_price_5 = median(predict_price) if predict_price~=.
by crop_code unit_cd, sort : egen float temp_predict_price_6 = median(predict_price) if predict_price~=.
mvencode temp_predict_price_1 temp_predict_price_2 temp_predict_price_3 temp_predict_price_4 temp_predict_price_5 temp_predict_price_6, mv(0)

by crop_code unit_cd rural region zone ward FA ea, sort : egen float predict_price_1 = max(temp_predict_price_1)
by crop_code unit_cd rural region zone ward FA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by crop_code unit_cd rural region zone ward, sort : egen float predict_price_3 = max(temp_predict_price_3)
by crop_code unit_cd rural region zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by crop_code unit_cd rural region, sort : egen float predict_price_5 = max(temp_predict_price_5)
by crop_code unit_cd, sort : egen float predict_price_6 = max(temp_predict_price_6)
drop temp*
mvdecode predict_price_*, mv(0)

* Build price prediction variable if at least 10 observations of purchased price in location counted above.
*	Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
replace predict_price = predict_price_1 if track==1 & predict_price==.
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2 & predict_price==.
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3 & predict_price==.
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4 & predict_price==.
replace track = 5 if count_5>=10 & count_5<100000 & predict_price==.
replace predict_price = predict_price_5 if track==5 & predict_price==.
replace track = 6 if predict_price==.
replace predict_price = predict_price_6 if track==6 & predict_price==.
drop predict_price_* count_*

* Calculate Rental Cost
gen cost_land_rent = harvest_qty * predict_price
replace cost_land_rent = 0 if cost_land_rent==.

collapse (sum) cost_land_rent , by (rural region zone ward FA ea household_id2 HHID)
drop if cost_land_rent==0

append using "clean data/intermediate/Ethiopia1516_land_rent_cost_1.dta"

save "clean data/intermediate/Ethiopia1516_land_rent_cost.dta", replace

* Irrigation
* NA

* Machinery and Animal Rent
* NA

* Transport to Market
use "$RAW/Post-Harvest/sect11_ph_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename ph_s11q09 cost_transport
replace cost_transport = 0 if cost_transport==.
collapse (sum) cost_transport, by (rural region zone ward FA ea household_id2 HHID)
drop if cost_transport==0
save "clean data/intermediate/Ethiopia1516_transport_cost.dta", replace

** Aggregate Costs
use "clean data/intermediate/Ethiopia1516_seed_cost_1.dta", clear
joinby using "clean data/intermediate/Ethiopia1516_fertilizer_cost.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Ethiopia1516_labor_cost_1.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Ethiopia1516_feed_cost.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Ethiopia1516_land_rent_cost.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Ethiopia1516_transport_cost.dta", unmatched(both) _merge(_merge)
drop _merge
append using "clean data/intermediate/Ethiopia1516_seed_cost_2.dta"
append using "clean data/intermediate/Ethiopia1516_labor_cost_2.dta"
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (rural region zone ward FA ea household_id2 HHID)
local costs "cost_seed cost_fertilizer cost_labor cost_feed cost_land_rent cost_transport"
foreach c in `costs' {
	replace `c' = `c'/365
}
joinby using "clean data/Ethiopia1516_demographics.dta", unmatched(none)
keep rural region zone ward town subcity FA ea household_id2 HHID cost_*
save "clean data/intermediate/Ethiopia1516_costs.dta", replace


**** COMBINE ALL ****
* Start with demographics
use "clean data/Ethiopia1516_demographics.dta", clear

* Bring in harvest value
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_crop_harvest.dta"
drop _merge

* Bring in Permanent Crops
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_perm_crop_harvest.dta"
drop _merge

* Bring in Crop Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_crop_sales_value.dta"
drop _merge

* Bring in Permanent Crop Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_perm_crop_sales_value.dta"
drop _merge

* Bring in Livestock Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_livestock_general.dta"
drop _merge

* Bring in Milk Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_livestock_milk.dta"
drop _merge

* Bring in Eggs Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_livestock_egg.dta"
drop _merge

* Bring in Livestock Services
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_livestock_services.dta"
drop _merge

* Bring in Dung Sales
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_livestock_dung.dta"
drop _merge

* Bring in Wage Income
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_wage_incomeCHEVAL.dta"
drop _merge

* Bring in PSNP & Casual Wage
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_other_w_inc.dta"
drop _merge

* Bring in Own Enterprise
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_nfe_inc.dta"
drop _merge

* Bring in Other Income
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_other_income.dta"
drop _merge

* Bring in Assistance Income
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_assistance_income.dta"
drop _merge

* Bring in Costs
merge 1:1 household_id2 using "clean data/intermediate/Ethiopia1516_costs.dta"
drop _merge

mvdecode crop_* perm_* livestock_* wage_* psnp_* casual_* nfe_* other_income_source assistance d_remittances cost_*, mv(0)
mvencode crop_* perm_* livestock_* wage_* psnp_* casual_* nfe_* other_income_source assistance d_remittances cost_*, mv(0)


* Convert to Per Capita USD constant PPP
local USD "wage_income psnp_income casual_income nfe_gross_inc nfe_costs nfe_net_inc other_income_source assistance crop_harvest_value perm_crop_harvest_value crop_sales_value perm_crop_sales_value livestock_income livestock_sold livestock_milk_income livestock_milk_sold livestock_egg_income livestock_egg_sold livestock_milk_nfe_gross livestock_services livestock_dung cost_seed cost_fertilizer cost_labor cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 1/1/2015
	gen USD_`u' = `u'*0.047209
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (1/4 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method) + (3/4 * 2016 GNI constant 2011 PPP / 2016 GNI Atlas Method)
	replace USD_`u' = USD_`u'*2.642
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 1/1/2015
	gen USD_`usd'`u' = `usd'`u'*0.047209
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (1/4 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method) + (3/4 * 2016 GNI constant 2011 PPP / 2016 GNI Atlas Method)
	replace USD_`usd'`u' = USD_`usd'`u'*2.642
}
}
*	
local USD "psnp_i_f0_c6 casual_i_f0_c6 psnp_i_f0_c12 casual_i_f0_c12"
foreach usd in `USD' {
	capture drop USD_`usd'
	* Currency conversion from XE.com 1/1/2015
	gen USD_`usd' = `usd'*0.047209
	replace USD_`usd' = USD_`usd'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data
	* (1/4 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method) + (3/4 * 2016 GNI constant 2011 PPP / 2016 GNI Atlas Method)
	replace USD_`usd' = USD_`usd'*2.642
}
*

* Drop former variables
local USD "wage_income psnp_income casual_income nfe_gross_inc nfe_costs nfe_net_inc other_income_source assistance crop_harvest_value perm_crop_harvest_value crop_sales_value perm_crop_sales_value livestock_income livestock_sold livestock_milk_income livestock_milk_sold livestock_egg_income livestock_egg_sold livestock_milk_nfe_gross livestock_services livestock_dung cost_seed cost_fertilizer cost_labor cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	
local USD "psnp_i_f0_c6 casual_i_f0_c6 psnp_i_f0_c12 casual_i_f0_c12"
foreach usd in `USD' {
	capture drop `usd'
}
*

* Rename
local USD "wage_income psnp_income casual_income nfe_gross_inc nfe_costs nfe_net_inc other_income_source assistance crop_harvest_value perm_crop_harvest_value crop_sales_value perm_crop_sales_value livestock_income livestock_sold livestock_milk_income livestock_milk_sold livestock_egg_income livestock_egg_sold livestock_milk_nfe_gross livestock_services livestock_dung cost_seed cost_fertilizer cost_labor cost_feed cost_land_rent cost_transport"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fsp0_c wage_i_fcon1_c wage_i_fsp1_c"
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	
local USD "psnp_i_f0_c6 casual_i_f0_c6 psnp_i_f0_c12 casual_i_f0_c12"
foreach usd in `USD' {
	rename USD_`usd' `usd'
}
*

* Impute farm income components before creating the aggregate variable
local varlist crop_harvest_value perm_crop_harvest_value livestock_income livestock_milk_income livestock_egg_income
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}

replace crop_sales_value= crop_harvest_value if crop_sales_value>crop_harvest_value & crop_sales_value!=.
replace perm_crop_sales_value=perm_crop_harvest_value if perm_crop_sales_value>perm_crop_harvest_value & perm_crop_sales_value!=.
replace livestock_sold=livestock_income if livestock_sold>livestock_income & livestock_sold!=.
replace livestock_milk_sold=livestock_milk_income if livestock_milk_sold>livestock_milk_income & livestock_milk_sold!=.

generate farm_sales = crop_sales_value + perm_crop_sales_value + livestock_sold + livestock_milk_sold
gen own_farm_crop_inc_costs = cost_seed + cost_fertilizer + cost_labor + cost_land_rent + cost_transport
gen own_farm_livestock_inc_costs = cost_feed
gen own_farm_crop_inc_gross = crop_harvest_value + perm_crop_harvest_value
gen own_farm_livestock_inc_gross = livestock_income + livestock_milk_income + livestock_egg_income
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

* Bring in livestock enterprise income
local livestock_nfe "nfe_gross_inc nfe_gi_c2 nfe_net_inc nfe_i_c2"
foreach ln in `livestock_nfe' {
	replace `ln' = `ln' + livestock_milk_nfe_gross + livestock_services + livestock_dung
}
*

** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + other_income_source + assistance
}
*

merge 1:1 household_id2 using "$RAW/Consumption Aggregate\cons_agg_w3.dta" , nogen

merge 1:1 household_id2 using "clean data/Ethiopia1516_hhid.dta", nogen

rename d_remittances remit

* Convert to Per Capita USD constant PPP

ren total_cons_ann hhexp
replace hhexp= hhexp*price_index_hce
replace hhexp = hhexp/365 

* Currency conversion from XE.com 1/1/2015
gen USD_hhexp = hhexp*0.047209
g USD_pcexp = USD_hhexp/hhsize   // note: if  we use adult equivalent poverty is lower 
* Constant 2011 PPP conversion using World Bank GNI Data
* (1/4 * 2015 GNI constant 2011 PPP / 2015 GNI Atlas Method) + (3/4 * 2016 GNI constant 2011 PPP / 2016 GNI Atlas Method)
replace USD_pcexp = USD_pcexp*2.642
replace USD_hhexp = USD_hhexp*2.642

drop hhexp
ren USD_pcexp  pcexp
ren USD_hhexp hhexp

g poor = (pcexp<1.9)

g N_pension=.
g sh_pension=.
g N_health_insurance=.
g sh_health_insurance=.

keep hhid rural hhweight hhsize youth_share_24 youth_share_34 female_head school_secondary_hh educ_max credit land_d land_owned remit pcexp job1 job2 Ninformal N_pension sh_pension N_health_insurance sh_health_insurance informal informalcon informalsp nfe_i* nfe_g* nfe_c* wage_i_f* farm_sales own_farm* i_gi* i_ni* i_nnni* region zone ward town subcity FA ea t_PSNP nfe_gross_inc nfe_costs nfe_net_inc farm_sales
	

save "clean data/ETHIOPIA_incomesharesCHEVAL.dta", replace
