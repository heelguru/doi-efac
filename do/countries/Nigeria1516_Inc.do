********************************************************************************
***************** NIGERIA 2015/2016: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:
-	Share of farming income in total overall income (household level)
	o	Following RIGA/RuLiS method
	o	Farming income: crops, livestock, forestry and by-products
-	Share of farm sales income in total farm income (household level)
	o	Sales of crops, livestock, forestry, livestock by-products (all from own production)


The dataset used for this is:
Nigeria 2015/2016 General Household Survey-Panel Wave 3

and
"General_ISIC_AFS.dta" (ISIC industry codes, imported using do-file "ISIC_codes_AFS.do")


Stata Version 15.1
-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
CHEVAL saves:
save "clean data/intermediate/Nigeria1516_wage_incomeCHEVAL.dta", replace
save "clean data/NIGERIA_incomesharesCHEVAL.dta", replace

NB some glossary
AFS = Agriculture and Food System
CIC = Country Industry Code
FTE = Full Time Equivalent
OLF = Out of Labor Force
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\Nigeria_GHS_1516" 

gl DO   	    ${NIGERIA}/do
gl raw data   	${NIGERIA}/raw data 
gl clean data 	${NIGERIA}/clean data
gl FIG 	    	${NIGERIA}/graphs
gl TAB 		    ${NIGERIA}/tables
 
********************************************************************************
cd "$NIGERIA"

*** INCOME SHARES OF FARMING AND FARM PRODUCE SALES ***

*get from each module the value of production, sales, own consumption and aggregate at household level:
*** AG-INCOME = crops, crop products, forestry, livestock, livestock by-products
		*reference period for ag. income is past 12 months


** Occupations in Non-Farm Own Enterprises (ONLY in POST HARVEST)
use "raw data/sect9_harvestw3.dta", clear
rename s9q1b nfe_CIC
rename s9q13a1 nfe_hourspd
rename s9q13a nfe_mem_a
rename s9q13b nfe_mem_b
rename s9q13c nfe_mem_c
rename s9q13d nfe_mem_d
rename s9q13e nfe_mem_e
rename s9q13f nfe_mem_f
rename s9q13_2 nfe_days_a
rename s9q13_4 nfe_days_b
rename s9q13_6 nfe_days_c
rename s9q13_8 nfe_days_d
rename s9q13_10 nfe_days_e
rename s9q13_12 nfe_days_f
local alph "a b c d e f g h i j k l m n o"
foreach a in `alph' {
	replace s9q10`a' = "1" if s9q10`a'=="X"
}
*
destring s9q10*, replace
mvencode s9q10*, mv(0)
egen float nfe_months = rowtotal(s9q10*)
* question asks for the last 12 months, so must assume that those who reported greater 
* than 12 months did not understand the question. It is possible that those who understood
* the question only reported 12 months even if they worked more than 12 of past 15.
replace nfe_months = 12 if nfe_months>12
rename s9q27a nfe_moprofit
rename s9q27 nfe_mo_gross_inc
gen nfe_mo_costs = 0
local costs "s9q28a s9q28b s9q28c s9q28d s9q28e s9q28f s9q28g s9q28h s9q28i s9q28j"
foreach c in `costs' {
	replace nfe_mo_costs = nfe_mo_costs + `c' if `c'~=.
}
*
gen nfe_mo_net_inc = nfe_mo_gross_inc - nfe_mo_costs

* Keep relevant data
keep hhid nfe_*
replace nfe_hourspd = 0 if nfe_hourspd==. & nfe_days_a==. & nfe_days_b==. & nfe_days_c==. & nfe_days_d==. & nfe_days_e==. & nfe_days_f==.
drop if nfe_hourspd==0 & nfe_months==0
drop if nfe_mo_net_inc==0 & nfe_mo_gross_inc==0 & nfe_mo_costs==0
drop if nfe_mo_net_inc==.

*** 11 observations with zero months and profit earned last month. Use median months by CIC to estimate number of months when initially coded zero
mvdecode nfe_months, mv(0)
* Bring in Demographic Info
joinby hhid using "clean data/Nigeria1516_demographics.dta", unmatched(none)
* Count observations with nfe_months reported by geographic location.
by nfe_CIC rural Zone State LGA EA, sort : egen float count_1 = count(1) if nfe_months~=.
by nfe_CIC rural Zone State LGA, sort : egen float count_2 = count(1) if nfe_months~=.
by nfe_CIC rural Zone State, sort : egen float count_3 = count(1) if nfe_months~=.
by nfe_CIC rural Zone, sort : egen float count_4 = count(1) if nfe_months~=.
by nfe_CIC rural, sort : egen float count_5 = count(1) if nfe_months~=.
* Calculate median of nfe_months by geographic location - later used for predictive purposes.
by nfe_CIC rural Zone State LGA EA, sort : egen float temp_est_nfe_months_1 = median(nfe_months) if nfe_months~=.
by nfe_CIC rural Zone State LGA, sort : egen float temp_est_nfe_months_2 = median(nfe_months) if nfe_months~=.
by nfe_CIC rural Zone State, sort : egen float temp_est_nfe_months_3 = median(nfe_months) if nfe_months~=.
by nfe_CIC rural Zone, sort : egen float temp_est_nfe_months_4 = median(nfe_months) if nfe_months~=.
by nfe_CIC rural, sort : egen float temp_est_nfe_months_5 = median(nfe_months) if nfe_months~=.
mvencode temp_*, mv(0)
by nfe_CIC rural Zone State LGA EA, sort : egen float est_nfe_months_1 = max(temp_est_nfe_months_1)
by nfe_CIC rural Zone State LGA, sort : egen float est_nfe_months_2 = max(temp_est_nfe_months_2)
by nfe_CIC rural Zone State, sort : egen float est_nfe_months_3 = max(temp_est_nfe_months_3)
by nfe_CIC rural Zone, sort : egen float est_nfe_months_4 = max(temp_est_nfe_months_4)
by nfe_CIC rural, sort : egen float est_nfe_months_5 = max(temp_est_nfe_months_5)
drop temp*
* Build nfe_months prediction variable if at least 10 observations of reported nfe_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_nfe_months = est_nfe_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_nfe_months==.
replace est_nfe_months = est_nfe_months_4 if track==4
replace track = 5 if est_nfe_months==.
replace est_nfe_months = est_nfe_months_5 if track==5
drop est_nfe_months_* count_* track
**** Replace missing value of nfe_months (10 changes)
replace nfe_months = est_nfe_months if nfe_months==.
drop est_nfe_months

keep hhid nfe_CIC nfe_mo_net_inc nfe_mo_gross_inc nfe_mo_costs nfe_months

**** Have to assume that last month values are the typical monthly values
gen nfe_ann_gross_inc = nfe_mo_gross_inc * nfe_months
gen nfe_ann_costs= nfe_mo_costs* nfe_months
gen nfe_ann_net_inc = nfe_mo_net_inc * nfe_months

* Collapse to HH / ISIC level
collapse (sum) nfe_ann_net_inc nfe_ann_gross_inc nfe_ann_costs, by (hhid nfe_CIC)

* Calculate Income Per Day
gen nfe_gross_inc = nfe_ann_gross_inc/365
gen nfe_costs = nfe_ann_costs/365
gen nfe_net_inc = nfe_ann_net_inc/365

* Bring in information for ISIC Groupings
rename nfe_CIC CIC
joinby CIC using "clean data/Nigeria1516_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace ISIC_group = 612 if ISIC_group==.
joinby hhid using "clean data/Nigeria1516_demographics.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.607 if AFS_share==. & rural==1
replace AFS_share = 0.525 if AFS_share==. & rural==0

** Generate Categorical Variables
forvalues c=1(1)12 {
	gen nfe_i_c`c' = 0
	gen nfe_gi_c`c' = 0
	gen nfe_c_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_i_c`c' = nfe_net_inc if ISIC_group==`c'
	replace nfe_gi_c`c' = nfe_gross_inc if ISIC_group==`c'
	replace nfe_c_c`c' = nfe_costs if ISIC_group==`c'
}
*
* Four mixed CIC Groups
replace nfe_i_c3 = AFS_share*nfe_net_inc if ISIC_group==37
replace nfe_i_c7 = (1-AFS_share)*nfe_net_inc if ISIC_group==37
replace nfe_i_c5 = AFS_share*nfe_net_inc if ISIC_group==509
replace nfe_i_c9 = (1-AFS_share)*nfe_net_inc if ISIC_group==509
replace nfe_i_c4 = AFS_share*nfe_net_inc if ISIC_group==410
replace nfe_i_c10 = (1-AFS_share)*nfe_net_inc if ISIC_group==410
replace nfe_i_c6 = AFS_share*nfe_net_inc if ISIC_group==612
replace nfe_i_c12 = (1-AFS_share)*nfe_net_inc if ISIC_group==612
replace nfe_gi_c3 = AFS_share*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c7 = (1-AFS_share)*nfe_gross_inc if ISIC_group==37
replace nfe_gi_c5 = AFS_share*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c9 = (1-AFS_share)*nfe_gross_inc if ISIC_group==509
replace nfe_gi_c4 = AFS_share*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c10 = (1-AFS_share)*nfe_gross_inc if ISIC_group==410
replace nfe_gi_c6 = AFS_share*nfe_gross_inc if ISIC_group==612
replace nfe_gi_c12 = (1-AFS_share)*nfe_gross_inc if ISIC_group==612
replace nfe_c_c3 = AFS_share*nfe_costs if ISIC_group==37
replace nfe_c_c7 = (1-AFS_share)*nfe_costs if ISIC_group==37
replace nfe_c_c5 = AFS_share*nfe_costs if ISIC_group==509
replace nfe_c_c9 = (1-AFS_share)*nfe_costs if ISIC_group==509
replace nfe_c_c4 = AFS_share*nfe_costs if ISIC_group==410
replace nfe_c_c10 = (1-AFS_share)*nfe_costs if ISIC_group==410
replace nfe_c_c6 = AFS_share*nfe_costs if ISIC_group==612
replace nfe_c_c12 = (1-AFS_share)*nfe_costs if ISIC_group==612

* Keep relevant variables
order hhid ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*
keep hhid ISIC_group nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*

* Collapse to household level
collapse (sum) nfe_gross_inc nfe_costs nfe_net_inc nfe_i_* nfe_gi_* nfe_c_*, by (hhid)

save "clean data/intermediate/Nigeria1516_nfe_income.dta", replace


** Occupation by Wages (BOTH POST PLANTING AND POST HARVEST)
* Post Planting 
* Primary Wage
use "raw data/sect3_plantingw3.dta", clear

* Rename variables
rename s3q13b wage_CIC
rename s3q15e w_contract
rename s3q15d pension
rename s3q15f health_insurance
rename s3q16 wage_months
rename s3q17 wage_weeks
rename s3q18 wage_hourspw
rename s3q21a wage_payment
rename s3q21b wage_payment_duration
rename s3q24a wage_payment_inkind
rename s3q24b wage_payment_inkind_duration

gen wage_job=1

order hhid indiv wage_* w_contract pension health_insurance 
keep hhid indiv wage_* w_contract pension health_insurance 

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (19)
replace wage_hourspw = . if wage_hourspw>80
* Only 52 weeks in a year
replace wage_weeks = 52 if wage_weeks>52 & wage_weeks<.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (4 changes)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (22 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hourspw)/7)*(wage_weeks/4.3)*(wage_months/12) if wage_payment_duration==1 // (!)

* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/52) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/52) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.
* Payment in kind
* Duration in hours
replace wage_payment_inkind = ((wage_payment_inkind*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_payment_inkind = ((wage_payment_inkind*est_wage_days)/7)*(wage_weeks/52) if wage_payment_inkind_duration==2
* Duration in weeks
replace wage_payment_inkind = wage_payment_inkind*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==3
* Duration in fortnights
replace wage_payment_inkind = wage_payment_inkind*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==4
* Duration in months
replace wage_payment_inkind = wage_payment_inkind*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==5
* Duration in quarters
replace wage_payment_inkind = wage_payment_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==6
* Duration in half years
replace wage_payment_inkind = wage_payment_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==7
* Duration in years
replace wage_payment_inkind = wage_payment_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==8
replace wage_payment_inkind = 0 if wage_payment_inkind_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_payment_inkind if wage_payment_inkind~=.
drop if wage_income==0

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
*replace formal = 1 if offer_materpater==1 //not added as none reported.
*replace formal = 1 if withold_taxes==1 //not added as none reported

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
//replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
//replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

** objective(!)
** inf1= A simple share of employment types to capture the diversification into different income sources:  to do
// the share of inf work in agri & share of inf work non agri 

** inf3= the income share from each employment type to tell us how much a household's welfare depends on a specific source of income.:  to do
//the share of wage income informal
//the share of wage income formal
//the share of agri income biz
//the share of any other non-agri income biz
gen sw = 1

collapse (sum) wage_income (first) wage_job , by (hhid indiv wage_CIC formal formalcon formalsp health_insurance w_contract pension sw)

save "clean data/intermediate/Nigeria1516_wage_inc_PP1.dta", replace

* Post Planting 
* Secondary Wage
use "raw data/sect3_plantingw3.dta", clear

* Rename variables
rename s3q26b wage_CIC
rename s3q28e w_contract
rename s3q28d pension
rename s3q28f health_insurance
rename s3q29 wage_months
rename s3q30 wage_weeks
rename s3q31 wage_hourspw
rename s3q34a wage_payment
rename s3q34b wage_payment_duration
rename s3q37a wage_payment_inkind
rename s3q37b wage_payment_inkind_duration
gen wage_job=2

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance 
	
* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/52) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/52) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.
* Payment in kind
* Duration in hours
replace wage_payment_inkind = ((wage_payment_inkind*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_payment_inkind = ((wage_payment_inkind*est_wage_days)/7)*(wage_weeks/52) if wage_payment_inkind_duration==2
* Duration in weeks
replace wage_payment_inkind = wage_payment_inkind*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==3
* Duration in fortnights
replace wage_payment_inkind = wage_payment_inkind*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==4
* Duration in months
replace wage_payment_inkind = wage_payment_inkind*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==5
* Duration in quarters
replace wage_payment_inkind = wage_payment_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==6
* Duration in half years
replace wage_payment_inkind = wage_payment_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==7
* Duration in years
replace wage_payment_inkind = wage_payment_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==8
replace wage_payment_inkind = 0 if wage_payment_inkind_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_payment_inkind if wage_payment_inkind~=.
drop if wage_income==0

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
*replace formal = 1 if offer_materpater==1 //not added as none reported.
*replace formal = 1 if withold_taxes==1 //not added as none reported

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

** objective(!)
** inf1= A simple share of employment types to capture the diversification into different income sources:  to do
// the share of inf work in agri & share of inf work non agri 

** inf3= the income share from each employment type to tell us how much a household's welfare depends on a specific source of income.:  to do
//the share of wage income informal
//the share of wage income formal
//the share of agri income biz
//the share of any other non-agri income biz
gen sw = 2

collapse (sum) wage_income (first) wage_job, by (hhid indiv wage_CIC formal formalcon formalsp health_insurance w_contract pension sw)

save "clean data/intermediate/Nigeria1516_wage_inc_PP2.dta", replace

* Post Harvest 
* Primary Wage
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q13b wage_CIC
rename s3q15e w_contract
rename s3q15d pension
rename s3q15f health_insurance
rename s3q16 wage_months
rename s3q17 wage_weeks
rename s3q18 wage_hourspw
rename s3q21a wage_payment
rename s3q21b wage_payment_duration
rename s3q24a wage_payment_inkind
rename s3q24b wage_payment_inkind_duration
gen wage_job=3
order hhid indiv wage_* w_contract pension health_insurance wage_job
keep hhid indiv wage_* w_contract pension health_insurance wage_job

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* Clear Errors in data
* Assume that reported hours per day above 16 hours per day are in error. (71)
replace wage_hourspw = . if wage_hourspw>80 & wage_hourspw~=.

mvdecode wage_months wage_weeks wage_hourspw, mv(0)

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_months reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_months~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_months~=.
* Calculate median of wage_months by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_months_1 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_months_2 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_months_3 = median(wage_months) if wage_months~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_months_4 = median(wage_months) if wage_months~=.
by wage_CIC female rural, sort : egen float temp_est_wage_months_5 = median(wage_months) if wage_months~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_months_1 = max(temp_est_wage_months_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_months_2 = max(temp_est_wage_months_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_months_3 = max(temp_est_wage_months_3)
by wage_CIC female rural Zone, sort : egen float est_wage_months_4 = max(temp_est_wage_months_4)
by wage_CIC female rural, sort : egen float est_wage_months_5 = max(temp_est_wage_months_5)
drop temp*
* Build wage_months prediction variable if at least 10 observations of reported wage_months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_months = est_wage_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_months==.
replace est_wage_months = est_wage_months_4 if track==4
replace track = 5 if est_wage_months==.
replace est_wage_months = est_wage_months_5 if track==5
drop est_wage_months_* count_* track
**** Replace missing value of wage_months (1 change)
replace wage_months = est_wage_months if wage_months==.
replace wage_months = 1 if wage_months==.
* Count observations with wage_weeks reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_weeks~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_weeks~=.
* Calculate median of wage_weeks by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_weeks_1 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_weeks_2 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_weeks_3 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_weeks_4 = median(wage_weeks) if wage_weeks~=.
by wage_CIC female rural, sort : egen float temp_est_wage_weeks_5 = median(wage_weeks) if wage_weeks~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_weeks_1 = max(temp_est_wage_weeks_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_weeks_2 = max(temp_est_wage_weeks_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_weeks_3 = max(temp_est_wage_weeks_3)
by wage_CIC female rural Zone, sort : egen float est_wage_weeks_4 = max(temp_est_wage_weeks_4)
by wage_CIC female rural, sort : egen float est_wage_weeks_5 = max(temp_est_wage_weeks_5)
drop temp*
* Build wage_weeks prediction variable if at least 10 observations of reported wage_weeks in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_weeks = est_wage_weeks_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_3 if track==4
replace track = 5 if est_wage_weeks==.
replace est_wage_weeks = est_wage_weeks_5 if track==5
drop est_wage_weeks_* count_* track
**** Replace missing value of wage_weeks (2 changes)
replace wage_weeks = est_wage_weeks if wage_weeks==.
replace wage_weeks = 1 if wage_weeks==.
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (71 changes)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/52) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/52) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.
* Payment in kind
* Duration in hours
replace wage_payment_inkind = ((wage_payment_inkind*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_payment_inkind = ((wage_payment_inkind*est_wage_days)/7)*(wage_weeks/52) if wage_payment_inkind_duration==2
* Duration in weeks
replace wage_payment_inkind = wage_payment_inkind*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==3
* Duration in fortnights
replace wage_payment_inkind = wage_payment_inkind*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==4
* Duration in months
replace wage_payment_inkind = wage_payment_inkind*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==5
* Duration in quarters
replace wage_payment_inkind = wage_payment_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==6
* Duration in half years
replace wage_payment_inkind = wage_payment_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==7
* Duration in years
replace wage_payment_inkind = wage_payment_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==8
replace wage_payment_inkind = 0 if wage_payment_inkind_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_payment_inkind if wage_payment_inkind~=.
drop if wage_income==0

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
*replace formal = 1 if offer_materpater==1 //not added as none reported.
*replace formal = 1 if withold_taxes==1 //not added as none reported

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

** objective(!)
** inf1= A simple share of employment types to capture the diversification into different income sources:  to do
// the share of inf work in agri & share of inf work non agri 

** inf3= the income share from each employment type to tell us how much a household's welfare depends on a specific source of income.:  to do
//the share of wage income informal
//the share of wage income formal
//the share of agri income biz
//the share of any other non-agri income biz

gen sw = 3

collapse (sum) wage_income (first) wage_job, by (hhid indiv wage_CIC formal formalcon formalsp health_insurance w_contract pension sw)

save "clean data/intermediate/Nigeria1516_wage_inc_PH1.dta", replace

* Post Harvest 
* Secondary Wage
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q26b wage_CIC
rename s3q28e w_contract
rename s3q28d pension
rename s3q28f health_insurance
rename s3q29 wage_months
rename s3q30 wage_weeks
rename s3q31 wage_hourspw
rename s3q34a wage_payment
rename s3q34b wage_payment_duration
rename s3q37a wage_payment_inkind
rename s3q37b wage_payment_inkind_duration
gen wage_job=4

order hhid indiv wage_* w_contract pension health_insurance
keep hhid indiv wage_* w_contract pension health_insurance

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.

* No occupation code, no reported earnings, but reported hours.
replace wage_CIC = 99999 if wage_CIC==.
replace wage_payment = 0 if wage_payment==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weeks/52) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weeks/52) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.
* Payment in kind
* Duration in hours
replace wage_payment_inkind = ((wage_payment_inkind*wage_hourspw)/7)*(wage_weeks/52) if wage_payment_inkind_duration==1
* Duration in days
****
**** Assume working for 8 hour days, if payment given in days
replace wage_payment_inkind = ((wage_payment_inkind*est_wage_days)/7)*(wage_weeks/52) if wage_payment_inkind_duration==2
* Duration in weeks
replace wage_payment_inkind = wage_payment_inkind*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==3
* Duration in fortnights
replace wage_payment_inkind = wage_payment_inkind*(1/2)*(1/7)*(wage_weeks/52) if wage_payment_inkind_duration==4
* Duration in months
replace wage_payment_inkind = wage_payment_inkind*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==5
* Duration in quarters
replace wage_payment_inkind = wage_payment_inkind*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==6
* Duration in half years
replace wage_payment_inkind = wage_payment_inkind*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==7
* Duration in years
replace wage_payment_inkind = wage_payment_inkind*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_inkind_duration==8
replace wage_payment_inkind = 0 if wage_payment_inkind_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
replace wage_income = wage_income + wage_payment_inkind if wage_payment_inkind~=.
drop if wage_income==0
* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formal = 1 if w_contract==1
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1
*replace formal = 1 if offer_materpater==1 //not added as none reported.
*replace formal = 1 if withold_taxes==1 //not added as none reported

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
replace formalsp = 1 if health_insurance==1
replace formalsp = 1 if pension==1

** objective(!)
** inf1= A simple share of employment types to capture the diversification into different income sources:  to do
// the share of inf work in agri & share of inf work non agri 

** inf3= the income share from each employment type to tell us how much a household's welfare depends on a specific source of income.:  to do
//the share of wage income informal
//the share of wage income formal
//the share of agri income biz
//the share of any other non-agri income biz
gen sw = 4

collapse (sum) wage_income (first) wage_job, by (hhid indiv wage_CIC formal formalcon formalsp health_insurance w_contract pension sw)

save "clean data/intermediate/Nigeria1516_wage_inc_PH2.dta", replace

* Post Harvest 
* Wage if NOT worked in past 7 days
use "raw data/sect3_harvestw3.dta", clear

* Rename variables
rename s3q43b wage_CIC
rename s3q45 wage_months
rename s3q46 wage_weekspy
rename s3q47 wage_hourspw
rename s3q49a wage_payment
rename s3q49b wage_payment_duration
gen wage_job=5

order hhid indiv wage_*
keep hhid indiv wage_*

* Clear Errors in data
* No occupation code, but reported earnings. 
replace wage_CIC = 99999 if wage_CIC==. & wage_payment~=.
* Assume that reported hours per day above 16 hours per day are in error. (3)
replace wage_hourspw = . if wage_hourspw>80 & wage_hourspw~=.

* Drop no data households
drop if wage_payment==. & wage_months==. & wage_weeks==. & wage_hourspw==.
* No occupation code, no reported earnings, but reported hours.
replace wage_payment = 0 if wage_payment==.

* Bring in Demographic Info
joinby using "clean data/Nigeria1516_member_demos.dta", unmatched(none)
* Count observations with wage_hourspw reported by geographic location.
by wage_CIC female rural Zone State LGA EA, sort : egen float count_1 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float count_2 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float count_3 = count(1) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float count_4 = count(1) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float count_5 = count(1) if wage_hourspw~=.
* Calculate median of wage_hourspw by geographic location - later used for predictive purposes.
by wage_CIC female rural Zone State LGA EA, sort : egen float temp_est_wage_hourspw_1 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State LGA, sort : egen float temp_est_wage_hourspw_2 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone State, sort : egen float temp_est_wage_hourspw_3 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural Zone, sort : egen float temp_est_wage_hourspw_4 = median(wage_hourspw) if wage_hourspw~=.
by wage_CIC female rural, sort : egen float temp_est_wage_hourspw_5 = median(wage_hourspw) if wage_hourspw~=.
mvencode temp_*, mv(0)
by wage_CIC female rural Zone State LGA EA, sort : egen float est_wage_hourspw_1 = max(temp_est_wage_hourspw_1)
by wage_CIC female rural Zone State LGA, sort : egen float est_wage_hourspw_2 = max(temp_est_wage_hourspw_2)
by wage_CIC female rural Zone State, sort : egen float est_wage_hourspw_3 = max(temp_est_wage_hourspw_3)
by wage_CIC female rural Zone, sort : egen float est_wage_hourspw_4 = max(temp_est_wage_hourspw_4)
by wage_CIC female rural, sort : egen float est_wage_hourspw_5 = max(temp_est_wage_hourspw_5)
drop temp*
* Build wage_hourspw prediction variable if at least 10 observations of reported wage_hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_wage_hourspw = est_wage_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_3 if track==4
replace track = 5 if est_wage_hourspw==.
replace est_wage_hourspw = est_wage_hourspw_5 if track==5
drop est_wage_hourspw_* count_* track
**** Replace missing value of wage_hourspw (3 change)
replace wage_hourspw = est_wage_hourspw if wage_hourspw==.
replace wage_hourspw = 1 if wage_hourspw==.

* Convert payment into average payment per day
* Duration in hours
replace wage_payment = ((wage_payment*wage_hourspw)/7)*(wage_weekspy/52) if wage_payment_duration==1
* Duration in days
**** Need estimated days per week
**** Assume working for 8 hour days, if payment given in days
gen est_wage_days = wage_hourspw/8
replace est_wage_days = 7 if est_wage_days>7
****
replace wage_payment = ((wage_payment*est_wage_days)/7)*(wage_weekspy/52) if wage_payment_duration==2
* Duration in weeks
replace wage_payment = wage_payment*(1/7)*(wage_weekspy/52) if wage_payment_duration==3
* Duration in fortnights
replace wage_payment = wage_payment*(1/2)*(1/7)*(wage_weekspy/52) if wage_payment_duration==4
* Duration in months
replace wage_payment = wage_payment*(1/30.4)*(wage_months/12) if wage_payment_duration==5
* Duration in quarters
replace wage_payment = wage_payment*(1/3)*(1/30.4)*(wage_months/12) if wage_payment_duration==6
* Duration in half years
replace wage_payment = wage_payment*(1/6)*(1/30.4)*(wage_months/12) if wage_payment_duration==7
* Duration in years
replace wage_payment = wage_payment*(1/12)*(1/30.4)*(wage_months/12) if wage_payment_duration==8
replace wage_payment = 0 if wage_payment_duration==.

* Combine wage types
gen wage_income = 0
replace wage_income = wage_income + wage_payment if wage_payment~=.
drop if wage_income==0

* Casual vs Formal Wage
gen formal = 0
*Formal if payment duration greater than 1 week
replace formal = 1 if wage_payment_duration>3 & wage_payment_duration<.
*replace formal = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
*replace formal = 1 if w_contract==1
*replace formal = 1 if pension==1
*replace formal = 1 if health_insurance==1
*replace formal = 1 if offer_materpater==1 //not added as none reported.
*replace formal = 1 if withold_taxes==1 //not added as none reported

gen formalcon = 0 //added definition of "formality" due to w_contract structure (!)
replace formalcon = 1 if wage_payment_duration>3 & wage_payment_duration<.
*replace formalcon = 1 if wage_payment_inkind_duration>3 & wage_payment_inkind_duration<.
*replace formalcon = 1 if w_contract==1

gen formalsp = 0 //added definition of "formality" due to SP(!)
*replace formalsp = 1 if health_insurance==1
*replace formalsp = 1 if pension==1

** objective(!)
** inf1= A simple share of employment types to capture the diversification into different income sources:  to do
// the share of inf work in agri & share of inf work non agri 

** inf3= the income share from each employment type to tell us how much a household's welfare depends on a specific source of income.:  to do
//the share of wage income informal
//the share of wage income formal
//the share of agri income biz
//the share of any other non-agri income biz
gen sw = 5

collapse (sum) wage_income (first) wage_job, by (hhid indiv wage_CIC formal formalcon formalsp sw)

save "clean data/intermediate/Nigeria1516_wage_inc_PH3.dta", replace

* Combine Wage files
use "clean data/intermediate/Nigeria1516_wage_inc_PP1.dta", clear
append using "clean data/intermediate/Nigeria1516_wage_inc_PP2.dta"
append using "clean data/intermediate/Nigeria1516_wage_inc_PH1.dta"
append using "clean data/intermediate/Nigeria1516_wage_inc_PH2.dta"
append using "clean data/intermediate/Nigeria1516_wage_inc_PH3.dta"

* If same occupation code occured twice in one season, sum. (3 cases)
* If same occupation code occured twice in two seasons, use the maximum values of FT & Occupational activity to not double count. (393)
gen season = cond(sw==1 | sw==2,1,cond(sw==3 | sw==4 | sw==5,2,.))
collapse (sum) wage_income (first) wage_job health_insurance pension w_contract , by (hhid indiv wage_CIC formal formalsp formalcon season)
collapse (max) wage_income (first) wage_job health_insurance pension w_contract , by (hhid indiv wage_CIC formal formalsp formalcon)

* Bring in information for ISIC Groupings
rename wage_CIC CIC
joinby CIC using "clean data/Nigeria1516_Alt_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby hhid using "clean data/Nigeria1516_demographics.dta", unmatched(none)

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
/// for Tanzania 2014/2015 the calculations are as follows
///   Rural -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
///   Urban -   0.XXX+(1-0.XXX)*0.0XX  ==  .XXX
*******************************************************************************************
replace AFS_share = 0.607 if AFS_share==. & rural==1
replace AFS_share = 0.525 if AFS_share==. & rural==0

** Generate Categorical Variables
** Generate Categorical Variables
forvalues f=0(1)1 {
	forvalues c=1(1)12 {
		gen wage_i_f`f'_c`c' = 0
		gen wage_i_fcon`f'_c`c' = 0
		gen wage_i_fsp`f'_c`c' = 0
	}
}
*

forvalues f=0(1)1 {
	forvalues c=1(1)12 {
		replace wage_i_f`f'_c`c' = wage_income if ISIC_group==`c' & formal==`f'
		replace wage_i_fcon`f'_c`c' = wage_income if ISIC_group==`c' & formalcon==`f'
		replace wage_i_fsp`f'_c`c' = wage_income if ISIC_group==`c' & formalsp==`f'
	}
}
*
* Four mixed CIC Groups
forvalues f=0(1)1 {
	replace wage_i_f`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formal==`f'
	replace wage_i_f`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formal==`f'
	replace wage_i_f`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formal==`f'
	replace wage_i_f`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formal==`f'
	replace wage_i_f`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formal==`f'

	replace wage_i_fcon`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalcon==`f'
	replace wage_i_fcon`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalcon==`f'
	replace wage_i_fcon`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalcon==`f'
	replace wage_i_fcon`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalcon==`f'
	replace wage_i_fcon`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalcon==`f'
	
	replace wage_i_fsp`f'_c3 = AFS_share*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c7 = (1-AFS_share)*wage_income if ISIC_group==37 & formalsp==`f'
	replace wage_i_fsp`f'_c5 = AFS_share*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c9 = (1-AFS_share)*wage_income if ISIC_group==509 & formalsp==`f'
	replace wage_i_fsp`f'_c4 = AFS_share*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c10 = (1-AFS_share)*wage_income if ISIC_group==410 & formalsp==`f'
	replace wage_i_fsp`f'_c6 = AFS_share*wage_income if ISIC_group==612 & formalsp==`f'
	replace wage_i_fsp`f'_c12 = (1-AFS_share)*wage_income if ISIC_group==612 & formalsp==`f'
}
*
*drop if age<16 
*g one=1
*bys hhid: egen tothhplus16=total(one) // (!) to change with those who declare to be active?? 
* Keep relevant variables

g informal=(formal==0)
g informalcon=(formalcon==0)
g informalsp=(formalsp==0)

order hhid CIC ISIC_group wage_* wage_i_* wage_job health_insurance 
keep hhid CIC ISIC_group wage_* wage_i_* informal* formal* wage_job pension health_insurance 
tab wage_job,g(job) 

recode   health_insurance (2=0) 	//0-1 variable for counts
recode   pension (2=0) 	//0-1 variable for counts

* Collapse to household level
collapse (sum) wage_*  job*  Ninformal=informal N_health_insurance=health_insurance N_pension=pension (mean) informal* formal* sh_health_insurance=health_insurance sh_pension=pension , by (hhid) 

**labels
foreach v in  health_insurance pension{
la var sh_`v' "Share of HHmemb with `v'"
la var N_`v' "N HHmemb with `v'"
}
la var job1 		"N HHmemb with wagejob(primary)"
la var job2 		"N HHmemb with wagejob(secondary)"
la var informal 	"Informal sh of wagework(no w_contract,SP)"	// important variable
la var formal		"Formal sh of wagework(any w_contract,SP)"
la var informalcon 	"Informal(no w_contract)"
la var formalcon  	"Formal sh of wagework(w_contract)"
la var informalsp 	"Informal sh of wagework(SP:insurance/matpat)"
la var formalsp 	"Formal sh of wagework(SP:insurance/matpat)"
la var Ninformal	"N HHmemb with informal wagework"

global lab1 "OwnFarming"	
global lab2 "Farm Labor"	
global lab3 "Food&Ag Manufacturing w/in AFS"
global lab4 "FoodPrep"	
global lab5 "FoodSystem Marketing&Transport"
global lab6 "AFS Unclassified"	
global lab7 "Non-AFS Manufacturing"
global lab8 "Non-AFS Industry"	
global lab9 "Non-AFS Marketing&Transport"
global lab10 "Non-AFS Other"
global lab11 "Non-AFS PublicService"
global lab12 "Non-AFS Unclassifed"

forv f=1/12 {
la var wage_i_f0_c`f'    "Informal w from ${lab`f'}"
la var wage_i_fcon0_c`f' "Informal w from ${lab`f'}(w_contract)"
la var wage_i_fsp0_c`f'  "Informal w from ${lab`f'}(SP)"
la var wage_i_f1_c`f'    "Formal w from ${lab`f'}"
la var wage_i_fcon1_c`f' "Formal w from ${lab`f'}(w_contract)"
la var wage_i_fsp1_c`f'  "Formal w from ${lab`f'}(SP)"
}

save "clean data/intermediate/Nigeria1516_wage_incomeCHEVAL.dta", replace


*** Crop Harvest
* Harvest Sales Data - Price Estimates
use "raw data/secta3ii_harvestw3.dta", clear
rename cropcode crop_cd
rename sa3iiq5a sales_qty
rename sa3iiq5b unit_cd
rename sa3iiq5b_os unit_other
rename sa3iiq6 sales_value

keep hhid crop_cd sales_qty unit_cd unit_other sales_value
drop if sales_qty==0
drop if sales_qty==.
replace sales_value = . if sales_value==0

joinby crop_cd unit_cd unit_other using "raw data/ag_conv_w3.dta", unmatched(both) _merge(_merge)
drop if _merge==2

* Convert to KG
replace sales_qty = sales_qty * conv_national if conv_national~=.
replace unit_cd = 1 if conv_national~=.
replace unit_other = "" if conv_national~=.

keep hhid crop_cd sales_qty unit_cd unit_other sales_value

gen revealed_price = sales_value / sales_qty

* Bring in Location Info
joinby hhid using "clean data/Nigeria1516_location.dta", unmatched(none)
* Count observations with revealed_price reported by geographic location.
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float count_1 = count(1) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float count_2 = count(1) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone State, sort : egen float count_3 = count(1) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone, sort : egen float count_4 = count(1) if revealed_price~=.
by crop_cd unit_cd unit_other rural, sort : egen float count_5 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float temp_est_revealed_price_1 = median(revealed_price) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float temp_est_revealed_price_2 = median(revealed_price) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone State, sort : egen float temp_est_revealed_price_3 = median(revealed_price) if revealed_price~=.
by crop_cd unit_cd unit_other rural Zone, sort : egen float temp_est_revealed_price_4 = median(revealed_price) if revealed_price~=.
by crop_cd unit_cd unit_other rural, sort : egen float temp_est_revealed_price_5 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float est_revealed_price_1 = max(temp_est_revealed_price_1)
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float est_revealed_price_2 = max(temp_est_revealed_price_2)
by crop_cd unit_cd unit_other rural Zone State, sort : egen float est_revealed_price_3 = max(temp_est_revealed_price_3)
by crop_cd unit_cd unit_other rural Zone, sort : egen float est_revealed_price_4 = max(temp_est_revealed_price_4)
by crop_cd unit_cd unit_other rural, sort : egen float est_revealed_price_5 = max(temp_est_revealed_price_5)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_revealed_price = est_revealed_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_revealed_price==.
replace est_revealed_price = est_revealed_price_4 if track==4
replace track = 5 if est_revealed_price==.
replace est_revealed_price = est_revealed_price_5 if track==5
drop est_revealed_price_* count_* track

rename est_revealed_price est_price

collapse (max) est_price, by (crop_cd unit_cd unit_other rural Zone State LGA EA)

save "clean data/intermediate/Nigeria1516_harvest_prices.dta", replace


* Post Harvest Loss Data
use "raw data/secta3ii_harvestw3.dta", clear
rename cropcode crop_cd
rename sa3iiq18a loss_qty
rename sa3iiq18b unit_cd
rename sa3iiq18b_os unit_other
rename sa3iiq18c loss_percentage

keep hhid crop_cd loss_qty unit_cd unit_other loss_percentage
replace loss_qty = 0 if loss_qty==.
drop if loss_qty==0 & loss_percentage==.
replace loss_qty = 0 if loss_percentage~=.
replace unit_cd = . if loss_percentage~=.
replace unit_other = "" if loss_percentage~=.

joinby crop_cd unit_cd unit_other using "raw data/ag_conv_w3.dta", unmatched(both) _merge(_merge)
drop if _merge==2

* Convert to KG
replace loss_qty = loss_qty * conv_national if conv_national~=.
replace unit_cd = 1 if conv_national~=.
replace unit_other = "" if conv_national~=.

keep hhid crop_cd loss_qty unit_cd unit_other loss_percentage

collapse (sum) loss_qty , by (hhid crop_cd unit_cd loss_percentage)

save "clean data/intermediate/Nigeria1516_harvest_temp_loss.dta", replace

drop if loss_percentage~=.
drop loss_percentage
save "clean data/intermediate/Nigeria1516_harvest_loss_byunit.dta", replace

use "clean data/intermediate/Nigeria1516_harvest_temp_loss.dta", clear

drop if loss_percentage==.
drop loss_qty unit_cd
save "clean data/intermediate/Nigeria1516_harvest_loss_bypercent.dta", replace

* Post Harvest Data
use "raw data/secta3i_harvestw3.dta", clear
rename cropcode crop_cd
rename sa3iq6i harvest_qty
rename sa3iq6ii unit_cd
rename sa3iq6ii_os unit_other
rename sa3iq6a s_est_value

keep hhid crop_cd harvest_qty unit_cd unit_other s_est_value
drop if harvest_qty==. & s_est_value==.
drop if unit_cd==.

joinby crop_cd unit_cd unit_other using "raw data/ag_conv_w3.dta", unmatched(both) _merge(_merge)
drop if _merge==2

* Convert to KG
replace harvest_qty = harvest_qty * conv_national if conv_national~=.
replace unit_cd = 1 if conv_national~=.
replace unit_other = "" if conv_national~=.

keep hhid crop_cd harvest_qty unit_cd unit_other s_est_value

collapse (sum) harvest_qty s_est_value, by (hhid crop_cd unit_cd unit_other)

joinby hhid crop_cd unit_cd using "clean data/intermediate/Nigeria1516_harvest_loss_byunit.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby hhid crop_cd using "clean data/intermediate/Nigeria1516_harvest_loss_bypercent.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

mvencode loss_qty loss_percentage, mv(0)

replace harvest_qty = harvest_qty - loss_qty
replace harvest_qty = 0 if harvest_qty<0
replace harvest_qty = harvest_qty * ((100-loss_percentage)/100)

joinby hhid using "clean data/Nigeria1516_location.dta", unmatched(none)

joinby crop_cd unit_cd unit_other rural Zone State LGA EA using "clean data/intermediate/Nigeria1516_harvest_prices.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

* Use prices to determine missing prices from regions where the crop was not sold
* Count observations with est_price reported by geographic location.
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float count_1 = count(1) if est_price~=.
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float count_2 = count(1) if est_price~=.
by crop_cd unit_cd unit_other rural Zone State, sort : egen float count_3 = count(1) if est_price~=.
by crop_cd unit_cd unit_other rural Zone, sort : egen float count_4 = count(1) if est_price~=.
by crop_cd unit_cd unit_other rural, sort : egen float count_5 = count(1) if est_price~=.
* Calculate median of est_price by geographic location - later used for predictive purposes.
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float temp_est_est_price_1 = median(est_price) if est_price~=.
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float temp_est_est_price_2 = median(est_price) if est_price~=.
by crop_cd unit_cd unit_other rural Zone State, sort : egen float temp_est_est_price_3 = median(est_price) if est_price~=.
by crop_cd unit_cd unit_other rural Zone, sort : egen float temp_est_est_price_4 = median(est_price) if est_price~=.
by crop_cd unit_cd unit_other rural, sort : egen float temp_est_est_price_5 = median(est_price) if est_price~=.
mvencode temp_*, mv(0)
by crop_cd unit_cd unit_other rural Zone State LGA EA, sort : egen float est_est_price_1 = max(temp_est_est_price_1)
by crop_cd unit_cd unit_other rural Zone State LGA, sort : egen float est_est_price_2 = max(temp_est_est_price_2)
by crop_cd unit_cd unit_other rural Zone State, sort : egen float est_est_price_3 = max(temp_est_est_price_3)
by crop_cd unit_cd unit_other rural Zone, sort : egen float est_est_price_4 = max(temp_est_est_price_4)
by crop_cd unit_cd unit_other rural, sort : egen float est_est_price_5 = max(temp_est_est_price_5)
drop temp*
mvdecode est_est_*, mv(0)
* Build est_price prediction variable if at least 10 observations of reported est_price in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_price = est_est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_price==.
replace est_est_price = est_est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_price==.
replace est_est_price = est_est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_price==.
replace est_est_price = est_est_price_4 if track==4
replace track = 5 if est_est_price==.
replace est_est_price = est_est_price_5 if track==5
drop est_est_price_* count_* track

replace est_price = est_est_price if est_price==.
* 2392 changes
drop est_est_price

generate harvest_value = harvest_qty * est_price

drop if harvest_value==.

collapse (sum) harvest_value, by (hhid)

replace harvest_value = harvest_value/365

save "clean data/intermediate/Nigeria1516_harvest.dta", replace

* Harvest Sales Data - Sales Estimates
use "raw data/secta3ii_harvestw3.dta", clear
rename cropcode crop_cd
rename sa3iiq5a sales_qty
rename sa3iiq5b unit_cd
rename sa3iiq5b_os unit_other
rename sa3iiq6 s_sales_value

keep hhid crop_cd sales_qty unit_cd unit_other s_sales_value
drop if sales_qty==0
drop if sales_qty==.
replace s_sales_value = . if s_sales_value==0

joinby crop_cd unit_cd unit_other using "raw data/ag_conv_w3.dta", unmatched(both) _merge(_merge)
drop if _merge==2

* Convert to KG
replace sales_qty = sales_qty * conv_national if conv_national~=.
replace unit_cd = 1 if conv_national~=.
replace unit_other = "" if conv_national~=.

keep hhid crop_cd sales_qty unit_cd unit_other s_sales_value

joinby hhid using "clean data/Nigeria1516_location.dta", unmatched(none)

joinby crop_cd unit_cd unit_other rural Zone State LGA EA using "clean data/intermediate/Nigeria1516_harvest_prices.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge

generate sales_value = sales_qty * est_price

drop if sales_value==.

collapse (sum) sales_value, by (hhid)

replace sales_value = sales_value/365

save "clean data/intermediate/Nigeria1516_harvest_sales.dta", replace

** Livestock
* General Livestock
use "raw data/sect11i_plantingw3.dta", clear
rename animal_cd livestock_code
rename s11iq13 livestock_givenaway
rename s11iq14 livestock_as_payment
rename s11iq16 livestock_sold
rename s11iq17 livestock_sold_value
rename s11iq19a livestock_slaughtered_sold
rename s11iq19b livestock_consumption
keep hhid livestock*
mvdecode livestock_*, mv(0)
drop if livestock_givenaway==. & livestock_as_payment==. & livestock_sold==. & livestock_sold_value==. & livestock_slaughtered_sold==. & livestock_consumption==.

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_sold_value / livestock_sold

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Nigeria1516_demographics.dta", unmatched(none)

* Count observations with revealed_price reported by geographic location.
by livestock_code rural Zone State LGA EA, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_code rural Zone State LGA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_code rural Zone State, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_code rural Zone, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_code rural, sort : egen float count_5 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by livestock_code rural Zone State LGA EA, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_code rural Zone State LGA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_code rural Zone State, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_code rural Zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_code rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by livestock_code rural Zone State LGA EA, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_code rural Zone State LGA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_code rural Zone State, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_code rural Zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_code rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if predict_price==.
replace predict_price = predict_price_5 if track==5
drop predict_price_* count_* track

mvencode livestock_*, mv(0)
gen livestock_to_income = livestock_givenaway+livestock_as_payment+livestock_sold+livestock_slaughtered_sold+livestock_consumption
gen livestock_to_sales = livestock_sold+livestock_slaughtered_sold

gen livestock_income_yr = livestock_to_income*predict_price
gen livestock_sold_yr = livestock_to_sales*predict_price

keep hhid livestock_income_yr livestock_sold_yr

gen livestock_income = livestock_income_yr/365
gen livestock_sold = livestock_sold_yr/365

collapse (sum) livestock_*, by (hhid)

save "clean data/intermediate/Nigeria1516_livestock.dta", replace

* Livestock Products
use "raw data/sect11k_plantingw3.dta", clear
rename prod_cd livestock_product_code
rename s11kq2 livestock_product_months
rename s11kq3a livestock_product_quantity
rename s11kq3b livestock_product_unit
rename s11kq5a livestock_sales_quantity
rename s11kq5b livestock_sales_unit
rename s11kq6 livestock_sales_value
keep hhid livestock*
mvdecode livestock_*, mv(0)
drop if livestock_product_quantity==. & livestock_sales_quantity==.

* estimate value of livestock income with estimated prices from sales
* revealed prices
gen revealed_price = livestock_sales_value / livestock_sales_quantity

* Estimate prices based on median price of minimum 10 observations by smallest spatial variable possible
joinby using "clean data/Nigeria1516_demographics.dta", unmatched(none)

* Count observations with revealed_price reported by geographic location.
by livestock_product_code livestock_sales_unit rural Zone State LGA EA, sort : egen float count_1 = count(1) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone State LGA, sort : egen float count_2 = count(1) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone State, sort : egen float count_3 = count(1) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone, sort : egen float count_4 = count(1) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural, sort : egen float count_5 = count(1) if revealed_price~=.
* Calculate median of revealed_price by geographic location - later used for predictive purposes.
by livestock_product_code livestock_sales_unit rural Zone State LGA EA, sort : egen float temp_predict_price_1 = median(revealed_price) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone State LGA, sort : egen float temp_predict_price_2 = median(revealed_price) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone State, sort : egen float temp_predict_price_3 = median(revealed_price) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural Zone, sort : egen float temp_predict_price_4 = median(revealed_price) if revealed_price~=.
by livestock_product_code livestock_sales_unit rural, sort : egen float temp_predict_price_5 = median(revealed_price) if revealed_price~=.
mvencode temp_*, mv(0)
by livestock_product_code livestock_sales_unit rural Zone State LGA EA, sort : egen float s_predict_price_1 = max(temp_predict_price_1)
by livestock_product_code livestock_sales_unit rural Zone State LGA, sort : egen float s_predict_price_2 = max(temp_predict_price_2)
by livestock_product_code livestock_sales_unit rural Zone State, sort : egen float s_predict_price_3 = max(temp_predict_price_3)
by livestock_product_code livestock_sales_unit rural Zone, sort : egen float s_predict_price_4 = max(temp_predict_price_4)
by livestock_product_code livestock_sales_unit rural, sort : egen float s_predict_price_5 = max(temp_predict_price_5)
by livestock_product_code livestock_product_unit rural Zone State LGA EA, sort : egen float predict_price_1 = max(temp_predict_price_1)
by livestock_product_code livestock_product_unit rural Zone State LGA, sort : egen float predict_price_2 = max(temp_predict_price_2)
by livestock_product_code livestock_product_unit rural Zone State, sort : egen float predict_price_3 = max(temp_predict_price_3)
by livestock_product_code livestock_product_unit rural Zone, sort : egen float predict_price_4 = max(temp_predict_price_4)
by livestock_product_code livestock_product_unit rural, sort : egen float predict_price_5 = max(temp_predict_price_5)
drop temp*
* Build revealed_price prediction variable if at least 10 observations of reported revealed_price in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate predict_price = predict_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & predict_price==.
replace predict_price = predict_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & predict_price==.
replace predict_price = predict_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & predict_price==.
replace predict_price = predict_price_4 if track==4
replace track = 5 if predict_price==.
replace predict_price = predict_price_5 if track==5
generate s_track = 1 if count_1>=10 & count_1<100000
generate s_predict_price = s_predict_price_1 if s_track==1
replace s_track = 2 if count_2>=10 & count_2<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_2 if s_track==2
replace s_track = 3 if count_3>=10 & count_3<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_3 if s_track==3
replace s_track = 4 if count_4>=10 & count_4<100000 & s_predict_price==.
replace s_predict_price = s_predict_price_4 if s_track==4
replace s_track = 5 if s_predict_price==.
replace s_predict_price = s_predict_price_5 if s_track==5
drop predict_price_* s_predict_price_* count_* track s_track

mvencode livestock_*, mv(0)

gen livestock_product_income_yr = livestock_product_quantity*livestock_product_months*predict_price
gen livestock_product_sold_yr = livestock_sales_quantity*s_predict_price

keep hhid livestock_product_income_yr livestock_product_sold_yr
drop if livestock_product_income_yr==0 & livestock_product_sold_yr==0

gen livestock_product_income = livestock_product_income_yr/365
gen livestock_product_sold = livestock_product_sold_yr/365

collapse (sum) livestock_*, by (hhid)

save "clean data/intermediate/Nigeria1516_livestock_products.dta", replace

* Fishing
use "raw data/secta9a2_harvestw3.dta", clear
rename fish_cd fish_species
rename sa9aq3 fish_weeks_prod
rename sa9aq4a1 fish_capture_qty
rename sa9aq4a2 fish_capture_unit
rename sa9aq4b1 fish_harvest_qty
rename sa9aq4b2 fish_harvest_unit
rename sa9aq5a fish_capture_sales_qty
rename sa9aq5b fish_capture_sales_unit
rename sa9aq6 fish_capture_sales_price
rename sa9aq7a fish_harvest_sales_qty
rename sa9aq7b fish_harvest_sales_unit
rename sa9aq8 fish_harvest_sales_price
rename sa9aq9 fish_weeks_sales
rename sa9aq17 fish_weeks_processed
rename sa9aq13a fish_processed_qty
rename sa9aq13b fish_processed_unit
rename sa9aq14 fish_processed_price
keep hhid fish_*
drop fish_desc fish_other
save "clean data/intermediate/Nigeria1516_temp_fish.dta", replace
use "raw data/secta9a2_harvestw3.dta", clear
rename fish_cd fish_species
rename sa9aq17 fish_weeks_processed
rename sa9aq15a fish_processed_qty
rename sa9aq15b fish_processed_unit
rename sa9aq16 fish_processed_price
keep hhid fish_*
drop fish_desc fish_other
append using  "clean data/intermediate/Nigeria1516_temp_fish.dta"

mvdecode fish_*, mv(0)
replace fish_weeks_processed = . if fish_weeks_processed>52
replace fish_weeks_processed = . if fish_processed_qty==.
replace fish_weeks_sales = . if fish_capture_sales_qty ==. & fish_harvest_sales_qty ==.

* Price estimates are estimated medians at national level due to low counts of species / unit pairings
by fish_species fish_processed_unit, sort : egen float temp_pr_processed = median(fish_processed_price) if fish_processed_price~=.
by fish_species fish_processed_unit, sort : egen float temp_pr_capture = median(fish_capture_sales_price) if fish_capture_sales_price~=.
by fish_species fish_processed_unit, sort : egen float temp_pr_harvest = median(fish_harvest_sales_price) if fish_harvest_sales_price~=.
mvencode temp_*, mv(0)
by fish_species fish_processed_unit, sort : egen float pr_processed = max(temp_pr_processed)
by fish_species fish_processed_unit, sort : egen float pr_capture = max(temp_pr_capture)
by fish_species fish_processed_unit, sort : egen float pr_harvest = max(temp_pr_harvest)

mvencode fish_*, mv(0)

gen fish_income_yr = fish_capture_qty*pr_capture*fish_weeks_prod + fish_harvest_qty*pr_harvest*fish_weeks_prod
gen fish_sold_yr = fish_capture_sales_qty*pr_capture*fish_weeks_sales + fish_harvest_sales_qty*pr_harvest*fish_weeks_sales
gen fish_nfe_gross_yr = fish_processed_qty*pr_processed*fish_weeks_processed

keep hhid fish_income_yr fish_sold_yr fish_nfe_gross_yr

gen fish_income = fish_income_yr/365
gen fish_sold = fish_sold_yr/365
gen fish_nfe_gross = fish_nfe_gross_yr/365

collapse (sum) fish_*, by (hhid)
drop if fish_income==0 & fish_sold==0 & fish_nfe_gross==0
save "clean data/intermediate/Nigeria1516_fish.dta", replace


** Non-Employment Income

* Rent proceeds from ag supplies
use "raw data/secta4_harvestw3.dta", clear
rename sa4q7 rent_supplies
drop if rent_supplies==.
collapse (sum) rent_supplies, by (hhid)
replace rent_supplies = rent_supplies/365
save "clean data/intermediate/Nigeria1516_rent_supplies.dta", replace

* Remittances
use "raw data/sect6_harvestw3.dta", clear
rename s6q4a remittances_cash
rename s6q4b remittances_cash_currency
rename s6q8a remittances_inkind
rename s6q8b remittances_inkind_currency
keep hhid remittances*
keep if remittances_cash~=. | remittances_inkind~=.
gen us_naira_conver = 0.0050244944
gen euro_naira_conver = 0.0045185425
replace remittances_cash = remittances_cash / us_naira_conver if remittances_cash_currency==1
replace remittances_cash = remittances_cash / euro_naira_conver if remittances_cash_currency==2
replace remittances_inkind = remittances_inkind / us_naira_conver if remittances_inkind_currency==1
mvdecode remittances*, mv(0)
mvencode remittances*, mv(0)
collapse (sum) remittances_cash remittances_inkind, by (hhid)
drop if remittances_cash==0 & remittances_inkind==0
replace remittances_cash = remittances_cash/365
replace remittances_inkind = remittances_inkind/365
save "clean data/intermediate/Nigeria1516_remittances.dta", replace

* Other general income
use "raw data/sect13_harvestw3.dta", clear
rename s13q2 interest_income
rename s13q5 rental_income
rename s13q8 gen_other_income
keep hhid interest_income rental_income gen_other_income
mvdecode interest_income rental_income gen_other_income, mv(0)
mvencode interest_income rental_income gen_other_income, mv(0)
replace gen_other_income = gen_other_income + interest_income + rental_income
drop if gen_other_income==0
collapse (sum) gen_other_income, by (hhid)
replace gen_other_income = gen_other_income/365
save "clean data/intermediate/Nigeria1516_gen_other_inc.dta", replace

* Safety Net
use "raw data/sect14_harvestw3.dta", clear
rename s14q2a safety_cash
rename s14q2d safety_food
rename s14q2e safety_other
mvdecode safety*, mv(0)
mvencode safety*, mv(0)
generate safety_net = safety_cash + safety_food + safety_other
drop if safety_net==0
collapse (sum) safety_net, by (hhid)
replace safety_net = safety_net/365
save "clean data/intermediate/Nigeria1516_safety_net_inc.dta", replace



* Costs
* Seeds
use "raw data/sect11e_plantingw3.dta", clear
mvdecode s11eq12 s11eq19 s11eq21 s11eq31 s11eq33, mv(0)
mvencode s11eq12 s11eq19 s11eq21 s11eq31 s11eq33, mv(0)
gen cost_seeds = s11eq12 + s11eq19 + s11eq21 + s11eq31 + s11eq33
collapse (sum) cost_seeds, by (hhid)
drop if cost_seeds==0
save "clean data/intermediate/Nigeria1516_costs_seeds.dta", replace

* Fertilizer
use "raw data/secta11c2_harvestw3.dta", clear
mvdecode s11c2q4a s11c2q4b s11c2q5a s11c2q5b s11c2q13a s11c2q13b s11c2q14a s11c2q14b, mv(0)
mvencode s11c2q4a s11c2q4b s11c2q5a s11c2q5b s11c2q13a s11c2q13b s11c2q14a s11c2q14b, mv(0)
gen cost_fertilizer = s11c2q4a + s11c2q4b + s11c2q5a + s11c2q5b + s11c2q13a + s11c2q13b + s11c2q14a + s11c2q14b
collapse (sum) cost_fertilizer, by (hhid)
drop if cost_fertilizer==0
save "clean data/intermediate/Nigeria1516_costs_fertilizer_1.dta", replace

use "raw data/secta11d_harvestw3.dta", clear
mvdecode s11dq10 s11dq17 s11dq19 s11dq29 s11dq31 s11dq39 s11dq41, mv(0)
mvencode s11dq10 s11dq17 s11dq19 s11dq29 s11dq31 s11dq39 s11dq41, mv(0)
gen cost_fertilizer = s11dq10 + s11dq17 + s11dq19 + s11dq29 + s11dq31 + s11dq39 + s11dq41
collapse (sum) cost_fertilizer, by (hhid)
drop if cost_fertilizer==0
append using "clean data/intermediate/Nigeria1516_costs_fertilizer_1.dta"
collapse (sum) cost_fertilizer, by (hhid)
drop if cost_fertilizer==0
save "clean data/intermediate/Nigeria1516_costs_fertilizer.dta", replace

* Hired Crop Labor
use "raw data/sect11c1_plantingw3.dta", clear
mvdecode s11c1q2 s11c1q3 s11c1q4 s11c1q5 s11c1q6 s11c1q7 s11c1q8 s11c1q9 s11c1q10, mv(0)
mvencode s11c1q2 s11c1q3 s11c1q4 s11c1q5 s11c1q6 s11c1q7 s11c1q8 s11c1q9 s11c1q10, mv(0)
gen cost_crop_labor = (s11c1q2*s11c1q3*s11c1q4) + (s11c1q5*s11c1q6*s11c1q7) + (s11c1q8*s11c1q9*s11c1q10)
collapse (sum) cost_crop_labor, by (hhid)
drop if cost_crop_labor==0
save "clean data/intermediate/Nigeria1516_costs_crop_labor_1.dta", replace

use "raw data/secta2_harvestw3.dta", clear
mvdecode sa2q1c sa2q1d sa2q1e sa2q1f sa2q1g sa2q1h sa2q1i sa2q1j sa2q1k, mv(0)
mvencode sa2q1c sa2q1d sa2q1e sa2q1f sa2q1g sa2q1h sa2q1i sa2q1j sa2q1k, mv(0)
gen cost_grow_labor_cash = (sa2q1c*sa2q1d*sa2q1e) + (sa2q1f*sa2q1g*sa2q1h) + (sa2q1i*sa2q1j*sa2q1k)
mvdecode sa2q2 sa2q3 sa2q4 sa2q5 sa2q6 sa2q7 sa2q8 sa2q9 sa2q10, mv(0)
mvencode sa2q2 sa2q3 sa2q4 sa2q5 sa2q6 sa2q7 sa2q8 sa2q9 sa2q10, mv(0)
gen cost_harvesting_labor_cash = (sa2q2*sa2q3*sa2q4) + (sa2q5*sa2q6*sa2q7) + (sa2q8*sa2q9*sa2q10)
collapse (sum) cost_*, by (hhid)
drop if cost_grow_labor_cash==0 & cost_harvesting_labor_cash==0
save "clean data/intermediate/Nigeria1516_costs_crop_labor_2.dta", replace

use "raw data/secta2_harvestw3.dta", clear
keep hhid sa2q1m_*
rename sa2q1m_a crop_cd
rename sa2q1m_b qty
rename sa2q1m_c unit_cd
save "clean data/intermediate/Nigeria1516_costs_crop_labor_temp.dta", replace
use "raw data/secta2_harvestw3.dta", clear
keep hhid sa2q11*
rename sa2q11a crop_cd
rename sa2q11b qty
rename sa2q11c unit_cd
append using "clean data/intermediate/Nigeria1516_costs_crop_labor_temp.dta"
collapse (sum) qty, by (hhid crop_cd unit_cd)
joinby using "clean data/Nigeria1516_location.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby using "clean data/intermediate/Nigeria1516_harvest_prices.dta", unmatched(both) _merge(_merge)
drop _merge
* Use prices to determine missing prices from regions where the crop was not sold
* Count observations with est_price reported by geographic location.
by crop_cd unit_cd rural Zone State LGA EA, sort : egen float count_1 = count(1) if est_price~=.
by crop_cd unit_cd rural Zone State LGA, sort : egen float count_2 = count(1) if est_price~=.
by crop_cd unit_cd rural Zone State, sort : egen float count_3 = count(1) if est_price~=.
by crop_cd unit_cd rural Zone, sort : egen float count_4 = count(1) if est_price~=.
by crop_cd unit_cd rural, sort : egen float count_5 = count(1) if est_price~=.
* Calculate median of est_price by geographic location - later used for predictive purposes.
by crop_cd unit_cd rural Zone State LGA EA, sort : egen float temp_est_est_price_1 = median(est_price) if est_price~=.
by crop_cd unit_cd rural Zone State LGA, sort : egen float temp_est_est_price_2 = median(est_price) if est_price~=.
by crop_cd unit_cd rural Zone State, sort : egen float temp_est_est_price_3 = median(est_price) if est_price~=.
by crop_cd unit_cd rural Zone, sort : egen float temp_est_est_price_4 = median(est_price) if est_price~=.
by crop_cd unit_cd rural, sort : egen float temp_est_est_price_5 = median(est_price) if est_price~=.
mvencode temp_*, mv(0)
by crop_cd unit_cd rural Zone State LGA EA, sort : egen float est_est_price_1 = max(temp_est_est_price_1)
by crop_cd unit_cd rural Zone State LGA, sort : egen float est_est_price_2 = max(temp_est_est_price_2)
by crop_cd unit_cd rural Zone State, sort : egen float est_est_price_3 = max(temp_est_est_price_3)
by crop_cd unit_cd rural Zone, sort : egen float est_est_price_4 = max(temp_est_est_price_4)
by crop_cd unit_cd rural, sort : egen float est_est_price_5 = max(temp_est_est_price_5)
drop temp*
mvdecode est_est_*, mv(0)
* Build est_price prediction variable if at least 10 observations of reported est_price in location counted above.
* Start with smallest qualifying geographic region
generate track = 1 if count_1>=10 & count_1<100000
generate est_est_price = est_est_price_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_est_price==.
replace est_est_price = est_est_price_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_est_price==.
replace est_est_price = est_est_price_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_est_price==.
replace est_est_price = est_est_price_4 if track==4
replace track = 5 if est_est_price==.
replace est_est_price = est_est_price_5 if track==5
drop est_est_price_* count_* track
replace est_price = est_est_price if est_price==.
drop est_est_price
generate cost_labor_inkind = qty * est_price
keep if cost_labor_inkind ~=.
collapse (sum) cost_*, by (hhid)

joinby using "clean data/intermediate/Nigeria1516_costs_crop_labor_1.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_crop_labor_2.dta", unmatched(both) _merge(_merge)
drop _merge
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
replace cost_crop_labor = cost_crop_labor + cost_labor_inkind + cost_grow_labor_cash + cost_harvesting_labor_cash
keep hhid cost_crop_labor

save "clean data/intermediate/Nigeria1516_costs_crop_labor.dta", replace

* Hired Animal Labor & Animal Feed
use "raw data/sect11j_plantingw3.dta", clear
mvdecode s11jq2a s11jq2b, mv(0)
mvencode s11jq2a s11jq2b, mv(0)
gen cost_animal_labor = s11jq2a + s11jq2b if item_cd==1 | item_cd==4
*gen cost_animal_fuel = 
gen cost_animal_feed = s11jq2a + s11jq2b if item_cd==2 | item_cd==6
collapse (sum) cost_*, by (hhid)
drop if cost_animal_labor==0 & cost_animal_feed==0
save "clean data/intermediate/Nigeria1516_costs_animal_1.dta", replace

use "raw data/secta9b3_harvestw3.dta", clear
gen cost_animal_labor = sa9bq10a * sa9bq10b * sa9bq15a
mvdecode sa9bq22a sa9bq22b, mv(0)
mvencode sa9bq22a sa9bq22b, mv(0)
gen cost_animal_feed = sa9bq22a + sa9bq22b
collapse (sum) cost_*, by (hhid)
drop if cost_animal_labor==0 & cost_animal_feed==0
save "clean data/intermediate/Nigeria1516_costs_animal_2.dta", replace

use "clean data/intermediate/Nigeria1516_costs_animal_1.dta", clear
append using "clean data/intermediate/Nigeria1516_costs_animal_2.dta"
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (hhid)
save "clean data/intermediate/Nigeria1516_costs_animal_labor_feed.dta", replace

* Animal/Fish fuel and transport
use "raw data/secta9b2_harvestw3.dta", clear
rename sa9bq8 cost_animal_fuel
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (hhid)
drop if cost_animal_fuel==0
save "clean data/intermediate/Nigeria1516_costs_animal_fuel.dta", replace

* Land Rent
use "raw data/sect11b1_plantingw3.dta", clear
mvdecode s11b1q13 s11b1q14, mv(0)
mvencode s11b1q13 s11b1q14, mv(0)
gen cost_land_rent = s11b1q13 + s11b1q14
collapse (sum) cost_*, by (hhid)
drop if cost_land_rent==0
save "clean data/intermediate/Nigeria1516_land_rent_1.dta", replace
use "raw data/secta1_harvestw3.dta", clear
mvdecode sa1q19 sa1q20, mv(0)
mvencode sa1q19 sa1q20, mv(0)
gen cost_land_rent = sa1q19 + sa1q20
collapse (sum) cost_*, by (hhid)
drop if cost_land_rent==0
append using "clean data/intermediate/Nigeria1516_land_rent_1.dta"
collapse (sum) cost_*, by (hhid)
save "clean data/intermediate/Nigeria1516_costs_land_rent.dta", replace

* Machine & Animal Rent
use "raw data/secta11c2_harvestw3.dta", clear
mvdecode s11c2q21 s11c2q23a s11c2q24a s11c2q25 s11c2q32 s11c2q33, mv(0)
mvencode s11c2q21 s11c2q23a s11c2q24a s11c2q25 s11c2q32 s11c2q33, mv(0)
* If time unit for animal rental was 1 hour, assume 8 hour days
gen cost_machine_rent = 0
replace cost_machine_rent = cost_machine_rent + s11c2q21*s11c2q23a/8 if s11c2q23b==1
replace cost_machine_rent = cost_machine_rent + s11c2q21*s11c2q23a if s11c2q23b==2
replace cost_machine_rent = cost_machine_rent + s11c2q23a if s11c2q23b==6
replace cost_machine_rent = cost_machine_rent + s11c2q21*s11c2q24a/8 if s11c2q24b==1
replace cost_machine_rent = cost_machine_rent + s11c2q21*s11c2q24a if s11c2q24b==2
replace cost_machine_rent = cost_machine_rent + s11c2q24a if s11c2q24b==6
replace cost_machine_rent = cost_machine_rent + s11c2q25 + s11c2q32 + s11c2q33
collapse (sum) cost_*, by (hhid)
drop if cost_machine==0
save "clean data/intermediate/Nigeria1516_costs_machine_1.dta", replace

use "raw data/secta4_harvestw3.dta", clear
mvdecode sa4q7, mv(0)
mvencode sa4q7, mv(0)
* If time unit for animal rental was 1 hour, assume 8 hour days
gen cost_machine_rent = sa4q7
collapse (sum) cost_*, by (hhid)
drop if cost_machine==0
append using "clean data/intermediate/Nigeria1516_costs_machine_1.dta"
collapse (sum) cost_*, by (hhid)
drop if cost_machine==0
save "clean data/intermediate/Nigeria1516_costs_machine.dta", replace

* Transport Costs
* NA

* Aggregate Costs
use "clean data/intermediate/Nigeria1516_costs_seeds.dta", clear
joinby using "clean data/intermediate/Nigeria1516_costs_fertilizer.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_crop_labor.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_land_rent.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_machine.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_animal_labor_feed.dta", unmatched(both) _merge(_merge)
drop _merge
joinby using "clean data/intermediate/Nigeria1516_costs_animal_fuel.dta", unmatched(both) _merge(_merge)
drop _merge

* Convert Missing to zero
mvdecode cost_*, mv(0)
mvencode cost_*, mv(0)
collapse (sum) cost_*, by (hhid)
local costs "cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_animal_fuel"
foreach c in `costs' {
	replace `c' = `c'/365
}
*

save "clean data/intermediate/Nigeria1516_costs.dta", replace

******************
* FOOD EXPENDITURE
******************

* HARVEST SEASON

* Food away from home
use "raw data/sect10a_harvestw3.dta", clear

g foodaway1 = s10aq2 

collapse (sum) foodaway1, by (hhid)

replace foodaway1 = foodaway1/7

label var foodaway1 "Food away - hh food consumption per day - harvest season"


tempfile foodaway1
save `foodaway1', replace

* Food consumed within the household 

use "raw data/sect10b_harvestw3.dta", clear

ren s10bq2a q_tot
ren s10bq2b unit_tot
ren s10bq3a q_purch
ren s10bq3b unit
ren s10bq4 v_purch

g price = v_purch/q_purch

preserve

drop if price==.

bys zone state lga sector ea item_cd unit: egen m_price1 = median (price)

collapse (mean) m_price1, by (zone state lga sector ea item_cd unit)

tempfile m_price1
save `m_price1',replace 
restore

preserve

drop if price==.
bys zone state lga sector item_cd unit: egen m_price2 = median (price)

collapse (mean) m_price2, by (zone state lga sector  item_cd unit)

tempfile m_price2
save `m_price2',replace 
restore

preserve

drop if price==.

bys zone state lga  item_cd unit: egen m_price3 = median (price)

collapse (mean) m_price3, by (zone state lga  item_cd unit)

tempfile m_price3
save `m_price3',replace 
restore

preserve

drop if price==.

bys zone state item_cd unit: egen m_price4 = median (price)

collapse (mean) m_price4, by (zone state  item_cd unit)

tempfile m_price4
save `m_price4',replace 
restore

preserve

drop if price==.

bys zone item_cd unit: egen m_price5 = median (price)

collapse (mean) m_price5, by (zone item_cd unit)

tempfile m_price5
save `m_price5',replace 
restore

preserve

drop if price==.

bys item_cd unit: egen m_price6 = median (price)

collapse (mean) m_price6, by ( item_cd unit)

tempfile m_price6
save `m_price6',replace 
restore

use "raw data/sect10b_harvestw3.dta", clear

ren s10bq2a q_tot
ren s10bq2b unit


merge m:1 zone state lga sector ea item_cd unit using `m_price1', nogen keep (1 3)
merge m:1 zone state lga sector item_cd unit using `m_price2', nogen keep (1 3)
merge m:1 zone state lga  item_cd unit using `m_price3', nogen keep (1 3)
merge m:1 zone state  item_cd unit using `m_price4', nogen keep (1 3)
merge m:1 zone  item_cd unit using `m_price5', nogen keep (1 3)
merge m:1  item_cd unit using `m_price6', nogen keep (1 3)

g unit_price = m_price1
replace unit_price = m_price2 if unit_price==.
replace unit_price= m_price3 if unit_price==.
replace unit_price = m_price4 if unit_price==.
replace unit_price = m_price5 if unit_price==.
replace unit_price = m_price6 if unit_price==.

g foodin1 = q_tot*unit_price

count if q_tot!=. & q_tot!=0 & foodin1==.  // 245 

collapse (sum) foodin1, by (hhid)

replace foodin1 = foodin1/7

label var foodin1 "Food within the household - hh food consumption per day - harvest season"


tempfile foodin1
save `foodin1', replace


* PLANTING SEASON

* Food away from home

use "raw data/sect7a_plantingw3.dta", clear

g foodaway2 = s7aq2

collapse (sum) foodaway2, by (hhid)

replace foodaway2 = foodaway2/7


label var foodaway2 "Food away - hh food consumption per day - planting season"


tempfile foodaway2
save `foodaway2', replace


* Food consumed within the household 

use "raw data/sect7b_plantingw3.dta", clear

ren s7bq2a q_tot
ren s7bq2b unit_tot
ren s7bq3a q_purch
ren s7bq3b unit
ren s7bq4 v_purch

g price = v_purch/q_purch

preserve

drop if price==.

bys zone state lga sector ea item_cd unit: egen m_price1 = median (price)

collapse (mean) m_price1, by (zone state lga sector ea item_cd unit)

tempfile m_price1_pp
save `m_price1_pp',replace 
restore

preserve

drop if price==.
bys zone state lga sector item_cd unit: egen m_price2 = median (price)

collapse (mean) m_price2, by (zone state lga sector  item_cd unit)

tempfile m_price2_pp
save `m_price2_pp',replace 
restore

preserve

drop if price==.

bys zone state lga  item_cd unit: egen m_price3 = median (price)

collapse (mean) m_price3, by (zone state lga  item_cd unit)

tempfile m_price3_pp
save `m_price3_pp',replace 
restore

preserve

drop if price==.

bys zone state item_cd unit: egen m_price4 = median (price)

collapse (mean) m_price4, by (zone state  item_cd unit)

tempfile m_price4_pp
save `m_price4_pp',replace 
restore

preserve

drop if price==.

bys zone item_cd unit: egen m_price5 = median (price)

collapse (mean) m_price5, by (zone item_cd unit)

tempfile m_price5_pp
save `m_price5_pp',replace 
restore

preserve

drop if price==.

bys item_cd unit: egen m_price6 = median (price)

collapse (mean) m_price6, by ( item_cd unit)

tempfile m_price6_pp
save `m_price6_pp',replace 
restore

use "raw data/sect7b_plantingw3.dta", clear

ren s7bq2a q_tot
ren s7bq2b unit


merge m:1 zone state lga sector ea item_cd unit using `m_price1_pp', nogen keep (1 3)
merge m:1 zone state lga sector item_cd unit using `m_price2_pp', nogen keep (1 3)
merge m:1 zone state lga  item_cd unit using `m_price3_pp', nogen keep (1 3)
merge m:1 zone state  item_cd unit using `m_price4_pp', nogen keep (1 3)
merge m:1 zone  item_cd unit using `m_price5_pp', nogen keep (1 3)
merge m:1  item_cd unit using `m_price6_pp', nogen keep (1 3)

g unit_price = m_price1
replace unit_price = m_price2 if unit_price==.
replace unit_price= m_price3 if unit_price==.
replace unit_price = m_price4 if unit_price==.
replace unit_price = m_price5 if unit_price==.
replace unit_price = m_price6 if unit_price==.

g foodin2 = q_tot*unit_price

count if q_tot!=. & q_tot!=0 & foodin2==.  // 183

collapse (sum) foodin2, by (hhid)

replace foodin2 = foodin2/7

label var foodin2 "Food within the household - hh food consumption per day - planting season"


tempfile foodin2
save `foodin2', replace


* Merge harvest and planting seasons

use `foodin1', clear
merge 1:1 hhid using `foodin2', nogen
merge 1:1 hhid using `foodaway1', nogen
merge 1:1 hhid using `foodaway2', nogen

recode foodin1 foodin2 foodaway1 foodaway2 (0=.)

egen foodin = rowmean (foodin1 foodin2)
egen foodaway = rowmean (foodaway1 foodaway2)

egen foodexp = rsum (foodin foodaway)

label var foodexp "Household  food consumption expenditure per day"

tempfile foodexp
save `foodexp', replace 


************************
* NON - FOOD EXPENDITURE
************************

* HARVEST SEASON

* 7 days recall 

use "raw data/sect11a_harvestw3.dta", clear

g nonfoodexp7_1 = s11aq2

collapse (sum) nonfoodexp7_1, by (hhid)

replace nonfoodexp7_1 = nonfoodexp7_1/7 

label var nonfoodexp7_1 " 7 days recall - hh non-food expenditure per day - harvest season"

tempfile nonfoodexp7_1
save `nonfoodexp7_1',replace

* 1 month recall 

use "raw data/sect11b_harvestw3.dta", clear


g nonfoodexp30_1 = s11bq4

collapse (sum) nonfoodexp30_1, by (hhid)

replace nonfoodexp30_1 = nonfoodexp30_1/30.4

label var nonfoodexp30_1 "1 month recall - hh non-food expenditure per day - harvest season"

tempfile nonfoodexp30_1
save `nonfoodexp30_1',replace

* 6 months recall 

use "raw data/sect11c_harvestw3.dta", clear


g nonfoodexp6_1 = s11cq6

collapse (sum) nonfoodexp6_1, by (hhid)

replace nonfoodexp6_1 = nonfoodexp6_1/(30.4*6)

label var nonfoodexp6_1 "6 months recall - hh non-food expenditure per day - harvest season"

tempfile nonfoodexp6_1
save `nonfoodexp6_1',replace

* 12 months recall 

use "raw data/sect11d_harvestw3.dta", clear

g nonfoodexp12_1 = s11dq8

collapse (sum) nonfoodexp12_1, by (hhid)

replace nonfoodexp12_1 = nonfoodexp12_1/(30.4*12)

label var nonfoodexp12_1 "12 months recall - hh non-food expenditure per day - harvest season"

tempfile nonfoodexp12_1
save `nonfoodexp12_1',replace


* PLANTING SEASON

* 7 days recall 

use "raw data/sect8a_plantingw3.dta", clear

g nonfoodexp7_2 = s8q2

collapse (sum) nonfoodexp7_2, by (hhid)

replace nonfoodexp7_2 = nonfoodexp7_2/7 

label var nonfoodexp7_2 " 7 days recall - hh non-food expenditure per day - planting season"

tempfile nonfoodexp7_2
save `nonfoodexp7_2',replace

* 1 month recall 

use "raw data/sect8b_plantingw3.dta", clear

g nonfoodexp30_2 = s8q4

collapse (sum) nonfoodexp30_2, by (hhid)

replace nonfoodexp30_2 = nonfoodexp30_2/30.4

label var nonfoodexp30_2 "1 month recall - hh non-food expenditure per day - planting season"

tempfile nonfoodexp30_2
save `nonfoodexp30_2',replace

* 6 months recall 

use "raw data/sect8c_plantingw3.dta", clear

g nonfoodexp6_2 = s8q6

collapse (sum) nonfoodexp6_2, by (hhid)

replace nonfoodexp6_2 = nonfoodexp6_2/(30.4*6)

label var nonfoodexp6_2 "6 months recall - hh non-food expenditure per day - planting season"

tempfile nonfoodexp6_2
save `nonfoodexp6_2',replace

* Merge all non-food datasets together

use `nonfoodexp7_1', clear
merge 1:1 hhid using `nonfoodexp7_2', nogen
merge 1:1 hhid using `nonfoodexp30_1', nogen
merge 1:1 hhid using `nonfoodexp30_2', nogen
merge 1:1 hhid using `nonfoodexp6_1', nogen
merge 1:1 hhid using `nonfoodexp6_2', nogen
merge 1:1 hhid using `nonfoodexp12_1', nogen

recode _all (0=.)

egen nonfoodexp7 = rowmean (nonfoodexp7_1 nonfoodexp7_2)
egen nonfoodexp30 = rowmean (nonfoodexp30_1 nonfoodexp30_2)
egen nonfoodexp6 = rowmean (nonfoodexp6_1 nonfoodexp6_2)

egen nonfoodexp = rsum (nonfoodexp7 nonfoodexp30 nonfoodexp6 nonfoodexp12_1)

label var nonfoodexp "Household non-food expenditure per day "


merge 1:1 hhid using `foodexp', nogen

egen hhexp = rsum (foodexp nonfoodexp)

label var hhexp "Household total expenditure per day "

save "clean data/Expenditure.dta", replace 

**** COMBINE ALL ****

use "clean data/Nigeria1516_demographics.dta", clear
joinby hhid using "clean data/intermediate/Nigeria1516_nfe_income.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_wage_incomeCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_harvest.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_harvest_sales.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_livestock.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_livestock_products.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_fish.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_rent_supplies.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_remittances.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_gen_other_inc.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_safety_net_inc.dta", unmatched(both) _merge(_merge)
drop _merge
joinby hhid using "clean data/intermediate/Nigeria1516_costs.dta", unmatched(both) _merge(_merge)
drop _merge

merge 1:1 hhid using  "clean data/Expenditure.dta", nogen keep (1 3) 

mvdecode nfe_* wage_* harvest_value sales_value livestock_* fish_* rent_supplies remittances_cash remittances_inkind gen_other_income safety_net cost_* informal* formal* job1 job2, mv(0)
mvencode nfe_* wage_* harvest_value sales_value livestock_* fish_* rent_supplies remittances_cash remittances_inkind gen_other_income safety_net cost_* informal* formal* job1 job2, mv(0)

gen pop_people = hhsize*hhweight
gen pop_youth = hhsize*youth_share_24*hhweight
gen data_obs = 1
gen hh_represented = hhweight

* Convert expenditures variables to  USD constant PPP

g pcexp = hhexp/hhsize

foreach u of varlist pcexp hhexp {
	gen USD_`u' = `u'*0.0050244944
	replace USD_`u' = USD_`u'*2.0476707
	drop `u'
	ren USD_`u' `u'
	}
	
* Convert to Per Capita USD constant PPP
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_nfe_gross rent_supplies remittances_cash remittances_inkind gen_other_income safety_net cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_animal_fuel "
foreach u in `USD' {
	capture drop USD_`u'
	* Currency conversion from XE.com 12/1/2016
	gen USD_`u' = `u'*0.0050244944
	replace USD_`u' = USD_`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2016 data not available
	* 2015 GNI per capita constant 2011 PPP / 2015 GNI per capita Atlas Method
	replace USD_`u' = USD_`u'*2.0476707
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop USD_`usd'`u'
	* Currency conversion from XE.com 12/1/2016
	gen USD_`usd'`u' = `usd'`u'*0.0050244944
	replace USD_`usd'`u' = USD_`usd'`u'/hhsize
	* Constant 2011 PPP conversion using World Bank GNI Data *** 2016 data not available
	* 2015 GNI per capita constant 2011 PPP / 2015 GNI per capita Atlas Method
	replace USD_`usd'`u' = USD_`usd'`u'*2.0476707
}
}
*	
* Drop former variables
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_nfe_gross rent_supplies remittances_cash remittances_inkind gen_other_income safety_net cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_animal_fuel"
foreach u in `USD' {
	capture drop `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	capture drop `usd'`u'
}
}
*	
* Rename
local USD "nfe_gross_inc nfe_costs nfe_net_inc wage_income harvest_value sales_value livestock_income livestock_sold livestock_product_income livestock_product_sold fish_income fish_sold fish_nfe_gross rent_supplies remittances_cash remittances_inkind gen_other_income safety_net cost_seeds cost_fertilizer cost_crop_labor cost_land_rent cost_machine_rent cost_animal_labor cost_animal_feed cost_animal_fuel"
foreach u in `USD' {
	rename USD_`u' `u'
}
*	
local USD "nfe_i_c nfe_gi_c nfe_c_c wage_i_f0_c wage_i_f1_c wage_i_fcon0_c wage_i_fcon1_c wage_i_fsp0_c wage_i_fsp1_c"	// changed (!)
foreach usd in `USD' {
forvalues u=1(1)12 {
	rename USD_`usd'`u' `usd'`u'
}
}
*	

* Impute farm income components before creating the aggregate variable
local varlist harvest_value livestock_income livestock_product_income fish_income
	foreach v of local varlist{
		recode `v' .=0
		qui: sum `v' if `v' !=. & `v'>0 , detail
		scalar p99 = r(p99)
		replace `v' =p99 if `v'>=p99 & `v'!=. & `v'>0 
}

replace sales_value =harvest_value if sales_value>harvest_value & sales_value!=.
replace livestock_sold=livestock_income if livestock_sold>livestock_income & livestock_sold!=.
replace livestock_product_sold=livestock_product_income if livestock_product_sold>livestock_product_income & livestock_product_sold!=.
replace fish_sold=fish_income if fish_sold>fish_income & fish_sold!=.


generate farm_sales = sales_value + livestock_sold + livestock_product_sold + fish_sold
gen own_farm_crop_inc_costs = cost_seeds + cost_fertilizer + cost_crop_labor + cost_land_rent + cost_machine_rent
gen own_farm_livestock_inc_costs = cost_animal_labor + cost_animal_feed + cost_animal_fuel
gen own_farm_crop_inc_gross = harvest_value
gen own_farm_livestock_inc_gross = livestock_income + livestock_product_income + fish_income
gen own_farm_crop_inc_net = own_farm_crop_inc_gross - own_farm_crop_inc_costs
gen own_farm_livestock_inc_net = own_farm_livestock_inc_gross - own_farm_livestock_inc_costs
gen own_farm_crop_inc_nnnet = own_farm_crop_inc_net
replace own_farm_crop_inc_nnnet = 0 if own_farm_crop_inc_nnnet<=0
gen own_farm_livestock_inc_nnnet = own_farm_livestock_inc_net
replace own_farm_livestock_inc_nnnet = 0 if own_farm_livestock_inc_nnnet<=0

local fish_nfe "nfe_gross_inc nfe_gi_c2 nfe_net_inc nfe_i_c2"
foreach fn in `fish_nfe' {
	replace `fn' = `fn' + fish_nfe_gross
}
*

** Combine various income variables into a total income by general type and categories
forvalues c=1(1)12 {
	gen i_gi_f0_c`c' = 0
	gen i_gi_f1_c`c' = 0
	gen i_gi_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_ni_f0_c`c' = 0
	gen i_ni_f1_c`c' = 0
	gen i_ni_f2_c`c' = 0
}
*
forvalues c=1(1)12 {
	gen i_nnni_f0_c`c' = 0
	gen i_nnni_f1_c`c' = 0
	gen i_nnni_f2_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace i_gi_f0_c`c' = i_gi_f0_c`c' + wage_i_f0_c`c'
	replace i_gi_f1_c`c' = i_gi_f1_c`c' + wage_i_f1_c`c'
	replace i_gi_f2_c`c' = i_gi_f2_c`c' + nfe_gi_c`c'
}
*
replace i_gi_f2_c1 = i_gi_f2_c1 + own_farm_crop_inc_gross + own_farm_livestock_inc_gross

forvalues c=1(1)12 {
	replace i_ni_f0_c`c' = i_ni_f0_c`c' + wage_i_f0_c`c'
	replace i_ni_f1_c`c' = i_ni_f1_c`c' + wage_i_f1_c`c'
	replace i_ni_f2_c`c' = i_ni_f2_c`c' + nfe_i_c`c'
}
*
replace i_ni_f2_c1 = i_ni_f2_c1 + own_farm_crop_inc_net + own_farm_livestock_inc_net

forvalues c=1(1)12 {
	replace i_nnni_f0_c`c' = i_nnni_f0_c`c' + wage_i_f0_c`c'
	replace i_nnni_f1_c`c' = i_nnni_f1_c`c' + wage_i_f1_c`c'
	replace i_nnni_f2_c`c' = i_nnni_f2_c`c' + nfe_i_c`c' if nfe_i_c`c'>=0
}
*
replace i_nnni_f2_c1 = i_nnni_f2_c1 + own_farm_crop_inc_nnnet + own_farm_livestock_inc_nnnet

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)13 {
	gen i_`it'_f3_c`c' = 0
}
}
*

local inc_type "gi ni nnni"
foreach it in `inc_type' {
forvalues c=1(1)12 {
	replace i_`it'_f3_c`c' = i_`it'_f3_c`c' + i_`it'_f0_c`c' + i_`it'_f1_c`c' + i_`it'_f2_c`c'
}
*
replace i_`it'_f3_c13 = i_`it'_f3_c13 + rent_supplies + remittances_cash + remittances_inkind + gen_other_income + safety_net
}
*

rename hhid HHID
joinby using "clean data/Nigeria1516_hhid.dta", unmatched(none)

gen remit = 1 if remittances_cash>0 | remittances_inkind>0
replace remit = 0 if remit==.

save "clean data/NIGERIA_incomesharesCHEVAL.dta", replace

 