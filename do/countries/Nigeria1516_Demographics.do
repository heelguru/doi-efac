********************************************************************************
*********************** NIGERIA 2015/2016: DEMOGRAPHICS ************************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Nigeria 2015/2016 General Household Survey-Panel Wave 3

The resulting data set is "Nigeria1516_demographics"

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl NIGERIA	"$path/data\Nigeria_GHS_1516" 

gl DO   	    ${NIGERIA}/do
gl raw data   	${NIGERIA}/raw data 
gl clean data 	${NIGERIA}/clean data
 
********************************************************************************
cd "$NIGERIA"

* Location - MUST use planting because harvest does not include the hhweight
use "raw data/secta_plantingw3.dta", clear
rename zone Zone
rename state State
rename lga LGA
rename ea EA
gen rural = 0
replace rural = 1 if sector==2
rename wt_wave3 hhweight
order hhid Zone State LGA EA rural hhweight
keep hhid Zone State LGA EA rural hhweight

save "clean data/Nigeria1516_location.dta", replace

* HH demographics
use "raw data/sect1_plantingw3.dta", clear

* drop if no longer a member of the household
drop if s1q4==2
rename s1q6 age
gen female = s1q2-1
gen head = 1 if s1q3==1

keep hhid indiv age female head

joinby hhid indiv using "raw data/sect1_harvestw3.dta", unmatched(both) _merge(_merge)

* drop if no longer a member of the household
drop if s1q4a==2

replace age = s1q4 if s1q4~=.
replace head = 1 if s1q3==1
gen female_head = 1 if female==1 & head==1

keep hhid indiv age female head female_head

mvdecode female head female_head, mv(0)
mvencode female head female_head, mv(0)

gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth youth_34, mv(0)

* Bring in Education
joinby hhid indiv using "raw data/sect2_harvestw3.dta", unmatched(both) _merge(_merge)
drop _merge

gen school_read = 1 if s2aq5==1
gen school_any = 1 if s2aq6==1
gen school_currently = 1 if s2aq13==1
gen school_complete = 1 if s2aq9>=16 & s2aq9~=.
gen school_secondary = (s2aq9>=26 & s2aq9~=. & s2aq9!=51 & s2aq9!=52 & s2aq9!=61)
gen school_level = s2aq9
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep hhid indiv age age_tier female head female_head school_* youth*

* Location & Weights
joinby hhid using "clean data/Nigeria1516_location.dta", unmatched(none)
drop if hhweight==.

* HH Size
by hhid, sort : egen float hhsize = count(hhid)

save "clean data/Nigeria1516_member_demos.dta", replace

** Credit
use "raw data/sect4c1_plantingw3.dta", clear
gen credit = 1 if s4cq1==1
keep if credit==1
keep hhid credit
collapse (max) credit, by (hhid)

save "clean data/intermediate/Nigeria1516_credit.dta", replace


** Land Holdings
use "raw data/sect11a1_plantingw3.dta", clear
joinby using "raw data/sect11b1_plantingw3.dta", unmatched(none)

* land ownership (purchase, from community/family, inheritance)
gen land = 1 if s11b1q4==1 | s11b1q4==4 | s11b1q4==5

* GPS measured in sq meters
gen hectares = s11aq4c

* Estimate non measured plots
gen obs_conversion = s11aq4c/s11aq4a
by s11aq4b, sort : egen float med_conversion = median(obs_conversion) if obs_conversion~=.
by s11aq4b, sort : egen float est_conversion = min(med_conversion)
replace hectares = s11aq4a*est_conversion if hectares==.
replace hectares = hectares/10000
keep if land==1
collapse (sum) hectares (max) land, by (hhid)
mvdecode hectares, mv(0)

drop land
rename hectares land_owned

save "clean data/intermediate/Nigeria1516_land.dta", replace
 
* Create Household Variables
use "clean data/Nigeria1516_member_demos.dta", clear
* Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
gen fem_share_24 = 1 if age>=15 & age<=24 & female==1 
gen fem_share_34 = 1 if age>=15 & age<=34 & female==1
gen mal_share_24 = 1 if age>=15 & age<=24 & female==0
gen mal_share_34 = 1 if age>=15 & age<=34 & female==0
mvencode youth_share_*, mv(0)

collapse (sum) youth_share*  fem_share* mal_share* (max) female_head school_level (mean) school_secondary (first) Zone State LGA EA rural hhweight hhsize , by (hhid )

foreach v in youth fem mal { 						
replace `v'_share_24 = `v'_share_24/hhsize
replace `v'_share_34 = `v'_share_34/hhsize
}
rename school_secondary school_secondary_hh
rename school_level educ_max

joinby hhid using "clean data/intermediate/Nigeria1516_credit.dta", unmatched(both) _merge(_merge)
mvencode credit, mv(0)
drop if _merge==2
drop _merge

joinby hhid using "clean data/intermediate/Nigeria1516_land.dta", unmatched(both) _merge(_merge)
mvencode land_owned, mv(0)
drop if _merge==2
drop _merge

* Popultion weight
gen popweight = hhweight*hhsize

save "clean data/Nigeria1516_demographics.dta", replace


** Build hhid & iid
use "clean data/Nigeria1516_member_demos.dta", clear
keep hhid indiv
rename hhid HHID

tostring indiv, g(iid)
tostring HHID, g(hhid)
replace iid = "0" +iid if indiv<10
replace iid = hhid+iid

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Nigeria1516_iid.dta", replace
collapse (min) indiv, by (HHID hhid)
drop indiv
save "clean data/Nigeria1516_hhid.dta", replace
