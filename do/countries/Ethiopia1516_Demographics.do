********************************************************************************
*********************** ETHIOPIA - 2015/2016: DEMOGRAPHICS *********************
********************************************************************************

/*
This file creates demographic indicators:
	- age groups
	- sex
	- education
	- household size

The dataset used for this is:
Ethiopia 2015/2016 Socioeconomic Survey, Wave 3

The resulting data set is "Ethiopia1516_demographics"

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************
gl ETHIOPIA	"$path\data\Ethiopia_ESS_1516"

gl DO   	    ${ETHIOPIA}/do
gl raw data   	${ETHIOPIA}/raw data 
gl clean data 	${ETHIOPIA}/clean data
gl FIG 		    ${ETHIOPIA}/graphs
gl TAB 		    ${ETHIOPIA}/tables
 
********************************************************************************
cd "$ETHIOPIA"

* Location
use "raw data/Household/sect_cover_hh_w3.dta", clear
*rename hh_s1q00 member_id
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename pw_w3 hhweight
order HHID rural region zone ward town subcity FA ea hhweight household_id2
keep HHID rural region zone ward town subcity FA ea hhweight household_id2


save "clean data/Ethiopia1516_location.dta", replace


** Household Member Demographic information
use "raw data/Household/sect1_hh_w3.dta", clear
rename hh_s1q00 member_id

* Keep only members of household
drop if hh_s1q04c==2

gen female = hh_s1q03-1
gen head = 0
replace head = 1 if hh_s1q02==1
gen female_head = 0
replace female_head = 1 if head==1 & female==1

/* Age Tiers
1 = 0-14
2 = 15-17
3 = 18-24
4 = 25-34
5 = 35-64
6 = 65+
*/
rename hh_s1q04a age
* Three missing ages. One head (only hh member), consider average age = 18. Other two are children of parents in 20s, so select age 5.
replace age = 18 if age==. & hh_s1q02==1
replace age = 5 if age==.
gen age_tier = .
replace age_tier = 1 if age<15
replace age_tier = 2 if age>=15 & age<18
replace age_tier = 3 if age>=18 & age<25
replace age_tier = 4 if age>=25 & age<35
replace age_tier = 5 if age>=35 & age<65
replace age_tier = 6 if age>=65

gen youth = 1 if age>=15 & age<=24
gen youth_34 = 1 if age>=15 & age<=34
mvencode youth youth_34, mv(0)

keep saq01 saq02 saq03 saq04 saq05 saq06 saq07 saq08 member_id age age_tier female head female_head youth youth_34 household_id2
order saq01 saq02 saq03 saq04 saq05 saq06 saq07 saq08 member_id age age_tier female head female_head youth youth_34 household_id2

* Bring in Education
rename member_id hh_s2q00
joinby saq01 saq02 saq03 saq04 saq05 saq06 saq07 saq08 hh_s2q00 using "raw data/Household/sect2_hh_w3.dta", unmatched(both) _merge(_merge)
drop _merge
rename hh_s2q00 member_id
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID

gen school_read = 1 if hh_s2q02==1
gen school_any = 1 if hh_s2q03==1
gen school_currently = 1 if hh_s2q06==1
gen school_complete = 1 if hh_s2q05>=8 & hh_s2q05<95
gen school_secondary = 1 if hh_s2q05>=12 & hh_s2q05<95
replace school_secondary = . if hh_s2q05>=21 & hh_s2q05<24
gen school_level = hh_s2q05
replace school_level = . if hh_s2q05>=95
mvdecode school_*, mv(0)
mvencode school_*, mv(0)

keep HHID region zone ward town subcity FA ea member_id age age_tier female head female_head youth youth_34 school_* household_id2
order HHID region zone ward town subcity FA ea member_id age age_tier female head female_head youth youth_34 school_* household_id2

joinby HHID region zone ward town subcity FA ea using "clean data/Ethiopia1516_location.dta", unmatched(both) _merge(_merge)
drop _merge

* HH Size

bys household_id2:  egen float hhsize = count(household_id2)

save "clean data/Ethiopia1516_member_demos.dta", replace

** Credit
use "Raw Data/Household/sect14a_hh_w3.dta", clear
gen credit = ( hh_s14q01==1)
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 town
rename saq05 subcity
rename saq06 FA
rename saq07 ea
rename saq08 HHID


save "clean data/intermediate/Ethiopia1516_credit.dta", replace


** Land Holdings
use "Raw Data/Post-Planting/sect2_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 ward
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
keep if pp_s2q03==1 | pp_s2q03==2 | pp_s2q03==7
gen owned = 1
collapse (max) owned, by (region zone ward FA ea HHID household_id2 rural_data parcel_id )
save "clean data/intermediate/Ethiopia1516_temp_land.dta", replace
  
use "Raw Data/Post-Planting/sect3_pp_w3.dta", clear
rename saq01 region
rename saq02 zone
rename saq03 woreda
rename saq04 FA
rename saq05 ea
rename saq06 HHID
rename rural rural_data
gen rural = 0
replace rural = 1 if rural_data==1
rename pp_s3q02_a farmerest 
rename pp_s3q02_c local_unit
joinby region zone woreda local_unit using "Raw Data/Land Area Conversion Factor/ET_local_area_unit_conversion.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename woreda ward

* Use median conversion values for local units in locations where there is no conversion. Use provided conversion if available.
egen float med_timad = median(conversion) if conversion~=. & local_unit==3
egen float med_boy = median(conversion) if conversion~=. & local_unit==4
egen float med_senga = median(conversion) if conversion~=. & local_unit==5
egen float med_kert = median(conversion) if conversion~=. & local_unit==6
egen float est_timad = min(med_timad) if local_unit==3
egen float est_boy = min(med_boy) if local_unit==4
egen float est_senga = min(med_senga) if local_unit==5
egen float est_kert = min(med_kert) if local_unit==6
replace conversion = 10000 if local_unit==1
replace conversion = 1 if local_unit==2
replace conversion = est_timad if conversion==. & local_unit==3
replace conversion = est_boy if conversion==. & local_unit==4
replace conversion = est_senga if conversion==. & local_unit==5
replace conversion = est_kert if conversion==. & local_unit==6

rename pp_s3q04	GPS_meas
rename pp_s3q05_a GPS_area
rename pp_s3q08_a ROPE_meas
rename pp_s3q08_b ROPE_area

gen 	plotarea_m2=GPS_area if GPS_area>0
replace plotarea_m2=. if GPS_area==0
replace plotarea_m2=ROPE_area if plotarea_m2==. 
replace plotarea_m2=farmerest*conversion if plotarea_m2==. 
replace plotarea_m2=. if plotarea_m2==0 

gen hectares = plotarea_m2*0.0001
label var hectares "Area of field, in hectares"

collapse (sum) hectares, by (region zone ward FA ea HHID household_id2 rural_data parcel_id)

joinby using "clean data/intermediate/Ethiopia1516_temp_land.dta", unmatched(both) _merge(_merge)
drop if _merge==1
drop _merge
replace hectares = 0 if hectares==.

rename owned land
gen rural = 0
replace rural = 1 if rural_data==1
drop rural_data parcel_id

collapse (sum) hectares (max) land, by (region zone ward FA ea household_id2 HHID rural)
mvdecode hectares, mv(0)

drop land
rename hectares land_owned

save "clean data/intermediate/Ethiopia1516_land.dta", replace


* HH demographics
use "clean data/Ethiopia1516_member_demos.dta", clear

*Youth Share
gen youth_share_24 = 1 if age>=15 & age<=24
gen youth_share_34 = 1 if age>=15 & age<=34
mvencode youth_share_*, mv(0)

collapse (sum) youth_share_* (max) female_head school_secondary school_level, by (household_id2 HHID region zone ward town subcity FA ea rural hhweight hhsize)

replace youth_share_24 = youth_share_24/hhsize
replace youth_share_34 = youth_share_34/hhsize
rename school_secondary school_secondary_hh
rename school_level educ_max

gen popweight = hhweight*hhsize
joinby household_id2 region zone ward town subcity FA ea using "clean data/intermediate/Ethiopia1516_credit.dta", unmatched(both) _merge(_merge)
drop _merge

joinby household_id2 region zone ward rural FA ea using "clean data/intermediate/Ethiopia1516_land.dta", unmatched(both) _merge(_merge)
drop if _merge==2
mvencode land_owned, mv(0)
g land_d = (land_owned>0)
drop _merge

save "clean data/Ethiopia1516_demographics.dta", replace

** Build hhid & iid
use "clean data/Ethiopia1516_member_demos.dta", clear
keep household_id2 HHID region zone ward town subcity FA ea member_id


g hhid = household_id2
tostring member_id, g(iid)
replace iid = "0" +iid if member_id<10
replace iid = hhid + iid 

lab var hhid "Household identifier"
lab var iid "Individual identifier"

save "clean data/Ethiopia1516_iid.dta", replace
collapse (min) member_id, by (HHID household_id2 region zone ward town subcity FA ea hhid)
drop member_id
save "clean data/Ethiopia1516_hhid.dta", replace