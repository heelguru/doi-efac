********************************************************************************
***************** UGANDA: GENERATE AFS INDICATORS ***************
********************************************************************************

/*
This file cleans the data to then create these indicators are used for the country typology along AFS engagement:

-	FTE of employment (individual level)
	o	FTE = 40 hours/week worth of work in a given sector and type of job
		Type of job: Self-employed, Wage employed, Casual wage labor
		Sectors: AFS (On farm, off farm), Non-AFS (Off farm)

The dataset used for this is:
Uganda 2013/2014 National Panel Survey

generates 
save "$CLEAN\UGANDA_FTEsCHEVAL.dta", replace

Stata Version 15.1
*/

capture log close
clear
clear matrix
set more off

********************************************************************************
******************************* SET DRIVES *************************************

gl UGANDA	"$path/data\Uganda_NPS_1314"

gl DO   	    ${UGANDA}/do
gl raw data   	${UGANDA}/raw data 
gl clean data 	${UGANDA}/clean data
gl FIG 		    ${UGANDA}/graphs
gl TAB 		    ${UGANDA}/tables
 
********************************************************************************
cd "$UGANDA"

	** HOURS WORKED BY SECTOR **
/*
For each individual we first compute the hours worked from each job and annualise it.
Then we sum the income by individual and sector (On-farm ASF, Off-farm ASF, non ASF).
Finally we aggregate sectoral income at household level. 

We assume: 12 months per year, 4.3 weeks per month, 40 hours per week
* Generate Full Time Equivalents
FTE = hours worked / 40 /52 by sector and occupation

*/


* Wage Labor information
use "raw data/GSEC8_1.dta", clear
rename h8q20A main_CIC_name
rename h8q20B main_CIC
rename h8q22 main_type
gen main_hourspw = 0
local alpha "a b c d e f g"
foreach a in `alpha' {
	replace  main_hourspw = main_hourspw + h8q36`a' if h8q36`a'~=.
}
*
rename h8q23 main_pension
rename h8q25 main_health_insurance
rename h8q27 main_contract
rename h8q30a main_months
rename h8q30b main_weekspm
rename h8q31a main_cash_pmt
rename h8q31b main_inkind_pmt
rename h8q31c main_pmt_duration
rename h8q39A secondary_CIC_name
rename h8q39B secondary_CIC
rename h8q41 secondary_type
rename h8q43 secondary_hourspw
rename h8q44 secondary_months
rename h8q44b secondary_weekspm
rename h8q45a secondary_cash_pmt
rename h8q45b secondary_inkind_pmt
rename h8q45c secondary_pmt_duration
rename h8q50A umain_CIC_name
rename h8q50B umain_CIC
rename h8q51 umain_type
rename h8q52 umain_months
rename h8q52_1 umain_weekspm
rename h8q52_2 umain_hourspw
rename h8q53a umain_cash_pmt
rename h8q53b umain_inkind_pmt
rename h8q53c umain_pmt_duration
rename h8q56A usecondary_CIC_name
rename h8q56B usecondary_CIC
rename h8q51a usecondary_type
rename h8q57 usecondary_months
rename h8q57_1 usecondary_weekspm
rename h8q57_2 usecondary_hourspw
rename h8q58a usecondary_cash_pmt
rename h8q58b usecondary_inkind_pmt
rename h8q58c usecondary_pmt_duration
keep HHID PID main* secondary* umain* usecondary*

* Allocate generally defined umain_CICs
replace main_CIC = "47" if main_CIC=="Y9998" & main_CIC_name=="Sales in a shop"
replace main_CIC = "111" if main_CIC=="Y9998" & main_CIC_name=="Agricultural field assistant"
replace main_CIC = "502" if main_CIC=="Y9998" & main_CIC_name=="road maintainance"
replace main_CIC = "502" if main_CIC=="Y9998" & main_CIC_name=="vechicle mechanic activities"
replace main_CIC = "525" if main_CIC=="Y9998" & main_CIC_name=="taking peoples cattle to market places for pay"
replace main_CIC = "525" if main_CIC=="Y9998" & main_CIC_name=="transporting fish"
replace main_CIC = "710" if main_CIC=="Y9998" & main_CIC_name=="FISHING"
replace main_CIC = "710" if main_CIC=="Y9998" & main_CIC_name=="sand minning Sector"
replace main_CIC = "1061" if main_CIC=="Y9998" & main_CIC_name=="Slaughters animals"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="business of griding peoples maize"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="casual worker at the milling factory"
replace main_CIC = "3211" if main_CIC=="Y9998" & main_CIC_name=="Making a hebal drink"
replace main_CIC = "3220" if main_CIC=="Y9998" & main_CIC_name=="Fabrication and welding"
replace main_CIC = "3220" if main_CIC=="Y9998" & main_CIC_name=="Making Bricks"
replace main_CIC = "4100" if main_CIC=="Y9998" & main_CIC_name=="Brick laying.."
replace main_CIC = "4100" if main_CIC=="Y9998" & main_CIC_name=="Builder"
replace main_CIC = "4312" if main_CIC=="Y9998" & main_CIC_name=="Selling charcoal"
replace main_CIC = "4600" if main_CIC=="Y9998" & main_CIC_name=="retailsale of agricultural produce"
replace main_CIC = "4600" if main_CIC=="Y9998" & main_CIC_name=="SELLINGAGRICULTURAL PRODUCE"
replace main_CIC = "4921" if main_CIC=="Y9998" & main_CIC_name=="CAB DRIVER IN OWN CAR"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="schedules delivery of polythene paper"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="transporting luggage"
replace main_CIC = "4941" if main_CIC=="Y9998" & main_CIC_name=="whole sale and retail sale of cement( construction materials)"
replace main_CIC = "5510" if main_CIC=="Y9998" & main_CIC_name=="logging"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Education"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Inspector of Schools under ministry of Education"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Secondary school"
replace main_CIC = "8510" if main_CIC=="Y9998" & main_CIC_name=="Teaching"
replace main_CIC = "9491" if main_CIC=="Y9998" & main_CIC_name=="preaching"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="field interviewer..."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Law and Order"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="local council five"
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Naads cordinator...."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Population officer.."
replace main_CIC = "9499" if main_CIC=="Y9998" & main_CIC_name=="Town council"
replace main_CIC = "9511" if main_CIC=="Y9998" & main_CIC_name=="bcycle repair"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="casual labourer"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="doing all HH chores at the Ejons Hh"
replace main_CIC = "99999" if main_CIC=="Y9998" & main_CIC_name=="personal services"
replace main_CIC = "47" if main_CIC=="Z9999" & main_CIC_name=="Middleman"
replace main_CIC = "111" if main_CIC=="Z9999" & main_CIC_name=="subsistance crop farming"
replace main_CIC = "111" if main_CIC=="Z9999" & main_CIC_name=="works on peoples farms for pay"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="BUILDING houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="building houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="construction of buildings"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="construction of houses"
replace main_CIC = "4100" if main_CIC=="Z9999" & main_CIC_name=="Contruction industry"
replace main_CIC = "5510" if main_CIC=="Z9999" & main_CIC_name=="logging"
replace main_CIC = "6419" if main_CIC=="Z9999" & main_CIC_name=="bibco uganda investment"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="bethel primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="bombo barracks army primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="glenburnie primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="primary teaching"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="private primary school"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="rakai district headquaters"
replace main_CIC = "8510" if main_CIC=="Z9999" & main_CIC_name=="school matron"
replace main_CIC = "9499" if main_CIC=="Z9999" & main_CIC_name=="district health assistant"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="casual labourer"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="housemaid"
replace main_CIC = "99999" if main_CIC=="Z9999" & main_CIC_name=="subsistance causal labourer"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="retail trade of general merchandise"
replace main_CIC = "3211" if main_CIC=="" & main_CIC_name=="butcher services"
replace main_CIC = "8510" if main_CIC=="" & main_CIC_name=="primary school"
replace main_CIC = "" if main_CIC=="Y9998" | main_CIC=="Z9999"
replace main_CIC = "4540" if main_CIC=="" & main_CIC_name=="Buying and selling roduce"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="Buys and sells charcoal"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="buys and sells used metals"
replace main_CIC = "4540" if main_CIC=="" & main_CIC_name=="buys matooke from villages and distributes them in town"
replace main_CIC = "210" if main_CIC=="" & main_CIC_name=="Charcoal burning and selling"
replace main_CIC = "210" if main_CIC=="" & main_CIC_name=="Charcoal production"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="clinic owner"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="computer service activites"
replace main_CIC = "410" if main_CIC=="" & main_CIC_name=="consruction of buldings"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="dry cleaners"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="Dry cleaning..."
replace main_CIC = "2511" if main_CIC=="" & main_CIC_name=="Fabrication of metal products"
replace main_CIC = "8100" if main_CIC=="" & main_CIC_name=="fumigation"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="Hardware shop"
replace main_CIC = "3900" if main_CIC=="" & main_CIC_name=="Interior designing"
replace main_CIC = "5610" if main_CIC=="" & main_CIC_name=="Makes local porridge and sells it"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="Manicure shop"
replace main_CIC = "2030" if main_CIC=="" & main_CIC_name=="manufacturing"
replace main_CIC = "4730" if main_CIC=="" & main_CIC_name=="manufacturing an mobile retail trade of metal products s"
replace main_CIC = "454" if main_CIC=="" & main_CIC_name=="mechanic"
replace main_CIC = "4730" if main_CIC=="" & main_CIC_name=="mobile retail trade of metal products"
replace main_CIC = "454" if main_CIC=="" & main_CIC_name=="Motorcycle Repairing"
replace main_CIC = "99999" if main_CIC=="" & main_CIC_name=="not reported...."
replace main_CIC = "7410" if main_CIC=="" & main_CIC_name=="operating a saloon"
replace main_CIC = "9601" if main_CIC=="" & main_CIC_name=="phone charging service"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="provides a service of selling milk"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="providing rentals....."
replace main_CIC = "479" if main_CIC=="" & main_CIC_name=="pushing acart  carriage as aform of transport"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="Rairs of machinery"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="Repairing chain saw machines"
replace main_CIC = "9529" if main_CIC=="" & main_CIC_name=="REPARES BICYCLES"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail sale of Agriculture inputs such as fertilizers & pesticides"
replace main_CIC = "4659" if main_CIC=="" & main_CIC_name=="retail sale of electrical wiring equipments"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="retail sale of household items"
replace main_CIC = "4759" if main_CIC=="" & main_CIC_name=="retail sale of household merchandise"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="retail sale of meat..."
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="Retail Shop"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail trade of loca brew <maalwa>"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="retail trader"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Retail trading in food staffs"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="retailsale of cooking oil at home"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Sale of food stuffs in the market.."
replace main_CIC = "8610" if main_CIC=="" & main_CIC_name=="sanyu clinic"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="selling charcoal"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="selling firewood"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Sells cows"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="sells goods in spouse's grocery shop"
replace main_CIC = "479" if main_CIC=="" & main_CIC_name=="Service Deiverly Providers"
replace main_CIC = "1393" if main_CIC=="" & main_CIC_name=="Sewing clothes"
replace main_CIC = "4722" if main_CIC=="" & main_CIC_name=="tends to household bar"
replace main_CIC = "8621" if main_CIC=="" & main_CIC_name=="Traddition medicine"
replace main_CIC = "8621" if main_CIC=="" & main_CIC_name=="Tradditional healing"
replace main_CIC = "47" if main_CIC=="" & main_CIC_name=="transporting of Goods"
replace main_CIC = "9511" if main_CIC=="" & main_CIC_name=="weldinng"
replace main_CIC = "525" if main_CIC=="" & main_CIC_name=="Wholesale trade of fish"
destring main_CIC, replace
replace secondary_CIC = "502" if secondary_CIC=="Y9998" & secondary_CIC_name=="road maintencece service(clearing drainage ,blockages and shoulders)"
replace secondary_CIC = "3220" if secondary_CIC=="Y9998" & secondary_CIC_name=="makes bricks"
replace secondary_CIC = "4100" if secondary_CIC=="Y9998" & secondary_CIC_name=="BULDING ACTIVITY"
replace secondary_CIC = "4600" if secondary_CIC=="Y9998" & secondary_CIC_name=="fish selling"
replace secondary_CIC = "4600" if secondary_CIC=="Y9998" & secondary_CIC_name=="taking animals to the market for sale"
replace secondary_CIC = "4921" if secondary_CIC=="Y9998" & secondary_CIC_name=="land transport"
replace secondary_CIC = "8610" if secondary_CIC=="Y9998" & secondary_CIC_name=="Proffessional nurse.."
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="DUTIES OF AN L.C1 CHAIRPERSON"
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="provides a leadership service to the community"
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="provides security i.e an Askari..."
replace secondary_CIC = "9499" if secondary_CIC=="Y9998" & secondary_CIC_name=="store guard"
replace secondary_CIC = "454" if secondary_CIC=="Z9999" & secondary_CIC_name=="retail trade in repair and sale of bicycle and motor cycle parts"
replace secondary_CIC = "502" if secondary_CIC=="Z9999" & secondary_CIC_name=="Road repairs"
replace secondary_CIC = "1701" if secondary_CIC=="Z9999" & secondary_CIC_name=="nile victoria industry dealing in making plastics"
replace secondary_CIC = "4600" if secondary_CIC=="Z9999" & secondary_CIC_name=="retail sale of pork"
replace secondary_CIC = "9499" if secondary_CIC=="Z9999" & secondary_CIC_name=="local government"
replace secondary_CIC = "" if secondary_CIC=="Y9998" | secondary_CIC=="Z9999"
destring secondary_CIC, replace
replace umain_CIC = "111" if umain_CIC_name=="WORKING ON OTHER PEOPLES FARMS FOR PAYMENT" & umain_CIC=="9609"
replace umain_CIC = "111" if umain_CIC_name=="WORKING ON PEOPLES FARMS" & umain_CIC=="9609"
replace umain_CIC = "4922" if umain_CIC_name=="service in transport sector" & umain_CIC=="9609"
replace umain_CIC = "4773" if umain_CIC_name=="she is aretailer who deals in selling household machandise" & umain_CIC=="9609"
replace umain_CIC = "111" if umain_CIC_name=="EMPLOYED BY VARIOUS HOUSEHOLDS TO DO THEIR FARMING" & umain_CIC=="9700"
replace umain_CIC = "111" if umain_CIC_name=="WORKS FOR DIFFERENT HOUSEHOLDS ON THEIR HOUSEHOLD FARMS" & umain_CIC=="9700"
replace umain_CIC = "111" if umain_CIC_name=="Subsistance farmer" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="Tending to farm crops" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="subsistance farmer" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="subsistance farming" & umain_CIC=="Y9998"
replace umain_CIC = "8621" if umain_CIC_name=="Cultural Surgeon<Circumsicion" & umain_CIC=="Y9998"
replace umain_CIC = "9609" if umain_CIC_name=="Dry cleaning.." & umain_CIC=="Y9998"
replace umain_CIC = "1410" if umain_CIC_name=="TAIRLOR" & umain_CIC=="Y9998"
replace umain_CIC = "1071" if umain_CIC_name=="burning and selling of charcoal" & umain_CIC=="Y9998"
replace umain_CIC = "200" if umain_CIC_name=="collecting firewood" & umain_CIC=="Y9998"
replace umain_CIC = "500" if umain_CIC_name=="mining clay for brick moulding" & umain_CIC=="Y9998"
replace umain_CIC = "4773" if umain_CIC_name=="trade" & umain_CIC=="Y9998"
replace umain_CIC = "5100" if umain_CIC_name=="works at the airport" & umain_CIC=="Y9998"
replace umain_CIC = "111" if umain_CIC_name=="growing of perenial and non perenial crops" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance  crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance animal and crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop  farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop and animal farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop farmer" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance crop farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed crop and animal farming" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed farking" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC_name=="subsistance mixed farming" & umain_CIC=="Z9999"
replace umain_CIC = "4620" if umain_CIC_name=="retail brewing of local beer" & umain_CIC=="Z9999"
replace umain_CIC = "8510" if umain_CIC_name=="koboyo primary school" & umain_CIC=="Z9999"
replace umain_CIC = "8510" if umain_CIC_name=="primary teacher" & umain_CIC=="Z9999"
replace umain_CIC = "1701" if umain_CIC_name=="making metal objects" & umain_CIC=="Z9999"
replace umain_CIC = "1701" if umain_CIC_name=="manufacturing" & umain_CIC=="Z9999"
replace umain_CIC = "500" if umain_CIC_name=="minin clay for moulding bricks" & umain_CIC=="Z9999"
replace umain_CIC = "111" if umain_CIC=="" & umain_CIC_name=="crop production"
replace umain_CIC = "9609" if umain_CIC=="" & umain_CIC_name=="casual lab"
replace umain_CIC = "4771" if umain_CIC=="" & umain_CIC_name=="retail sale"
destring umain_CIC, replace
replace usecondary_CIC = "111" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Crop farming"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Gold mining"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Gold minning"
replace usecondary_CIC = "500" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Sand mining"
replace usecondary_CIC = "1071" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="manufacture of charcoal"
replace usecondary_CIC = "1622" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="MANUFACTURE OF CONSTRUCTION MATERIALS-BRICKS"
replace usecondary_CIC = "1701" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="MANUFACTURE AND SALE  OF PAPER CRAFTS"
replace usecondary_CIC = "4540" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="makes and se'ls malwa"
replace usecondary_CIC = "4610" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="WHOLESALE OF TIMBER FOR FISHING RODS"
replace usecondary_CIC = "4610" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="collecting and selling firwood.."
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="retail sale of agricultural produce..maize,beans,"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sale of cassava"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells caattlle"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells produce"
replace usecondary_CIC = "4721" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="sells waragi"
replace usecondary_CIC = "4722" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="retailing alcohol"
replace usecondary_CIC = "6120" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="communication"
replace usecondary_CIC = "8100" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="cleaner at the university"
replace usecondary_CIC = "9499" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="Defence"
replace usecondary_CIC = "9609" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="odd jobsperson"
replace usecondary_CIC = "9609" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="repairs of bicycles"
replace usecondary_CIC = "4771" if usecondary_CIC=="Y9998" & usecondary_CIC_name=="trade"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop and animal farming"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop and animal mixed farming"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance crop farming"
replace usecondary_CIC = "1071" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="Manufacturing Charcoal"
replace usecondary_CIC = "4610" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="buying and selling timber"
replace usecondary_CIC = "4721" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="offers a service of selling onions"
replace usecondary_CIC = "4721" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="provides a sales service by selling passion fruits at the market"
replace usecondary_CIC = "9609" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="casual labourer"
replace usecondary_CIC = "111" if usecondary_CIC=="Z9999" & usecondary_CIC_name=="subsistance mixed farming"
replace usecondary_CIC = "4610" if usecondary_CIC=="9609" & usecondary_CIC_name=="baskets for sale"
replace usecondary_CIC = "4771" if usecondary_CIC=="9609" & usecondary_CIC_name=="retai shop"
replace usecondary_CIC = "4721" if usecondary_CIC=="9609" & usecondary_CIC_name=="retail sale in agricultural goods"
replace usecondary_CIC = "4771" if usecondary_CIC=="9609" & usecondary_CIC_name=="sells clothes"
replace usecondary_CIC = "1410" if usecondary_CIC=="9609" & usecondary_CIC_name=="tailoring"
replace usecondary_CIC = "4721" if usecondary_CIC=="9810" & usecondary_CIC_name=="buys and sells agricultural produce"
replace usecondary_CIC = "1101" if usecondary_CIC=="9820" & usecondary_CIC_name=="brewing"
destring usecondary_CIC, replace

* Bring in ISIC Groupings
rename main_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC main_CIC
rename ISIC_group main_ISIC_group
rename AFS_share main_AFS_share
rename secondary_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC secondary_CIC
rename ISIC_group secondary_ISIC_group
rename AFS_share secondary_AFS_share
rename umain_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC umain_CIC
rename ISIC_group umain_ISIC_group
rename AFS_share umain_AFS_share
rename usecondary_CIC CIC
joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
rename CIC usecondary_CIC
rename ISIC_group usecondary_ISIC_group
rename AFS_share usecondary_AFS_share

save "clean data/intermediate/Uganda1314_wage_activities.dta", replace

* Convert Data to one observation per activity per individual
use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID main*
keep if main_type==1 | main_type==5
gen apprentice = 1 if main_type==5
mvdecode main_months main_weekspm main_hourspw main_cash_pmt main_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share pension health_insurance contract"
foreach v in `vars' {
	rename main_`v' `v'
}
*
gen job = 1
save "clean data/intermediate/Uganda1314_main_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID secondary*
keep if secondary_type==1 | secondary_type==5
gen apprentice = 1 if secondary_type==5
mvdecode secondary_months secondary_weekspm secondary_hourspw secondary_cash_pmt secondary_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename secondary_`v' `v'
}
*
gen job = 2
save "clean data/intermediate/Uganda1314_secondary_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID umain*
keep if umain_type==1 | umain_type==5
gen apprentice = 1 if umain_type==5
mvdecode umain_months umain_weekspm umain_hourspw umain_cash_pmt umain_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename umain_`v' `v'
}
*
gen job = 3
save "clean data/intermediate/Uganda1314_umain_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID usecondary*
keep if usecondary_type==1 | usecondary_type==5
gen apprentice = 1 if usecondary_type==5
mvdecode usecondary_months usecondary_weekspm usecondary_hourspw usecondary_cash_pmt usecondary_inkind_pmt, mv(0)
local vars "CIC_name CIC type months weekspm hourspw cash_pmt inkind_pmt pmt_duration ISIC_group AFS_share"
foreach v in `vars' {
	rename usecondary_`v' `v'
}
*
gen job = 4
save "clean data/intermediate/Uganda1314_usecondary_wage_activities.dta", replace

use "clean data/intermediate/Uganda1314_main_wage_activities.dta", clear
append using "clean data/intermediate/Uganda1314_secondary_wage_activities.dta"
append using "clean data/intermediate/Uganda1314_umain_wage_activities.dta"
append using "clean data/intermediate/Uganda1314_usecondary_wage_activities.dta"
replace ISIC_group = 612 if ISIC_group==.
replace CIC = 99999 if CIC==.

* Estimate missing values of months, weeks per month and hours per month if one of these are missing
joinby HHID PID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Count observations with months reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if months~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if months~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if months~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if months~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if months~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if months~=.
by type CIC female rural, sort : egen float count_7 = count(1) if months~=.
by type CIC female, sort : egen float count_8 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_months_2 = median(months) if months~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_months_3 = median(months) if months~=.
by type CIC female rural region subregion district, sort : egen float temp_est_months_4 = median(months) if months~=.
by type CIC female rural region subregion, sort : egen float temp_est_months_5 = median(months) if months~=.
by type CIC female rural region, sort : egen float temp_est_months_6 = median(months) if months~=.
by type CIC female rural, sort : egen float temp_est_months_7 = median(months) if months~=.
by type CIC female, sort : egen float temp_est_months_8 = median(months) if months~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_months_1 = max(temp_est_months_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_months_2 = max(temp_est_months_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_months_3 = max(temp_est_months_3)
by type CIC female rural region subregion district, sort : egen float est_months_4 = max(temp_est_months_4)
by type CIC female rural region subregion, sort : egen float est_months_5 = max(temp_est_months_5)
by type CIC female rural region, sort : egen float est_months_6 = max(temp_est_months_6)
by type CIC female rural, sort : egen float est_months_7 = max(temp_est_months_7)
by type CIC female, sort : egen float est_months_8 = max(temp_est_months_8)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_months==.
replace est_months = est_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_months==.
replace est_months = est_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_months==.
replace est_months = est_months_7 if track==7
replace track = 8 if est_months==.
replace est_months = est_months_8 if track==8
drop est_months_* count_* track
**** Replace missing value of months (20 changes)
replace months = est_months if months==.
* Count observations with weekspm reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if weekspm~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if weekspm~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if weekspm~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if weekspm~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if weekspm~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if weekspm~=.
by type CIC female rural, sort : egen float count_7 = count(1) if weekspm~=.
by type CIC female, sort : egen float count_8 = count(1) if weekspm~=.
* Calculate median of weekspm by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_weekspm_1 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_weekspm_2 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_weekspm_3 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion district, sort : egen float temp_est_weekspm_4 = median(weekspm) if weekspm~=.
by type CIC female rural region subregion, sort : egen float temp_est_weekspm_5 = median(weekspm) if weekspm~=.
by type CIC female rural region, sort : egen float temp_est_weekspm_6 = median(weekspm) if weekspm~=.
by type CIC female rural, sort : egen float temp_est_weekspm_7 = median(weekspm) if weekspm~=.
by type CIC female, sort : egen float temp_est_weekspm_8 = median(weekspm) if weekspm~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_weekspm_1 = max(temp_est_weekspm_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_weekspm_2 = max(temp_est_weekspm_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_weekspm_3 = max(temp_est_weekspm_3)
by type CIC female rural region subregion district, sort : egen float est_weekspm_4 = max(temp_est_weekspm_4)
by type CIC female rural region subregion, sort : egen float est_weekspm_5 = max(temp_est_weekspm_5)
by type CIC female rural region, sort : egen float est_weekspm_6 = max(temp_est_weekspm_6)
by type CIC female rural, sort : egen float est_weekspm_7 = max(temp_est_weekspm_7)
by type CIC female, sort : egen float est_weekspm_8 = max(temp_est_weekspm_8)
drop temp*
* Build weekspm prediction variable if at least 10 observations of reported weekspm in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_weekspm = est_weekspm_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_7 if track==7
replace track = 8 if est_weekspm==.
replace est_weekspm = est_weekspm_8 if track==8
drop est_weekspm_* count_* track
**** Replace missing value of weekspm (55 changes)
replace weekspm = est_weekspm if weekspm==.
* Count observations with hourspw reported by geographic location.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if hourspw~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if hourspw~=.
by type CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if hourspw~=.
by type CIC female rural region subregion district, sort : egen float count_4 = count(1) if hourspw~=.
by type CIC female rural region subregion, sort : egen float count_5 = count(1) if hourspw~=.
by type CIC female rural region, sort : egen float count_6 = count(1) if hourspw~=.
by type CIC female rural, sort : egen float count_7 = count(1) if hourspw~=.
by type CIC female, sort : egen float count_8 = count(1) if hourspw~=.
* Calculate median of hourspw by geographic location - later used for predictive purposes.
by type CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_hourspw_1 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district subcounty parish, sort : egen float temp_est_hourspw_2 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district subcounty, sort : egen float temp_est_hourspw_3 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion district, sort : egen float temp_est_hourspw_4 = median(hourspw) if hourspw~=.
by type CIC female rural region subregion, sort : egen float temp_est_hourspw_5 = median(hourspw) if hourspw~=.
by type CIC female rural region, sort : egen float temp_est_hourspw_6 = median(hourspw) if hourspw~=.
by type CIC female rural, sort : egen float temp_est_hourspw_7 = median(hourspw) if hourspw~=.
by type CIC female, sort : egen float temp_est_hourspw_8 = median(hourspw) if hourspw~=.
mvencode temp_*, mv(0)
by type CIC female rural region subregion district subcounty parish ea, sort : egen float est_hourspw_1 = max(temp_est_hourspw_1)
by type CIC female rural region subregion district subcounty parish, sort : egen float est_hourspw_2 = max(temp_est_hourspw_2)
by type CIC female rural region subregion district subcounty, sort : egen float est_hourspw_3 = max(temp_est_hourspw_3)
by type CIC female rural region subregion district, sort : egen float est_hourspw_4 = max(temp_est_hourspw_4)
by type CIC female rural region subregion, sort : egen float est_hourspw_5 = max(temp_est_hourspw_5)
by type CIC female rural region, sort : egen float est_hourspw_6 = max(temp_est_hourspw_6)
by type CIC female rural, sort : egen float est_hourspw_7 = max(temp_est_hourspw_7)
by type CIC female, sort : egen float est_hourspw_8 = max(temp_est_hourspw_8)
drop temp*
* Build hourspw prediction variable if at least 10 observations of reported hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_hourspw = est_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_7 if track==7
replace track = 8 if est_hourspw==.
replace est_hourspw = est_hourspw_8 if track==8
drop est_hourspw_* count_* track
**** Replace missing value of hourspw (221 changes)
replace hourspw = est_hourspw if hourspw==.
* If cannot use other observations to estimate, assume at least one month, one week, one hour (Changes: 13, 12, 13)
replace months = 1 if months==.
replace weekspm = 1 if weekspm==.
replace hourspw = 1 if hourspw==.

* Replace missing AFS Share Values with Country Specific AFS factors
************** FUTURE VERSION *************************************************************
/// AFS ADJUSTMENT FACTOR CALCULATION
///   We adjust i) share of cash food expenditures in All cash expenditures
///        and ii) share of non-food agriculture in all agriculture
///  can be expressed as i+(1-i)*ii
*******************************************************************************************
replace AFS_share = 0.453 if AFS_share==. & rural==1
replace AFS_share = 0.386 if AFS_share==. & rural==0

gen fte_wage = months*weekspm*hourspw/2016
mvdecode fte_wage, mv(0)
mvencode fte_wage, mv(0)
replace fte_wage = 2 if fte_wage>2 & fte_wage~=.
* (9 changes)

* Casual vs Formal Wage
gen formal = 0
replace formal = 1 if pmt_duration==4 //Formal if payment is monthly
replace formal = 1 if contract==1 //written contract vs. verbal
replace formal = 1 if pension==1
replace formal = 1 if health_insurance==1

//added definition of "formality" due to contract structure 
g formalcon =  (contract==1) 

//added definition of "formality" due to SP
g formalsp = (pension==1 | health_insurance==1)

* Demographic FTEs
forvalues f=0(1)1 {
	gen fte_f`f'_wage = fte_wage if formal==`f'
	mvdecode fte_f`f', mv(0)
	mvencode fte_f`f', mv(0)
}
*

* Simple count of Wages earned.
forvalues f=0(1)1 {
	gen emp_f`f'_wage = 1 if fte_wage~=0 & formal==`f'
	mvdecode emp_f`f', mv(0)
	mvencode emp_f`f', mv(0)
}
*
** the full time equivalents of hours worked by employment types to inform about how households distribute their time across formal and informal activities;
* Demographic FTEs contract
forvalues f=0(1)1 {
	gen fte_fcon`f'_wage = fte_wage if formalcon==`f'
	mvdecode fte_fcon`f'_wage, mv(0)
	mvencode fte_fcon`f'_wage, mv(0)
}
*
* Simple count of Wages earned (contract).
forvalues f=0(1)1 {
	gen emp_fcon`f'_wage = 1 if fte_wage~=0 & formalcon==`f'
	mvdecode emp_fcon`f'_wage, mv(0)
	mvencode emp_fcon`f'_wage, mv(0)
}
*
* Demographic FTEs for SP receipt dummy
forvalues f=0(1)1 {
	gen fte_fsp`f'_wage = fte_wage if formalsp==`f'
	mvdecode fte_fsp`f'_wage, mv(0)
	mvencode fte_fsp`f'_wage, mv(0)
}
*
* Simple count of Wages earned for SP receipt dummy
forvalues f=0(1)1 {
	gen emp_fsp`f'_wage = 1 if fte_wage~=0 & formalsp==`f'
	mvdecode emp_fsp`f'_wage, mv(0)
	mvencode emp_fsp`f'_wage, mv(0)
}
*

** Generate Categorical Variables

*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_o_f`f'_c`c' = 0
	gen w_o_spf`f'_c`c' = 0
	gen w_o_conf`f'_c`c' = 0
}
}

*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	gen w_f_f`f'_c`c' = 0
	gen w_f_spf`f'_c`c' = 0
	gen w_f_conf`f'_c`c' = 0
}
}
*
*! added: formality by social protection ro contract status
forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace w_o_f`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_f_f`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formal==`f'
	replace w_o_spf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_f_spf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' & formalsp==`f'
	replace w_o_conf`f'_c`c' = emp_f`f'_wage if ISIC_group==`c' & formalcon==`f'
	replace w_f_conf`f'_c`c' = fte_f`f'_wage if ISIC_group==`c' &  formalcon==`f'
}
}
*
* For mixed CIC Groups
forvalues f=0(1)1 {
forvalues w=1(1)2 {
	replace w_o_f`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_o_f`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_o_f`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_o_f`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_o_f`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formal==`f'
	replace w_f_f`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formal==`f'
	replace w_f_f`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formal==`f'
	replace w_f_f`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	replace w_f_f`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formal==`f'
	**! formal by social protection status
	replace w_o_spf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_o_spf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_o_spf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_o_spf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_o_spf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalsp==`f'
	replace w_f_spf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalsp==`f'
	replace w_f_spf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalsp==`f'
	replace w_f_spf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	replace w_f_spf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalsp==`f'
	**! formal by contract status
	replace w_o_conf`f'_c3 = AFS_share*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c7 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_o_conf`f'_c5 = AFS_share*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c9 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_o_conf`f'_c4 = AFS_share*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c10 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_o_conf`f'_c6 = AFS_share*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_o_conf`f'_c12 = (1-AFS_share)*emp_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c3 = AFS_share*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c7 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==37 & formalcon==`f'
	replace w_f_conf`f'_c5 = AFS_share*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c9 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==509 & formalcon==`f'
	replace w_f_conf`f'_c4 = AFS_share*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c10 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==410 & formalcon==`f'
	replace w_f_conf`f'_c6 = AFS_share*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
	replace w_f_conf`f'_c12 = (1-AFS_share)*fte_f`f'_wage if ISIC_group==612 & formalcon==`f'
}
}
*
recode health_insurance pension (2=0)
* Collapse to the household level
collapse (sum) w_o_* w_f_* (max) health_insurance pension , by (HHID PID)

save "clean data/intermediate/Uganda1314_wage_occCHEVAL.dta", replace


** Own Farm Employment

* First Season
use "raw data/AGSEC3A.dta", clear
drop HHID
rename hh HHID

* Expand data to have one observation per hh member
joinby HHID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
drop if plotID==.
gen memID = substr(PID,-3,.)
tostring a3aq33a a3aq33b a3aq33c a3aq33d a3aq33e, replace
local mems "a b c d e"
foreach m in `mems' {
	gen farm_mem_`m' = substr(a3aq33`m',-3,.)
}
*
gen farm_days = 0
local mems "a b c d e"
foreach m in `mems' {
	replace farm_days = a3aq33`m'_1 if farm_mem_`m'==memID & a3aq33`m'_1~=.
}
*
	
* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_days>0

* Generate Full Time Equivalents (252 days in year is full time)

replace farm_days = farm_days * 0.5485 if rural==0
replace farm_days = farm_days * 0.6348 if rural==1

gen farm_fte = farm_days/252
mvdecode farm_fte, mv(0)
mvencode farm_fte, mv(0)
*

* Collapse to HH Member
collapse (sum) farm_worker farm_fte, by (HHID PID)

* Indicate that this is the First Season
rename farm_worker farm_L_work
rename farm_fte farm_L_fte

save "clean data/intermediate/Uganda1314_farm_first_occ.dta", replace


* Second Season
use "raw data/AGSEC3B.dta", clear
drop HHID
rename hh HHID

* Expand data to have one observation per hh member
joinby HHID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
drop if plotID==.
gen memID = substr(PID,-3,.)
tostring a3bq33a a3bq33b a3bq33c a3bq33d a3bq33e, replace
local mems "a b c d e"
foreach m in `mems' {
	gen farm_mem_`m' = substr(a3bq33`m',-3,.)
}
*
gen farm_days = 0
local mems "a b c d e"
foreach m in `mems' {
	replace farm_days = a3bq33`m'_1 if farm_mem_`m'==memID & a3bq33`m'_1~=.
}
*
	
* Generate dummy variable to see if the hh member worked on the farm
gen farm_worker = 0
replace farm_worker = 1 if farm_days>0

* Generate Full Time Equivalents (252 days in year is full time)

replace farm_days = farm_days * 0.5485 if rural==0
replace farm_days = farm_days * 0.6348 if rural==1

gen farm_fte = farm_days/252

* Collapse to HH Member
collapse (sum) farm_worker farm_fte, by (HHID PID)

* Indicate that this is the First Season
rename farm_worker farm_S_work
rename farm_fte farm_S_fte

save "clean data/intermediate/Uganda1314_farm_second_occ.dta", replace

* Combine First and Second Farming Seasons
use "clean data/intermediate/Uganda1314_farm_first_occ.dta", clear
joinby HHID PID using "clean data/intermediate/Uganda1314_farm_second_occ.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode farm*, mv(0)
mvencode farm*, mv(0)

* Combine rainy & dry Seasons
gen farm = 1 if farm_L_work>0 | farm_S_work>0
gen farm_fte = farm_L_fte + farm_S_fte
mvdecode farm*, mv(0)
mvencode farm*, mv(0)

* Collapse to HH
collapse (sum) farm farm_fte, by (HHID PID)
replace farm_fte = 2 if farm_fte>2
* (8 changes)

* All Own Farming is 100% AFS and self employed labor

** Rename to be consistent with other Categorical Variables
rename farm farm_o_c1
rename farm_fte farm_f_c1

* Keep Farming Data at the household level
keep HHID PID farm_o_c1 farm_f_c1

save "clean data/intermediate/Uganda1314_farm_occ.dta", replace


** Occupations in Non-Farm Own Enterprises
********************
** The labor module does not collect months, weeks or hours for household enterprises if the household enterprise is
** the main form of labor. If a household member responds with "an employer", "an own-account worker", or "helping
** without pay in a household busisess," I will bring in months from the Non-Farm Own Enterprise module if available, and
** after bringing in that information I will estimate missing months, weeks and hours from what I do have.
********************

** Months from Non-Farm Own Enterprises
use "raw data/gsec12.dta", clear
rename hhid HHID
rename h12q4 CIC
rename h12q3b CIC_name
rename h12q12 months
rename h12q13 gross_revenue
rename h12q15 wages
rename h12q16 materials
rename h12q17 other_costs
drop if h12q1==. & months==.
drop if h12q1==2 & months==.
drop if h12q1==2 & months==0
drop if h12q4a==2 & months==.
drop if h12q4a==2 & months==0
drop if months==. & gross_revenue==.
*keep HHID CIC months
keep HHID CIC months h12q11a h12q11b h12q11c h12q11d h12q11e
* If claimed to have worked during past year, and have income, it is not possible to have worked zero months
replace months = . if months==0
* Estimate missing months, revenue and costs, using medians of like enterprises by spatial regions
joinby HHID using "clean data/Uganda1314_location.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Count observations with months reported by geographic location.
by CIC rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if months~=.
by CIC rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if months~=.
by CIC rural region subregion district subcounty, sort : egen float count_3 = count(1) if months~=.
by CIC rural region subregion district, sort : egen float count_4 = count(1) if months~=.
by CIC rural region subregion, sort : egen float count_5 = count(1) if months~=.
by CIC rural region, sort : egen float count_6 = count(1) if months~=.
by CIC rural, sort : egen float count_7 = count(1) if months~=.
by CIC, sort : egen float count_8 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by CIC rural region subregion district subcounty parish ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by CIC rural region subregion district subcounty parish, sort : egen float temp_est_months_2 = median(months) if months~=.
by CIC rural region subregion district subcounty, sort : egen float temp_est_months_3 = median(months) if months~=.
by CIC rural region subregion district, sort : egen float temp_est_months_4 = median(months) if months~=.
by CIC rural region subregion, sort : egen float temp_est_months_5 = median(months) if months~=.
by CIC rural region, sort : egen float temp_est_months_6 = median(months) if months~=.
by CIC rural, sort : egen float temp_est_months_7 = median(months) if months~=.
by CIC, sort : egen float temp_est_months_8 = median(months) if months~=.
mvencode temp_*, mv(0)
by CIC rural region subregion district subcounty parish ea, sort : egen float est_months_1 = max(temp_est_months_1)
by CIC rural region subregion district subcounty parish, sort : egen float est_months_2 = max(temp_est_months_2)
by CIC rural region subregion district subcounty, sort : egen float est_months_3 = max(temp_est_months_3)
by CIC rural region subregion district, sort : egen float est_months_4 = max(temp_est_months_4)
by CIC rural region subregion, sort : egen float est_months_5 = max(temp_est_months_5)
by CIC rural region, sort : egen float est_months_6 = max(temp_est_months_6)
by CIC rural, sort : egen float est_months_7 = max(temp_est_months_7)
by CIC, sort : egen float est_months_8 = max(temp_est_months_8)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_months==.
replace est_months = est_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_months==.
replace est_months = est_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_months==.
replace est_months = est_months_7 if track==7
replace track = 8 if est_months==.
replace est_months = est_months_8 if track==8
drop est_months_* count_* track
**** Replace missing value of months (7 changes)
replace months = est_months if months==.

* Adjusted code here to retain member level data from the enterprise module

joinby CIC using "clean data/Uganda1314_ISIC_AFS.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
replace ISIC_group = 612 if ISIC_group==.
replace CIC = 99999 if CIC==.

*collapse (max) months, by (HHID CIC)

expand 2, generate(copy1)
expand 2, generate(copy2)
expand 2 if copy1==0 & copy2==0, generate(copy3)
gen obs = .
replace obs = 1 if copy1==0 & copy2==0
replace obs = 2 if copy1==1 & copy2==0
replace obs = 3 if copy1==0 & copy2==1
replace obs = 4 if copy1==1 & copy2==1
replace obs = 5 if copy3==1

gen PID = h12q11a if obs==1
replace PID = h12q11b if obs==2
replace PID = h12q11c if obs==3
replace PID = h12q11d if obs==4
replace PID = h12q11e if obs==5
drop if PID==""
collapse (max) months, by (HHID PID CIC ISIC_group AFS_share)
rename months nfe_months

* Conclude added code here

save "clean data/intermediate/Uganda1314_nfe_months.dta", replace

* Use the previous cleaning from the wage calculations for Non-Farm Household Enterprises
use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID main*
keep if main_type>1 & main_type<5
local vars "CIC_name CIC ISIC_group AFS_share"
foreach v in `vars' {
	rename main_`v' `v'
}
*
keep HHID PID CIC_name CIC ISIC_group AFS_share

gen months = .
gen weekspm = .
gen hourspw = .

gen job = 1
save "clean data/intermediate/Uganda1314_main_nfe_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID secondary*
keep if secondary_type>1 & secondary_type<5
mvdecode secondary_months secondary_weekspm secondary_hourspw, mv(0)
local vars "CIC_name CIC months weekspm hourspw ISIC_group AFS_share"
foreach v in `vars' {
	rename secondary_`v' `v'
}
*
keep HHID PID CIC_name CIC months weekspm hourspw ISIC_group AFS_share
gen job = 2
save "clean data/intermediate/Uganda1314_secondary_nfe_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID umain*
keep if umain_type>1 & umain_type<5
mvdecode umain_months umain_weekspm umain_hourspw, mv(0)
local vars "CIC_name CIC months weekspm hourspw ISIC_group AFS_share"
foreach v in `vars' {
	rename umain_`v' `v'
}
*
keep HHID PID CIC_name CIC months weekspm hourspw ISIC_group AFS_share
gen job = 3
save "clean data/intermediate/Uganda1314_umain_nfe_activities.dta", replace

use "clean data/intermediate/Uganda1314_wage_activities.dta", clear
keep HHID PID usecondary*
keep if usecondary_type>1 & usecondary_type<5
mvdecode usecondary_months usecondary_weekspm usecondary_hourspw, mv(0)
local vars "CIC_name CIC months weekspm hourspw ISIC_group AFS_share"
foreach v in `vars' {
	rename usecondary_`v' `v'
}
*
keep HHID PID CIC_name CIC months weekspm hourspw ISIC_group AFS_share
gen job = 4
save "clean data/intermediate/Uganda1314_usecondary_nfe_activities.dta", replace

use "clean data/intermediate/Uganda1314_main_nfe_activities.dta", clear
append using "clean data/intermediate/Uganda1314_secondary_nfe_activities.dta"
append using "clean data/intermediate/Uganda1314_umain_nfe_activities.dta"
append using "clean data/intermediate/Uganda1314_usecondary_nfe_activities.dta"
replace ISIC_group = 612 if ISIC_group==.
replace CIC = 99999 if CIC==.

* Bring in Months and additional members from enterprise module.
joinby HHID PID CIC ISIC_group AFS_share using "clean data/intermediate/Uganda1314_nfe_months.dta", unmatched(both) _merge(_merge)
drop _merge
* Use months data captured from labor module if different than enterprise module, as months calculated at individual level in labor module
replace months = nfe_months if months==.
drop nfe_months

* Estimate missing values of months, weeks per month and hours per month if one of these are missing
joinby HHID PID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop if _merge==2
drop _merge
* Count observations with months reported by geographic location.
by CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if months~=.
by CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if months~=.
by CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if months~=.
by CIC female rural region subregion district, sort : egen float count_4 = count(1) if months~=.
by CIC female rural region subregion, sort : egen float count_5 = count(1) if months~=.
by CIC female rural region, sort : egen float count_6 = count(1) if months~=.
by CIC female rural, sort : egen float count_7 = count(1) if months~=.
by CIC female, sort : egen float count_8 = count(1) if months~=.
* Calculate median of months by geographic location - later used for predictive purposes.
by CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_months_1 = median(months) if months~=.
by CIC female rural region subregion district subcounty parish, sort : egen float temp_est_months_2 = median(months) if months~=.
by CIC female rural region subregion district subcounty, sort : egen float temp_est_months_3 = median(months) if months~=.
by CIC female rural region subregion district, sort : egen float temp_est_months_4 = median(months) if months~=.
by CIC female rural region subregion, sort : egen float temp_est_months_5 = median(months) if months~=.
by CIC female rural region, sort : egen float temp_est_months_6 = median(months) if months~=.
by CIC female rural, sort : egen float temp_est_months_7 = median(months) if months~=.
by CIC female, sort : egen float temp_est_months_8 = median(months) if months~=.
mvencode temp_*, mv(0)
by CIC female rural region subregion district subcounty parish ea, sort : egen float est_months_1 = max(temp_est_months_1)
by CIC female rural region subregion district subcounty parish, sort : egen float est_months_2 = max(temp_est_months_2)
by CIC female rural region subregion district subcounty, sort : egen float est_months_3 = max(temp_est_months_3)
by CIC female rural region subregion district, sort : egen float est_months_4 = max(temp_est_months_4)
by CIC female rural region subregion, sort : egen float est_months_5 = max(temp_est_months_5)
by CIC female rural region, sort : egen float est_months_6 = max(temp_est_months_6)
by CIC female rural, sort : egen float est_months_7 = max(temp_est_months_7)
by CIC female, sort : egen float est_months_8 = max(temp_est_months_8)
drop temp*
* Build months prediction variable if at least 10 observations of reported months in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_months = est_months_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_months==.
replace est_months = est_months_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_months==.
replace est_months = est_months_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_months==.
replace est_months = est_months_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_months==.
replace est_months = est_months_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_months==.
replace est_months = est_months_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_months==.
replace est_months = est_months_7 if track==7
replace track = 8 if est_months==.
replace est_months = est_months_8 if track==8
drop est_months_* count_* track
**** Replace missing value of months (719 changes)
replace months = est_months if months==.
* Count observations with weekspm reported by geographic location.
by CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if weekspm~=.
by CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if weekspm~=.
by CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if weekspm~=.
by CIC female rural region subregion district, sort : egen float count_4 = count(1) if weekspm~=.
by CIC female rural region subregion, sort : egen float count_5 = count(1) if weekspm~=.
by CIC female rural region, sort : egen float count_6 = count(1) if weekspm~=.
by CIC female rural, sort : egen float count_7 = count(1) if weekspm~=.
by CIC female, sort : egen float count_8 = count(1) if weekspm~=.
* Calculate median of weekspm by geographic location - later used for predictive purposes.
by CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_weekspm_1 = median(weekspm) if weekspm~=.
by CIC female rural region subregion district subcounty parish, sort : egen float temp_est_weekspm_2 = median(weekspm) if weekspm~=.
by CIC female rural region subregion district subcounty, sort : egen float temp_est_weekspm_3 = median(weekspm) if weekspm~=.
by CIC female rural region subregion district, sort : egen float temp_est_weekspm_4 = median(weekspm) if weekspm~=.
by CIC female rural region subregion, sort : egen float temp_est_weekspm_5 = median(weekspm) if weekspm~=.
by CIC female rural region, sort : egen float temp_est_weekspm_6 = median(weekspm) if weekspm~=.
by CIC female rural, sort : egen float temp_est_weekspm_7 = median(weekspm) if weekspm~=.
by CIC female, sort : egen float temp_est_weekspm_8 = median(weekspm) if weekspm~=.
mvencode temp_*, mv(0)
by CIC female rural region subregion district subcounty parish ea, sort : egen float est_weekspm_1 = max(temp_est_weekspm_1)
by CIC female rural region subregion district subcounty parish, sort : egen float est_weekspm_2 = max(temp_est_weekspm_2)
by CIC female rural region subregion district subcounty, sort : egen float est_weekspm_3 = max(temp_est_weekspm_3)
by CIC female rural region subregion district, sort : egen float est_weekspm_4 = max(temp_est_weekspm_4)
by CIC female rural region subregion, sort : egen float est_weekspm_5 = max(temp_est_weekspm_5)
by CIC female rural region, sort : egen float est_weekspm_6 = max(temp_est_weekspm_6)
by CIC female rural, sort : egen float est_weekspm_7 = max(temp_est_weekspm_7)
by CIC female, sort : egen float est_weekspm_8 = max(temp_est_weekspm_8)
drop temp*
* Build weekspm prediction variable if at least 10 observations of reported weekspm in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_weekspm = est_weekspm_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_weekspm==.
replace est_weekspm = est_weekspm_7 if track==7
replace track = 8 if est_weekspm==.
replace est_weekspm = est_weekspm_8 if track==8
drop est_weekspm_* count_* track
**** Replace missing value of weekspm (1048 changes)
replace weekspm = est_weekspm if weekspm==.
* Count observations with hourspw reported by geographic location.
by CIC female rural region subregion district subcounty parish ea, sort : egen float count_1 = count(1) if hourspw~=.
by CIC female rural region subregion district subcounty parish, sort : egen float count_2 = count(1) if hourspw~=.
by CIC female rural region subregion district subcounty, sort : egen float count_3 = count(1) if hourspw~=.
by CIC female rural region subregion district, sort : egen float count_4 = count(1) if hourspw~=.
by CIC female rural region subregion, sort : egen float count_5 = count(1) if hourspw~=.
by CIC female rural region, sort : egen float count_6 = count(1) if hourspw~=.
by CIC female rural, sort : egen float count_7 = count(1) if hourspw~=.
by CIC female, sort : egen float count_8 = count(1) if hourspw~=.
* Calculate median of hourspw by geographic location - later used for predictive purposes.
by CIC female rural region subregion district subcounty parish ea, sort : egen float temp_est_hourspw_1 = median(hourspw) if hourspw~=.
by CIC female rural region subregion district subcounty parish, sort : egen float temp_est_hourspw_2 = median(hourspw) if hourspw~=.
by CIC female rural region subregion district subcounty, sort : egen float temp_est_hourspw_3 = median(hourspw) if hourspw~=.
by CIC female rural region subregion district, sort : egen float temp_est_hourspw_4 = median(hourspw) if hourspw~=.
by CIC female rural region subregion, sort : egen float temp_est_hourspw_5 = median(hourspw) if hourspw~=.
by CIC female rural region, sort : egen float temp_est_hourspw_6 = median(hourspw) if hourspw~=.
by CIC female rural, sort : egen float temp_est_hourspw_7 = median(hourspw) if hourspw~=.
by CIC female, sort : egen float temp_est_hourspw_8 = median(hourspw) if hourspw~=.
mvencode temp_*, mv(0)
by CIC female rural region subregion district subcounty parish ea, sort : egen float est_hourspw_1 = max(temp_est_hourspw_1)
by CIC female rural region subregion district subcounty parish, sort : egen float est_hourspw_2 = max(temp_est_hourspw_2)
by CIC female rural region subregion district subcounty, sort : egen float est_hourspw_3 = max(temp_est_hourspw_3)
by CIC female rural region subregion district, sort : egen float est_hourspw_4 = max(temp_est_hourspw_4)
by CIC female rural region subregion, sort : egen float est_hourspw_5 = max(temp_est_hourspw_5)
by CIC female rural region, sort : egen float est_hourspw_6 = max(temp_est_hourspw_6)
by CIC female rural, sort : egen float est_hourspw_7 = max(temp_est_hourspw_7)
by CIC female, sort : egen float est_hourspw_8 = max(temp_est_hourspw_8)
drop temp*
* Build hourspw prediction variable if at least 10 observations of reported hourspw in location counted above.
* Start with smallest qualifying geographic region
mvdecode est_*, mv(0)
generate track = 1 if count_1>=10 & count_1<100000
generate est_hourspw = est_hourspw_1 if track==1
replace track = 2 if count_2>=10 & count_2<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_2 if track==2
replace track = 3 if count_3>=10 & count_3<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_3 if track==3
replace track = 4 if count_4>=10 & count_4<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_4 if track==4
replace track = 5 if count_5>=10 & count_5<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_5 if track==5
replace track = 6 if count_6>=10 & count_6<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_6 if track==6
replace track = 7 if count_7>=10 & count_7<100000 & est_hourspw==.
replace est_hourspw = est_hourspw_7 if track==7
replace track = 8 if est_hourspw==.
replace est_hourspw = est_hourspw_8 if track==8
drop est_hourspw_* count_* track
**** Replace missing value of hourspw (1124 changes)
replace hourspw = est_hourspw if hourspw==.
* If cannot use other observations to estimate, assume at least one month, one week, one hour (Changes: 103, 176, 203)
replace months = 1 if months==.
replace weekspm = 1 if weekspm==.
replace hourspw = 1 if hourspw==.

gen fte = months*weekspm*hourspw/2016
replace fte = 2 if fte>2 & fte~=.
* (0 changes)

rename fte fte_nfe

* Simple count of employed in NFE.
gen emp_nfe = 1

* Collapse to HH / ISIC level
collapse (sum) fte_nfe emp_nfe, by (HHID PID ISIC_group AFS_share)

** Generate Categorical Variables

forvalues c=1(1)12 {
	gen nfe_o_c`c' = 0
}
*

forvalues c=1(1)12 {
	gen nfe_f_c`c' = 0
}
*

forvalues c=1(1)12 {
	replace nfe_o_c`c' = emp_nfe if ISIC_group==`c'
	replace nfe_f_c`c' = fte_nfe if ISIC_group==`c'
}
*

* Four mixed ISIC Groups
replace nfe_o_c3 = AFS_share*emp_nfe if ISIC_group==37
replace nfe_o_c7 = (1-AFS_share)*emp_nfe if ISIC_group==37
replace nfe_o_c5 = AFS_share*emp_nfe if ISIC_group==509
replace nfe_o_c9 = (1-AFS_share)*emp_nfe if ISIC_group==509
replace nfe_o_c4 = AFS_share*emp_nfe if ISIC_group==410
replace nfe_o_c10 = (1-AFS_share)*emp_nfe if ISIC_group==410
replace nfe_o_c6 = AFS_share*emp_nfe if ISIC_group==612
replace nfe_o_c12 = (1-AFS_share)*emp_nfe if ISIC_group==612
replace nfe_f_c3 = AFS_share*fte_nfe if ISIC_group==37
replace nfe_f_c7 = (1-AFS_share)*fte_nfe if ISIC_group==37
replace nfe_f_c5 = AFS_share*fte_nfe if ISIC_group==509
replace nfe_f_c9 = (1-AFS_share)*fte_nfe if ISIC_group==509
replace nfe_f_c4 = AFS_share*fte_nfe if ISIC_group==410
replace nfe_f_c10 = (1-AFS_share)*fte_nfe if ISIC_group==410
replace nfe_f_c6 = AFS_share*fte_nfe if ISIC_group==612
replace nfe_f_c12 = (1-AFS_share)*fte_nfe if ISIC_group==612

* Collapse to the household level
collapse (sum) nfe_o_* nfe_f_*, by (HHID PID)

save "clean data/intermediate/Uganda1314_nfe_occ.dta", replace


**** COMBINE ALL ****

use "clean data/intermediate/Uganda1314_nfe_occ.dta", clear
joinby HHID PID using "clean data/intermediate/Uganda1314_wage_occCHEVAL.dta", unmatched(both) _merge(_merge)
drop _merge
joinby HHID PID using "clean data/intermediate/Uganda1314_farm_occ.dta", unmatched(both) _merge(_merge)
drop _merge

* Bring in Demographics
joinby HHID PID using "clean data/Uganda1314_member_demos.dta", unmatched(both) _merge(_merge)
drop _merge

mvdecode nfe* w* farm*, mv(0)
mvencode nfe* w* farm*, mv(0)

** Generate Grouped Categorical Variables (!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen o_f`f'_c`c' = 0
	gen o_spf`f'_c`c' = 0
	gen o_conf`f'_c`c' = 0
}
}
*
*(!)EVA: added formality categories
forvalues f=0(1)2 {
forvalues c=1(1)12 {
	gen f_f`f'_c`c' = 0
	gen f_spf`f'_c`c' = 0
	gen f_conf`f'_c`c' = 0
}
}
*

forvalues f=0(1)1 {
forvalues c=1(1)12 {
	replace o_f`f'_c`c' = w_o_f`f'_c`c'
	replace f_f`f'_c`c' = w_f_f`f'_c`c'
	replace o_spf`f'_c`c' = w_o_spf`f'_c`c'
	replace f_spf`f'_c`c' = w_f_spf`f'_c`c'
	replace o_conf`f'_c`c' = w_o_conf`f'_c`c'
	replace f_conf`f'_c`c' = w_f_conf`f'_c`c'
}
}
*

forvalues c=1(1)12 {
	replace o_f2_c`c' = nfe_o_c`c'
}
*

replace o_f2_c1 = o_f2_c1 + farm_o_c1

forvalues c=1(1)12 {
	replace f_f2_c`c' = nfe_f_c`c'
}
*

replace f_f2_c1 = f_f2_c1 + farm_f_c1

* Total (f3) Casual, Formal and Self Employed
forvalues c=1(1)12 {
	generate o_f3_c`c' = o_f0_c`c' + o_f1_c`c' + o_f2_c`c'
	generate f_f3_c`c' = f_f0_c`c' + f_f1_c`c' + f_f2_c`c'
}
*

* Generate Aggregated Categories
local measure "o f"
foreach m in `measure' {
forvalues f = 0(1)3 {
	generate `m'_f`f'_nc1 = `m'_f`f'_c1
	generate `m'_f`f'_nc2 = `m'_f`f'_c2
	generate `m'_f`f'_nc3 = `m'_f`f'_c3 + `m'_f`f'_c7
	generate `m'_f`f'_nc4 = `m'_f`f'_c8
	generate `m'_f`f'_nc5 = `m'_f`f'_c5 + `m'_f`f'_c9
	generate `m'_f`f'_nc6 = `m'_f`f'_c4 + `m'_f`f'_c10
	generate `m'_f`f'_nc7 = `m'_f`f'_c11
	generate `m'_f`f'_nc8 = `m'_f`f'_c6 + `m'_f`f'_c12
}
}
*

gen total_jobs = 0
forvalues c=1(1)12 {
	replace total_jobs = total_jobs + o_f3_c`c'
}
*
gen occ_total = total_jobs

gen multiple_jobs = 0
replace multiple_jobs = 1 if total_jobs>1

** Calculate total FT per HH Member
gen fte_total = 0
forvalues f=1(1)12 {
	replace fte_total = fte_total + f_f3_c`f'
}	
*

* keep only relevant variables:
keep PID HHID fte_total o_f* o_spf* o_conf* f_f* f_spf* f_conf* nfe_* w_* farm_* total_jobs occ_total age age_tier female head female_head youth youth_34 school_read school_any school_currently school_complete school_secondary school_level health_insurance pension
joinby using "clean data/Uganda1314_iid.dta", unmatched(none)
drop PID HHID
order iid hhid female age age_tier school_* youth*

gen country="Uganda"

save "clean data/UGANDA_FTEsCHEVAL.dta", replace

