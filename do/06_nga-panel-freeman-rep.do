
******************************* SET DRIVES *************************************
gl DRIVE	"$path/data"
gl DO	"$path/do"
gl TAB		"$path/tables"
gl FIG		"$path/figures"
cd "${DRIVE}" 

	tempfile main new
	tempvar todrop
	use $DRIVE/NGA_panel.dta ,clear
	ren *_*_*_* ****
	ren *_*_* ***
	gen ltot=ln(pcexp_n)
	keep if !rural
	egen nid=group(hhid_unique)

	tostring year,replace
	encode year, gen(newt)
	tab year,gen(y)
	sort nid newt
	xtset nid newt
	la var female_head "Female Head"
la var land_d "Owns Land"
svyset hhid_unique,  weight(hhweight) vce(linearized)
local outregopts starlevels(10 5 1) starloc(1) tex replace varlabels se a4 landscape fragment nolegend
local con1 hhsize dep_ratio shschoolsec y2 y3
local keep hhsize dep_ratio shschoolsec y2 y3
local if estsample

	foreach var of var shfteformal shfteformalsp{
		gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
	}	
	gen informalhead=cond(head_f==1 & !mi(head_f),0,1,.)
	label var informalhead "Informal HH Head"
	gen informalheadsp=cond(head_sp==1 & !mi(head_sp),0,1,.)
	label var informalheadsp "Informal HH Head"
		la def nv 0 "Formal" 1 "0-99% informal" 2 "100% informal"
	local shempYinformal "Sh Inf Inc"
	local shfteinformal "Sh FTE Inf"
	local poor_n "Prob(Poor=1)"
	local ltot "ln(Total Expenditure)"
	local sp "(SocProt Based)"
	local con "(Contract Based)"
	foreach bit in " " sp{
	foreach var in shfteinformal{
		gen n`var'`bit'=cond(`var'`bit'==0,0, ///
											cond(`var'`bit'>0 & `var'`bit'<1,1, ///
											cond(`var'`bit'==1,2,.)))
		la val n`var'`bit' nv
		la var n`var'`bit' `"``var''``bit''"'
		}
	}
tempfile master
save `master'
keep if newt==1 | newt==2
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
gen post=newt==2
tempfile stacked
egen stacknid=group(nid)
su nid, mean
local max=`r(max)'
save `stacked'
use `master',clear
keep if newt==2 | newt==3
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
egen stacknid=group(nid)
replace stacknid=stacknid+`max'
gen post=newt==3
append using `stacked'
save `stacked',replace
keep nshfteinformalsp stacknid post
reshape wide nshfteinformalsp, i(stacknid) j(post)
merge m:1 nshfteinformalsp0 nshfteinformalsp1 using combinations-freeman, gen(freeman)
merge 1:m stacknid using `stacked', gen(spm2)
xtset stacknid post
xtbalance , range(0 1) miss(`con1' poor_n ltot)
foreach lhs of var poor_n ltot{
	foreach rhs of var ii fi iif ff mi im mm mf fm joinersinf leaversinf  joinersmix leaversmix{
	xtreg `lhs' 1.post##`rhs' `con1', vce(cl stacknid) fe
	gen estsample=e(sample)
		reg `lhs' if `if' & `rhs'==1 & post==0, vce(cl stacknid)
		mat temp=r(table)
		mat t`rhs'`lhs'=nullmat(t`rhs'`lhs'), temp[1,1] , temp[2,1] , e(N)
		mat li t`rhs'`lhs'
		reg `lhs' if `if' & `rhs'==1 & post==1, vce(cl stacknid)
		mat temp=r(table)
		mat t`rhs'`lhs'=nullmat(t`rhs'`lhs'), temp[1,1] , temp[2,1] , e(N)
		reg `lhs' `rhs' if `if', vce(cl stacknid)
		mat temp=r(table)
		mat t`rhs'`lhs' =nullmat(t`rhs'`lhs'), temp[1,1] , temp[2,1] , e(N)
		reg `lhs' `rhs' `con1' if `if', vce(cl stacknid)
		mat temp=r(table)
		mat t`rhs'`lhs' =nullmat(t`rhs'`lhs'), temp[1,1] , temp[2,1] , e(N)
		xtreg `lhs' 1.post##`rhs' `con1' if `if', vce(cl stacknid) fe
		mat temp=r(table)
		mat t`rhs'`lhs'=nullmat(t`rhs'`lhs'), temp[1,5] , temp[2,5] , e(N)
		mat `lhs'=nullmat(`lhs') \ t`rhs'`lhs'
		drop estsample
		mat res=`lhs'
		local bc = rowsof(res)
		local cols = colsof(res)
		mat stars = J(`bc',`cols',0)
		forv j=1(3)`cols' {
			forv k = 1/`bc' {
				mat stars[`k' ,`j'] = ///
				(abs(res[`k' ,`j']/res[`k' ,`j'+1]) > invttail(res[`k' ,`j'+2] ,0.10/2)) + ///
				(abs(res[`k' ,`j']/res[`k' ,`j'+1]) > invttail(res[`k' ,`j'+2] ,0.05/2)) + ///
				(abs(res[`k' ,`j']/res[`k' ,`j'+1]) > invttail(res[`k' ,`j'+2] ,0.01/2))
			}
		}
		frmttable using $TAB/diff-nga-freemanrep-`lhs'.tex, statmat(res) substat(2) sdec(3) annotate(stars) asymbol(*,**,***) tex replace ///
		rtitles("IIvFF"\""\"N"\"FIvFF"\""\"N"\"IFvII"\""\"N"\ "FFvII"\""\"N"\"MIvMM"\""\"N"\ ///
		"IMvII"\"" \"N" \ "MMvII"\"" \"N" \ "MFvMM"\"" \"N"\ "FMvFF"\"" \"N"\  ///
		"JoinersI (MI\&FIvFF\&MM)"\"" \"N"\ "LeaversI (MF\&IFvII\&MM)" \ "" \"N"\ ///
		"JoinersM (FM\&IMvFF\&MM)"\"" \"N"\ "LeaversM (MF\&MIvFF\&MM)" \ "" \"N") ///
		ctitles("","``lhs''","","","","" \ ///
		"","(1)","(2)","(3)","(4)","(5)" \ ///
		"Portfolio v control","Before","After","Raw $\Delta$","Cond $\Delta$","DiD") fragment plain ///
		multicol(1,2,5;)
	
		!perl -pi -e 's/\\noalign\{\\smallskip\}//g' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -pi -e 's/[\[\]]+//g' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -0777 -pi -e 's/hline/toprule/' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -0777 -pi -e 's/hline/midrule/' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -0777 -pi -e 's/hline/bottomrule/' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -pi -e 's/(\d{3})\.000/$1/g' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -pi -e 's/\\(begin|end)\{center\}//g' $TAB/diff-nga-freemanrep-`lhs'.tex
		!perl -pi -e 's/(\\bottomrule\\end\{tabular\})\\\\/$1/' $TAB/diff-nga-freemanrep-`lhs'.tex

	}
}
