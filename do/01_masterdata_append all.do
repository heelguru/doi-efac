********************************************************************************
********** CHEVAL : FORMALITY AT HOUSEHOLD LEVEL *******************************
********************************************************************************
** Create one data file from all countries with indicators of interest *********
********************************************************************************

/*
This file creates one dataset combining the data from all countries and generating the indicators of interest.

1) From each country, the _Occ data sets are appended
2) the do file Occ_clean is run for the _Occ data to construct the indicators of interest from the individual level
	work data (FTE shares, occupation count shares)
3) the data is collapsed to household level with a unique household identifier: hhid_unique
4) from each country, the _Inc data sets are appended
5) the do file Inc_clean is run for the _Inc data to construct the indicators of interest from the household level
	income data (income shares)
6) the _occ data is merged with the income data using the unique household identifier: hhid_unique

The datasets used for this are:
for Sub-Saharan Africa:
Ethiopia
Malawi
Niger
Nigeria
Tanzania

Resulting dataset with all countries "CHEVAL_allcountries_raw.dta"

Stata MP version 15 
*/

capture log close
clear
clear matrix
set more off

********************************************************************************

******************************* SET DRIVES *************************************
gl DRIVE	"$path\data"

*by country:
gl ETH "${DRIVE}/Ethiopia_ESS_1516\clean data"
gl MWI "${DRIVE}/Malawi_IHS_1617\clean data"
gl NER "${DRIVE}/Niger_ECVMA_14\clean data"
gl NGA "${DRIVE}/Nigeria_GHS_1516\clean data"
gl TZN "${DRIVE}/Tanzania_NPS_1415\clean data"

gl TEMP	"${DRIVE}/temp"
gl DO	"$path\do"

cd "${DRIVE}" 
********************************************************************************

**** OCCUPATION FILES ****

* 1) From each country, the _Occ data sets are appended

use "$ETH/ETHIOPIA_FTEsCHEVAL" , clear
append using "$MWI/MALAWI_FTEsCHEVAL"
append using "$NER/NIGER_FTEsCHEVAL"
append using "$NGA/NIGERIA_FTEsCHEVAL" 
append using "$TZN/TANZANIA_FTEsCHEVAL"

* generate country code to create unique identifiers:
g ccode=""
tokenize Ethiopia Malawi Niger Nigeria Tanzania  
local c 1
foreach i in ETH MWI NER NGA TZA  {
	replace ccode="`i'" if country=="``c''"
	local ++c
}
lab var ccode "Country code"

** gen household identifiers including the country code:
g hhid_unique = ccode + hhid
	lab var hhid_unique "Household ID, unique"

* 2) run codes to create aggregate variables
do "$DO/Occ_clean.do"

* 3) collapse to household level and generate codes of interest
*save variable labels
 foreach v of var * {
	local l`v' : variable label `v'
	if `"`l`v''"' == "" {
		local l`v' "`v'"
	}
}
collapse (sum) occ_* fte_* d_* NEET NEET_34 out_labor_mkt total_jobs dep_n=dep indep_n=indep total_active_mem=active_mem ///
		total_active_youth=active_youth_24 N_inschool=school_currently N_out_labor_mkt=out_labor_mkt ///
		(mean) sh_NEET=NEET sh_NEET_34=NEET_34 sh_out_labor_mkt=out_labor_mkt avg_total_jobs=total_jobs ///
			sh_school_secondary=school_secondary female_head age_mean=age ///
		(max) age_head head_f head_con head_sp ///
		(first) country ccode ///
		, by(hhid_unique)
*add labels
foreach v of var * {
	label var `v' "`l`v''"
}

save "$TEMP/CHEVAL_OCC_allcountries.dta", replace

********************************************************************************
* INCOME FILES *

*4) from each country, the _Inc data sets are appended; ! For some countries, have to get unique enumeration area ID to merge later with spatial data

	//ETHIOPIA
use "Ethiopia_ESS_1516/Ethiopia1516_spatial", clear
collapse region zone ward town subcity FA ea, by(ea_id2)
g admin_1 = region
g admin_2 = zone
g admin_3 = ward
save "$TEMP/ethiopia_ea", replace
use "$ETH\ETHIOPIA_incomesharesCHEVAL.dta", clear
merge m:1 region zone ward town subcity FA ea using "$TEMP/ethiopia_ea", nogen keep(3)
ren ea_id2 ea_id
replace ea_id=regexr(ea_id,"^(0)","")
drop region zone ward town subcity FA ea 
g country = "Ethiopia"
lab var country "Country"
save "$TEMP/eth", replace

	//MALAWI
use "Malawi_IHS_1617\Malawi1617_spatial", clear
keep case_id ea_id TA district
ren case_id hhid
ren district admin_1
ren TA admin_2
g admin_3=.
save "$TEMP/malawi_ea", replace
use "$MWI\MALAWI_incomesharesCHEVAL.dta", clear
merge 1:1 hhid using "$TEMP/malawi_ea", nogen keep(3)
g land_d = (land_owned>0 & land_owned!=.)
g country = "Malawi"
lab var country "Country"
save "$TEMP/mwi", replace

	//NIGER
use "$NER\NIGER_incomesharesCHEVAL.dta", clear 
tostring GRAPPE , g(ea_id)
g land_d = (land_owned>0 & land_owned!=.)
g country = "Niger"
lab var country "Country"
save "$TEMP/nig", replace	

	//NIGERIA
**need to get correct ea_id from other dataset:
use "Nigeria_GHS_1516\Nigeria1516_spatial_EAlevel", clear
gen double ea_id=Zone*1000000000+LGA*100000+sector*10000+EA
format ea_id %12.0f
g admin_1= State
g admin_2= LGA
g admin_3=.
keep Zone LGA EA sector ea_id admin_1-admin_3
save "$TEMP/nigeria_location", replace
use  "$NGA\NIGERIA_incomesharesCHEVAL.dta", clear
drop if EA==0 //drops movers: 270 households
merge m:1 Zone LGA EA using "$TEMP/nigeria_location", nogen keep(3) 
tostring ea_id, replace
g land_d = (land_owned>0 & land_owned!=.)
g country = "Nigeria"
lab var country "Country"
save "$TEMP/nga", replace		

	//TANZANIA
use "$TZN\TANZANIA_incomesharesCHEVAL.dta", clear
tostring clusterid, g(ea_id) format(%16.0g)
g land_d = (land_owned>0 & land_owned!=.)
g country = "Tanzania"
lab var country "Country"
save "$TEMP/tzn", replace


* APPEND all:
use "$TEMP/eth" , clear
append using "$TEMP/mwi"
append using "$TEMP/nig"
append using "$TEMP/nga"
append using "$TEMP/tzn"

* generate country code to create unique identifiers:
g ccode=""
tokenize Ethiopia Malawi Niger Nigeria Tanzania  
local c 1
foreach i in ETH MWI NER NGA TZA  {
	replace ccode="`i'" if country=="``c''"
	local ++c
}
lab var ccode "Country code"

** gen household identifiers including the country code:
g hhid_unique = ccode + hhid
	lab var hhid_unique "Household ID, unique"
	
* 5) the do file XX is run for the _Inc data to construct the indicators of interest from the household level income data (income shares)
do "$DO/Inc_clean.do"

save "$TEMP/CHEVAL_INC_allcountries.dta", replace

********************************************************************************
* MERGE* 

* 6) the _occ data is merged with the income data using the unique household identifier: hhid_unique
use "$TEMP/CHEVAL_INC_allcountries.dta", clear
merge 1:1 hhid_unique using "$TEMP/CHEVAL_OCC_allcountries.dta", nogen keep(3)
	
save "CHEVAL_allcountries_raw.dta", replace
	lab data "Household level data from 5 African LSMS-ISA datasets, all variables of interest"

ex
