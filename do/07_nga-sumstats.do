
	
capture log close
clear
clear matrix
set more off
******************************* SET DRIVES *************************************
gl DRIVE	"$path/data"
gl DO	"$path/do"
gl TAB		"$path/tables"
gl FIG		"$path/figures"
cd "${DRIVE}" 

	tempfile main new
	tempvar todrop
	use $DRIVE/NGA_panel.dta ,clear
	ren *_*_*_* ****
	ren *_*_* ***
	gen ltot=ln(totcons)
	keep if !rural
	egen nid=group(hhid_unique)

	tostring year,replace
	encode year, gen(newt)
	sort nid newt
	xtset nid newt
	la var female_head "Female Head"
la var land_d "Owns Land"
svyset hhid_unique,  weight(hhweight) vce(linearized)
local outregopts starlevels(10 5 1) starloc(1) tex replace varlabels se a4 landscape fragment nolegend
local con1 hhsize dep_ratio shschoolsec
local keep hhsize dep_ratio shschoolsec
local if estsample

	foreach var of var shfteformal shfteformalsp{
		gen `=subinstr("`var'","formal","informal",.)'=1-`var' if !mi(`var')
	}	
	la var shschoolsec "Share in household with secondary school"
	la var dep_ratio "Dependency ratio"
	la var hhsize "Household size"
	gen informalhead=cond(head_f==1 & !mi(head_f),0,1,.)
	label var informalhead "Informal HH Head"
	gen informalheadsp=cond(head_sp==1 & !mi(head_sp),0,1,.)
	label var informalheadsp "Informal HH Head"
		la def nv 0 "Formal" 1 "0-99% informal" 2 "100% informal"
	local shempYinformal "Sh Inf Inc"
	local shfteinformal "Sh FTE Inf"
	local poor "P(Poor=1)"
	local ltot "ln(TotExp)"
	local sp "(SocProt Based)"
	local con "(Contract Based)"
	foreach bit in " " sp{
	foreach var in shfteinformal{
		gen n`var'`bit'=cond(`var'`bit'==0,0, ///
											cond(`var'`bit'>0 & `var'`bit'<1,1, ///
											cond(`var'`bit'==1,2,.)))
		la val n`var'`bit' nv
		la var n`var'`bit' `"``var''``bit''"'
		}
	}
tempfile master
save `master'
*keep nshfteinformal hhid_unique newt
*reshape wide nshfteinformal, i(hhid_unique) j(newt)
*merge m:1 nshfteinformal1 nshfteinformal2 nshfteinformal3 using combinationsnshfteinformal, gen(m1)
*merge 1:m hhid_unique using `master', gen(m2)
*save `master',replace
keep nshfteinformalsp hhid_unique newt
reshape wide nshfteinformalsp, i(hhid_unique) j(newt)
merge m:1 nshfteinformalsp1 nshfteinformalsp2 nshfteinformalsp3 using combinationsnga, gen(spm1)
merge 1:m hhid_unique using `master', gen(spm2)
*gen naivetreat=nshfteinformal>=1
gen naivetreatsp=nshfteinformalsp>=1
keep if newt==1 | newt==2
duplicates tag nid, gen(timesinpanel)
keep if timesinpanel==1
drop timesinpanel
gen post=newt==2
egen stacknid=group(nid)
xtset stacknid post
xtbalance , range(0 1) miss(`con1' poor ltot)
ren *_*_* ***
foreach rhs in bfullffullinf dfullinffullf efullfmix ffullinfmix gmixfullf hmixfullinf {

iebaltab `con1',grpvar(`rhs') savetex($TAB/`rhs'nga.tex) rowv order(1 0) grpl("1 Early Switchers @ 0 Late Switchers") replace

}